<?php

class Grocery { 
 const HTTP_CATALOG = '';
    
    public function __construct($registry) {
        
        $this->config = $registry->get('config');
        
        $key = $this->config->get('domain_auth_key');
        
        if(isset($_GET['route']) && !$this->validate($key) && $_GET['route'] != 'grocery/authenticate'){
            
            //derive home path 
            if(self::HTTP_CATALOG){
                header('Location: '.self::HTTP_CATALOG.'index.php?route=grocery/authenticate');
            }else{
                header('Location: '.self::HTTP_CATALOG.'index.php?route=grocery/authenticate');
            }
        }
    }
    
   public function validate($key){


      
        
        
        $search = array('www.','https://','http://');
        $replace = array('','','');
                
        $host = str_replace($search, $replace, $_SERVER['HTTP_HOST']);
                
        //remove forward slash 
        $mi = strlen($host) - 1;//get max index 
        
        if($host[$mi] == '/'){
            $host = substr($host, 0, $mi);
        }
        
        //get hash to compare 
        $salt = 'bdqaoduwmm7634d;ssxc4sdiip;[],.#$@`~*';
        $hash = sha1($salt . sha1($salt . sha1($host)));
        
        if($key == $hash){
            return true;
        }else{
            return false;
        }
    }
}
