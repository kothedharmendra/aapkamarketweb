//<![CDATA[
function doLiveSearch( ev, keywords ) {

	if( ev.keyCode == 38 || ev.keyCode == 40 ) {
		return false;
	}	

	$('#livesearch_search_results').remove();
	updown = -1;

	if( keywords == '' || keywords.length < 2 ) {
		return false;
	}
	keywords = encodeURI(keywords);

	$.ajax({url: $('base').attr('href') + 'index.php?route=product/search/ajax&keyword=' + keywords, dataType: 'json', success: function(result) {
		if( result.length > 0 ) {
			var eList = document.createElement('ul');
			eList.id = 'livesearch_search_results';
			var eListElem;
			var eLink;
			for( var i in result ) {
				eListElem = document.createElement('li');
				
				elinkdiv = document.createElement('div');
				
				eLink = document.createElement('a');
				eLink.appendChild( document.createTextNode(result[i].name));
				
				if(result[i].cartqty!=0)
				{
					ediv= document.createElement('div');
					ediv.id="shqtydivc"+result[i].proid;
					ediv.className="floatleft";
					
					
					eprodplus= document.createElement('div');
					//eprodplus.className="productplus";
					eprodplus.setAttribute("style","float: left;margin-right:5px;margin-top: 6px;");
					
					
					
					eminus=document.createElement('img');
					eminus.src="catalog/view/theme/grocery/image/minus.png";
					eminus.setAttribute("onclick","minusquantityc("+result[i].proid+")");
					eminus.setAttribute("style","cursor:pointer");
					eminus.setAttribute("onmousemove","this.src='catalog/view/theme/grocery/image/minushover.png'");
					eminus.setAttribute("onmouseout","this.src='catalog/view/theme/grocery/image/minus.png'");
					
					
					equantity= document.createElement('div');					
					equantity.setAttribute("style","float:left;margin-right:5px;");
					//equantity.className="quantityinput";
					
					eqtyinput= document.createElement('input');
					eqtyinput.className="order_qua";
					eqtyinput.type="text";
					eqtyinput.name="quantityc"+result[i].proid;
					eqtyinput.id="quantityc"+result[i].proid;				
					eqtyinput.setAttribute("value",result[i].cartqty);
					//eqtyinput.setAttribute("size","4");
					
					eqtyinput.setAttribute("style","background:#ffffff;border:1px solid #CCCCCC;margin-left:0px;margin-right:0px;padding:3px;border-radius:3px 3px 3px 3px;color: #808080;display: inline-block;font-size: 13px;line-height: 18px;outline: medium none;padding: 0 !important;text-align: center;transition: border 0.2s linear 0s, box-shadow 0.2s linear 0s;width:23px;height:16px;");
					
					/*eqtyinput.setAttribute("style.background","#ffffff");
					eqtyinput.setAttribute("style.border","1px solid #CCCCCC");
					eqtyinput.setAttribute("style.margin-left","0px");
					eqtyinput.setAttribute("style.margin-right","0px");
					eqtyinput.setAttribute("style.padding","3px");
					eqtyinput.setAttribute("style.border-radius","3px 3px 3px 3px");	
					eqtyinput.setAttribute("style.width","25px");*/
					eqtyinput.setAttribute("onchange","callupdatec("+result[i].proid+")");
					eqtyinput.setAttribute("onkeyup","chkzerominusc("+result[i].proid+",event)");
					
					eplusdiv=document.createElement('div');
					eplusdiv.id="changemplusc"+result[i].proid;
					eplusdiv.setAttribute("style","float:left;margin-top:6px;");
					//eplusdiv.className="productminus";	
																				
					eplus=document.createElement('img');
					eplus.src="catalog/view/theme/grocery/image/plus.png";
					eplus.setAttribute("onclick","plusquantityc("+result[i].proid+")");
					eplus.setAttribute("style","cursor:pointer");
					eplus.setAttribute("onmousemove","this.src='catalog/view/theme/grocery/image/plushover.png'");
					eplus.setAttribute("onmouseout","this.src='catalog/view/theme/grocery/image/plus.png'");
					
					eclear=document.createElement('div');
					eclear.className="clear";
					
					eListElem.appendChild(elinkdiv);
					
					elinkdiv.appendChild(eLink);		
					eListElem.appendChild(ediv);									
							
					eListElem.appendChild(eplusdiv);	
					eListElem.appendChild(eclear);
					
					eprodplus.appendChild(eminus);
					ediv.appendChild(eprodplus);
					ediv.appendChild(equantity);	
					
					equantity.appendChild(eqtyinput);
					eplusdiv.appendChild(eplus);
					
					eList.appendChild(eListElem);
					
				}
				else
				{
					ediv= document.createElement('div');
					ediv.id="shqtydivc"+result[i].proid;
					ediv.setAttribute("style","float:left;");
					//ediv.className="floatleft";
					
					eanchortag= document.createElement('a');
					eanchortag.href="javascript:plusquantity1c('"+result[i].proid+"')";
					eanchortag.setAttribute("style","line-height:22px;");
					eanchortag.innerHTML="Add&nbsp;";
					
					eplusdiv=document.createElement('div');
					eplusdiv.id="changemplusc"+result[i].proid;
					eplusdiv.setAttribute("style","float:left;margin-top:6px;");
					//eplusdiv.className="productminus";
										
					eplus=document.createElement('img');
					eplus.src="catalog/view/theme/grocery/image/plus.png";
					eplus.setAttribute("onclick","plusquantity1c("+result[i].proid+")");
					eplus.setAttribute("style","cursor:pointer");
					eplus.setAttribute("onmousemove","this.src='catalog/view/theme/grocery/image/plushover.png'");
					eplus.setAttribute("onmouseout","this.src='catalog/view/theme/grocery/image/plus.png'");
					
					eclear=document.createElement('div');
					eclear.className="clear";					
					
					
					eListElem.appendChild(elinkdiv);
					
					elinkdiv.appendChild(eLink);
					eListElem.appendChild(ediv);			
					ediv.appendChild(eanchortag);
					
					eListElem.appendChild(ediv);
					eListElem.appendChild(eplusdiv);
					eplusdiv.appendChild(eplus);
					
					eListElem.appendChild(eclear);
					
					eList.appendChild(eListElem);
				}

				
				
				if( typeof(result[i].href) != 'undefined' ) {
					eLink.href = result[i].href;
				}
				else {
					eLink.href = $('base').attr('href') + 'index.php?route=product/product&product_id=' + result[i].product_id + '&keyword=' + keywords;
				}
				
				
			}
			if( $('#livesearch_search_results').length > 0 ) {
				$('#livesearch_search_results').remove();
			}
			$('#search').append(eList);
                        
                        $('#livesearch_search_results').mouseleave(function(){
                           $(this).remove(); 
                        });
		}
	}});

	return true;
}


function callupdatec(productid)
{
	//alert(document.getElementById('quantity'+productid).value);
	updateToCart(productid,document.getElementById('quantityc'+productid).value);
	if(document.getElementById('quantityc'+productid).value==0)
	{
		document.getElementById('shqtydivc'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1c("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changemplusc'+productid).innerHTML="<img src='catalog/view/theme/grocery/image/plus.png' onclick='plusquantity1c("+productid+")' onmousemove='this.src=\"catalog/view/theme/grocery/image/plushover.png\"' onmouseout='this.src=\"catalog/view/theme/grocery/image/plus.png\"' />";
	}
}



function plusquantityc(productid)
{
	document.getElementById('quantityc'+productid).value = parseInt(document.getElementById('quantityc'+productid).value)+parseInt(1);	
	updateToCart(productid,document.getElementById('quantityc'+productid).value);
}
function minusquantityc(productid)
{
	document.getElementById('quantityc'+productid).value = parseInt(document.getElementById('quantityc'+productid).value)-parseInt(1);
	updateToCart(productid,document.getElementById('quantityc'+productid).value);
	if(document.getElementById('quantityc'+productid).value==0)
	{
		document.getElementById('shqtydivc'+productid).innerHTML="<a class='addquantity' style='line-height:23px;' href='javascript:plusquantity1c("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changemplusc'+productid).innerHTML="<img src='catalog/view/theme/grocery/image/plus.png' onclick='plusquantity1c("+productid+")' onmousemove='this.src=\"catalog/view/theme/grocery/image/plushover.png\"' onmouseout='this.src=\"catalog/view/theme/grocery/image/plus.png\"' />";
	}
}

function chkzerominusc(productid,e)
{
	var keynum;

            if(window.event){ // IE					
            	keynum = e.keyCode;
            }else
                if(e.which){ // Netscape/Firefox/Opera					
            		keynum = e.which;
                 }
           // alert(String.fromCharCode(keynum));
		   if(String.fromCharCode(keynum)=='m')
		   {
			   updateToCart(productid,"0");
			   
			   document.getElementById('shqtydivc'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1c("+productid+")'>Add&nbsp;&nbsp;</a>";
			   document.getElementById('changemplusc'+productid).innerHTML="<img src='catalog/view/theme/grocery/image/plus.png' onclick='plusquantity1c("+productid+")' onmousemove='this.src=\"catalog/view/theme/grocery/image/plushover.png\"' onmouseout='this.src=\"catalog/view/theme/grocery/image/plus.png\"' />";
			   
		   }
}

function plusquantity1c(productid)
{
	document.getElementById('shqtydivc'+productid).innerHTML="<div style='float:left;margin-right:5px;margin-top:6px;'><img src='catalog/view/theme/grocery/image/minus.png' onclick='minusquantityc("+productid+")' onmousemove='this.src=\"catalog/view/theme/grocery/image/minushover.png\"' onmouseout='this.src=\"catalog/view/theme/grocery/image/minus.png\"'/></div><div style='float:left;margin-right:5px;'><input style='background:#ffffff;border:1px solid #CCCCCC;margin-left:0px;margin-right:0px;padding:3px;border-radius:3px 3px 3px 3px;color: #808080;display: inline-block;font-size: 13px;line-height: 18px;outline: medium none;padding: 0 !important;text-align:center;transition: border 0.2s linear 0s,box-shadow 0.2s linear 0s;width:23px;height:16px;' type='text' name='quantityc"+productid+"' id='quantityc"+productid+"' value='0' onchange='callupdatec("+productid+")' onkeyup='chkzerominusc("+productid+",event)'/></div>";	
	
	document.getElementById('changemplusc'+productid).innerHTML="<img src='catalog/view/theme/grocery/image/plus.png' onclick='plusquantityc("+productid+")' onmousemove='this.src=\"catalog/view/theme/grocery/image/plushover.png\"' onmouseout='this.src=\"catalog/view/theme/grocery/image/plus.png\"'/>";
	
	document.getElementById('quantityc'+productid).value = parseInt(document.getElementById('quantityc'+productid).value)+parseInt(1);
	
	updateToCart(productid,document.getElementById('quantityc'+productid).value);
}

function upDownEvent( ev ) {
	var elem = document.getElementById('livesearch_search_results');
	var fkey = $('#search').find('[name=filter_name]').first();

	if( elem ) {
		var length = elem.childNodes.length - 1;

		if( updown != -1 && typeof(elem.childNodes[updown]) != 'undefined' ) {
			$(elem.childNodes[updown]).removeClass('highlighted');
		}

		// Up
		if( ev.keyCode == 38 ) {
			updown = ( updown > 0 ) ? --updown : updown;
		}
		else if( ev.keyCode == 40 ) {
			updown = ( updown < length ) ? ++updown : updown;
		}

		if( updown >= 0 && updown <= length ) {
			$(elem.childNodes[updown]).addClass('highlighted');

			var text = elem.childNodes[updown].childNodes[0].text;
			if( typeof(text) == 'undefined' ) {
				text = elem.childNodes[updown].childNodes[0].innerText;
			}

			$('#search').find('[name=filter_name]').first().val( new String(text).replace(/(\s\(.*?\))$/, '') );
		}
	}

	return false;
}

var updown = -1;

$(document).ready(function(){
	$('#search').find('[name=filter_name]').first().keyup(function(ev){
		doLiveSearch(ev, this.value);
	}).focus(function(ev){
		doLiveSearch(ev, this.value);
	}).keydown(function(ev){
		upDownEvent( ev );
	});
        
        /*.blur(function(){
		window.setTimeout("$('#livesearch_search_results').remove();updown=0;", 1500);
	});*/
    
	$(document).bind('keydown', function(ev) {
		try {
			if( ev.keyCode == 13 && $('.highlighted').length > 0 ) {
				document.location.href = $('.highlighted').find('a').first().attr('href');
			}
		}
		catch(e) {}
	});
});
//]]>
