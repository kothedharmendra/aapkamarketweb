<?php
    	$name = $this->customer->getFirstName().' '.$this->customer->getLastName();        
        $email_address = $this->customer->getEmail();
?>
<html>
<head>    
    <link rel="stylesheet" href="catalog/view/theme/grocery/stylesheet/stylesheet.css" />
    <link rel="stylesheet" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
</head>
<body style='color: black;'>
    <div id="content" class="pop_up_container">
        <h1><?php echo $heading_title; ?></h1>
        <?php if(isset($msg)){ ?>
        <h2 class="success">
            <?php echo $msg; ?>
                <script>
                     $('#fancybox-content').height(250);
                </script>
        </h2>        
	<?php }  ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    
    <div class="content">
        
    <?php if(isset($email_address)){ ?>            
        <input type="hidden" name="name" value="<?= $name; ?>" />
    <?php }else{ ?>    
        <b><?php echo $entry_name; ?></b><br />
        <input type="text" name="name" value="" />
        <br />
        <?php if ($error_name) { ?>
        <span class="error"><?php echo $error_name; ?></span>
        <?php } ?>
        <br />
    <?php } ?>
    
    <?php if(isset($email_address)){ ?>
        <input type="hidden" name="email" value="<?= $email_address; ?>" />
    <?php }else{ ?>
        <b><?php echo $entry_email; ?></b><br />
        <input type="text" name="email" value="" />
        <br />        
        <?php if ($error_email) { ?>
        <span class="error"><?php echo $error_email; ?></span>
        <?php } ?>    
        <br />
    <?php } ?>
    
    <b><?php echo $entry_enquiry; ?></b><br />
    <textarea name="enquiry" cols="40" rows="10" style="width: 99%;"></textarea>
    <br />
    
    <?php if ($error_enquiry) { ?>
    <span class="error"><?php echo $error_enquiry; ?></span>
    <?php } ?>
    <br />

    <div class="popup-button-wrapper">
         <div class="left">
             <input type="submit" value="Send suggestion" class="button" />
        </div> 
    </div>    
    
    </div>
      
  </form>
</div>
    