<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <table class="list">
    <thead>
      <tr>
        <td class="left" colspan="2"><?php echo $text_order_detail; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="left" style="width: 50%;"><?php if ($invoice_no) { ?>
          <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />
          <?php } ?>
          <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
          <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?></td>
        <td class="left" style="width: 50%;"><?php if ($payment_method) { ?>
          <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
          <?php } ?>
          <?php if ($shipping_method) { ?>
          <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
          <?php } ?></td>
      </tr>
    </tbody>
  </table>
  <table class="list">
    <thead>
      <tr>
        <td class="left"><?php echo $text_payment_address; ?></td>
        <?php if ($shipping_address) { ?>
        <td class="left"><?php echo $text_shipping_address; ?></td>
        <?php } ?>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="left"><?php echo $payment_address; ?></td>
        <?php if ($shipping_address) { ?>
        <td class="left"><?php echo $shipping_address; ?></td>
        <?php } ?>
      </tr>
    </tbody>
  </table>
  <table class="list">
    <thead>
      <tr>
        <td class="left"><?php echo $column_name; ?></td>
        <td class="left"><?php echo $column_model; ?></td>
        <!--<td class="left">Food Coupon</td>
        <td class="left">Tax</td>-->
        <td class="right"><?php echo $column_quantity; ?></td>
        <td class="right"><?php echo $column_price; ?></td>
        <td class="right" colspan="2"><?php echo $column_total; ?></td>
        <?php if ($products) { ?>
        <td style="width: 1px;"></td>
        <?php } ?>
      </tr>
    </thead>
    <tbody>
      <?php $fca=0; ?>
      <?php foreach ($products as $product) { ?>
      <tr>
        <td class="left"><?php echo $product['name']; ?>
          <?php foreach ($product['option'] as $option) { ?>
          <br />
          &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
          <?php } ?></td>
         <?php if ($product['jan']) { ?>
      	<?php $fca+=str_replace("Rs ","",$product['price']) * $product['quantity']; 
               }?>
        <td class="left"><?php echo $product['model']; ?></td>
        <!--<td class="right"><?php echo $product['jan']; ?></td>
        <td class="right"><?php echo $product['tax']; ?></td>-->
        <td class="right"><?php echo $product['quantity']; ?></td>
        <td class="right"><?php echo $product['price']; ?></td>
        <td class="right" colspan="2"><?php echo $product['total']; ?></td>
        <td class="right"><a href="<?php echo $product['return']; ?>"><img src="catalog/view/theme/grocery/image/return.png" alt="<?php echo $button_return; ?>" title="<?php echo $button_return; ?>" /></a></td>
      </tr>
      <?php } ?>
      <?php foreach ($vouchers as $voucher) { ?>
      <tr>
        <td class="left"><?php echo $voucher['description']; ?></td>
        <td class="left"></td>
        <td class="right">1</td>
        <td class="right"><?php echo $voucher['amount']; ?></td>
        <td class="right"><?php echo $voucher['amount']; ?></td>
        <?php if ($products) { ?>
        <td></td>
        <?php } ?>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <?php foreach ($totals as $total) {
      if($total['code'] != 'sub_total' && $total['code'] != 'tax'){ ?>
      <tr>
        <td colspan="5"></td>
        <td class="right"><b><?php echo $total['title']; ?>:</b></td>
        <td class="right"><?php echo $total['text']; ?></td>
        <?php if ($products) { ?>
        <td></td>
        <?php } } ?>
      </tr>
      <?php } ?>
     <!-- <tr>
      <td colspan="5"></td>
      <td align="right" class="right"><b>Max Food Coupons Allowed:</b></td>
      <td align="right" class="right">Rs <?php echo ($fca < 0 ? 0 : $fca); ?></td>
      <td></td>
    </tr>-->
    </tfoot>
  </table>
  <?php if ($comment) { ?>
  <!--<table class="list">
    <thead>
      <tr>
        <td class="left"><?php echo $text_comment; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="left"><?php echo $comment; ?></td>
      </tr>
    </tbody>
  </table>-->
  <?php } ?>
   <!--<table width="100%" class="list">
    <thead>
    <tr>
      <td class="left"><b>Tax Category</b></td>
      <td><b>A</b></td>
      <td class="left" align="right"><b>B</b></td>
      <td class="left" align="right"><b>C</b></td>
      <td class="left" align="right"><b>D</b></td>
      <td class="left" align="right"><b>E</b></td>
    </tr>
    </thead>
    <tbody>
     <tr>
      <td class="left">Tax %</td>
      <td class="left">0%</td>
      <td class="left" align="right">17.0%</td>
      <td class="left" align="right">5.5%</td>
      <td class="left" align="right">14.5%</td>
      <td class="left" align="right">2%</td>
    </tr>
   </tbody>
 </table>-->
  <?php if ($histories) { ?>
  <h2><?php echo $text_history; ?></h2>
  
  <!--------------------JIPL Start------------------------->
  <table class="list">
    <thead>
      <tr>
        <td class="left"><?php echo $column_date_added; ?></td>
        <td class="left"><?php echo $column_status; ?></td>
        <!--<td class="left"><?php echo $column_comment; ?></td>-->
        <td class="left">Delivery Date</td>
        <td class="left">Time Slot</td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($histories as $history) { ?>
      <tr>
        <td class="left"><?php echo $history['date_added']; ?></td>
        <td class="left"><?php echo $history['status']; ?></td>
        <!--<td class="left"><?php echo $history['comment']; ?></td>-->
        <td class="left"><?php if($history['datepicker']!='0000-00-00'  && $history['datepicker']!='1970-01-01') { echo date('d/m/Y',strtotime($history['datepicker'])); } ?></td>
        <td class="left"><?php echo $history['timeslot']; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
  <!--------------------JIPL End------------------------->
  <?php } ?>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?> 