<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?><img src="catalog/view/theme/grocery/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <?php if ($products) { ?>
   <div class="wishlist-info">
    <table>
      <thead>
        <tr>
          <td class="image"><?php echo $column_image; ?></td>
          <td class="name"><?php echo $column_name; ?></td>
          <td class="model"><?php echo $column_model; ?></td>
          <td class="stock"><?php echo $column_stock; ?></td>
          <td class="price"><?php echo $column_price; ?></td>
          <td class="action"><?php echo $column_action; ?></td>
        </tr>
      </thead>
      <?php foreach ($products as $product) { ?>
      <tbody id="wishlist-row<?php echo $product['product_id']; ?>">
        <tr>
          <td class="image"><?php if ($product['thumb']) { ?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
            <?php } ?></td>
          <td class="name">
          
          <div><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
          
          <div class="cart">
        
            <?php
            $cartproducts = $this->cart->getProducts();
            ?>
            
            <?php 
            $cflag=0;$cquantity=0;
            foreach($cartproducts as $cproduct) {
                if($cproduct['product_id']==$product['product_id'])
                { 
                    $cquantity=$cproduct['quantity'];
                    $cflag=1;
                }
            }
            ?>
            
            
            <?php  if($cquantity!=0) { ?>
            
            <div class="floatleft" id="shqtydiv<?php echo $product['product_id']; ?>">
            <div class="productplus"><img src="<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/minus.jpg" onclick="minusquantity('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/minushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/minus.jpg'"/></div> 
    
            <div class="quantityinput"><input class="order_qua" type="text" name="quantity<?php echo $product['product_id']; ?>" id="quantity<?php echo $product['product_id']; ?>" value="<?=$cquantity?>" onchange="callupdate('<?php echo $product['product_id']; ?>')" onkeyup="chkzerominus('<?php echo $product['product_id']; ?>',event)"/></div>
            </div>
            
            <div class="productminus" id="changemplus<?php echo $product['product_id']; ?>"><img src="<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
            
            <div class="clear"></div>
            
            <?php } else {?>
            
            <div class="floatleft" id="shqtydiv<?php echo $product['product_id']; ?>">
            <a class="addquantity" href="javascript:plusquantity1('<?php echo $product['product_id']; ?>')">Add&nbsp;&nbsp;</a>
            </div>
            
            <div class="productminus" id="changemplus<?php echo $product['product_id']; ?>"><img src="<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity1('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
            
            <div class="clear"></div>
            
            <?php } ?>
            
            
     
          </div>
          
          <div class="comparee"><a class="compare tooltip" onclick="addToCompare('<?php echo $product['product_id']; ?>');"><span>Add to Compare</span></a></div>
             
            
          <div class="clear"></div>
          
          
          
          
        </div>
          
          </td>
          <td class="model"><?php echo $product['model']; ?></td>
          <td class="stock"><?php echo $product['stock']; ?></td>
          <td class="price"><?php if ($product['price']) { ?>
            <div class="price">
              <?php if (!$product['special']) { ?>
              <?php echo $product['price']; ?>
              <?php } else { ?>
              <s><?php echo $product['price']; ?></s> <b><?php echo $product['special']; ?></b>
              <?php } ?>
            </div>
            <?php } ?></td>
          <td class="action">
              <a href="<?php echo $product['remove']; ?>"><img src="catalog/view/theme/grocery/image/remove.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" /></a></td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
  </div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } else { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } ?>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>
<script type="text/javascript">
function callupdate(productid)
{
	//alert(document.getElementById('quantity'+productid).value);
	updateToCart(productid,document.getElementById('quantity'+productid).value);
	if(document.getElementById('quantity'+productid).value==0)
	{
		document.getElementById('shqtydiv'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
	}
}



function plusquantity(productid)
{
	document.getElementById('quantity'+productid).value = parseInt(document.getElementById('quantity'+productid).value)+parseInt(1);	
	updateToCart(productid,document.getElementById('quantity'+productid).value);
}
function minusquantity(productid)
{
	document.getElementById('quantity'+productid).value = parseInt(document.getElementById('quantity'+productid).value)-parseInt(1);
	updateToCart(productid,document.getElementById('quantity'+productid).value);
	if(document.getElementById('quantity'+productid).value==0)
	{
		document.getElementById('shqtydiv'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
	}
}

function chkzerominus(productid,e)
{
	var keynum;

            if(window.event){ // IE					
            	keynum = e.keyCode;
            }else
                if(e.which){ // Netscape/Firefox/Opera					
            		keynum = e.which;
                 }
           // alert(String.fromCharCode(keynum));
		   if(String.fromCharCode(keynum)=='m')
		   {
			   updateToCart(productid,"0");
			   
			   document.getElementById('shqtydiv'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1("+productid+")'>Add&nbsp;&nbsp;</a>";
			   document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
			   
		   }
}

function plusquantity1(productid)
{
	document.getElementById('shqtydiv'+productid).innerHTML="<div class='productplus'><img src='<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/minus.jpg' onclick='minusquantity("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/minushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/minus.jpg\"'/></div><div class='quantityinput'><input class='order_qua' type='text' name='quantity"+productid+"' id='quantity"+productid+"' value='0' onchange='callupdate("+productid+")' onkeyup='chkzerominus("+productid+",event)'/></div>";	
	
	document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER  ?>catalog/view/theme/grocery/image/plus.jpg\"'/>";
	
	document.getElementById('quantity'+productid).value = parseInt(document.getElementById('quantity'+productid).value)+parseInt(1);
	
	updateToCart(productid,document.getElementById('quantity'+productid).value);
}

</script>
<style>
.cart a
{
	text-decoration:none;
}
.cart a:hover
{
	text-decoration:none;
}
</style>