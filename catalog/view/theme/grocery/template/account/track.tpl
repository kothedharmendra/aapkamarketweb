<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
    
  <h1><?php echo $heading_title; ?></h1>
  
  <div class="order-track-content">
  <?php if(!$order){ ?>    
    <div class="left">
      <form action="<?php echo $action; ?>" method="post">
        <div class="content">
          <p>Enter your order id to track your order status</p>
                
          <b><?php echo $entry_order_id; ?></b><br />
          <input type="text" name="order_id" />
          <br />
          <br />
          <input type="submit" value="Track" class="button" />
        </div>
      </form>
    </div>
<?php }else{ ?>      
    <div class="left">
        <div class="content">
            <div class="line">
                <label>
                    Order ID
                </label>
                <span>
                    <?= $order['order_id'] ?>
                </span>
            </div>    
            <div class="line">
                <label>
                    Name
                </label>
                <span>
                    <?= $order['payment_firstname'].' '.$order['payment_lastname'] ?>
                </span>
            </div>    
            <div class="line">
                <label>
                    Shipping Method
                </label>
                <span>
                    <?=  $order['shipping_method'] ?>
                </span>
            </div>              
            <div class="line">
                <label>
                    Order Total
                </label>
                <span>
                    <?= $order['total'] ?>
                </span>
            </div>
            <div class="line">
                <label>
                    Order Date
                </label>
                <span>
                    <?= $order['date_added'] ?>
                </span>
            </div>
             <div class="line">
                <label>
                    Order Status
                </label>
                <span>
                    <?= $order['order_status'] ?>
                </span>
            </div>
            </div>
        </div>  
<?php } ?>                            
</div>
<?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('#login input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#login').submit();
	}
});
//--></script> 
<?php echo $footer; ?>