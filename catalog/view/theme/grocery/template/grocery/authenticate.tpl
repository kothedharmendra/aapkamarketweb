<html>
    <head>
        <title>Authenticate</title>        
        <link rel="stylesheet" type="text/css" href="<?= HTTP_SERVER ?>catalog/view/theme/grocery/stylesheet/bootstrap.css" />        
    </head>
    <body>

        <br />

        <div class="container ">
            <div class="theme-showcase">
                <div class="jumbotron">
                    <h1 style="margin-top: 0;">Verify License!</h1>
                    <p>You must have gotten License Key via email for this specific domain. Please paste it in below box and press Submit. If you haven't gotten it, email us at care@groceryscript.com</p>
                </div>    
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <form method="post" class="form form-horizontal">
                        <div class="form-control-group">
                            <label>License Key</label>
                            <input class="form-control" name="key" placeholder="Enter License Key" />                            
                        </div>
                        
                        <br />
                        
                        <div class="button-set">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                        </div>
                    </form>
                </div>    
            </div>
        </div>
    </body>
</html>