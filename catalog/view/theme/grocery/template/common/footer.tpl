<!--Aboutus-->
<?php if($this->config->get('dailyshopin_custom_widget_title') != '') { ?>
<div class="grid_12">
    <div class="marB30">
        <div class="about">
            <span class="second-line"><?php echo $this->config->get('dailyshopin_shipping_text')?></span><br/>
        </div>
        <?php } ?>

        <div class="clearfix"></div>

        <?php if($this->config->get('dailyshopin_shipping_text') != '') { ?>
        <div class="shipping">
            <?php if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') $path_image = HTTPS_IMAGE;
            else $path_image = HTTP_IMAGE; 
            if($this->config->get('dailyshopin_cus_img')!='') { ?>
            <img src="<?php echo $path_image . $this->config->get('dailyshopin_cus_img') ?>" alt="">
            <?php } ?>
            <p style="margin-top:14px;">
                <?php echo $this->config->get('dailyshopin_footer_info_text')?>
            </p>
            <span class="third-line"><?php echo $this->config->get('dailyshopin_shipping_last_text')?></span>
        </div>
    </div>
</div>
<?php } ?>

</div><!--/container_12-->


<footer>

    <div class="container_12 footer-bg">
        <!--Aboutus-->

        <!--contact us-->

        <?php if($this->config->get('dailyshopin_phone') != '') { ?>
        <div class="grid_3">
            <h3 class="widgetsTitle"><?php echo $text_contact; ?></h3>
            <div class="contact marB30">
                <?php if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') $path_image = HTTPS_IMAGE;
            else $path_image = HTTP_IMAGE; 
            if($this->config->get('dailyshopin_contact_img')!='') { ?>
                <img src="<?php echo $path_image . $this->config->get('dailyshopin_contact_img') ?>" alt="" width="100"
                     class="contactImg">
                <?php } ?>
                <div class="rightCont">
                    <?php if($this->config->get('dailyshopin_address') != '') { ?>
                    <img class="icon" src="catalog/view/theme/grocery/image/home-2.png" width="25"
                         alt=""> <?php echo $this->config->get('dailyshopin_address')?>
                    <?php } ?>
                    <br/><br/>
                    <div class="clearfix"></div>
                    <?php if($this->config->get('dailyshopin_phone') != '') { ?>
                    <img class="icon" src="catalog/view/theme/grocery/image/phone.png" width="25"
                         alt=""> <?php echo $this->config->get('dailyshopin_phone')?> <br/><?php echo $this->
                    config->get('dailyshopin_phone_second')?>
                    <?php } ?>
                    <br/><br/>
                    <div class="clearfix"></div>
                    <?php if($this->config->get('dailyshopin_email') != '') { ?>
                    <img class="icon" src="catalog/view/theme/grocery/image/mail.png" width="25" alt=""> <a
                            href="mailto:<?php echo $this->config->get('dailyshopin_email')?>"><?php echo $this->
                        config->get('dailyshopin_email')?></a> <br/>
                    <a href="mailto:<?php echo $this->config->get('dailyshopin_email_second')?>"><?php echo $this->
                        config->get('dailyshopin_email_second')?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>


        <!--twitter-->

        <?php if($this->config->get('dailyshopin_twitter_username') != '') { ?>
        <div class="grid_3">
            <h3 class="widgetsTitle"><?php echo $text_twitter; ?></h3>
            <div class="tweet marB30"></div>
            <script>
                jQuery(function ($) {
                    $(".tweet").tweet({
                        modpath: 'catalog/view/javascript/jquery/twitter/',
                        username: "<?php echo $this->config->get('dailyshopin_twitter_username'); ?>",
                        avatar_size: 32,
                        count: 4,
                        loading_text: "loading tweets..."
                    });
                });
            </script>
        </div>
        <?php } ?>


        <!--facebook-->

        <?php if($this->config->get('dailyshopin_facebook_id') != '') { ?>
        <div class="grid_3">
            <h3 class="widgetsTitle"><?php echo $text_facebook; ?></h3>
            <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2F<?php echo $this->config->get('dailyshopin_facebook_id'); ?>&amp;width=292&amp;height=258&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;show_border=false&amp;header=false"
                    scrolling="no" frameborder="0"
                    style="border:none; overflow:hidden; width:94%; height:260px; margin-bottom: 30px; background:<?php echo $this->config->get('facebook_bg_color'); ?>; padding: 3%"
                    allowTransparency="true"></iframe>
        </div>
        <?php } ?>

        <?php if ($informations) { ?>
        <div class="column grid_3">
            <h3>About Us</h3>
            <ul>
                <?php foreach ($informations as $information) { ?>
                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>
        <div class="column grid_3">
            <h3>Shopping @ Dailyshopin</h3>
            <ul>
                <li><a href="how-do-i-shop">How Do I Shop?</a></li>
                <li><a href="index.php?route=checkout/cart">Shopping Cart</a></li>
                <li><a href="index.php?route=checkout/checkout">Checkout</a></li>
            </ul>
        </div>

        <div class="column grid_3">
            <h3>My Account</h3>
            <ul>
                <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
            </ul>
        </div>
        <div class="column grid_3">
            <h3>Social Links</h3>
            <ul class="social_links">
                <li>
                    <a href="<?php echo $this->config->get('dailyshopin_facebook_link'); ?>" title="facebook"
                       target="_blank">
                        <img src="catalog/view/theme/grocery/image/social/facebook.png" width="30" alt=""/>
                    </a>
                    <a href="<?php echo $this->config->get('dailyshopin_twitter_link'); ?>" title="twitter"
                       target="_blank">
                        <img src="catalog/view/theme/grocery/image/social/twitter.png" width="30" alt=""/>
                    </a>
                </li>
            </ul>
        </div>
    </div><!--/container_12-->
    <div id="powered"><?php echo $powered; ?></div>

</footer>

<!--<noindex style="display:inline;position:fixed;left:10px;bottom:0;width:90px">
    <div class="suggest">
        <a class="pop-up fancybox.iframe" href="index.php?route=module/suggestion" style="color: #ecb41c;">
            SUGGEST<br/> PRODUCT
        </a>
    </div>
</noindex>

<noindex style="display:inline;position:fixed;right:30px;bottom:0;width:90px">
    <div class="feed-back">
        <a class="pop-up fancybox.iframe" href="index.php?route=module/feedback" style="color: #ecb41c;">Feedback</a>
    </div>
</noindex>-->

</body>
</html>

<?php $is_login = $this->customer->isLogged(); ?>

<script>
    $(document).ready(function () {
        $(".pop-up").fancybox({
            maxWidth: 400,
            maxHeight: <?php
             if ($is_login)
                echo '350';
             else
               echo '470';
        ?>,
        scrolling : 'no',
        fitToView:false,
        width:'70%',
        height:'70%',
        autoSize :false,
        closeClick: false,
        openEffect:'none',
        closeEffect:'none',
        'iframe' : {'scrolling': 'no'}
    })
        ;
    });
</script>