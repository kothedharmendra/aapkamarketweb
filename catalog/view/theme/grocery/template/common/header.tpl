<?php
$this->load->model('account/customer');
if(isset($this->session->data['customer_id'])) {
$customer_info = $this->model_account_customer->getCustomer($this->session->data['customer_id']);
if(isset($customer_info['customer_group_id'])) {
$login_customer_group = $customer_info['customer_group_id'];
} else {
$login_customer_group = 0;
}
} else {
$login_customer_group = 0;
}
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <!--<![endif]--><head>
        <meta charset="UTF-8" />

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <title><?php echo $title; ?></title>

        <base href="<?php echo $base; ?>" />

        <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; ?>" />
        <?php } ?>

        <?php if ($keywords) { ?>
        <meta name="keywords" content="<?php echo $keywords; ?>" />
        <?php } ?>

        <?php if ($icon) { ?>
        <link href="<?php echo $icon; ?>" rel="icon" />
        <?php } ?>

        <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>

        <?php foreach ($styles as $style) { ?>
        <link rel="<?php echo $style['rel']; ?>" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>

        <!-- jQuery Library --><script src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

        <!-- ///////////// IF RTL ///////////////// -->
        <?php if ($direction == 'rtl') { ?>
        <!-- MAIN STYLE --><link rel="stylesheet" href="catalog/view/theme/grocery/stylesheet/rtl/stylesheet.css" />
        <!-- cloud zoom --><link href="catalog/view/theme/grocery/stylesheet/rtl/cloud-zoom.css" />
        <!-- Custom.js for included scripts --><script src="catalog/view/javascript/jquery/custom-rtl.js"></script>
        <!-- cloud zoom --><script src="catalog/view/javascript/jquery/cloud-zoom.1.0.2.min-rtl.js"></script>

        <!-- ///////////// IF LTR ///////////////// -->
        <?php } else { ?>
        <!-- MAIN STYLE --><link rel="stylesheet" href="catalog/view/theme/grocery/stylesheet/stylesheet.css" />
        <!-- cloud zoom --><link href="catalog/view/theme/grocery/stylesheet/cloud-zoom.css" />
        <!-- Custom.js for included scripts --><script src="catalog/view/javascript/jquery/custom.js"></script>
        <!-- cloud zoom --><script src="catalog/view/javascript/jquery/cloud-zoom.1.0.2.min.js"></script>
        <?php } ?>

        <!--960 GRID SYSTEM--><script src="catalog/view/javascript/jquery/adapt.min.js"></script>

        <!--flex slider--><link rel="stylesheet" href="catalog/view/theme/grocery/stylesheet/flexslider.css" media="screen" />
        <!--flex slider--><script src="catalog/view/javascript/jquery/jquery.easing.js"></script>
        <!--flex slider--><script defer src="catalog/view/javascript/jquery/jquery.flexslider.min.js"></script>

        <?php
        $_SESSION['consumer_keyy']=$this->config->get('consumer_keyy');
        $_SESSION['consumer_secrett']=$this->config->get('consumer_secrett');
        $_SESSION['access_token']=$this->config->get('access_token');
        $_SESSION['token_secret']=$this->config->get('token_secret');
        ?>

        <!-- UItoTop plugin --><link rel="stylesheet" media="screen,projection" href="catalog/view/theme/grocery/stylesheet/ui.totop.css" />
        <!-- UItoTop plugin --><script src="catalog/view/javascript/jquery/jquery.ui.totop.js" ></script>
        <!-- Autofill search --><script src="catalog/view/javascript/jquery/livesearch.js"></script>

        <script src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
        <link rel="stylesheet" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
        <script src="catalog/view/javascript/jquery/ui/external/jquery.cookie.js"></script>

        <script src="catalog/view/javascript/jquery/colorbox/jquery.colorbox.js"></script>
        <link rel="stylesheet" href="catalog/view/javascript/jquery/colorbox/colorbox.css" media="screen" />

        <script src="catalog/view/javascript/jquery/pop-up/jquery.fancybox.js"></script>
        <link rel="stylesheet" href="catalog/view/javascript/jquery/pop-up/jquery.fancybox.css" />

        <script src="catalog/view/javascript/jquery/tabs.js"></script>
        <script src="catalog/view/javascript/common.js"></script>
        <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>"></script>
        <?php } ?>

        <?php echo $google_analytics; ?>

        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Archivo+Narrow">

        <style>
            body {
                background-color: #FFFFFF;
                background-image: none;

            }

            /** Container background image/pattern/color **/
            .container_12 {
                background-image: none;

            }
            .title-block-top {
                color: #071248 !important;
            }
            .top-blocks li p {
                color: #00933F;
                font-size: 11px;
                font-weight: bold;

            }
            body, #maincontent a, #maincontent a:visited, a.backtotop, #cart .content a, .contentset {
                color: #000000;
            }
            .footer-bg {
                background: #F7F7F7 !important;
            }
            footer {
                background: #F7F7F7 !important;
            }
            footer h3, .about h3 {
                color: #000000 !important;
            }
            h3.widgetsTitle {
                color: #000000 !important;
            }
            h3.widgetsTitle {
                background: #000000;
            }
            h3.widgetsTitle {
                border-bottom: 3px solid #000000;
            }
            .about h3 {
                border-bottom: 1px solid #000000;
            }
            hr {
                border-top: 1px solid #000000;
            }
            hr {
                border-bottom: 1px solid #000000;
            }
            .footer-bg, .about, .shipping{
                color: #000000 !important;
            }
            footer a, .footer-bg a, .tweet_list li a {
                color: #000000 !important;
            }
            h1, h2, h3, h4, .contentset, legend, .checkout-heading, .welcome {
                font-family:'Archivo Narrow', Tahoma, Geneva, sans-serif;
                font-size: px !important;
            }
            .top-menu ul li, .top-menu ul li a {
                font-family:'Archivo Narrow', Tahoma, Geneva, sans-serif;
                font-size: 19px !important;
            }
            .box-heading, h2.cat-title {
                font-family:'Archivo Narrow', Tahoma, Geneva, sans-serif !important;
                font-size: 25px !important;
            }

        </style>
    </head>



    <body>

        <div id="fade"></div>

        <header>



            <div class="container_12">

                <header>

                    <?php echo $cart; ?>

                    <div class="clearfix"></div>



                    <?php if ($logo) { ?>
                    <div id="logo" class="grid_3">
                        <a href="<?php echo $home; ?>">
                            <img src="<?php echo $logo; ?>" />
                        </a>
                    </div>
                    <?php } ?>

                    <!-- Top Blocks -->
                    <div class="grid_7">
                        <ul class="top-blocks">
                            <li>
                                <?php
                                if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') $path_image = HTTPS_IMAGE;
                                else $path_image = HTTP_IMAGE; 
                                if($this->config->get('dailyshopin_firstblock_img')!='') { ?>
                                <img src="<?php echo $path_image . $this->config->get('dailyshopin_firstblock_img') ?>" alt="" ><!--custom image-->
                                <?php } ?>
                                <!--title-->
                                <?php if($this->config->get('dailyshopin_firstblock_title') != '') { ?>
                                <h5 class="title-block-top"><?php echo $this->config->get('dailyshopin_firstblock_title') ?></h5><!--custom title-->
                                <?php } ?>
                                <p><!--custom data-->
                                    <?php if($this->config->get('dailyshopin_firstblock_desc') != '') { ?>
                                    <?php echo $this->config->get('dailyshopin_firstblock_desc')?>
                                    <?php } ?>
                                </p>
                            </li>
                            <li>
                                <?php
                                if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') $path_image = HTTPS_IMAGE;
                                else $path_image = HTTP_IMAGE; 
                                if($this->config->get('dailyshopin_secondblock_img')!='') { ?>
                                <img src="<?php echo $path_image . $this->config->get('dailyshopin_secondblock_img') ?>" alt="" ><!--custom image-->
                                <?php } ?>
                                <!--title-->
                                <?php if($this->config->get('dailyshopin_secondblock_title') != '') { ?>
                                <h5 class="title-block-top"><?php echo $this->config->get('dailyshopin_secondblock_title')?></h5><!--custom title-->
                                <?php } ?>
                                <p><!--custom data-->
                                    <?php if($this->config->get('dailyshopin_secondblock_desc') != '') { ?>
                                    <?php echo $this->config->get('dailyshopin_secondblock_desc')?>
                                    <?php } ?>
                                </p>
                            </li>
                            <li>
                                <?php
                                if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') $path_image = HTTPS_IMAGE;
                                else $path_image = HTTP_IMAGE; 
                                if($this->config->get('dailyshopin_thirdblock_img')!='') { ?>
                                <img src="<?php echo $path_image . $this->config->get('dailyshopin_thirdblock_img') ?>" alt="" ><!--custom image-->
                                <?php } ?>
                                <!--title-->
                                <?php if($this->config->get('dailyshopin_thirdblock_title') != '') { ?>
                                <h5 class="title-block-top"><?php echo $this->config->get('dailyshopin_thirdblock_title')?></h5></a><!--custom title-->
                                <?php } ?>
                                <p><!--custom data-->
                                    <?php if($this->config->get('dailyshopin_thirdblock_desc') != '') { ?>
                                    <?php echo $this->config->get('dailyshopin_thirdblock_desc')?>
                                    <?php } ?>
                                </p>
                            </li>
                        </ul>
                    </div>
					<div>
					<ul>
					<li style="margin-top:20px;float:left;width:350px;text-align:center;font-weight:bold">

                                <!--title-->

                                <?php //if($this->config->get('dailyshopin_fourthblock_title') != '') { ?>
                                <?php if (!$logged) { ?>
                                    <p><a href="index.php?route=account/login">Login</a>&nbsp;>>&nbsp;
                                    <a href="index.php?route=account/register">Signup</a>&nbsp;
									<br /><a href="https://play.google.com/store/apps/details?id=com.aapkamarket.com&hl=en"><img src="<?php echo $path_image . 'data/app.png' ?>" /></a>
                                    <?php } else { ?>
                                        <a href="index.php?route=account/account">My Account</a>&nbsp;>>&nbsp;
                                        <a href="index.php?route=account/logout">Logout</a>&nbsp;
										<br />
									<a href="https://play.google.com/store/apps/details?id=com.aapkamarket.com&hl=en"><img src="<?php echo $path_image . 'data/app.png' ?>" /></a>
										<p>
                                        <?php } ?>

                            </li>
							</ul>
							
					</div>
                    <?php if($this->config->get('dailyshopin_time_text') != '') { ?>
                   <h5 style="float:left;padding-top:22px;margin-left:34px;color: #071248;font-size: 15px;"><?php echo $this->config->get('dailyshopin_time_text')?>
				   </h5>
				   
                    <?php } ?>



                    <!-- <div class="clearfix"></div>

                     <div class="festival" style="float:left;margin-top:0px;margin-left:100px;padding-top:10px;">


                               <?php if($this->config->get('dailyshopin_fourthblock_title') != '') { ?>

                               <h3 style="float:right;padding-top:3px;">&nbsp;<?php echo $this->config->get('dailyshopin_fourthblock_desc')?></h3>

                               <?php } ?>

                               <?php
                               if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') $path_image = HTTPS_IMAGE;
                               else $path_image = HTTP_IMAGE;
                               if($this->config->get('dailyshopin_fourthblock_img')!='') { ?>
                               <img src="<?php echo $path_image . $this->config->get('dailyshopin_fourthblock_img') ?>" alt="" style="float:right;max-width:20px;" >
                               <?php } ?>

                           </div>-->

                    <div class="grid_4 search" style="float:right;padding-bottom:5px;margin-top:6px;">

                        <div id="search">
                            <?php $curl="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                            if($curl!=HTTP_SERVER.'index.php?route=product/search') { ?>
                            <!--btn-->
                            <div class="button-search"><img src="catalog/view/theme/grocery/image/search.png" alt="search" ></div>
                            <!--input-->
                            <?php if ($filter_name) { ?>
                            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
                            <?php } else { ?>
                            <input type="text" name="filter_name" value="<?php echo $text_search; ?>" onClick="this.value = '';" onKeyDown="this.style.color = '#000000';" />
                            <?php } ?>
                            <?php } ?>
                        </div>

                    </div>
                    <!--<h3 style="float:left;padding-top:8px;margin-left:50px;">Get Discount on all products • Click n Pick @ no extra charge • Slots from 9am to 9pm</h3>-->
                </header>
            </div>
        </header>


        <div id="notification"></div>



        <div class="clearfix"></div>

        <div class="top-menu-full">
            <div class="top-menu-wrapper">
                <div class="top-menu grid_12">

                    <?php

                    $this->load->model('catalog/category');
                    $this->load->model('catalog/product');

                    $categories_1 = $this->model_catalog_category->getCategories(0);  											// get the categories

                    if ($categories_1) {$output = '<ul>';}

                    // if there are categories available, start off with the unordered list tag

                    foreach ($categories_1 as $category_1) {																			// for each result
                    $output .= '<li class="cats" /*onmouseover="document.getElementById(\'fade\').style.display=\'block\'"*/ onmouseout="document.getElementById(\'fade\').style.display=\'none\'">';																					// put a list item tag																// get the id of this category
                    $unrewritten  = $this->url->link('product/category', 'path=' . $category_1['category_id']);								// otherwise, it will be this one
                    $output .= '<a href="'.($unrewritten).'">' . $category_1['name'] . '</a>';								// finally, generate the category name and wrap its link

                    $categories_2 = $this->model_catalog_category->getCategories($category_1['category_id']);								// if this result has a sub-list, get the set of new categories
                     if($categories_2){
			        $output .= '<div class="cat-wrap">'; // finally, generate the category name and wrap its link
    			}

                    // if this is a subresult, start off with an unordered list tag

                    foreach ($categories_2 as $category_2) {																// for each of this subresult item
                    $output .= '<div class="one-dept">';																				// put a list item tag													// get its category
                    $sub_unrewritten = $this->url->link('product/category', 'path=' . $category_1['category_id'] . '_' . $category_2['category_id']);
                    $output .= '<a class="mainCat" href="'.($sub_unrewritten).'">' . $category_2['name'] . '</a>';					// now, generate the category name and wrap its link

                    $categories_3 = $this->model_catalog_category->getCategories($category_2['category_id']);				// if this result has a sub-list, get the set of new categories

                    $output .= '<div>';								// if this is a subresult, start off with an unordered list tag



                    $output .= '</div></div>';												// if this was the third list, clost the list
                    // then or otherwise, close the second level list tag
                    }
                    // if all sub results have been listed, close the second level
                    $output .= '</li>';																				// then or otherwise close first list parent
                    }
                    /*if ($categories_1) {$output .= '</ul>';}																		// then close the first level list table
                    echo $output;*/

                    if ($categories_1) {$output .= '</ul></div>';}

                    //<!--***** MENU FOR MOBILE DEVICES RETURNS INTO SELECT ****-->
                    if ($categories_1) {
                    $output .= '<div class="menuDevices grid_9" style="display:none">';
                    $output .= '<div class="select_outer">';
                    $output .= '<div class="bg_select"></div>';
                    $output .= '<select onchange="location=this.value">';
                    $output .= '<option>MENU</option>';


                    foreach ($categories_1 as $category_1) {
                    $unrewritten  = $this->url->link('product/category', 'path=' . $category_1['category_id']);
                    $output .= '<option value="'. ($unrewritten) .'">' . $category_1['name'] . '</option>';

                    $categories_2 = $this->model_catalog_category->getCategories($category_1['category_id']);								// if this result has a sub-list, get the set of new categories

                    // if this is a subresult, start off with an unordered list tag

                    foreach ($categories_2 as $category_2) {																// for each of this subresult item
                    $sub_unrewritten = $this->url->link('product/category', 'path=' . $category_1['category_id'] . '_' . $category_2['category_id']);
                    $output .= '<option value="'. ($sub_unrewritten) .'">--' . $category_2['name'] . '</option>';					// now, generate the category name and wrap its link											// if this was the third list, clost the list
                    // then or otherwise, close the second level list tag
                    }




                    }
                    $output .= '</select>';
                    $output .= '</div>';
                    $output .= '</div>';

                    echo $output;

                    }

                    //<!--***** MENU END ****-->																						// now produce the results
                    ?>

                    <!--</div>-->

                    <!-- Search -->
                </div>
            </div><!-- END .top-menu-wrapper -->


            <div class="container_12">

                <div class="clearfix"></div>

                <div class="clearfix"></div>
                <?php  $_SESSION['cgid']=$login_customer_group; ?>