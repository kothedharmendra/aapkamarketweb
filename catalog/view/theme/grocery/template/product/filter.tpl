<?php echo $header; ?>
<?php echo $column_left; ?>
<?php echo $column_right; ?>
<div class="wrap">
	
    <!--breadcrumb
    ==============================================================-->
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    
    <?php echo $content_top; ?>
    
    <!--title
    ==============================================================-->
    
 <?php
 //print_r($products);
 ?>
  
  
  <?php if ($products) { ?>
  
  <!--product filter
  ==============================================================-->
  <div class="product-filter">
    
    <div class="limit"><b><?php echo $text_limit; ?></b>
      <select onchange="location = this.value;">
        <?php foreach ($limits as $limits) { ?>
        <?php if ($limits['value'] == $limit) { ?>
        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
    
    <div class="sort"><b><?php echo $text_sort; ?></b>
      <select onchange="location = this.value;">
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
    
    <div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
	
    
  </div><!--/product-filter-->
  
    
  <?php
  $allproductsid[]=array();
  $similarproducts=array();
  $removeproducts=array();
  $i=0;$j=0;$k=0;
  
  foreach ($products as $product)
  {
  	$allproductsid[$i]=$product['product_id'];
  	$i++;
  }
  
  //print_r($allproductsid);
  
  $l=0;
  
  for($i=0;$i<count($allproductsid);$i++)
  {
  if(!in_array($allproductsid[$i],$removeproducts)){
  	$query = $this->db->query("select * from product_similar where product_id='".$allproductsid[$i]."'");
        $rsallproducts = $query->rows;
        
    $num_rows = sizeof($rsallproducts);
    if ($num_rows > 0)
    {
    	$similarproducts[$j]=$allproductsid[$i];
        $j++;

        foreach($rsallproducts as $row) {
        	$removeproducts[$l]=$row['similar_id'];
       		$l++;
        }
    }
    }
  } 
  $m=0;
  ?>  
    
  
  <!--Managed from js in the bottom of this file (go down)-->
  <div class="product-list">
  
    <?php foreach ($products as $product) {     
    ?>
    
    
    <?php
    	$flag=0;
        
        //echo $product['product_id'];
        
    	for($k=0;$k<count($removeproducts);$k++)
        {
        	if($product['product_id']==$removeproducts[$k])
            {
            	$flag=1;
            }	
        }
    	if($flag!=1)
        {
    ?> 
    
    <div>     
    
    <?php
    	$search=0;
        $searchproduct="";
        foreach ($products as $product2) {

            if($search==0)
            {                 	
                    $searchproduct.=$product2['product_id'];
             }
            else
            {
                    $searchproduct.=",".$product2['product_id'];
            }

            $search++;
        }

        $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."' and similar_id IN (".$searchproduct.")");
        $rsallproducts = $query->rows;
        $num_rows = sizeof($rsallproducts);
    ?> 
    
    <div id="divproduct<?=$product['product_id']?>">
            <?php if ($product['thumb_swap']) { ?>
              <div class="image">
                  <a href="<?php echo $product['href']; ?>">
                     <img oversrc="<?php echo $product['thumb_swap']; ?>" src="<?php echo $product['thumb']; ?>" 
                     title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" style="border:none"/>
                  </a>
              </div>

              <?php } else {?>

              <div class="image">
                  <a href="<?php echo $product['href']; ?>">
                      <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" 
                      alt="<?php echo $product['name']; ?>" style="border:none"/>
                  </a>
              </div>

              <?php } ?>
      
      <?php
      if($num_rows>0) {  
		
        foreach($rsallproducts as $row) {            	                
            $rsprod = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
            $rsprod = $query->row;

            $rs=strspn($product['name'] ^ $rsprod['name'], "\0");
            break;
        }
        
        $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."' and similar_id IN (".$searchproduct.")");
        $rsallproducts = $query->rows;
        $num_rows = sizeof($rsallproducts);
       }
      ?>
      <?php if($num_rows>0) { ?>
      <div class="name"><a href="<?php echo $product['href']; ?>"><?=substr($product['name'],0,$rs);?></a></div>
      <?php } else { ?>
      <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
      <?php } ?>
      
      
      <div class="description"><?php echo $product['description']; ?></div>
      <?php if ($product['price']) { ?>
      <div class="price">
		
        
        <?php
        if($num_rows>0) {   
        	
        ?>
        <ul class="ultabonproductdisplay">
        	<li><a onclick="displayblocknone('divproduct<?=$product['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($product['name'],$rs);?></a></li>
        	<?php
            while ($row = mysql_fetch_array($rsallproducts)) 
       		{
            	$rsprod=mysql_query("select * from product_description where product_id='".$row['similar_id']."'");
                $rsprod=mysql_fetch_array($rsprod);                
                
                $rs=strspn($product['name'] ^ $rsprod['name'], "\0");
                
                $pname=substr($rsprod['name'],($rs-1),1);                
                
                if(is_numeric($pname) && $pname!=' ')
                {
                	$rs=$rs-2;
                	$pname=substr($rsprod['name'],$rs,1);                	
                }
                
            	//$rs=strspn($product['name'] ^ $rsprod['name'], "\0");
            ?>
            <li style="border:none"><a onclick="displayblocknone('divproduct<?=$product['product_id'].'_'.$rsprod['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($rsprod['name'],$rs);?></a></li>
            <?php } ?>
        </ul>
        <div class="clear"></div>
        <div style=" border-top:1px #BDBDBD solid;"></div>        
        <div style="padding-top:10px;"></div>        
        <?php } else {?>
                       <div style="padding-top:29px;"></div>
                        <?php } ?>
       

        <?php if (!$product['special']) { ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
        <span class="price-new"><?php echo $product['special']; ?></span>
        <span class="price-old"><?php echo $product['price']; ?></span>
        <span class="sale"><b><?php echo $product['saving']; ?>/-<br />Off</b></span>
        <?php } ?>
        <?php if ($product['mpn']) { ?>
        <div class="offer"><?php echo $product['mpn']; ?></div>
        <?php } ?>
        <?php if ($product['tax']) { ?>
        <br />
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
        <?php } ?>
      </div>
      <?php } ?>
      
      
      <div class="cart">
        <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" />
      </div>
      <div class="wishlist"><a class="wish tooltip" onclick="addToWishList('<?php echo $product['product_id']; ?>');"><span><?php echo $button_wishlist; ?></span></a></div>
      <div class="comparee"><a class="compare tooltip" onclick="addToCompare('<?php echo $product['product_id']; ?>');"><span><?php echo $button_compare; ?></span></a></div>
    </div>
    
    
    

    
    <?php  
    	$y=0; 	
     $search=0;
                $searchproduct="";
            	foreach ($products as $product2) {
                	            
                                if($search==0)
                                {                 	
                            		$searchproduct.=$product2['product_id'];
                                 }
                                else
                                {
                                	$searchproduct.=",".$product2['product_id'];
                                }
                                
                                $search++;
                            }
                  
                    $y=0; 	
                 $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."' and similar_id IN (".$searchproduct.")");
                 $rsallproducts1 = $query->rows;
                 $num_rows = sizeof($rsallproducts1);
                 
                if($num_rows>0) { 
                while ($row = mysql_fetch_array($rsallproducts1)) 
                {        	
                        $z=0;
                        foreach ($products as $product1)
                        {

                    if($row['similar_id']==$product1['product_id'])
                    {
            
    ?>
    	<div id="divproduct<?=$row['product_id'].'_'.$product1['product_id']?>" style="display:none;">
                <?php if ($product['thumb_swap']) { ?>
                  <div class="image">
                      <a href="<?php echo $product1['href']; ?>">
                         <img oversrc="<?php echo $product1['thumb_swap']; ?>" src="<?php echo $product1['thumb']; ?>" 
                         title="<?php echo $product1['name']; ?>" alt="<?php echo $product1['name']; ?>" style="border:none"/>
                      </a>
                  </div>
    
                  <?php } else {?>
    
                  <div class="image">
                      <a href="<?php echo $product1['href']; ?>">
                          <img src="<?php echo $product1['thumb']; ?>" title="<?php echo $product1['name']; ?>" 
                          alt="<?php echo $product1['name']; ?>" style="border:none"/>
                      </a>
                  </div>
    
                  <?php } ?>
                
          <div class="name"><a href="<?php echo $product1['href']; ?>"><?=substr($product['name'],0,$rs);?></a></div>
          
          <div class="description"><?php echo $product1['description']; ?></div>
          <?php if ($product1['price']) { ?>
          <div class="price">
    
    		
            <?php
            $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."' and similar_id IN (".$searchproduct.")");
            $rsallproducts = $query->rows;
            $num_rows = sizeof($rsallproducts);
            
            if($num_rows>0) {   
            ?>
            <ul class="ultabonproductdisplay">
                <li style="border:none"><a onclick="displayblocknone('divproduct<?=$product['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($product['name'],$rs);?></a></li>
                <?php
                foreach($rsallproducts as $row) 
                {                	
                    $query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
                    $rsprod = $query->row;                    
                    
                    $rs=strspn($product['name'] ^ $rsprod['name'], "\0");
                
                    $pname=substr($rsprod['name'],($rs-1),1);                
                    
                    if(is_numeric($pname) && $pname!=' ')
                    {
                        $rs=$rs-2;
                        $pname=substr($rsprod['name'],$rs,1);                	
                    }
                    
                    //$rs=strspn($product['name'] ^ $rsprod['name'], "\0");
                ?>
                <?php if($y==$z) {?>
                <li><a onclick="displayblocknone('divproduct<?=$product['product_id'].'_'.$rsprod['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($rsprod['name'],$rs);?></a></li>
                <?php } else { ?>
                <li style="border:none"><a onclick="displayblocknone('divproduct<?=$product['product_id'].'_'.$rsprod['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($rsprod['name'],$rs);?></a></li>
                <?php } ?>
                <?php $z++; 
                } ?>
            </ul>
            <div class="clear"></div>
            <div style="border-top:1px #BDBDBD solid;"></div>        
            <div style="padding-top:10px;"></div>        
            <?php } ?>
            
            <?php if (!$product1['special']) { ?>
            <?php echo $product1['price']; ?>
            <?php } else { ?>
            <span class="price-new"><?php echo $product1['special']; ?></span>
            <span class="price-old"><?php echo $product1['price']; ?></span>
            <span class="sale"><b><?php echo $product1['saving']; ?>/-<br />Off</b></span>
            <div class="offer">Buy 1 Get 1 Free</div>
            <?php } ?>
            <?php if ($product1['tax']) { ?>
            <br />
            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product1['tax']; ?></span>
            <?php } ?>
          </div>
          <?php } ?>
          
          
          <div class="cart">
            <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product1['product_id']; ?>');" class="button" />
          </div>
          <div class="wishlist"><a class="wish tooltip" onclick="addToWishList('<?php echo $product1['product_id']; ?>');"><span><?php echo $button_wishlist; ?></span></a></div>
          <div class="comparee"><a class="compare tooltip" onclick="addToCompare('<?php echo $product1['product_id']; ?>');"><span><?php echo $button_compare; ?></span></a></div>
        </div>
        
    <?php }
    	} 
        	$y++;
    		}                               
    	}
    ?>


    </div>
    
    
    <?php } ?>
    
    
    <?php 
    
    }
    ?>
    
  </div>
  
  
  
  <div class="pagination"><?php echo $pagination; ?></div>
  <!--<div class="pagination1"><?php echo $pagination; ?></div>-->
  
  <?php } ?>
  
  <?php if (!$categories && !$products) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } ?>
  <?php echo $content_bottom; ?>
  
</div>
<!--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>-->

<script><!--
function display(view) {
	if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');
		
		$('.product-list > div > div').each(function(index, element) {
			
			<!-- ********************** right ********************** -->
			html  = '<div class="right">';
			
			<!-- price -->
			var price = $(element).find('.price').html();
			
			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}
			<!--/price-->
			
			
			
			html += '  <div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			html += '  <div class="comparee">' + $(element).find('.comparee').html() + '</div>';
			html += '</div>';			
			
			
			<!-- ********************** left ********************** -->
			html += '<div class="left">';
			
			<!-- image -->
			var image = $(element).find('.image').html();
			
			if (image != null) { 
				html += '<div class="image">' + image + '</div>';
			}
			<!-- /image -->
			
			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';
			
			html += '</div>';

						
			$(element).html(html);
		});		
		
		$('.display').html('<b><?php echo $text_display; ?></b> <div class="display_list"><?php echo $text_list; ?></div> <div class="display_grid"><a  onclick="display(\'grid\');" title="<?php echo $text_grid; ?>"></a></div>');
		
		$.cookie('display', 'grid'); 
	} else {
		$('.product-list').attr('class', 'product-grid');
		
		$('.product-grid > div > div').each(function(index, element) {
			html = '';
			
			var image = $(element).find('.image').html();
			
			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}
			
			html += '<div class="name">' + $(element).find('.name').html() + '</div>';
			html += '<div class="description">' + $(element).find('.description').html() + '</div>';
			
			var price = $(element).find('.price').html();
			
			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}
						
			html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			html += '<div class="comparee">' + $(element).find('.comparee').html() + '</div>';
			
			$(element).html(html);
		});	
					
		$('.display').html('<b><?php echo $text_display; ?></b> <div class="display_list"><a onclick="display(\'list\');"  title="<?php echo $text_list; ?>"></a></div> <div class="display_grid"><?php echo $text_grid; ?></div>');
		
		$.cookie('display', 'grid');
                
	}
}

view = $.cookie('display');

if (view) {
	display(view);
} else {
	display('grid');
}

function displayblocknone(divtoshow,common)
{
	document.getElementById(common).style.display="none";
	var $eles = $("div[id^="+common+"]").css("display","none");
	document.getElementById(divtoshow).style.display="block";	
}
//--></script> 
<?php echo $footer; ?>