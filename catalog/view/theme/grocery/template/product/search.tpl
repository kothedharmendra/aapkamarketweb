<?php	

$this->load->model('account/customer');

if(isset($this->session->data['customer_id'])) { 
    $customer_info = $this->model_account_customer->getCustomer($this->session->data['customer_id']);
    if(isset($customer_info['customer_group_id'])) { 
        $login_customer_group = $customer_info['customer_group_id'];
    } else { 
        $login_customer_group = 0;
    }
} else { 
        $login_customer_group = 0;
}

$a = $this->db->query("select name from ".DB_PREFIX."customer_group_description where customer_group_id='".$login_customer_group."'")->row;

if(array_key_exists('name', $a)){
    $user_group = $a['name'];
}else{
    $user_group = '';
}

?>
<?php echo $header; ?>
<?php echo $column_left; ?>
<?php echo $column_right; ?>
<div class="wrap">
<script src="catalog/view/javascript/slides.min.jquery.js"></script>
	<div class="box-content">  
    <div class="box-product">
    
  <!--breadcrumb
    ==============================================================-->
    <div class="breadcrumb"> 
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
	
    <?php echo $content_top; ?>
  
  <?php if ($products) { ?>
      <div class="product-filter">    
        <div class="sort" style="margin-left:15px;"><b><?php echo $text_sort; ?></b>
          <select onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php }//foreach sort  ?>
          </select>
        </div>    
        <div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
      </div>
  
      <?php if(sizeof($keywords) > 1){ 
                require_once 'search_by_list.php';
            }else{
                require_once 'search_by_name.php';
            }
      ?>
  </div>
  </div>
  
  
  <?php } else { //if no products ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php }?>
  <?php echo $content_bottom; ?>
</div>
<script type="text/javascript"><!--
$('#content input[name=\'filter_name\']').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';
	
	var filter_name = $('#content input[name=\'filter_name\']').attr('value');
	
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_category_id = $('#content select[name=\'filter_category_id\']').attr('value');
	
	if (filter_category_id > 0) {
		url += '&filter_category_id=' + encodeURIComponent(filter_category_id);
	}
	
	var filter_sub_category = $('#content input[name=\'filter_sub_category\']:checked').attr('value');
	
	if (filter_sub_category) {
		url += '&filter_sub_category=true';
	}
		
	var filter_description = $('#content input[name=\'filter_description\']:checked').attr('value');
	
	if (filter_description) {
		url += '&filter_description=true';
	}

	location = url;
});

function display(view) {
    $('.product-list').attr('class', 'product-grid');

    $('.product-grid > div > div').each(function(index, element) {
            html = '';

            var image = $(element).find('.image').html();

            if (image != null) {
                    html += '<div class="image">' + image + '</div>';
            }

            html += '<div class="name">' + $(element).find('.name').html() + '</div>';
            html += '<div class="description">' + $(element).find('.description').html() + '</div>';

            var price = $(element).find('.price').html();

            if (price != null) {
                    html += '<div class="price">' + price  + '</div>';
            }


            html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';
            html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
            html += '<div class="comparee">' + $(element).find('.comparee').html() + '</div>';

            $(element).html(html);
    });	

    $('.display').html('<b><?php echo $text_display; ?></b> <div class="display_list"><a onclick="display(\'list\');"  title="<?php echo $text_list; ?>"></a></div> <div class="display_grid"><?php echo $text_grid; ?></div>');

    $.cookie('display', 'grid');
}

view = $.cookie('display');

if (view) {
	display(view);
} else {
	display('grid');
}
 
function displayblocknone(divtoshow,common)
{      
    $("div[id^="+common+"]").css("display","none");    
    $('.'+divtoshow).css('display','block');
}

function plusquantityw(productid)
{
	/*maximum=document.getElementById('maximum'+productid).value;
	qty=parseInt(document.getElementById('quantity'+productid).value)+parseInt(1);
	customerid=<?php echo $login_customer_group;?>;
	
	if(maximum!=0 && qty==maximum && customerid!=2) { 
		document.getElementById('quantity'+productid).value = parseInt(document.getElementById('quantity'+productid).value)+parseInt(1);	
		updateToCart(productid,document.getElementById('quantity'+productid).value);
		document.getElementById('changemplus'+productid).innerHTML="";
	} else if(maximum!=0 && qty>maximum && customerid!=2) { 
	
	} else { */
		document.getElementById('quantity'+productid).value = parseInt(document.getElementById('quantity'+productid).value)+parseInt(1);	
		updateToCart(productid,document.getElementById('quantity'+productid).value);
	//}
}

function minusquantityw(productid)
{
	document.getElementById('quantity'+productid).value = parseInt(document.getElementById('quantity'+productid).value)-parseInt(1);
	updateToCart(productid,document.getElementById('quantity'+productid).value);
	/*
	maximum=document.getElementById('maximum'+productid).value;
	qty=document.getElementById('quantity'+productid).value;
	customerid=<?php echo $login_customer_group;?>;
	
	if(maximum!=0 && qty<maximum) { 
		document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantityw("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"'/>";
	}*/
	
	if(document.getElementById('quantity'+productid).value==0)
	{
		document.getElementById('shqtydiv'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantityw1("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantityw1("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
	}
}


function callupdate(productid)
{
	//alert(document.getElementById('quantity'+productid).value);
	updateToCart(productid,document.getElementById('quantity'+productid).value);
	if(document.getElementById('quantity'+productid).value==0)
	{
		document.getElementById('shqtydiv'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
	}
}



function plusquantity(productid)
{
	document.getElementById('quantity'+productid).value = parseInt(document.getElementById('quantity'+productid).value)+parseInt(1);	
	updateToCart(productid,document.getElementById('quantity'+productid).value);
}
function minusquantity(productid)
{
	document.getElementById('quantity'+productid).value = parseInt(document.getElementById('quantity'+productid).value)-parseInt(1);
	updateToCart(productid,document.getElementById('quantity'+productid).value);
	if(document.getElementById('quantity'+productid).value==0)
	{
		document.getElementById('shqtydiv'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
	}
}

function chkzerominus(productid,e)
{
	var keynum;

            if(window.event){ // IE					
            	keynum = e.keyCode;
            }else
                if(e.which){ // Netscape/Firefox/Opera					
            		keynum = e.which;
                 }
           // alert(String.fromCharCode(keynum));
		   if(String.fromCharCode(keynum)=='m')
		   {
			   updateToCart(productid,"0");
			   
			   document.getElementById('shqtydiv'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1("+productid+")'>Add&nbsp;&nbsp;</a>";
			   document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
			   
		   }
}

function plusquantity1(productid)
{
	document.getElementById('shqtydiv'+productid).innerHTML="<div class='productplus'><img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg' onclick='minusquantity("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg\"'/></div><div class='quantityinput'><input class='order_qua' type='text' name='quantity"+productid+"' id='quantity"+productid+"' value='0' onchange='callupdate("+productid+")' onkeyup='chkzerominus("+productid+",event)'/></div>";	
	
	document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"'/>";
	
	document.getElementById('quantity'+productid).value = parseInt(document.getElementById('quantity'+productid).value)+parseInt(1);
	
	updateToCart(productid,document.getElementById('quantity'+productid).value);
}

</script>

<?php echo $footer; ?>