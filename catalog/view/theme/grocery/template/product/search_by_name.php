
  <?php if ($products) {
  $allproductsid[]=array();
  $similarproducts=array();
  $removeproducts=array();
  $i=0;$j=0;$k=0;
  
  foreach ($products as $product)
  {
  	$allproductsid[$i]=$product['product_id'];
  	$i++;
  }
  
  $l=0;
  
  for($i=0;$i<count($allproductsid);$i++)
  {
      if(!in_array($allproductsid[$i],$removeproducts)){
            $query = $this->db->query("select * from product_similar where product_id='".$allproductsid[$i]."'");
            $rsallproducts = $query->rows;
            $num_rows = sizeof($rsallproducts);
            if ($num_rows > 0)
            {
                $similarproducts[$j]=$allproductsid[$i];
                $j++;

                foreach($rsallproducts as $row) {
                        $removeproducts[$l]=$row['similar_id'];
                        $l++;
                }
            }
      }
  } 
  
  $m=0;
  ?>  
    
  
  <!--Managed from js in the bottom of this file (go down)-->
  <div class="product-list">
  
    <?php foreach ($products as $product) {     
    ?>
    
    
    <?php
    	$flag=0;
        
    	for($k=0;$k<count($removeproducts);$k++)
        {
        	if($product['product_id']==$removeproducts[$k])
            {
            	$flag=1;
            }	
        }
    	if($flag!=1)
        {
    ?> 
    
    <div class="item">     
    
    <?php    	
        $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."'");
        $rsallproducts = $query->rows;        
        $num_rows = sizeof($rsallproducts);
        
    ?> 
    
        <div id="divproduct<?=$product['product_id']?>" class="divproduct<?=$product['product_id']?>">
            <?php if ($product['thumb_swap']) { ?>
              <div class="image">
                  <a href="<?php echo $product['href']; ?>">
                     <img oversrc="<?php echo $product['thumb_swap']; ?>" src="<?php echo $product['thumb']; ?>" 
                     title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" style="border:none"/>
                  </a>
              </div>

              <?php } else {?>

              <div class="image">
                  <a href="<?php echo $product['href']; ?>">
                      <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" 
                      alt="<?php echo $product['name']; ?>" style="border:none"/>
                  </a>
              </div>

              <?php } ?>
      
      <?php
      if($num_rows>0) {  
		
      foreach($rsallproducts as $row) {            	                
            $query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
            $rsprod = $query->row;

            $rs=strspn($product['name'] ^ $rsprod['name'], "\0");
            break;
        }
       }
      ?>
            
      <?php if($num_rows>0 && $rs) { ?>
      <div class="name"><a href="<?php echo $product['href']; ?>"><?=substr($product['name'],0,$rs);?> </a></div>
      <?php } else { ?>
      <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?> </a></div>
      <?php } ?>
      
      
      <div class="description"><?php echo $product['description']; ?></div>
      <?php if ($product['price']) { ?>
      <div class="price">
		
        
        <?php
        if($num_rows>0 && $rs) {   
        	
        ?>
        <ul class="ultabonproductdisplay">
        	<li><a onclick="displayblocknone('divproduct<?=$product['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($product['name'],$rs);?></a></li>
        	<?php
            foreach($rsallproducts as $row) {
            
            	$query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
                $rsprod = $query->row;
                
                $rs=strspn($product['name'] ^ $rsprod['name'], "\0");
                
                $pname=substr($rsprod['name'],($rs-1),1);                
                
                if(is_numeric($pname) && $pname!=' ')
                {
                	$rs=$rs-2;
                	$pname=substr($rsprod['name'],$rs,1);                	
                }
            ?>
            <li style="border:none">
                <a onclick="displayblocknone('divproduct<?=$product['product_id'].'_'.$rsprod['product_id']?>','divproduct<?=$product['product_id']?>')">
                    <?=substr($rsprod['name'],$rs);?>
                </a>
            </li>
            <?php } ?>
        </ul>
        <div class="clear"></div>
        <div style=" border-top:1px #BDBDBD solid;"></div>        
        <div style="padding-top:10px;"></div>        
        <?php } else {?>
       <div style="padding-top:29px;"></div>
		<?php } ?>
        
        <?php if (!$product['special']) { ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
        <span class="price-new"><?php echo $product['special']; ?></span>
        <span class="price-old"><?php echo $product['price']; ?></span>
        <span class="sale"><b><?php echo $product['saving']; ?>/-<br />Off</b></span>
        <!--<div class="offer">Buy 1 Get 1 Free</div>-->
        <?php } ?>
        <?php if ($product['mpn']) { ?>
        <div class="offer"><?php echo $product['mpn']; ?></div>
        <?php } ?>
        <?php if ($product['tax']) { ?>
        <br />
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
        <?php } ?>
      </div>
      <?php } ?>
      
      
      <div class="cart">
        
        <?php
        $cartproducts = $this->cart->getProducts();
        ?>
        
        <?php 
        $cflag=0;$cquantity=0;
        foreach($cartproducts as $cproduct) {
            if($cproduct['product_id']==$product['product_id'])
            { 
            	$cquantity=$cproduct['quantity'];
                $cflag=1;
            }
        }
		?>
        
        
        <?php  if($cquantity!=0) { ?>
        
        <div class="floatleft" id="shqtydiv<?php echo $product['product_id']; ?>">
        <div class="productplus"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg" onclick="minusquantity('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg'"/></div> 

        <div class="quantityinput"><input class="order_qua" type="text" name="quantity<?php echo $product['product_id']; ?>" id="quantity<?php echo $product['product_id']; ?>" value="<?=$cquantity?>" onchange="callupdate('<?php echo $product['product_id']; ?>')" onkeyup="chkzerominus('<?php echo $product['product_id']; ?>',event)"/></div>
        </div>
        
        <div class="productminus" id="changemplus<?php echo $product['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
        
        <div class="clear"></div>
        
        <?php } else {
       
        if($product['quantity'] > 0 || $user_group == 'Wholesaler'){  ?>
         
        <div class="floatleft" id="shqtydiv<?php echo $product['product_id']; ?>">
        <a class="addquantity" href="javascript:plusquantity1('<?php echo $product['product_id']; ?>')">Add&nbsp;&nbsp;</a>
        </div>
        
        <div class="productminus" id="changemplus<?php echo $product['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity1('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
        
       <?php }else{ ?>

        <img class="out_of_stock" src="<?= HTTP_SERVER ?>image/data/assets/no_stock.png" />

        <?php } ?>
        
        <div class="clear"></div>
        
        <?php } ?>
        
        
 
      </div>
      
      <div class="wishlist"><a class="wish tooltip" onclick="addToWishList('<?php echo $product['product_id']; ?>');"><span><?php echo $button_wishlist; ?></span></a></div> 
      <div class="comparee"><a class="compare tooltip" onclick="addToCompare('<?php echo $product['product_id']; ?>');"><span><?php echo $button_compare; ?></span></a></div>
         
        
      <div class="clear"></div>
    </div>
    
    <?php 
    $y=0; 

     if($num_rows>0) { 
            
     foreach($rsallproducts as $rsallproduct) {  
         $z=0;
          $product1 = $products[$rsallproduct['similar_id']];
          
        ?>
        <div class="divproduct<?=$row['product_id'].'_'.$product1['product_id']?>" id="divproduct<?=$row['product_id'].'_'.$product1['product_id']?>" style="display:none;">
                <?php if ($product['thumb_swap']) { ?>
                  <div class="image">
                      <a href="<?php echo $product1['href']; ?>">
                         <img oversrc="<?php echo $product1['thumb_swap']; ?>" src="<?php echo $product1['thumb']; ?>" 
                         title="<?php echo $product1['name']; ?>" alt="<?php echo $product1['name']; ?>" style="border:none"/>
                      </a>
                  </div>
    
                  <?php } else {?>
    
                  <div class="image">
                      <a href="<?php echo $product1['href']; ?>">
                          <img src="<?php echo $product1['thumb']; ?>" title="<?php echo $product1['name']; ?>" 
                          alt="<?php echo $product1['name']; ?>" style="border:none"/>
                      </a>
                  </div>
    
                  <?php } ?>
                
          <div class="name">
              <a href="<?php echo $product1['href']; ?>">
                <?=substr($product['name'],0,$rs);?>
              </a>
          </div>
          
          <div class="description"><?php echo $product1['description']; ?></div>
          <?php if ($product1['price']) { ?>
          <div class="price">
    
    		
            <?php if($num_rows>0) {   ?>
            <ul class="ultabonproductdisplay">
                <li style="border:none"><a onclick="displayblocknone('divproduct<?=$product['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($product['name'],$rs);?></a></li>
                <?php
                foreach($rsallproducts as $row) 
                {            
                    $query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
                    $rsprod = $query->row;
                    
                    $rs=strspn($product['name'] ^ $rsprod['name'], "\0");
                
                    $pname=substr($rsprod['name'],($rs-1),1);                
                    
                    if(is_numeric($pname) && $pname!=' ')
                    {
                        $rs=$rs-2;
                        $pname=substr($rsprod['name'],$rs,1);                	
                    }
                ?>
                <?php if($y==$z) {?>
                <li><a onclick="displayblocknone('divproduct<?=$product['product_id'].'_'.$rsprod['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($rsprod['name'],$rs);?></a></li>
                <?php } else { ?>
                <li style="border:none"><a onclick="displayblocknone('divproduct<?=$product['product_id'].'_'.$rsprod['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($rsprod['name'],$rs);?></a></li>
                <?php } ?>
                <?php $z++; 
                } ?>
            </ul>
            <div class="clear"></div>
            <div style="border-top:1px #BDBDBD solid;"></div>        
            <div style="padding-top:10px;"></div>        
            <?php } else {?>
           <div style="padding-top:29px;"></div>
            <?php } ?>
            
            <?php if (!$product1['special']) { ?>
            <?php echo $product1['price']; ?>
            <?php } else { ?>
            <span class="price-new"><?php echo $product1['special']; ?></span>
            <span class="price-old"><?php echo $product1['price']; ?></span>
            <span class="sale"><b><?php echo $product1['saving']; ?>/-<br />Off</b></span>
            <div class="offer">Buy 1 Get 1 Free</div>
            <?php } ?>
            <?php if ($product1['tax']) { ?>
            <br />
            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product1['tax']; ?></span>
            <?php } ?>
          </div>
          <?php } ?>
          
          <div class="cart">
        
            <?php
            $cartproducts = $this->cart->getProducts();
            ?>
            
            <?php 
            $cflag=0;$cquantity=0;
            foreach($cartproducts as $cproduct) {
                if($cproduct['product_id']==$product1['product_id'])
                { 
                    $cquantity=$cproduct['quantity'];
                    $cflag=1;
                }
            }
            ?>
            
            
            <?php  if($cquantity!=0) { ?>
            
            <div class="floatleft" id="shqtydiv<?php echo $product1['product_id']; ?>">
            <div class="productplus"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg" onclick="minusquantity('<?php echo $product1['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg'"/></div> 
    
            <div class="quantityinput"><input class="order_qua" type="text" name="quantity<?php echo $product1['product_id']; ?>" id="quantity<?php echo $product1['product_id']; ?>" value="<?=$cquantity?>" onchange="callupdate('<?php echo $product1['product_id']; ?>')" onkeyup="chkzerominus('<?php echo $product1['product_id']; ?>',event)"/></div>
            </div>
            
            <div class="productminus" id="changemplus<?php echo $product1['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity('<?php echo $product1['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
            
            <div class="clear"></div>
            
            <?php } else {
            
            if($product1['quantity'] > 0 || $user_group == 'Wholesaler'){ ?>
            
            <div class="floatleft" id="shqtydiv<?php echo $product1['product_id']; ?>">
                <a class="addquantity" href="javascript:plusquantity1('<?php echo $product1['product_id']; ?>')">Add&nbsp;&nbsp;</a>
            </div>
            
            <div class="productminus" id="changemplus<?php echo $product1['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity1('<?php echo $product1['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
            
            <?php }else{ ?>

                <img class="out_of_stock" src="<?= HTTP_SERVER ?>image/data/assets/no_stock.png" />

            <?php } ?>

            <div class="clear"></div>
            
            <?php } ?>
            
            
     
          </div>
          
          <div class="wishlist"><a class="wish tooltip" onclick="addToWishList('<?php echo $product1['product_id']; ?>');"><span><?php echo $button_wishlist; ?></span></a></div> 
          <div class="comparee"><a class="compare tooltip" onclick="addToCompare('<?php echo $product1['product_id']; ?>');"><span><?php echo $button_compare; ?></span></a></div>
             
            
          <div class="clear"></div>
          
          
          
          
        </div>
        
    <?php 
        	$y++;
    		}                               
    	}
    ?>


    </div>
    
    
    <?php } ?>
    
    
    <?php 
    
    }
    ?>
    
  </div>
  
  <div class="pagination" style="display: none"><?php echo $pagination; ?></div>
  <div class="loading_wrapper"></div>
  
  <?php } ?>
  