        <div id="feedback-body">
            <div id="feedback-body-head"><?php echo $feedback['heading']; ?></div>
            <div id="feedback-body-forma">
                <form action="" id="feedback-form">
                    <input type="text" hidden name="getid" value="<?php echo $getid?>" style="display:none;"/>
                    <?php foreach ($feedback['filds'] as $key=>$field) { ?>
                        <?php if(!empty($field['onlytext'])) { ?>
                            <div id="feedback-body-text"><?=$field['fieldname']; ?></div> <div id="feedback_<?=$key?>" style="display:inline;"></div>
                            <br />
                        <?php } elseif (!empty($field['mask'])) { ?>
                            <?php if (!empty($field['requaried'])) { ?>
                            <input type="text" name="feedback[<?=$key?>]" id="feedback_<?=$key?>" placeholder="<?=$field['fieldname']; ?>" class="feedback-body-forma-inputreq"  maxlength="34"/>
                            <br />
                            <?php } else { ?>
                            <input type="text" name="feedback[<?=$key?>]" id="feedback_<?=$key?>" placeholder="<?=$field['fieldname']; ?>" class="feedback-body-forma-input"  maxlength="34"/>
                            <br />
                            <?php } ?>
                            <script type="text/javascript">
                                var key = '<?php echo $key; ?>';
                                $('#feedback_'+key).mask('<?php echo $field["mask"]; ?>');
                            </script>
                        <?php } elseif (!empty($field['comment'])) { ?>
                            <?php if (!empty($field['requaried'])) { ?>
                            <textarea type="text" name="feedback[<?=$key?>]" id="feedback_<?=$key?>" placeholder="<?=$field['fieldname']; ?>" class="feedback-body-forma-inputreq"></textarea>
                            <br />
                            <?php } else { ?>
                            <textarea type="text" name="feedback[<?=$key?>]" id="feedback_<?=$key?>" placeholder="<?=$field['fieldname']; ?>" class="feedback-body-forma-input"></textarea>
                            <br />
                            <?php } ?>
                        <?php } elseif (!empty($field['time'])) { ?>
                            <?php if (!empty($field['requaried'])) { ?>
                            <input type="text" name="feedback[<?=$key?>]" id="feedback_<?=$key?>" placeholder="<?=$field['fieldname']; ?>" class="time feedback-body-forma-inputreq"  maxlength="34"/>
                            <br />
                            <?php } else { ?>
                            <input type="text" name="feedback[<?=$key?>]" id="feedback_<?=$key?>" placeholder="<?=$field['fieldname']; ?>" class="time feedback-body-forma-input"  maxlength="34"/>
                            <br />
                            <?php } ?>
                            <script type="text/javascript">$('.time').datetimepicker();</script>
                        <?php } elseif (!empty($field['requaried'])) { ?>
                            <input type="text" name="feedback[<?=$key?>]" id="feedback_<?=$key?>" placeholder="<?=$field['fieldname']; ?>" class="feedback-body-forma-inputreq"  maxlength="34"/>
                            <br />
                        <?php } else { ?>
                            <input type="text" name="feedback[<?=$key?>]" id="feedback_<?=$key?>" placeholder="<?=$field['fieldname']; ?>" class="feedback-body-forma-input"  maxlength="34"/>
                            <br />
                        <?php } ?>
                    <?php } ?>
                    
                    <div style="width:94%;text-align:center;margin:10px auto 20px auto;"><a onclick="FeedbackProSend();" id="feedbackprosendform">Submit</a></div>
                </form>
            </div>
            <div id="feedback-result"></div>
        </div>

<script type="text/javascript">
function FeedbackProSend() {
    $.ajax({
        type: "POST",
        url: "/index.php?route=module/feedbackpro/send",
        data: $('#feedback-form input[type=\'text\'], #feedback-form textarea'),
        dataType: 'json',
        success: function(json) {          
            if (json['error']) {
                $('.falseerror').remove();
                $.each(json['error'], function(i, val) {
                    $("#feedback_" + i).after('<span id="requiredName" class="falseerror">Вы не ввели '+val+'</span>');
                    $(this).colorbox.resize();
                });
            }    
            if (json['success']) {
                $('#feedback-body-forma').remove();
                $("#feedback-result").show();
                $("#feedback-result").html('<span>'+ json['success'] + '</span>');
                $(this).colorbox.resize();
            }  
        }
    });
}
</script>
<script type="text/javascript">
function hasPlaceholderSupport() {
  var input = document.createElement('input');
  return ('placeholder' in input);
}

if(!hasPlaceholderSupport()){
    var inputs = $('input');
    for(var i=0,  count = inputs.length;i<count;i++){
        if(inputs[i].getAttribute('placeholder')){
            inputs[i].style.cssText = "color:#777;"
            inputs[i].value = inputs[i].getAttribute("placeholder");
            inputs[i].onclick = function(){
                if(this.value == this.getAttribute("placeholder")){
                    this.value = '';
                    this.style.cssText = "color:#777;"
                }
            }
            inputs[i].onblur = function(){
                if(this.value == ''){
                    this.value = this.getAttribute("placeholder");
                    this.style.cssText = "color:#777;"
                }
            }
        }
    }
}
</script>