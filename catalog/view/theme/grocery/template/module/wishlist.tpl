<?php	$this->load->model('account/customer');
        if(isset($this->session->data['customer_id'])) { 
            $customer_info = $this->model_account_customer->getCustomer($this->session->data['customer_id']);
            if(isset($customer_info['customer_group_id'])) { 
                $login_customer_group = $customer_info['customer_group_id'];
            } else { 
                $login_customer_group = 0;
            }
        } else { 
        		$login_customer_group = 0;
        }
?>

<?php if ($products) { ?>
<div class="box featured">
  <div class="box-heading"><?php echo $heading_title; ?></span>
    <span id="wp-next" class="next"></span>
    <span id="wp-prev" class="prev"></span>    
  </div>
  <div class="box-content">  
    <div class="box-product">
    
    <div id="slides">
        <div class="slides_container wishlist-wrapper">              
              <?php
                $allproductsid[]=array();
                $similarproducts=array();
                $removeproducts=array();
                $i=0;$j=0;$k=0;
                $productcount=0;
                foreach ($products as $product)
                {
                  $allproductsid[$i]=$product['product_id'];
                  $i++; $productcount++;
                }
                
                $l=0;
                
                for($i=0;$i<count($allproductsid);$i++)
                {
                  if(!in_array($allproductsid[$i],$removeproducts)){
                      $query = $this->db->query("select * from product_similar where product_id='".$allproductsid[$i]."'");
                      $rsallproducts = $query->rows;
                      $num_rows = sizeof($rsallproducts);
                      if ($num_rows > 0)
                      {
                          $similarproducts[$j]=$allproductsid[$i];
                          $j++;

                          foreach($rsallproducts as $row) {
                              $removeproducts[$l]=$row['similar_id'];
                              $l++;
                          }//while
                      }//if                  
                  }
                } //end for
                $m=0;
                ?> 
              <ul class="bxslider list_carousel">   
                    
                    <?php $i=0; $pcount=0;
                    foreach ($products as $product) { ?>              
                          
                    
                  
                    <?php
                          $flag=0;
                          for($k=0;$k<count($removeproducts);$k++)
                          {
                              if($product['product_id']==$removeproducts[$k])
                              {
                                  $flag=1; $pcount++;
                              }	
                          }//end of for 
                          
                            if($flag!=1)
                            {  $i++; $pcount++;  
                        ?> 
                        
                              <?php                                 
                                  $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."'");
                                  $rsallproducts = $query->rows;
                                  $num_rows = sizeof($rsallproducts);
                              ?>
                              <li>  
                                  <div id="divproduct<?php echo $product['product_id'];?>" class="main divproduct<?php echo $product['product_id'];?>">
                                      <!-- image //
                                      =============-->
                                      <!--for swap image-->
                                      <?php if ($product['thumb_swap']) { ?>
                                        <div class="image">
                                            <a href="<?php echo $product['href']; ?>">
                                               <img oversrc="<?php echo $product['thumb_swap']; ?>" src="<?php echo $product['thumb']; ?>" 
                                               title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" style="border:none"/>
                                            </a>
                                        </div>
                          
                                        <?php } else {?>
                          
                                        <div class="image">
                                            <a href="<?php echo $product['href']; ?>">
                                                <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" 
                                                alt="<?php echo $product['name']; ?>" style="border:none"/>
                                            </a>
                                        </div>
                          
                                        <?php } ?>
                                      <!--/ swap img-->                              
                                      
                                      <?php
                                        if($num_rows>0) {  
                                          
                                        foreach($rsallproducts as $row) {            	                
                                          $query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
                                          $rsprod = $query->row;

                                          $rs=strspn($product['name'] ^ $rsprod['name'], "\0");
                                          break;
                                        }
                                              
                                              $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."'");
                                              $rsallproducts = $query->rows;
                                              $num_rows = sizeof($rsallproducts);
                                        }
                                        ?>
                                        <?php if($num_rows>0) { ?>
                                        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo substr($product['name'],0,$rs);?></a></div>
                                        <?php } else { ?>
                                        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                                      <?php } ?>
                                      
                                      <?php if ($product['price']) { ?>
                                      <div class="price">                              
                                          <?php
                                          if($num_rows>0) {
                                          ?>
                                          <ul class="ultabonproductdisplay">
                                              <li><a onclick="displayblocknone_wl('divproduct<?php echo $product['product_id'];?>','divproduct<?php echo $product['product_id'];?>')"><?php echo substr($product['name'],$rs);?></a></li>
                                              <?php
                                              foreach($rsallproducts as $row) 
                                              {
                                              
                                                  if(!array_key_exists($row['similar_id'],$products)) 
                                                        continue;
                                                        
                                                  $query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
                                                  $rsprod = $query->row;
                                                  
                                                  $rs=strspn($product['name'] ^ $rsprod['name'], "\0");
                                  
                                                  $pname=substr($rsprod['name'],($rs-1),1);                
                                                  
                                                  if(is_numeric($pname) && $pname!=' ')
                                                  {
                                                      $rs=$rs-2;
                                                      $pname=substr($rsprod['name'],$rs,1);                	
                                                  }
                                              ?>
                                              
                                              <li style="border:none"><a onclick="displayblocknone_wl('divproduct<?php echo $product['product_id'].'_'.$rsprod['product_id'];?>','divproduct<?php echo $product['product_id'];?>')"><?php echo substr($rsprod['name'],$rs);?></a></li>
                                              <?php } ?>
                                          </ul>                                  
                                          <div class="clear"></div>
                                          <div style=" border-top:1px #BDBDBD solid;"></div>        
                                          <div style="padding-top:10px;"></div>        
                                          <?php } else {?>
                                         <div style="padding-top:29px;"></div>
                                          <?php } ?>
                                      
                                        <?php if (!$product['special']) { ?>
                                        <?php echo $product['price']; ?>
                                        <?php } else { ?>
                                        <span class="price-old"><?php echo $product['price']; ?></span> 
                                        <span class="price-new"><?php echo $product['special']; ?></span>
                                        <span class="sale"><b><?php echo $product['saving']; ?>/-<br />Off</b></span>
                                        <?php } ?>
                                      </div>
                                      <?php } ?>
                                      
                                      <div class="cart">                  
                                      <?php
                                      $cartproducts = $this->cart->getProducts();
                                      ?>                              
                                      <?php 
                                      $cflag=0;$cquantity=0;
                                      foreach($cartproducts as $cproduct) {
                                          if($cproduct['product_id']==$product['product_id'])
                                          { 
                                              $cquantity=$cproduct['quantity'];
                                              $cflag=1;
                                          }
                                      }
                                      ?>
                                      
                                      
                                      <?php  if($cquantity!=0) { ?>
                                      
                                      <div class="floatleft" id="shqtydiv<?php echo $product['product_id']; ?>">
                                      <div class="productplus"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg" onclick="minusquantity_wl('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg'"/></div> 
                              
                                      <div class="quantityinput"><input class="order_qua" type="text" name="quantity<?php echo $product['product_id']; ?>" id="quantity<?php echo $product['product_id']; ?>" value="<?php echo $cquantity;?>" onchange="callupdate_wl('<?php echo $product['product_id']; ?>')" onkeyup="chkzerominus_wl('<?php echo $product['product_id']; ?>',event)"/></div>
                                      </div>
                                      
                                      <div class="productminus" id="changemplus<?php echo $product['product_id']; ?>">
                      <?php //if($cquantity<$product['maximum'] && $login_customer_group!=2) { ?>
                                      <img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity_wl('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/>
                       <?php /*} else if($login_customer_group==2){ ?>         
                      				 <img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity_wl('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/>
                      <?php }*/ ?>       
                                      </div>
                                      
                                      <div class="clear"></div>                              
                                      <?php } else {?>                              
                                      <div class="floatleft" id="shqtydiv<?php echo $product['product_id']; ?>">
                                      <a class="addquantity" href="javascript:plusquantity_wl('<?php echo $product['product_id']; ?>')">Add&nbsp;&nbsp;</a>
                                      </div>
                                      
                                      <div class="productminus" id="changemplus<?php echo $product['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity_wl('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
                                      
                                      <div class="clear"></div>                              
                                      <?php } ?>
                                    </div>
                                    
                                    <div class="wishlist"><a class="wish tooltip" onclick="addToWishList('<?php echo $product['product_id']; ?>');"><span><?php echo $button_wishlist; ?></span></a></div>    
                                    <div class="comparee"><a class="compare tooltip" onclick="addToCompare('<?php echo $product['product_id']; ?>');"><span><?php echo $button_compare; ?></span></a></div>
                                    <div class="clear"></div>
                                      
                                  </div><!--/main-->
                                  
                                  <?php                                

                                  $y=0; 	
                                      
                                   $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."'");
                                   $rsallproducts1 = $query->rows;             
                                   $num_rows = sizeof($rsallproducts1);
                                   
                                      if($num_rows>0) { 
                                      foreach($rsallproducts1 as $row) {
                                            if(array_key_exists($row['similar_id'],$products))
                                                $product1 = $products[$rsallproduct['similar_id']];
                                            else
                                                continue;
                                      
                                                $z=0;
                                  ?>
                                  
                                  <div id="divproduct<?php echo $row['product_id'].'_'.$product1['product_id'];?>" style="display:none;" class="divproduct<?php echo $row['product_id'].'_'.$product1['product_id'];?>">
                                      <?php if ($product['thumb_swap']) { ?>
                                        <div class="image">
                                            <a href="<?php echo $product1['href']; ?>">
                                               <img oversrc="<?php echo $product1['thumb_swap']; ?>" src="<?php echo $product1['thumb']; ?>" 
                                               title="<?php echo $product1['name']; ?>" alt="<?php echo $product1['name']; ?>" style="border:none"/>
                                            </a>
                                        </div>
                          
                                        <?php } else {?>
                          
                                        <div class="image">
                                            <a href="<?php echo $product1['href']; ?>">
                                                <img src="<?php echo $product1['thumb']; ?>" title="<?php echo $product1['name']; ?>" 
                                                alt="<?php echo $product1['name']; ?>" style="border:none"/>
                                            </a>
                                        </div>
                          
                                        <?php } ?>
                                  
                                        <div class="name"><a href="<?php echo $product1['href']; ?>"><?php echo substr($product['name'],0,$rs);?></a></div>
                                        
                                        <?php if ($product1['price']) { ?>
                                        <div class="price">
                                  
                                          
                                          <?php
                                          $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."' and similar_id IN (".$searchproduct.")");
                                          $rsallproducts = $query->rows;
                                          $num_rows = sizeof($rsallproducts);                        
                                          
                                          
                                          if($num_rows>0) {   
                                          ?>
                                          <ul class="ultabonproductdisplay">
                                              <li style="border:none"><a onclick="displayblocknone_wl('divproduct<?php echo $product['product_id'];?>','divproduct<?php echo $product['product_id'];?>')"><?php echo substr($product['name'],$rs);?></a></li>
                                              <?php
                                              foreach($rsallproducts as $row) 
                                              {        
                                                  if(!array_key_exists($row['similar_id'],$products)) 
                                                        continue;
                                                        
                                                  $query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
                                                  $rsprod = $query->row;
                                                  
                                                  $rs=strspn($product['name'] ^ $rsprod['name'], "\0");
                                              ?>
                                              <?php if($y==$z) {?>
                                              <li><a onclick="displayblocknone_wl('divproduct<?php echo $product['product_id'].'_'.$rsprod['product_id'];?>','divproduct<?php echo $product['product_id'];?>')"><?php echo substr($rsprod['name'],$rs);?></a></li>
                                              <?php } else { ?>
                                              <li style="border:none"><a onclick="displayblocknone_wl('divproduct<?php echo $product['product_id'].'_'.$rsprod['product_id'];?>','divproduct<?php echo $product['product_id'];?>')"><?php echo substr($rsprod['name'],$rs);?></a></li>
                                              <?php } ?>
                                              <?php $z++; 
                                              } ?>
                                          </ul>
                                          <div class="clear"></div>
                                          <div style="border-top:1px #BDBDBD solid;"></div>        
                                          <div style="padding-top:10px;"></div>        
                                          <?php } ?>
                                  
                                          <?php if (!$product1['special']) { ?>
                                          <?php echo $product1['price']; ?>
                                          <?php } else { ?>
                                          <span class="price-new"><?php echo $product1['special']; ?></span>
                                          <span class="price-old"><?php echo $product1['price']; ?></span>
                                          <span class="sale"><b><?php echo $product1['saving']; ?>/-<br />Off</b></span>
                                          <?php } ?>                        
                                        </div>
                                        <?php } ?>
                                        
                                        
                                        <div class="cart">                  
                                  <?php
                                  $cartproducts = $this->cart->getProducts();
                                  ?>
                                  
                                  <?php 
                                  $cflag=0;$cquantity=0;
                                  foreach($cartproducts as $cproduct) {
                                      if($cproduct['product_id']==$product1['product_id'])
                                      { 
                                          $cquantity=$cproduct['quantity'];
                                          $cflag=1;
                                      }
                                  }
                                  ?>
                                  
                                  <?php  if($cquantity!=0) { ?>
                                  
                                  <div class="floatleft" id="shqtydiv<?php echo $product1['product_id']; ?>">
                                  <div class="productplus"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg" onclick="minusquantity_wl('<?php echo $product1['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg'"/></div> 
                          
                                  <div class="quantityinput"><input class="order_qua" type="text" name="quantity<?php echo $product1['product_id']; ?>" id="quantity<?php echo $product1['product_id']; ?>" value="<?php echo $cquantity;?>" onchange="callupdate_wl('<?php echo $product1['product_id']; ?>')" onkeyup="chkzerominus_wl('<?php echo $product1['product_id']; ?>',event)"/></div>
                                  </div>
                                  
                                  <div class="productminus" id="changemplus<?php echo $product1['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity_wl('<?php echo $product1['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
                                  
                                  <div class="clear"></div>
                                  
                                  <?php } else {?>
                                  
                                  <div class="floatleft" id="shqtydiv<?php echo $product1['product_id']; ?>">
                                  <a class="addquantity" href="javascript:plusquantity_wl('<?php echo $product1['product_id']; ?>')">Add&nbsp;&nbsp;</a>
                                  </div>
                                  
                                  <div class="productminus" id="changemplus<?php echo $product1['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity_wl('<?php echo $product1['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
                                  
                                  <div class="clear"></div>                          
                                  <?php } ?>
                                                     
                                </div>
                            
                                <div class="wishlist"><a class="wish tooltip" onclick="addToWishList('<?php echo $product1['product_id']; ?>');"><span><?php echo $button_wishlist; ?></span></a></div> 
                                <div class="comparee"><a class="compare tooltip" onclick="addToCompare('<?php echo $product1['product_id']; ?>');"><span><?php echo $button_compare; ?></span></a></div>      
                                <div class="clear"></div>
                                </div>
                                  
                                  <?php   $y++;
                                          }                               
                                      }
                                  ?>
                           
                      <?php }//if flag != 1 ?>   
                      
                      </li>   
                    
                    <?php }//foreach products  ?>
                    
                     </ul> 
                </div>
              </div>
	    </div> 
    <div class="clear">&nbsp;</div>
    
    </div><!-- END .box-product-->    
</div><!-- END .featured -->    

<?php }//if products  ?>

<div class="clear">&nbsp;</div>
