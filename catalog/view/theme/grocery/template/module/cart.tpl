<div id="cart">
  <div class="heading">
    <h5 class="title-block-top"><?php echo $heading_title; ?></h5>
    <a><span id="cart-total"><?php echo $text_items; ?></span></a></div>
  <div class="content">
    
    <?php if ($products || $vouchers) { ?>
    <div class="mini-cart-info">
 	    
        <?php foreach ($products as $product) { ?>
      	<div class="one-product"> 
        
          <div class="name">
            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <div>
              <?php foreach ($product['option'] as $option) { ?>
              - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small><br />
              <?php } ?>
            </div>
          </div>
            
          
          <div class="total"><?php echo $product['price']; ?></div>
          
          
          <?php
            $cartproducts = $this->cart->getProducts();
            ?>
            
            <?php 
            $cflag=0;$cquantity=0;
            foreach($cartproducts as $cproduct) {
                if($cproduct['product_id']==$product['key'])
                { 
                    $cquantity=$cproduct['quantity'];
                    $cflag=1;
                }
            }
            ?>
          
          <?php  if($cquantity!=0) { ?>
          
          <div class="floatleft" id="shqtydivs<?php echo $product['key']; ?>">
          
            <div class="productplus"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.png" onclick="minusquantitys('<?php echo $product['key']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.png'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.png'"/></div> 
    
            <div class="quantityinput"><input class="order_qua" type="text" name="quantitys<?php echo $product['key']; ?>" id="quantitys<?php echo $product['key']; ?>" value="<?=$cquantity?>" onchange="callupdates('<?php echo $product['key']; ?>')" onkeyup="chkzerominuss('<?php echo $product['key']; ?>',event)"/></div>
            
          </div>
            
            <div class="productminus" id="changempluss<?php echo $product['key']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.png" onclick="plusquantitys('<?php echo $product['key']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.png'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.png'"/></div>
          
          
          <?php } else {?>
            
            <div class="floatleft" id="shqtydivs<?php echo $product['key']; ?>">
            <a class="addquantity" href="javascript:plusquantity1s('<?php echo $product['key']; ?>')">Add&nbsp;&nbsp;</a>
            </div>
            
            <div class="productminus" id="changempluss<?php echo $product['key']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.png" onclick="plusquantity1('<?php echo $product['key']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.png'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.png'"/></div>
            
            <div class="clear"></div>
            
         <?php } ?>
          
     
          
          <div class="remove">
          <img src="catalog/view/theme/grocery/image/remove-small.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" onclick="(getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') ? location = 'index.php?route=checkout/cart&remove=<?php echo $product['key']; ?>' : $('#cart').load('index.php?route=module/cart&remove=<?php echo $product['key']; ?>' + ' #cart > *');" />
          </div>
          
          
          </div>
        <?php } ?>
        
        
        
        
        <?php foreach ($vouchers as $voucher) { ?>
        
        
          <div class="one-product"> 
          <div class="image"></div>
          <div class="name"><?php echo $voucher['description']; ?></div>
          <div class="total"><?php echo $voucher['amount']; ?> x&nbsp;1</div>
          <div class="remove">
          <img src="catalog/view/theme/grocery/image/remove-small.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" onclick="(getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') ? location = 'index.php?route=checkout/cart&remove=<?php echo $voucher['key']; ?>' : $('#cart').load('index.php?route=module/cart&remove=<?php echo $voucher['key']; ?>' + ' #cart > *');" />
          </div>
          </div>
          
          
        <?php } ?>

    </div>
    
    
    <div class="clearfix"></div>
    
    <div class="mini-cart-total">
      <table>
        <?php foreach ($totals as $total) {
        if($total['code'] != 'sub_total' && $total['code'] != 'tax'){ ?>
        <tr>
          <td class="right"><b><?php echo $total['title']; ?>:</b></td>
          <td class="right"><?php echo $total['text']; ?></td>
        </tr>
        <?php } } ?>
      </table>
      
      <div class="checkout">
        <a class="ca" href="<?php echo $cart; ?>"><?php echo $text_cart; ?></a>
        <a class="ch" href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a>
      </div>
    </div><!-- END .min-cart-total -->
    
    <div class="mini-cart-coupon-form">
        <input type="text" value="" name="coupon" placeholder="Enter your coupon here..." />
        <input type="hidden" value="coupon" name="next">
        &nbsp;
        <input type="button" class="btn-coupon button small" onclick="apply_coupon();" value="Apply Coupon">
     </div>
    
    <?php } else { ?>
    <div class="empty"><?php echo $text_empty; ?></div>
    <?php } ?>
  </div><!-- END .content -->
</div><!-- END .cart -->


<script>
    
function callupdates(productid)
{
	//alert(document.getElementById('quantity'+productid).value);
	updateToCart(productid,document.getElementById('quantitys'+productid).value);
	if(document.getElementById('quantitys'+productid).value==0)
	{
		document.getElementById('shqtydivs'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1s("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changempluss'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.png' onclick='plusquantity1s("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.png\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.png\"' />";
	}
}
function plusquantitys(productid)
{
	document.getElementById('quantitys'+productid).value = parseInt(document.getElementById('quantitys'+productid).value)+parseInt(1);	
	updateToCart(productid,document.getElementById('quantitys'+productid).value);
}
function minusquantitys(productid)
{
	document.getElementById('quantitys'+productid).value = parseInt(document.getElementById('quantitys'+productid).value)-parseInt(1);
	updateToCart(productid,document.getElementById('quantitys'+productid).value);
	if(document.getElementById('quantitys'+productid).value==0)
	{
		document.getElementById('shqtydivs'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1s("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changempluss'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.png' onclick='plusquantity1s("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.png\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.png\"' />";
	}
}

function chkzerominuss(productid,e)
{
	var keynum;

            if(window.event){ // IE					
            	keynum = e.keyCode;
            }else
                if(e.which){ // Netscape/Firefox/Opera					
            		keynum = e.which;
                 }
           // alert(String.fromCharCode(keynum));
		   if(String.fromCharCode(keynum)=='m')
		   {
			   updateToCart(productid,"0");
			   
			   document.getElementById('shqtydivs'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1s("+productid+")'>Add&nbsp;&nbsp;</a>";
			   document.getElementById('changempluss'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.png' onclick='plusquantity1s("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.png\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.png\"' />";
			   
		   }
}

function plusquantity1s(productid){
    document.getElementById('shqtydivs'+productid).innerHTML="<div class='productplus'><img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.png' onclick='minusquantitys("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.png\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.png\"'/></div><div class='quantityinput'><input class='order_qua' type='text' name='quantitys"+productid+"' id='quantitys"+productid+"' value='0' onchange='callupdates("+productid+")' onkeyup='chkzerominuss("+productid+",event)'/></div>";	

    document.getElementById('changempluss'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.png' onclick='plusquantitys("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.png\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.png\"'/>";

    document.getElementById('quantitys'+productid).value = parseInt(document.getElementById('quantitys'+productid).value)+parseInt(1);

    updateToCart(productid,document.getElementById('quantitys'+productid).value);
}

function apply_coupon(){
  
    $.post('index.php?route=checkout/cart/couponAjax',{ coupon: $('input[name="coupon"]').val() },function(data){

        $html = '';
        var result = JSON.parse(data);    

        if(result['status']){
            
            var data = result['total_data'];

            for($i=0; $i < data.length; $i++) {
                if(data[$i]['code'] != 'sub_total' && data[$i]['code'] != 'tax'){ 
                    $html += '<tr>';
                    $html += '<td class="right"><b>'+data[$i]['title']+':</b></td>';
                    $html += '<td class="right">'+data[$i]['text']+'</td>';
                    $html += '</tr>';
                }
            } 

            $('.mini-cart-total table tbody').html($html);
            
            $('#notification').html('<div class="success">' + result.msg + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

            $('.success').fadeIn(500).delay(1000).fadeOut(1000);

            $('.success').fadeOut('slow');	
                                
             $('input[name="coupon"]').val('');
             
        }else{
            
             $('#notification').html('<div class="warning">' + result.msg + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
             $('.warning').fadeIn('slow');
             $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    });
}
</script>

<style>
.mini-cart-info img
{
	cursor:pointer;
}
</style>