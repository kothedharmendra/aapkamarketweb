<?php if ($this->customer->isLogged()) {
        $customer_group_id = $this->customer->getCustomerGroupId();
} else {
        $customer_group_id = $this->config->get('config_customer_group_id');
}

$a = $this->db->query("select name from ".DB_PREFIX."customer_group_description where customer_group_id='".$customer_group_id."'")->row;

if(array_key_exists('name', $a)){
    $user_group = $a['name'];
}else{
    $user_group = '';
}

?>

<div class="box latest">
  <div class="box-heading"><?php echo $heading_title; ?><span class="heading-shadow"></span></div>
  <div class="box-content">
    <div class="box-product">
    
    
      <?php foreach ($products as $product) { ?>
      <div class="main grid_2">
        <!-- image //
            =============-->
            


              <div class="image">
                  <a href="<?php echo $product['href']; ?>">
                      <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" 
                      alt="<?php echo $product['name']; ?>" style="border:none"/>
                  </a>
              </div>

             
        
        <!-- name //
        =============-->
        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
        
        <!-- price //
        =============-->
        <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-old"><?php echo $product['price']; ?></span> 
          <span class="price-new"><?php echo $product['special']; ?></span>
          <span class="sale"><b><?php echo $product['saving']; ?>/- Off</b></span>
          <?php } ?>
        </div>
        <?php } ?>
        
              
        <!-- wish  //  compare  //  cart
        =============-->
        <div>
        <div class="cart" style="display:block">
          <!--<input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" />-->
          
          
          <?php
        $cartproducts = $this->cart->getProducts();
        ?>
        
        <?php 
        $cflag=0;$cquantity=0;
        foreach($cartproducts as $cproduct) {
            if($cproduct['product_id']==$product['product_id'])
            { 
            	$cquantity=$cproduct['quantity'];
                $cflag=1;
            }
        }
		?>
        
        
        <?php  if($cquantity!=0) { ?>
        
        <div class="floatleft" id="shqtydivl<?php echo $product['product_id']; ?>">
        <div class="productplus"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg" onclick="minusquantityl('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg'"/></div> 

        <div class="quantityinput"><input class="order_qua" type="text" name="quantityl<?php echo $product['product_id']; ?>" id="quantityl<?php echo $product['product_id']; ?>" value="<?=$cquantity?>" onchange="callupdatel('<?php echo $product['product_id']; ?>')" onkeyup="chkzerominus('<?php echo $product['product_id']; ?>',event)"/></div>
        </div>
        
        <div class="productminus" id="changemplusl<?php echo $product['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
        
        <div class="clear"></div>
        
        <?php } else {
        
        if($product['quantity'] > 0 || $user_group == 'Wholesaler'){  ?>
        
        <div class="floatleft" id="shqtydivl<?php echo $product['product_id']; ?>">
            <a class="addquantity" href="javascript:plusquantity1l('<?php echo $product['product_id']; ?>')">Add&nbsp;&nbsp;</a>
        </div>
        
        <div class="productminus" id="changemplusl<?php echo $product['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity1l('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
        
        <?php }else{ ?>

        <img class="out_of_stock" src="<?= HTTP_SERVER ?>image/data/assets/no_stock.png" />

        <?php } ?>
        
        <div class="clear"></div>
        
        <?php } ?>
          
        </div>
        <!--<a class="wish tooltip" onclick="addToWishList('<?php echo $product['product_id']; ?>');"><span><?php echo $button_wishlist; ?></span></a>
        <a class="compare tooltip" onclick="addToCompare('<?php echo $product['product_id']; ?>');"><span><?php echo $button_compare; ?></span></a>-->
        </div>
      </div>
      <?php } ?>
      
      
    </div>
  </div>
</div>
<script type="text/javascript">
function callupdatel(productid)
{
	//alert(document.getElementById('quantity'+productid).value);
	updateToCart(productid,document.getElementById('quantityl'+productid).value);
	if(document.getElementById('quantityl'+productid).value==0)
	{
		document.getElementById('shqtydivl'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1l("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changemplusl'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1l("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
	}
}



function plusquantityl(productid)
{
	document.getElementById('quantityl'+productid).value = parseInt(document.getElementById('quantityl'+productid).value)+parseInt(1);	
	updateToCart(productid,document.getElementById('quantityl'+productid).value);
}
function minusquantityl(productid)
{
	document.getElementById('quantityl'+productid).value = parseInt(document.getElementById('quantityl'+productid).value)-parseInt(1);
	updateToCart(productid,document.getElementById('quantityl'+productid).value);
	if(document.getElementById('quantityl'+productid).value==0)
	{
		document.getElementById('shqtydivl'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changemplusl'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
	}
}

function chkzerominusl(productid,e)
{
	var keynum;

            if(window.event){ // IE					
            	keynum = e.keyCode;
            }else
                if(e.which){ // Netscape/Firefox/Opera					
            		keynum = e.which;
                 }
           // alert(String.fromCharCode(keynum));
		   if(String.fromCharCode(keynum)=='m')
		   {
			   updateToCart(productid,"0");
			   
			   document.getElementById('shqtydivl'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1l("+productid+")'>Add&nbsp;&nbsp;</a>";
			   document.getElementById('changemplusl'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1l("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
			   
		   }
}

function plusquantity1l(productid)
{
	document.getElementById('shqtydivl'+productid).innerHTML="<div class='productplus'><img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg' onclick='minusquantityl("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg\"'/></div><div class='quantityinput'><input class='order_qua' type='text' name='quantityl"+productid+"' id='quantityl"+productid+"' value='0' onchange='callupdatel("+productid+")' onkeyup='chkzerominusl("+productid+",event)'/></div>";	
	
	document.getElementById('changemplusl'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantityl("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"'/>";
	
	document.getElementById('quantityl'+productid).value = parseInt(document.getElementById('quantityl'+productid).value)+parseInt(1);
	
	updateToCart(productid,document.getElementById('quantityl'+productid).value);
}

</script>
