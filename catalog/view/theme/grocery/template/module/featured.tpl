<?php if ($this->customer->isLogged()) {
        $customer_group_id = $this->customer->getCustomerGroupId();
} else {
        $customer_group_id = $this->config->get('config_customer_group_id');
}

$a = $this->db->query("select name from ".DB_PREFIX."customer_group_description where customer_group_id='".$customer_group_id."'")->row;

if(array_key_exists('name', $a)){
    $user_group = $a['name'];
}else{
    $user_group = '';
}

?>

<div class="box featured">
  <div class="box-heading"><?php echo $heading_title; ?></span>
    <span id="fp-next" class="next"></span>
    <span id="fp-prev" class="prev"></span>   
  </div>
  <div class="box-content">  
    <div class="box-product">
    
    <div class="slides_container list_carousel responsive feature-wrapper">
    
    <?php
    
      $allproductsid[]=array();
      $similarproducts=array();
      $removeproducts=array();
      $i=0;$j=0;$k=0;
      
      foreach ($products as $product)
      {
        $allproductsid[$i]=$product['product_id'];
        $i++;
      }
      
      $l=0;
      
      for($i=0;$i<count($allproductsid);$i++){
     
        if(!in_array($allproductsid[$i],$removeproducts)){
            $query = $this->db->query("select * from product_similar where product_id='".$allproductsid[$i]."'"); //mysql_query

            $rsallproducts = $query->rows;

            $num_rows = sizeof($rsallproducts);

            if ($num_rows > 0)
            {
                $similarproducts[$j] = $allproductsid[$i];
                $j++;

                foreach($rsallproducts as $row) {
                    $removeproducts[$l]=$row['similar_id'];
                    $l++;
                }
            }
        }
      } 
       
      $m=0;
      ?>  
    
    <ul class="featured_list">
    
    	
          <?php foreach ($products as $product) { ?>
          
          
          <?php
                $flag=0;
                for($k=0;$k<count($removeproducts);$k++)
                {
                    if($product['product_id']==$removeproducts[$k])
                    {
                        $flag=1;
                    }	
                }
                if($flag!=1)
                {
                $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."'");
                
                $rsallproducts = $query->rows;
                
                $num_rows = sizeof($rsallproducts);
            ?> 
            
            
          	<li>
                <div class="main divproduct<?=$product['product_id']?>" id="divproduct<?=$product['product_id']?>">
                    <!-- image //
                    =============-->
                    <!--for swap image-->
                    <?php if ($product['thumb_swap']) { ?>
                      <div class="image">
                          <a href="<?php echo $product['href']; ?>">
                             <img oversrc="<?php echo $product['thumb_swap']; ?>" src="<?php echo $product['thumb']; ?>" 
                             title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" style="border:none"/>
                          </a>
                      </div>
        
                      <?php } else { ?>
        
                      <div class="image">
                          <a href="<?php echo $product['href']; ?>">
                              <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" 
                              alt="<?php echo $product['name']; ?>" style="border:none"/>
                          </a>
                      </div>
        
                      <?php } ?>
                    <!--/ swap img-->
                    
                    
                    <?php
                      if($num_rows>0) {  
                        
                        foreach($rsallproducts as $row) {            	                
                            $query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
                            $rsprod = $query->row;

                            $rs = strspn($product['name'] ^ $rsprod['name'], "\0");
                            break;
                        }

                        $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."'");
                        $rsallproducts = $query->rows;

                        $num_rows = sizeof($rsallproducts);//mysql_num_rows($rsallproducts);                            

                       }
                      ?>
                      <?php if($num_rows>0 && $rs) { ?>
                        <div class="name">
                            <a href="<?php echo $product['href']; ?>">
                                <?=substr($product['name'],0,$rs);?>
                            </a></div>
                      <?php } else { ?>
                        <div class="name">
                            <a href="<?php echo $product['href']; ?>">
                                <?php echo $product['name']; ?>
                            </a>
                        </div>
                    <?php } ?>
                    
                    <!-- name //
                    =============-->
                    <!--<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>-->
                    
                    
                    <!-- price //
                    =============-->
                    <?php if ($product['price']) { ?>
                    <div class="price">
                    
                    	<?php  if($num_rows>0 && $rs) {   ?>
                        
                        
                        <ul class="ultabonproductdisplay">
                            <li><a onclick="displayblocknone_fl('divproduct<?=$product['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($product['name'],$rs);?></a></li>
                            <?php
                            foreach($rsallproducts as $row) {
                            
                                if(!array_key_exists($row['similar_id'],$products))
                                    continue;
                                
                                $query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
                                $rsprod = $query->row;                                
                                
                                $rs=strspn($product['name'] ^ $rsprod['name'], "\0");
                
                                $pname=substr($rsprod['name'],($rs-1),1);                
                                
                                if(is_numeric($pname) && $pname!=' '){
                                    $rs=$rs-2;
                                    $pname=substr($rsprod['name'],$rs,1);                	
                                }
                                
                            ?>
                            
                            
                            <li style="border:none">
                                <a onclick="displayblocknone_fl('divproduct<?=$product['product_id'].'_'.$rsprod['product_id']?>','divproduct<?=$product['product_id']?>')">
                                    <?=substr($rsprod['name'],$rs);?>
                                </a>
                            </li>
                            
                            
                            <?php } ?>
                        </ul>
                        
                        <div class="clear"></div>
                        <div style=" border-top:1px #BDBDBD solid;"></div>        
                        <div style="padding-top:10px;"></div>        
                        <?php } else {?>
                       <div style="padding-top:29px;"></div>
                        <?php } ?>
                    
                      <?php if (!$product['special']) { ?>
                      <?php echo $product['price']; ?>
                      <?php } else { ?>
                      <span class="price-old"><?php echo $product['price']; ?></span> 
                      <span class="price-new"><?php echo $product['special']; ?></span>
                      <span class="sale"><b><?php echo $product['saving']; ?>/-<br />Off</b></span>
                      <?php } ?>
                    </div>
                    <?php } ?>
                    
                    <!-- rate //
                    =============-->
                    
                    <!-- wish  //  compare  //  cart
                    =============-->
                    
                    <div class="cart">
        
                    <?php
                    $cartproducts = $this->cart->getProducts();
                    ?>
                    
                    <?php 
                    $cflag=0;$cquantity=0;
                    foreach($cartproducts as $cproduct) {
                        if($cproduct['product_id']==$product['product_id'])
                        { 
                            $cquantity=$cproduct['quantity'];
                            $cflag=1;
                        }
                    }
                    ?>
                    
                    
                    <?php  if($cquantity!=0) { ?>
                    
                    <div class="floatleft" id="shqtydiv<?php echo $product['product_id']; ?>">
                    <div class="productplus">
                        <img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg" onclick="minusquantity_fl('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg'"/>
                    </div> 
            
                    <div class="quantityinput">
                        <input class="order_qua" type="text" name="quantity<?php echo $product['product_id']; ?>" id="quantity<?php echo $product['product_id']; ?>" value="<?=$cquantity?>" onchange="callupdate_fl('<?php echo $product['product_id']; ?>')" onkeyup="chkzerominus_fl('<?php echo $product['product_id']; ?>',event)"/></div>
                    </div>
                    
                    <div class="productminus" id="changemplus_fl<?php echo $product['product_id']; ?>">
                        <img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity_fl('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
                    
                    <div class="clear"></div>
                    
                    <?php } else { 
                    
                    if($product['quantity'] > 0 || $user_group == 'Wholesaler'){                    
                    
                    ?>
                    
                    <div class="floatleft" id="shqtydiv<?php echo $product['product_id']; ?>">
                        <a class="addquantity" href="javascript:plusquantity_fl('<?php echo $product['product_id']; ?>')">Add&nbsp;&nbsp;</a>
                    </div>
                    <div class="productminus" id="changemplus_fl<?php echo $product['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity_fl('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
                    
                    <?php }else{ ?>
                    
                    <img class="out_of_stock" src="<?= HTTP_SERVER ?>image/data/assets/no_stock.png" />
                    
                    <?php } ?>
                    
                    <div class="clear"></div>
                    
                    <?php } ?>
                    
                    
             
                  </div>
                  
                  <div class="wishlist"><a class="wish tooltip" onclick="addToWishList('<?php echo $product['product_id']; ?>');"><span><?php echo $button_wishlist; ?></span></a></div>    
                  <div class="comparee"><a class="compare tooltip" onclick="addToCompare('<?php echo $product['product_id']; ?>');"><span><?php echo $button_compare; ?></span></a></div>
                  
                    
                  <div class="clear"></div>
                    
                    
                    
                </div><!--/main-->
                
                
                
                <?php 
                
                 $y=0; 
                    
                 $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."'");
                 $rsallproducts1 = $query->rows;             
                 $num_rows = sizeof($rsallproducts1);
                 
                 $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."'");
                 $rsallproducts1 = $query->rows;             
                 $num_rows = sizeof($rsallproducts1);                 
                 
                 if($num_rows>0) { 
                 
                    foreach($rsallproducts1 as $row){ 
                        $z=0;
                        
                        if(array_key_exists($row['similar_id'],$products))
                            $product1 = $products[$row['similar_id']];
                        else
                            continue;
                            
                        
                        /*
                        foreach ($products as $product1)
                        {
                        if($row['similar_id']==$product1['product_id'])
                        {
                        */
                ?>
                
                <div class="divproduct<?=$row['product_id'].'_'.$product1['product_id']?>" id="divproduct<?=$row['product_id'].'_'.$product1['product_id']?>" style="display:none;">
                    <?php if ($product['thumb_swap']) { ?>
                      <div class="image">
                          <a href="<?php echo $product1['href']; ?>">
                             <img oversrc="<?php echo $product1['thumb_swap']; ?>" src="<?php echo $product1['thumb']; ?>" 
                             title="<?php echo $product1['name']; ?>" alt="<?php echo $product1['name']; ?>" style="border:none"/>
                          </a>
                      </div>
        
                      <?php } else {?>
        
                      <div class="image">
                          <a href="<?php echo $product1['href']; ?>">
                              <img src="<?php echo $product1['thumb']; ?>" title="<?php echo $product1['name']; ?>" 
                              alt="<?php echo $product1['name']; ?>" style="border:none"/>
                          </a>
                      </div>
        
                      <?php } ?>
                
                      <div class="name"><a href="<?php echo $product1['href']; ?>"><?=substr($product['name'],0,$rs);?></a></div>
                      
                      <?php if ($product1['price']) { ?>
                      <div class="price">
                
                        <?php
                        
                        $query = $this->db->query("select * from product_similar where product_id='".$product['product_id']."'");
                        $rsallproducts = $query->rows;
                        $num_rows = sizeof($rsallproducts);                        
                        
                        if($num_rows>0) {   
                        ?>
                        <ul class="ultabonproductdisplay">
                            <li style="border:none"><a onclick="displayblocknone_fl('divproduct<?=$product['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($product['name'],$rs);?></a></li>
                            <?php
                            foreach($rsallproducts as $row) 
                            {                	
                                $query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
                                $rsprod = $query->row;                                
                                $rs=strspn($product['name'] ^ $rsprod['name'], "\0");
                            ?>
                            <?php if($y==$z) {?>
                            <li><a onclick="displayblocknone_fl('divproduct<?=$product['product_id'].'_'.$rsprod['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($rsprod['name'],$rs);?></a></li>
                            <?php } else { ?>
                            <li style="border:none"><a onclick="displayblocknone_fl('divproduct<?=$product['product_id'].'_'.$rsprod['product_id']?>','divproduct<?=$product['product_id']?>')"><?=substr($rsprod['name'],$rs);?></a></li>
                            <?php } ?>
                            <?php $z++; 
                            } ?>
                        </ul>
                        <div class="clear"></div>
                        <div style="border-top:1px #BDBDBD solid;"></div>        
                        <div style="padding-top:10px;"></div>        
                        <?php } ?>
                        
                        <?php if (!$product1['special']) { ?>
                        <?php echo $product1['price']; ?>
                        <?php } else { ?>
                        <span class="price-new"><?php echo $product1['special']; ?></span>
                        <span class="price-old"><?php echo $product1['price']; ?></span>
                        <span class="sale"><b><?php echo $product1['saving']; ?>/-<br />Off</b></span>
                        <?php } ?>                        
                      </div>
                      <?php } ?>
                      
                      
                      
                      
                      <div class="cart">
        
                <?php
                $cartproducts = $this->cart->getProducts();
                ?>
                
                <?php 
                $cflag=0;$cquantity=0;
                foreach($cartproducts as $cproduct) {
                    if($cproduct['product_id']==$product1['product_id'])
                    { 
                        $cquantity=$cproduct['quantity'];
                        $cflag=1;
                    }
                }
                ?>
                
                
                <?php  if($cquantity!=0) { ?>
                
                <div class="floatleft" id="shqtydiv<?php echo $product1['product_id']; ?>">
                <div class="productplus"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg" onclick="minusquantity_fl('<?php echo $product1['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg'"/></div> 
        
                <div class="quantityinput"><input class="order_qua" type="text" name="quantity<?php echo $product1['product_id']; ?>" id="quantity<?php echo $product1['product_id']; ?>" value="<?=$cquantity?>" onchange="callupdate_fl('<?php echo $product1['product_id']; ?>')" onkeyup="chkzerominus_fl('<?php echo $product1['product_id']; ?>',event)"/></div>
                </div>
                
                <div class="productminus" id="changemplus_fl<?php echo $product1['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity_fl('<?php echo $product1['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
                
                <div class="clear"></div>
                
                <?php } else { 
                
                if($product1['quantity'] > 0 || $user_group == 'Wholesaler'){                    
                    
                    ?>
                    
                    <div class="floatleft" id="shqtydiv<?php echo $product['product_id']; ?>">
                        <a class="addquantity" href="javascript:plusquantity_fl('<?php echo $product['product_id']; ?>')">Add&nbsp;&nbsp;</a>
                    </div>
                    <div class="productminus" id="changemplus_fl<?php echo $product['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity_fl('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
                    
                <?php }else{ ?>
                    
                    <img class="out_of_stock" src="<?= HTTP_SERVER ?>image/data/assets/no_stock.png" />
                    
               <?php } ?>
                    
                <div class="clear"></div>
                
                <?php } ?>
                
              </div>

              <div class="wishlist">
                  <a class="wish tooltip" onclick="addToWishList('<?php echo $product1['product_id']; ?>');">
                      <span><?php echo $button_wishlist; ?></span>
                  </a>
              </div> 
              <div class="comparee">
                  <a class="compare tooltip" onclick="addToCompare('<?php echo $product1['product_id']; ?>');">
                      <span><?php echo $button_compare; ?></span>
                  </a>
              </div>
                      
                        
                      <div class="clear"></div>
                    </div>
                
                <?php 
                        $y++;
                        }                               
                    }
                ?>
            </li>           
            
            <?php } ?>            
            
          <?php } ?>
    </ul>
    
    </div> 
    </div><!--/box-product-->    
  </div><!--/box-content-->
<!-- liquid carousel -->

<script src="<?= HTTP_SERVER ?>catalog/view/javascript/jquery/bxslider/jquery.bxslider.min.js"></script>
<link rel="stylesheet" href="<?= HTTP_SERVER ?>catalog/view/javascript/jquery/bxslider/jquery.bxslider.css" type="text/css" />

<script>

 $('.featured_list').bxSlider({
  minSlides: 1,
  maxSlides: 5,
  auto: true,
  autoHover: true,
  slideWidth: 200,
  minThreshold: 200,
  slideMargin: 20,
  nextText: '&raquo;',
  prevText: '&laquo;',
 });
 

//----------------------------------- WISHLIST --------------------------//
function displayblocknone_wl(divtoshow,common)
{      
    $(".wishlist-wrapper div[id^="+common+"]").css("display","none");
    $('.wishlist-wrapper .'+divtoshow).css('display','block');
}

function plusquantity_wl(productid)
{
    if(!$('.wishlist-wrapper #shqtydiv'+productid+' input').length > 0){
        $('.wishlist-wrapper #shqtydiv'+productid).html("<div class='productplus'><img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg' onclick='minusquantity_wl("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg\"'/></div><div class='quantityinput'><input class='order_qua' type='text' name='quantity"+productid+"' id='quantity"+productid+"' value='0' onchange='callupdate_wl("+productid+")' onkeyup='chkzerominus_wl("+productid+",event)'/></div>");	
        $('.wishlist-wrapper #changemplus'+productid).html("<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity_wl("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"'/>");
    }

    $('.wishlist-wrapper #quantity'+productid).val(parseInt($('.wishlist-wrapper #quantity'+productid).val()) + parseInt(1));

    updateToCart(productid,$('.wishlist-wrapper #quantity'+productid).val());
}

function callupdate_wl(productid)
{
	//alert(document.getElementById('quantity'+productid).value);
	updateToCart(productid,$('.wishlist-wrapper #quantity'+productid).val());
	if($('.wishlist-wrapper #quantity'+productid).val()==0)
	{
		$('.wishlist-wrapper #shqtydiv'+productid).html("<a class='addquantity' href='javascript:plusquantity_wl("+productid+")'>Add&nbsp;&nbsp;</a>");
		$('.wishlist-wrapper #changemplus'+productid).html("<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity_wl("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />");
	}
}


function chkzerominus_wl(productid,e)
{
	var keynum;

            if(window.event){ // IE					
            	keynum = e.keyCode;
            }else
                if(e.which){ // Netscape/Firefox/Opera					
            		keynum = e.which;
                 }
           // alert(String.fromCharCode(keynum));
		   if(String.fromCharCode(keynum)=='m')
		   {
			   updateToCart(productid,"0");
			   
			   $('.wishlist-wrapper #shqtydiv'+productid).hrml("<a class='addquantity' href='javascript:plusquantity_wl("+productid+")'>Add&nbsp;&nbsp;</a>");
			   $('.wishlist-wrapper #changemplus'+productid).html("<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity_wl("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />");
			   
		   }
}
function minusquantity_wl(productid)
{
	$('.wishlist-wrapper #quantity'+productid).val(parseInt(document.getElementById('quantity'+productid).value)-parseInt(1));
	updateToCart(productid,document.getElementById('quantity'+productid).value);
	if($('.wishlist-wrapper #quantity'+productid).val()==0)
	{
		$('.wishlist-wrapper #shqtydiv'+productid).html("<a class='addquantity' href='javascript:plusquantity_wl("+productid+")'>Add&nbsp;&nbsp;</a>");
		$('.wishlist-wrapper #changemplus'+productid).html("<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity_wl("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />");
	}
}
//----------------------------------- FUTURE PRODUCTS --------------------------//
function displayblocknone_fl(divtoshow,common)
{      
    $(".feature-wrapper div[id^="+common+"]").css("display","none");
    $('.feature-wrapper .'+divtoshow).css('display','block');
}
function plusquantity_fl(productid)
{
    if(!$('.feature-wrapper #shqtydiv'+productid+' input').length > 0){
        $('.feature-wrapper #shqtydiv'+productid).html("<div class='productplus'><img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg' onclick='minusquantity_fl("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg\"'/></div><div class='quantityinput'><input class='order_qua' type='text' name='quantity"+productid+"' id='quantity"+productid+"' value='0' onchange='callupdate_fl("+productid+")' onkeyup='chkzerominus_fl("+productid+",event)'/></div>");	
        $('.feature-wrapper #changemplus'+productid).html("<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity_fl("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"'/>");
    }

    $('.feature-wrapper #quantity'+productid).val(parseInt($('.feature-wrapper #quantity'+productid).val()) + parseInt(1));

    updateToCart(productid,$('.feature-wrapper #quantity'+productid).val());
}


function callupdate_fl(productid)
{
	updateToCart(productid,$('.feature-wrapper #quantity'+productid).val());
	if($('.feature-wrapper #quantity'+productid).val()==0)
	{
		$('.feature-wrapper #shqtydiv'+productid).html("<a class='addquantity' href='javascript:plusquantity_fl("+productid+")'>Add&nbsp;&nbsp;</a>");
		$('.feature-wrapper #changemplus'+productid).html("<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity_fl("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />");
	}
}


function chkzerominus_fl(productid,e)
{
	var keynum;

            if(window.event){ // IE					
            	keynum = e.keyCode;
            }else
                if(e.which){ // Netscape/Firefox/Opera					
            		keynum = e.which;
                 }
         
		   if(String.fromCharCode(keynum)=='m')
		   {
			   updateToCart(productid,"0");
			   
			   $('.feature-wrapper #shqtydiv'+productid).hrml("<a class='addquantity' href='javascript:plusquantity_fl("+productid+")'>Add&nbsp;&nbsp;</a>");
			   $('.feature-wrapper #changemplus'+productid).html("<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity_fl("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />");
			   
		   }
}
function minusquantity_fl(productid)
{
	$('.feature-wrapper #quantity'+productid).val(parseInt(document.getElementById('quantity'+productid).value)-parseInt(1));
	updateToCart(productid,document.getElementById('quantity'+productid).value);
	if($('.feature-wrapper #quantity'+productid).val()==0)
	{
		$('.feature-wrapper #shqtydiv'+productid).html("<a class='addquantity' href='javascript:plusquantity_fl("+productid+")'>Add&nbsp;&nbsp;</a>");
		$('.feature-wrapper #changemplus'+productid).html("<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity_fl("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />");
	}
}


</script>