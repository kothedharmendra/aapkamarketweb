<?php
// Heading 
$_['heading_title']      = 'Track order';
$_['order_title']        = 'Order Details';

// Text
$_['text_track']         = 'Track Order';
$_['text_account']       = 'Account';
$_['text_my_orders']     = 'My Orders';

$_['entry_order_id']     = 'Enter Order ID';
?>