<?php
// Heading 
$_['heading_title']      = 'Credit Balance';

// Column
$_['column_date_added']  = 'Date Added';
$_['column_description'] = 'Description';
$_['column_amount']      = 'Amount (%s)';

// Text
$_['text_account']       = 'Account';
$_['text_transaction']   = 'Credit Balance';
$_['text_total']         = 'Your current balance is:';
$_['text_empty']         = 'You do not have any transactions!';
?>