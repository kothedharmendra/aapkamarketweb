<?php
// Heading
$_['heading_title']  = 'Feedback';

// Text 

$_['text_contact']   = 'Feedback Form';
$_['text_message']   = 'Success: Feedback sent!';

// Entry Fields
$_['entry_name']     = 'First Name:';
$_['entry_email']    = 'E-Mail Address:';
$_['entry_enquiry']  = 'Feedback:';
$_['entry_captcha']  = 'Enter the code in the box below:';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Feedback must be between 10 and 3000 characters!';
$_['error_captcha']  = 'Verification code does not match the image!';
?>
