<?php
// Heading
$_['heading_title']  = 'Suggestion';

// Text 

$_['text_contact']   = 'Suggestion Form';
$_['text_message']   = 'Success: Suggestion sent!';

// Entry Fields
$_['entry_name']     = 'First Name:';
$_['entry_email']    = 'E-Mail Address:';
$_['entry_enquiry']  = 'Suggestion:';
$_['entry_captcha']  = 'Enter the code in the box below:';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Suggestion must be between 10 and 3000 characters!';
$_['error_captcha']  = 'Verification code does not match the image!';
?>
