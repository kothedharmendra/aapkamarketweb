<?php
// Heading 
$_['heading_title'] = 'Latest Products';

// Text
$_['text_reviews']  = 'Based on %s reviews.';
$_['button_compare']  = 'Add to compare'; 
$_['button_wishlist']  = 'Add to wishlist';  
?>