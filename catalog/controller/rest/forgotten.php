<?php  

/**
 * forgotten.php
 *
 * Forgotten password
 *
 * @author     Makai Lajos
 * @copyright  2015
 * @license    License.txt
 * @version    2.0
 * @link       http://opencart-api.com/product/opencart-restful-api-pro-v2-0/
 * @see        http://opencartoauth.opencart-api.com/schema_v2.0_oauth/
 */
require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerRestForgotten extends RestController {
	
	/*
	* forgotten password
	*/
	public function forgotten() {

		$this->checkPlugin();
		
		$json = array('success' => true);
		
		if ( $_SERVER['REQUEST_METHOD'] === 'POST'){
			
			$requestjson = file_get_contents('php://input');
            $post = json_decode($requestjson, true);

			if ($this->customer->isLogged()) {
				$json['error']		= "User is already logged";
				$json['success']	= false;			
			} else {
                $this->language->load('mail/forgotten');
                $this->language->load('account/forgotten');
                $this->load->model('account/customer');

                $error = $this->validate($post);
                if(empty($error)){
                    $password = substr(sha1(uniqid(mt_rand(), true)), 0, 10);

                    $this->model_account_customer->editPassword($post['email'], $password);

                    $subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

                    $message  = sprintf($this->language->get('text_greeting'), $this->config->get('config_name')) . "\n\n";
                    $message .= $this->language->get('text_password') . "\n\n";
                    $message .= $password;

                    $mail = new Mail();
                    $mail->protocol = $this->config->get('config_mail_protocol');
                    $mail->parameter = $this->config->get('config_mail_parameter');
                    $mail->hostname = $this->config->get('config_smtp_host');
                    $mail->username = $this->config->get('config_smtp_username');
                    $mail->password = $this->config->get('config_smtp_password');
                    $mail->port = $this->config->get('config_smtp_port');
                    $mail->timeout = $this->config->get('config_smtp_timeout');
                    $mail->setTo($post['email']);
                    $mail->setFrom($this->config->get('config_email'));
                    $mail->setSender($this->config->get('config_name'));
                    $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
                    $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
                    $mail->send();

                } else {
                    $json["error"]		= $error;
                    $json["success"]	= false;
                }
            }
		} else {
				$json["error"]		= "Only POST request method allowed";
				$json["success"]	= false;
		}

		$this->sendResponse($json);
	
	}

    protected function validate($post) {
        $error = array();
        if (!isset($post['email'])) {
            $error[] = $this->language->get('error_email');
        } elseif (!$this->model_account_customer->getTotalCustomersByEmail($post['email'])) {
            $error[] = $this->language->get('error_email');
        }
        return $error;
    }
}