<?php
class ControllerModuleFeedbackpro extends Controller {
	private $error = array();
	public function index() {
		$this->id = 'feedback';

		$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
		$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox.js');
	}
	public function getform() {
		$this->language->load('module/feedbackpro');

		$this->data['text_required_inputs'] = $this->language->get('text_required_inputs');
		$this->data['text_feedbackpro_sendform'] = $this->language->get('text_feedbackpro_sendform');

		if (!empty($this->request->get['id'])) {
			$this->data['getid'] = $this->request->get['id'];
			$feedback = $this->config->get('feedback_'.$this->request->get['id']);
			$this->data['feedback'] = $feedback;
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/feedbackpro.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/feedbackpro.tpl';
		} else {
			$this->template = 'default/template/module/feedbackpro.tpl';
		}
		if (!empty($_GET['ajax'])) { 
			$this->template = $this->config->get('config_template') . '/template/module/feedbackpro.tpl'; 
		}
		$this->response->setOutput($this->render());
	}

	public function send() {

	$this->load->model('catalog/feedbackpro');
	$this->language->load('module/feedbackpro');

	$this->data['text_feedbackpro_success'] = $this->language->get('text_feedbackpro_success');

	if (!empty($this->request->post['getid'])) {
		$feedback = $this->config->get('feedback_'.$this->request->post['getid']);
		$this->data['feedback'] = $feedback;
	}
	
	$render = array();
	
	foreach($this->request->post['feedback'] as $key => $data) {
		if (isset($feedback['filds'][$key]['fieldname'])) { 
			if ( (!empty($feedback['filds'][$key]['requaried']))&&(empty($data))) {
				$this->error[$key] = $feedback['filds'][$key]['fieldname'];
			} else {
				$render[$feedback['filds'][$key]['fieldname']] = $data; 
			}
		}
	}

	$this->data['feedback']['filds'] = $render;

	$json = array();
	$headers = array();
	
    $headers['referer'] = '<a href="'.$_SERVER["HTTP_REFERER"].'" target="_blank">'.$_SERVER["HTTP_REFERER"].'</a>';
	$headers['ip'] = $_SERVER['REMOTE_ADDR'];
	
	$this->data['feedback']['filds']['info'] = $headers;
	$this->data['feedback']['description'] = (!empty($this->request->post['feedback'][$feedback['main']]))?$this->request->post['feedback'][$feedback['main']]:'---';
	$this->data['feedback']['name'] = $feedback['heading'];

		if(empty($this->error)) {

		    $json['success'] = $this->data['text_feedbackpro_success'];

			$sendsemail = $this->data['feedback']['adminemail'];

			if (!empty($sendsemail)) {
	            $ourmail = $this->data['feedback']['adminemail'];
	        } else {
				$ourmail = $this->config->get('config_email');
			}

		    $mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
		    $mail->setTo($ourmail);
		    $mail->setFrom(substr(preg_replace("#/$#", "", $this->config->get('config_url')), 7));
		    $mail->setSender(substr(preg_replace("#/$#", "", $this->config->get('config_url')), 7));
		    $mail->setSubject($feedback['heading']);
		           
			$feedbackresult = $this->data['feedback']['filds'];
					
			array_pop($feedbackresult);
				    
			$polya = array();

			foreach ($feedbackresult as $key => $mailvalues) {
				$polya[] = $key.' : '. $mailvalues;
			}

			$all = implode("\r\n", $polya);
		    $MailCom = $all;
		           
		    $mail->setText(strip_tags(html_entity_decode($MailCom, ENT_QUOTES, 'UTF-8')));
		    $mail->send();

			$this->model_catalog_feedbackpro->addFeedbackpro($this->data['feedback']);

		} else {
			$json['error'] = $this->error;
		}

		$this->response->setOutput(json_encode($json));	
	}
}
?>