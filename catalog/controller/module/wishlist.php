<?php 
class ControllerModuleWishList extends Controller {
	protected function index($setting) {
    	$this->language->load('module/wishlist');
    	
    	$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['button_cart'] = $this->language->get('button_cart');
		
		$this->data['button_wishlist'] = $this->language->get('button_wishlist');
		
		$this->data['button_compare'] = $this->language->get('button_compare'); 	    			
		
		$this->load->model('catalog/product');
		
		$this->load->model('tool/image');
		
		if (!isset($this->session->data['wishlist'])) {
			$this->session->data['wishlist'] = array();
		}
		
		if (isset($this->request->get['remove'])) {
			$key = array_search($this->request->get['remove'], $this->session->data['wishlist']);
			
			if ($key !== false) {
				unset($this->session->data['wishlist'][$key]);
			}
		
			$this->session->data['success'] = $this->language->get('text_remove');
		
			$this->redirect($this->url->link('account/wishlist'));
		}  	
				
	   if (empty($setting['limit'])) {
			$setting['limit'] = 5;
		}	
				
						
		$this->data['products'] = array();
	
		foreach ($this->session->data['wishlist'] as $key => $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			if ($product_info) { 
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['image_width'], $setting['image_height']);
				} else {
					$image = false;
				}
				
				//this for swap image
				
				$images = $this->model_catalog_product->getProductImages($product_info['product_id']);

               if(isset($images[0]['image']) && !empty($images[0]['image'])){
                  $images =$images[0]['image'];
               } 

				if ($product_info['quantity'] <= 0) {
					$stock = $product_info['stock_status'];
				} elseif ($this->config->get('config_stock_display')) {
					$stock = $product_info['quantity'];
				} else {
					$stock = $this->language->get('text_instock');
				}
							
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
				
				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
				} else {
					$tax = false;
				}	
				
				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
                                
                                $price_tax = round($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                                $spe_price_tax = round($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
											
				$this->data['products'][] = array(
					'product_id' => $product_info['product_id'],
					'thumb'      => $image,
					'name'       => $product_info['name'],
					'model'      => $product_info['model'],
					'stock'      => $stock,
					'price'      => $price,		
					'special'    => $special,
					'tax'        => $tax,
					'href'       => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
					// for swap image
					'thumb_swap'  => $this->model_tool_image->resize($images, $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height')), 
					
                                        'remove'      => $this->url->link('account/wishlist', 'remove=' . $product_info['product_id']),
					
					'saving'      => $price_tax - $spe_price_tax,
					
					'mpn'         => $product_info['mpn'],

				);
			} else {
				unset($this->session->data['wishlist'][$key]);
			}
		}	

		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/wishlist.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/wishlist.tpl';
		} else {
			$this->template = 'default/template/module/wishlist.tpl';
		}
		
								
		$this->render();		
	}
	
	
}
?>