<?php
class ControllerPaymentCod extends Controller {
	protected function index() {
    	$this->data['button_confirm'] = $this->language->get('button_confirm');

		$this->data['continue'] = $this->url->link('checkout/success');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/cod.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/cod.tpl';
		} else {
			$this->template = 'default/template/payment/cod.tpl';
		}	
		
		$this->render();
	}
	
	public function confirm() {
		$this->load->model('checkout/order');
		
		//$pickdate=$_REQUEST['datepicker']; $pickdate='2013/10/12';
		//$timeslot=$_REQUEST['timeslot']; $timeslot='9p.m. 10p.m.';		
                
		/*-----------------------JIPL Start--------------------------*/
		$this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('cod_order_status_id'),$this->session->data['comment'], true,$this->session->data['datepicker'],$this->session->data['timeslot']);
		/*-----------------------JIPL End--------------------------*/
		
	}
}
?>