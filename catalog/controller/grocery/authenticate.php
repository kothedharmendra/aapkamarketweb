<?php

class ControllerGroceryAuthenticate extends Controller {

    private $error = array();
    
    public function index() {
        
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $key = $this->request->post['key'];
            
            $this->db->query('delete from `setting` WHERE `key`="domain_auth_key"');
            $this->db->query('insert into setting SET store_id="'. (int)$this->config->get('config_store_id') .'", `group`="grocery", `key`="domain_auth_key", `value`="'.$key.'"');
            
            $this->redirect($this->url->link('common/home'));
        }   
        
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/grocery/authenticate.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/grocery/authenticate.tpl';
        } else {
            $this->template = 'default/template/grocery/authenticate.tpl';
        }

        $this->response->setOutput($this->render());
    }
    
    private function validate(){
        
        //key 
        $key = $this->request->post['key'];
        
        return $this->grocery->validate($key);
    }
}
