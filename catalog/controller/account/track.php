<?php 
class ControllerAccountTrack extends Controller { 
	public function index() {
            
	// get order
            $this->language->load('account/track');

            $this->data['order'] = false;
            
            $this->data['error_warning'] = ''; 

            $this->data['heading_title'] = $this->language->get('heading_title');

            if (!empty($this->request->post['order_id'])) {
                    $this->load->model('checkout/order');
                    $order = $this->model_checkout_order->getOrder($this->request->post['order_id']);

                    if(!empty($order)){
                        $this->data['order'] = array(
                            'order_id' => $order['order_id'],
                            'payment_firstname'  => $order['payment_firstname'],
                            'payment_lastname' => $order['payment_lastname'],
                            'shipping_method'  => $order['shipping_method'],                          
                            'order_status' => $order['order_status'], 
                            'total' => $this->currency->format($order['total'], $order['currency_code'] , $order['currency_value']),
                            'date_added' => date("F j, Y, g:i a",strtotime($order['date_added'])),
                        );         

                        $this->data['heading_title'] = $this->language->get('order_title');
                    }else{
                        $this->data['error_warning'] = 'Please, Enter a Valid Order ID!'; 
                    }

            }

            $this->document->setTitle($this->language->get('heading_title'));

            $this->data['breadcrumbs'] = array();

            $this->data['breadcrumbs'][] = array(
                    'text'      => $this->language->get('text_home'),
                    'href'      => $this->url->link('common/home'),
                    'separator' => false
            ); 

            $this->data['breadcrumbs'][] = array(       	
                    'text'      => $this->language->get('text_track'),
                    'href'      => $this->url->link('account/track', '', 'SSL'),
                    'separator' => $this->language->get('text_separator')
            );


            $this->data['entry_order_id'] = $this->language->get('entry_order_id');

            $this->data['text_my_orders'] = $this->language->get('text_my_orders');

            $this->data['action'] = $this->url->link('account/track');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/track.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/account/track.tpl';
            } else {
                    $this->template = 'default/template/account/track.tpl';
            }

            $this->children = array(
                    'common/column_left',
                    'common/column_right',
                    'common/content_top',
                    'common/content_bottom',
                    'common/footer',
                    'common/header'		
            );

            $this->response->setOutput($this->render());
  	}
}
?>