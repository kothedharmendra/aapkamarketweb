<?php
class ModelModuleFeedback extends Model {
	public function create(){
		$sq = "
				CREATE TABLE IF NOT EXISTS `".DB_PREFIX."feedback` (
				  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
				  `author` varchar(255) DEFAULT NULL,
				  `email` varchar(255) DEFAULT NULL,
				  `text` varchar(255) DEFAULT NULL,
				  `date_added` date DEFAULT NULL,
				  PRIMARY KEY (`feedback_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
		";
		$query = $this->db->query( $sq );
		return true;
	}
        
	public function addFeedback($data) {	
		$this->db->query("INSERT INTO " . DB_PREFIX . "feedback SET author = '" . $this->db->escape($data['author']) . "', email = '" . $this->db->escape($data['email']) . "', text = '" . $this->db->escape(strip_tags($data['text'])) . "', date_added = NOW()");
		                
                // HTML Mail
		$template = new Template();
                
                $template->data = $data;
               
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/feedback.tpl')) {
                        $html = $template->fetch($this->config->get('config_template') . '/template/mail/feedback.tpl');
                } else {
                        $html = $template->fetch('default/template/mail/feedback.tpl');
                }

                //subject 
                $subject = 'New Feedback From '.$data['author'];
                
                $mail = new Mail(); 
                $mail->protocol = $this->config->get('config_mail_protocol');
                $mail->parameter = $this->config->get('config_mail_parameter');
                $mail->hostname = $this->config->get('config_smtp_host');
                $mail->username = $this->config->get('config_smtp_username');
                $mail->password = $this->config->get('config_smtp_password');
                $mail->port = $this->config->get('config_smtp_port');
                $mail->timeout = $this->config->get('config_smtp_timeout');			
                $mail->setTo($this->config->get('config_email'));
                $mail->setFrom($data['email']);
                $mail->setSender($data['author']);
                $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
                $mail->setHtml($html);
                $mail->setText(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
                $mail->send();

		return true;
	}
}
?>