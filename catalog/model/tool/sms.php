<?php
class ModelToolSms extends Model {	
    public function send($to,$msg,$name=null, $orderId=null)
	{
        
        $sender_id = $this->config->get('config_sms_sender_id');
        $username  = $this->config->get('config_sms_username');
        $password  = $this->config->get('config_sms_password');
		if($orderId!='' && $orderId!=NULL){
			$order_status_query = $this->db->query("SELECT pick_date, time_slot FROM  order_history WHERE order_id= $orderId");
			if ($order_status_query->num_rows) {
				//$picDate = date('d F, Y',strtotime($order_status_query->row['pick_date']));
				$picDate = $order_status_query->row['pick_date'];
				$picTime = explode('-', $order_status_query->row['time_slot']);
				$picFrom = trim($picTime[0]);
				$picTo = trim($picTime[1]);
				$too = $to.',9178978601,8855866672,9890267215';
				$orderId = '#'.$orderId;
			}
		}
		
		$url = 'http://www.aishinfotech.net/sms/smsgateway.php?username='.urlencode($username).'&password='.urlencode($password).'&destination='.urlencode($too).'&template_name=apkamarketorderplaced&templateParameters[A]='.urlencode($orderId).'&templateParameters[B]='.urlencode($picDate).'&templateParameters[C]='.urlencode($picFrom).'&templateParameters[D]='.urlencode($picTo).'&response_format=json&sender_id='.urlencode($sender_id);
		
		if($orderId!=''){
			// Get cURL resource
			$curl = curl_init();
			// Set some options - we are passing in a useragent too here
			curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $url,
				CURLOPT_USERAGENT => 'Codular Sample cURL Request'
			));
			// Send the request & save response to $resp
			$resp = curl_exec($curl);
			// Close request to clear up some resources
			curl_close($curl);

			return $resp;
		}
    }
}
?>