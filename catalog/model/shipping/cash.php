<?php
class ModelShippingCash extends Model {
	function getQuote($address) {
		$this->load->language('shipping/cash');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('cash_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
	
		if (!$this->config->get('cash_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();
	
		if ($status) {
			$quote_data = array();
			
			/* JIPL */
			if($_SESSION['subtotal']<100) {
				$quote_data['cash'] = array(
					'code'         => 'cash.cash',
					'title'        => $this->language->get('text_description'),
					//'cost'         => 10,
					'cost'         => $this->config->get('cash_cost'),
					'tax_class_id' => $this->config->get('cash_tax_class_id'),
					'text'         => $this->currency->format($this->tax->calculate($this->config->get('cash_cost'), $this->config->get('cash_tax_class_id'), $this->config->get('config_tax')))
				); 
			} else { 
			
				$quote_data['cash'] = array(
        		'code'         => 'cash.cash',
        		'title'        => $this->language->get('text_description'),
				//'cost'         => $this->config->get('cash_cost'),
				'cost'         => 0,
        		'tax_class_id' => $this->config->get('cash_tax_class_id'),
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('cash_cost'), $this->config->get('cash_tax_class_id'), $this->config->get('config_tax')))
      		); 
			
			}
			/* JIPL */

      		$method_data = array(
        		'code'       => 'cash',
        		'title'      => $this->language->get('text_title'),
        		'quote'      => $quote_data,
				'sort_order' => $this->config->get('cash_sort_order'),
        		'error'      => false
      		);
		}
	
		return $method_data;
	}
}
?>