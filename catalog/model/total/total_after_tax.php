<?php
class ModelTotalTotalAfterTax extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		$this->load->language('total/total_after_tax');
		
		$total_data[] = array( 
			'code'       => 'total_after_tax',
			'title'      => $this->language->get('text_sub_total'),
			'text'       => $this->currency->format($total),
			'value'      => $total,
			'sort_order' => $this->config->get('total_after_tax_sort_order')
		);
		
		//$total += $sub_total;
	}
}
?>