<?php
// Heading
$_['heading_title']       = 'Suggestion';

// Text
$_['text_success']      = 'Selected Suggestions deleted successfully';
$_['button_refresh'] = 'Refresh';
// Column

$_['column_author']     = 'Author';
$_['column_email']     = 'Email';
$_['column_text']     = 'Suggestion';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';

// Entry

$_['entry_author']      = 'Author:';
$_['entry_email']      = 'Email:';
$_['entry_text']        = 'Text:';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify reviews!';

$_['error_author']      = 'Author must be between 3 and 64 characters!';
$_['error_text']        = 'Suggestion Text must be at least 1 character!';
$_['error_email']      = 'Email required!';
?>
