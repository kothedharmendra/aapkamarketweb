<?php
// Heading
$_['heading_title']    		= 'Suggestions/Feedback';
$_['text_success']			= 'Module settings updated!';
// Text
$_['text_namefield']        = 'Field name';
$_['text_namefieldfaq']     = 'Enter field name';
$_['text_maskfaq']          = 'Enter the mask in the format +79999999 for example, where the number 9 is the symbol of the mask. The mask can be used to create the output of input phone numbers';
$_['text_addmask']		    = 'Add mask';
$_['text_dellmask']		    = 'Remove mask';
$_['text_dellfield']        = 'Delete';
$_['text_addfield']         = 'Add field';
$_['text_namemodule']       = 'Module name';
$_['text_namemodulefaq']    = 'Enter the name of the module header';
$_['text_nameemail']        = 'Your email';
$_['text_nameemailfaq']     = 'Enter the email that will receive notifications about new posts';
$_['text_namecomment']      = 'The comments field';
$_['text_namecommentfaq']  	= 'If the status is active, this field will be displayed in the form';
$_['button_add_module']   	= 'Add new module';
$_['text_nameonlytext']    	= 'Only text';
$_['text_nameonlytextfaq'] 	= 'If the status is active, this field will only display the field name as text';
$_['text_namedatatime']    	= 'Data/Time';
$_['text_namedatatimefaq'] 	= 'If the status is active, this field is used to enter the date and time';
$_['text_namerequired']    	= 'Required';
$_['text_namerequiredfaq'] 	= 'If the status is active, this field will be checked for null values';
$_['text_namemainfield']    = 'Key field';
$_['text_namemainfieldfaq'] = 'If the status is active, this field will be a key';
$_['text_nameaction'] 	    = 'Action';
$_['text_copyright']        = 'FeedBack Module';
$_['text_module']           = 'Modules';

// Error
$_['error_permission']    = 'You are not authorized to change the module Feedback!';

?>