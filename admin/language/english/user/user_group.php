<?php
// Heading
$_['heading_title']     = 'Authority Group';

// Text
$_['text_success']      = 'Success: You have modified authority groups!';

// Column
$_['column_name']       = 'Authority Group Name';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Authority Group Name:';
$_['entry_access']      = 'Access Permission:';
$_['entry_modify']      = 'Modify Permission:';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify authority groups!';
$_['error_name']        = 'Authority Group Name must be between 3 and 64 characters!';
$_['error_user']        = 'Warning: This authority group cannot be deleted as it is currently assigned to %s authorities!';
?>
