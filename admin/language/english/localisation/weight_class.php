<?php
// Heading
$_['heading_title']    = 'Units';

// Text
$_['text_success']     = 'Success: You have modified units!';

// Column
$_['column_title']     = 'Title';
$_['column_unit']      = 'Unit';
$_['column_value']     = 'Value';
$_['column_action']    = 'Action';

// Entry
$_['entry_title']      = 'Title:';
$_['entry_unit']       = 'Unit:';
$_['entry_value']      = 'Value:<br /><span class="help">Set to 1.00000 if this is your default unit.</span>';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify units!';
$_['error_title']      = 'Title must be between 3 and 32 characters!';
$_['error_unit']       = 'Unit must be between 1 and 4 characters!';
$_['error_default']    = 'Warning: This unit cannot be deleted as it is currently assigned as the default store unit!';
$_['error_product']    = 'Warning: This unit cannot be deleted as it is currently assigned to %s products!';
?>