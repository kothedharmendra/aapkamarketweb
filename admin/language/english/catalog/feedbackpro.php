<?php
// Heading
$_['heading_title']     = 'Suggestions/Feedbacks';

// Text
$_['text_success']      = 'Success: You have modified Feedback list!';
$_['text_view']      	= 'View';
$_['text_back']      	= 'Back';

// Column
$_['column_mainfild']    = 'Main field';
$_['column_name']		 = 'Form name';
$_['column_action']      = 'Action';
$_['column_date_added']  = 'Date added';

// Error 
$_['error_permission']   = 'Warning: You do not have permission to modify Feedback list!';
?>
