<?php
// Heading
$_['heading_title']      = 'Brand';

// Text
$_['text_success']       = 'Success: You have modified brands!';
$_['text_default']       = 'Default';
$_['text_image_manager'] = 'Image Manager';
$_['text_browse']        = 'Browse Files';
$_['text_clear']         = 'Clear Image';
$_['text_percent']       = 'Percentage';
$_['text_amount']        = 'Fixed Amount';

// Column
$_['column_name']        = 'Brand Name';
$_['column_sort_order']  = 'Sort Order';
$_['column_action']      = 'Action';

// Entry
$_['entry_name']         = 'Brand Name:';
$_['entry_store']        = 'Stores:';
$_['entry_keyword']      = 'SEO Keyword:<br /><span class="help">Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.</span>';
$_['entry_image']        = 'Image:';
$_['entry_sort_order']   = 'Sort Order:';
$_['entry_type']         = 'Type:';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify brands!';
$_['error_name']         = 'Brand Name must be between 3 and 64 characters!';
$_['error_product']      = 'Warning: This brand cannot be deleted as it is currently assigned to %s products!';
?>
