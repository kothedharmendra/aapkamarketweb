<?php
class ModelLocalisationCity extends Model {
    
    public function getCityOptions(){
        return $this->db->query('select * from city WHERE status = 1 order by sort_order')->rows;        
    }
    
	public function addCity($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "city SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', sort_order = '" . $this->db->escape($data['sort_order']) . "'");
			
		$this->cache->delete('city');
	}
	
	public function editCity($city_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "city SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', sort_order = '" . $this->db->escape($data['sort_order']) . "' WHERE city_id = '" . (int)$city_id . "'");

		$this->cache->delete('city');
	}
	
	public function deleteCity($city_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "city WHERE city_id = '" . (int)$city_id . "'");

		$this->cache->delete('city');	
	}
	
	public function getCity($city_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city WHERE city_id = '" . (int)$city_id . "'");
		
		return $query->row;
	}
	
	public function getCities($data = array()) {
		$sql = "SELECT * from " . DB_PREFIX . "city";
			
		$sort_data = array(
			'name',
                	'status',    
			'sort_order'
		);	
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		
			
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getCitiesByCountryId($country_id) {
		$city_data = $this->cache->get('city.' . (int)$country_id);
	
		if (!$city_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city WHERE country_id = '" . (int)$country_id . "' ORDER BY name");
	
			$city_data = $query->rows;
			
			$this->cache->set('city.' . (int)$country_id, $city_data);
		}
	
		return $city_data;
	}
	
	public function getTotalCities() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "city");
		
		return $query->row['total'];
	}
				
	public function getTotalCitiesByCountryId($country_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "city WHERE country_id = '" . (int)$country_id . "'");
	
		return $query->row['total'];
	}
}
?>