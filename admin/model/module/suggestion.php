<?php
class ModelModuleSuggestion extends Model {
	
	public function deleteFeedback($suggestion_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "suggestion WHERE suggestion_id = '" . (int)$suggestion_id . "'");
		$this->cache->delete('product');
	}
	
        public function getReviewTotal($data = array()){
            $sql = "SELECT count(suggestion_id) as total FROM " . DB_PREFIX . "suggestion";
            $query = $this->db->query($sql);																		
            return $query->row['total'];	 
        }
        
	public function getReviews($data = array()) {
		$sql = "SELECT suggestion_id, author, email, text, date_added FROM " . DB_PREFIX . "suggestion";
		$sort_data = array(
			'author',
			'email',
			'text',
			'date_added'
		);	
			
		$sql .= " ORDER BY date_added DESC";	
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}																													 
		$query = $this->db->query($sql);																		
		return $query->rows;	
	}
	
}
?>