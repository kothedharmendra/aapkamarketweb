<?php
class ModelModuleFeedback extends Model {
	
	public function deleteFeedback($feedback_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "feedback WHERE feedback_id = '" . (int)$feedback_id . "'");
		$this->cache->delete('product');
	}
	
        public function getReviewTotal($data = array()){
            $sql = "SELECT count(feedback_id) as total FROM " . DB_PREFIX . "feedback";
            $query = $this->db->query($sql);																		
            return $query->row['total'];	 
        }
        
	public function getReviews($data = array()) {
            $sql = "SELECT feedback_id, author, email, text, date_added FROM " . DB_PREFIX . "feedback";
            $sort_data = array(
                    'author',
                    'email',
                    'text',
                    'date_added'
            );	

            $sql .= " ORDER BY date_added DESC";	

            if (isset($data['start']) || isset($data['limit'])) {
                    if ($data['start'] < 0) {
                            $data['start'] = 0;
                    }			

                    if ($data['limit'] < 1) {
                            $data['limit'] = 20;
                    }	

                    $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }		

            $query = $this->db->query($sql);																		
            return $query->rows;	
	}
	
}
?>