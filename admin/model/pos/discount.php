<?php
class ModelPosDiscount extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		
            if(array_key_exists('discount_amount',$this->session->data) && $this->session->data['discount_amount']){   
                
		$discount = $this->session->data['discount_amount'];

                $total_data[] = array( 
                    'code'       => 'discount',
                    'title'      => 'Discount' ,
                    'text'       => $this->currency->format(0-$discount),
                    'value'      => $discount,
                    'sort_order' => $this->config->get('discount_sort_order')
                );

                $total -= $discount;

            }
	}
}
?>