<?php 
$query['pavmegamenu'][]  = "DROP TABLE IF EXISTS `".DB_PREFIX ."megamenu`; ";
$query['pavmegamenu'][]  = "DROP TABLE IF EXISTS `".DB_PREFIX ."megamenu_description`; "; 
$query['pavmegamenu'][]  = "DROP TABLE IF EXISTS `".DB_PREFIX ."megamenu_widgets`; "; 
$query['pavmegamenu'][]  = "	
CREATE TABLE `".DB_PREFIX ."megamenu` (
  `megamenu_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL DEFAULT '',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `is_group` smallint(6) NOT NULL DEFAULT '2',
  `width` varchar(255) DEFAULT NULL,
  `submenu_width` varchar(255) DEFAULT NULL,
  `colum_width` varchar(255) DEFAULT NULL,
  `submenu_colum_width` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `colums` varchar(255) DEFAULT '1',
  `type` varchar(255) NOT NULL,
  `is_content` smallint(6) NOT NULL DEFAULT '2',
  `show_title` smallint(6) NOT NULL DEFAULT '1',
  `type_submenu` varchar(10) NOT NULL DEFAULT '1',
  `level_depth` smallint(6) NOT NULL DEFAULT '0',
  `published` smallint(6) NOT NULL DEFAULT '1',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `position` int(11) unsigned NOT NULL DEFAULT '0',
  `show_sub` smallint(6) NOT NULL DEFAULT '0',
  `url` varchar(255) DEFAULT NULL,
  `target` varchar(25) DEFAULT NULL,
  `privacy` smallint(5) unsigned NOT NULL DEFAULT '0',
  `position_type` varchar(25) DEFAULT 'top',
  `menu_class` varchar(25) DEFAULT NULL,
  `description` text,
  `content_text` text,
  `submenu_content` text,
  `level` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `right` int(11) NOT NULL,
  `widget_id` int(11) DEFAULT '0',
  PRIMARY KEY (`megamenu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

";

$query['pavmegamenu'][]  = "	
CREATE TABLE `".DB_PREFIX ."megamenu_description` (
`megamenu_id` int(11) NOT NULL,
`language_id` int(11) NOT NULL,
`title` varchar(255) NOT NULL,
`description` text NOT NULL,
PRIMARY KEY (`megamenu_id`,`language_id`),
KEY `name` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";

$query['pavmegamenu'][]  = "	
CREATE TABLE `".DB_PREFIX ."megamenu_widgets` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(250) NOT NULL,
`type` varchar(255) NOT NULL,
`params` text NOT NULL,
`store_id` int(11) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

";
	
$query['pavmegamenu'][] = "	
INSERT INTO `".DB_PREFIX ."megamenu` (`megamenu_id`, `image`, `parent_id`, `is_group`, `width`, `submenu_width`, `colum_width`, `submenu_colum_width`, `item`, `colums`, `type`, `is_content`, `show_title`, `type_submenu`, `level_depth`, `published`, `store_id`, `position`, `show_sub`, `url`, `target`, `privacy`, `position_type`, `menu_class`, `description`, `content_text`, `submenu_content`, `level`, `left`, `right`, `widget_id`) VALUES
(1, '', 0, 2, NULL, NULL, NULL, NULL, NULL, '1', '', 2, 1, '1', 0, 1, 0, 0, 0, NULL, NULL, 0, 'top', NULL, NULL, NULL, NULL, -5, 34, 47, 0),
(2, '', 1, 0, NULL, NULL, NULL, 'col1=4, col2=8', '20', '2', 'category', 0, 1, 'menu', 0, 0, 0, 2, 0, '', NULL, 0, 'top', 'multi-menu', NULL, '', '', 0, 0, 0, 1),
(3, '', 1, 0, NULL, NULL, NULL, '', '18', '1', 'category', 0, 1, 'menu', 0, 1, 0, 5, 0, '', NULL, 0, 'top', 'pav-parrent', NULL, '', '', 0, 0, 0, 1),
(4, '', 3, 0, NULL, NULL, NULL, '', '25', '1', 'category', 0, 1, 'menu', 0, 1, 0, 1, 0, '', NULL, 0, 'top', 'pav-parrent', NULL, '', '', 0, 0, 0, 1),
(5, '', 1, 0, NULL, NULL, NULL, '', '17', '1', 'category', 0, 1, 'menu', 0, 1, 0, 3, 0, '', NULL, 0, 'top', 'multi-menu', NULL, '', '', 0, 0, 0, 1),
(7, '', 1, 0, NULL, NULL, NULL, '', '33', '1', 'category', 0, 1, 'menu', 0, 1, 0, 4, 0, '', NULL, 0, 'top', 'multi-menu', NULL, '', '', 0, 0, 0, 1),
(8, '', 2, 1, NULL, NULL, NULL, '', '27', '1', 'category', 0, 1, 'menu', 0, 1, 0, 1, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '&lt;p&gt;test&lt;/p&gt;\r\n', 0, 0, 0, 1),
(9, '', 2, 1, NULL, NULL, NULL, '', '26', '1', 'category', 0, 1, 'menu', 0, 0, 0, 2, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(10, '', 8, 0, NULL, NULL, NULL, '', '59', '1', 'category', 0, 1, 'menu', 0, 1, 0, 1, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(11, '', 8, 0, NULL, NULL, NULL, '', '60', '1', 'category', 0, 1, 'menu', 0, 1, 0, 2, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(12, '', 8, 0, NULL, NULL, NULL, '', '61', '1', 'category', 0, 1, 'menu', 0, 1, 0, 3, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(13, '', 8, 0, NULL, NULL, NULL, '', '62', '1', 'category', 0, 1, 'menu', 0, 1, 0, 4, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(14, '', 8, 0, NULL, NULL, NULL, '', '63', '1', 'category', 0, 1, 'menu', 0, 1, 0, 5, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(15, '', 8, 0, NULL, NULL, NULL, '', '64', '1', 'category', 0, 1, 'menu', 0, 1, 0, 6, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(16, '', 8, 0, NULL, NULL, NULL, '', '65', '1', 'category', 0, 1, 'menu', 0, 1, 0, 7, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(17, '', 9, 0, NULL, NULL, NULL, '', '66', '1', 'category', 0, 1, 'menu', 0, 1, 0, 1, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(18, '', 9, 0, NULL, NULL, NULL, '', '67', '1', 'category', 0, 1, 'menu', 0, 1, 0, 2, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(19, '', 9, 0, NULL, NULL, NULL, '', '68', '1', 'category', 0, 1, 'menu', 0, 1, 0, 3, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(20, '', 9, 0, NULL, NULL, NULL, '', '71', '1', 'category', 0, 1, 'menu', 0, 1, 0, 4, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(21, '', 9, 0, NULL, NULL, NULL, '', '72', '1', 'category', 0, 1, 'menu', 0, 1, 0, 5, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(22, '', 9, 0, NULL, NULL, NULL, '', '69', '1', 'category', 0, 1, 'menu', 0, 1, 0, 6, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(23, '', 9, 0, NULL, NULL, NULL, '', '70', '1', 'category', 0, 1, 'menu', 0, 1, 0, 7, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0, 1),
(24, '', 2, 0, NULL, NULL, NULL, '', '', '1', 'html', 1, 1, 'menu', 0, 1, 0, 3, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '&lt;div class=&quot;pav-menu-video&quot;&gt;&lt;embed height=&quot;157&quot; src=&quot;http://www.youtube.com/v/EoydBE9LNm0&quot; type=&quot;application/x-shockwave-flash&quot; width=&quot;280&quot;&gt;&lt;/embed&gt;\r\n&lt;h3&gt;Lorem ipsum dolor sit&lt;/h3&gt;\r\n\r\n&lt;p&gt;Dorem ipsum dolor sit amet consectetur adipiscing elit congue sit amet erat roin tincidunt vehicula lorem in adipiscing urna iaculis vel.&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 0, 0, 0, 1),
(25, '', 3, 0, NULL, NULL, NULL, '', '79', '1', 'category', 0, 1, 'menu', 0, 1, 0, 2, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(26, '', 3, 0, NULL, NULL, NULL, '', '74', '1', 'category', 0, 1, 'menu', 0, 1, 0, 3, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(27, '', 3, 0, NULL, NULL, NULL, '', '73', '1', 'category', 0, 1, 'menu', 0, 1, 0, 4, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(28, '', 3, 0, NULL, NULL, NULL, '', '80', '1', 'category', 0, 1, 'menu', 0, 1, 0, 5, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(29, '', 3, 0, NULL, NULL, NULL, '', '', '1', 'category', 0, 1, 'menu', 0, 1, 0, 6, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(30, '', 3, 0, NULL, NULL, NULL, '', '46', '1', 'category', 0, 1, 'menu', 0, 1, 0, 7, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(31, '', 3, 0, NULL, NULL, NULL, '', '75', '1', 'category', 0, 1, 'menu', 0, 1, 0, 8, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(32, '', 3, 0, NULL, NULL, NULL, '', '78', '1', 'category', 0, 1, 'menu', 0, 1, 0, 9, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(33, '', 3, 0, NULL, NULL, NULL, '', '77', '1', 'category', 0, 1, 'menu', 0, 1, 0, 10, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(34, '', 3, 0, NULL, NULL, NULL, '', '77', '1', 'category', 0, 1, 'menu', 0, 1, 0, 11, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(35, '', 3, 0, NULL, NULL, NULL, '', '45', '1', 'category', 0, 1, 'menu', 0, 1, 0, 12, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(36, '', 3, 0, NULL, NULL, NULL, '', '45', '1', 'category', 0, 1, 'menu', 0, 1, 0, 13, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(37, '', 1, 0, NULL, NULL, NULL, '', '25', '1', 'category', 0, 1, 'menu', 0, 1, 0, 6, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(38, '', 1, 0, NULL, NULL, NULL, '', '57', '1', 'category', 0, 1, 'menu', 0, 0, 0, 7, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(40, '', 1, 0, NULL, NULL, NULL, '', '', '1', 'url', 0, 1, 'menu', 0, 1, 0, 1, 0, '?route=common/home', NULL, 0, 'top', 'home', NULL, '', '', 0, 0, 0, 1),
(41, '', 1, 0, NULL, NULL, NULL, '', '', '1', 'url', 0, 1, 'menu', 0, 1, 0, 99, 0, 'index.php?route=pavdeals/deals', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1),
(42, '', 1, 0, NULL, NULL, NULL, '', '', '1', 'url', 0, 1, 'menu', 0, 1, 0, 99, 0, '?route=pavblog/blogs', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0, 1);

";


$query['pavmegamenu'][] = "	
INSERT INTO `".DB_PREFIX ."megamenu_description` (`megamenu_id`, `language_id`, `title`, `description`) VALUES
(2, 4, 'من أما المتّبعة', ''),
(3, 4, 'للإمبراطورية ', ''),
(3, 1, 'Digital', ''),
(4, 1, 'Musical Instruments', ''),
(2, 1, 'Electronics', ''),
(5, 4, 'للإمبراطورية ', ''),
(37, 1, 'Watches', ''),
(8, 4, 'الألماني إذ.', ''),
(9, 4, 'Printer', ''),
(8, 1, 'Computers', ''),
(11, 4, 'بالمطالبة بالولايات', ''),
(12, 4, 'جديدة المذابح', ''),
(13, 1, 'Electronics Accessories', ''),
(14, 1, 'Office Products', ''),
(15, 1, 'Software', ''),
(16, 1, 'Video Games &amp; Consoles', ''),
(9, 1, 'Printer', ''),
(17, 1, 'Electronics Accessories', ''),
(18, 1, 'Musical Instruments', ''),
(19, 4, ' في, ثم يذكر', ''),
(19, 1, 'Office Products', ''),
(20, 1, 'Vehicle, GPS &amp; Navigation', ''),
(21, 1, 'Trade In Electronics', ''),
(22, 1, 'Video Games &amp; Consoles', ''),
(23, 1, 'Trade In Electronics', ''),
(24, 1, 'Lorem ipsum dolor sit ', ''),
(37, 4, 'طوكيو للإمبرا', ''),
(25, 1, 'Vehicle, GPS &amp; Navigation', ''),
(26, 1, 'Trade In Electronics', ''),
(27, 1, 'Electronics Accessories', ''),
(28, 1, 'Computers &amp; Tablets', ''),
(29, 4, 'عدد ثم تجهيز الجنرال بالدبابات', ''),
(30, 1, 'Home &amp; Portable Audio', ''),
(31, 1, 'Cell Phones &amp; Services', ''),
(32, 1, 'Camera &amp; Video', ''),
(33, 1, 'Vehicle, GPS &amp; Navigation', ''),
(34, 1, 'Musical Instruments', ''),
(35, 1, 'Video Games &amp; Consoles', ''),
(36, 1, 'Office Products', ''),
(38, 4, 'للإمبراطورية', ''),
(40, 1, 'Home', ''),
(7, 4, 'للإمبراطورية ', ''),
(5, 1, 'Books', ''),
(24, 4, 'للإمبراطورية في مدن, هذا', ''),
(41, 1, 'Deals', ''),
(42, 4, 'الجنرال با', ''),
(42, 1, 'Blog', ''),
(36, 4, 'وصل. ولم ٣٠', ''),
(38, 1, 'Tablets', ''),
(41, 4, 'ثم تجهيز الجنر', ''),
(12, 1, 'Trade In Electronics', ''),
(11, 1, 'Office Products', ''),
(10, 1, 'Electronics Accessories', ''),
(40, 4, 'الإمبراطورية.', ''),
(28, 4, 'لمّ أم. طوكيو للإمبراطورية', ''),
(29, 1, 'TV &amp; Home Theater', ''),
(30, 4, 'ثم تصرّف الغالي سنغافورة.', ''),
(31, 4, 'للغزو، والحلفاء غير.', ''),
(32, 4, 'ان مئات الجو مشقّة ضرب.', ''),
(33, 4, 'اقتصادية أم بال, ', ''),
(34, 4, 'الطائرات عن. النفط ', ''),
(35, 4, 'لفرنسا استطاعوا, هو سبتمبر', ''),
(27, 4, 'من أما المتّبعة استطاعوا,', ''),
(26, 4, 'من أما المتّبعة استطاعوا,', ''),
(25, 4, 'في مدن, هذا جديدة المذابح', ''),
(4, 4, 'هو سبتمبر المنتصر ', ''),
(16, 4, 'النفط الجنرال بال', ''),
(15, 4, 'في أواخر كُلفة لإعلان الا', ''),
(14, 4, 'حول. وجزر تنفّس أفريقيا', ''),
(13, 4, 'حادثة دول, ان مئات الجو', ''),
(10, 4, 'هذا جديدة ', ''),
(23, 4, 'من اعلان مقاطعة واندونيسيا،', ''),
(22, 4, 'جوي إذ سلاح الخاصّة.', ''),
(21, 4, 'هو سبتمبر المنتصر الجنرال كان', ''),
(20, 4, 'دول لفرنسا استطا', ''),
(18, 4, 'دول لفرنسا استطا', ''),
(17, 4, 'دول, ان مئات الجو مشقّة', ''),
(7, 1, 'Office', '');

";
 

$query['pavmegamenu'][] =  " 
INSERT INTO `".DB_PREFIX ."megamenu_widgets` (`id`, `name`, `type`, `params`, `store_id`) VALUES
(1, 'Video Opencart Installation', 'video_code', 'a:1:{s:10:\"video_code\";s:168:\"&lt;iframe width=&quot;300&quot; height=&quot;315&quot; src=&quot;//www.youtube.com/embed/cUhPA5qIxDQ&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;\";}', 0),
(2, 'Demo HTML Sample', 'html', 'a:1:{s:4:\"html\";a:2:{i:1;s:303:\"&lt;p&gt; Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidun auctor a ornare odio. &lt;/p&gt;\r\n&lt;p&gt; Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia &lt;/p&gt;\";i:4;s:614:\"&lt;p&gt; مما خيار وهولندا، بريطانيا تم. لإعلان الشتاء لمّ أم. طوكيو للإمبراطورية في مدن, هذا جديدة المذابح الألماني إذ. تزامناً الإقتصادي حدة و, حتى بل وقبل أسلحته  &lt;/p&gt;\r\n&lt;p&gt;\r\n	بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, \r\n&lt;/p&gt;\";}}', 0),
(3, 'Products Latest', 'product_list', 'a:4:{s:9:\"list_type\";s:6:\"newest\";s:5:\"limit\";s:1:\"1\";s:11:\"image_width\";s:3:\"150\";s:12:\"image_height\";s:3:\"150\";}', 0),
(4, 'Products In Cat 20', 'product_category', 'a:4:{s:11:\"category_id\";s:2:\"20\";s:5:\"limit\";s:1:\"1\";s:11:\"image_width\";s:3:\"150\";s:12:\"image_height\";s:3:\"150\";}', 0),
(5, 'Manufactures', 'banner', 'a:4:{s:8:\"group_id\";s:1:\"8\";s:11:\"image_width\";s:2:\"80\";s:12:\"image_height\";s:2:\"80\";s:5:\"limit\";s:2:\"12\";}', 0),
(6, 'PavoThemes Feed', 'feed', 'a:1:{s:8:\"feed_url\";s:55:\"http://www.pavothemes.com/opencart-themes.feed?type=rss\";}', 0),
(7, 'Image Sub Category', 'image', 'a:3:{s:10:\"image_path\";s:32:\"data/demo/image-sub-megamenu.jpg\";s:11:\"image_width\";s:3:\"248\";s:12:\"image_height\";s:3:\"178\";}', 0),
(8, 'Information', 'html', 'a:1:{s:4:\"html\";a:2:{i:1;s:638:\"&lt;ul&gt;\r\n	&lt;li class=&quot;first&quot;&gt;&lt;a href=&quot;index.php?route=information/information&amp;amp;information_id=4&quot;&gt;About Us&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;index.php?route=information/information&amp;amp;information_id=6&quot;&gt;Delivery Information&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;index.php?route=information/information&amp;amp;information_id=3&quot;&gt;Privacy Policy&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li class=&quot;last&quot;&gt;&lt;a href=&quot;index.php?route=information/information&amp;amp;information_id=5&quot;&gt;Terms &amp;amp; Conditions&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\";i:4;s:731:\"&lt;ul&gt;\r\n	&lt;li class=&quot;first&quot;&gt;&lt;a href=&quot;index.php?route=information/information&amp;amp;information_id=4&quot;&gt;الشتاء لمّ أم. طوكيو&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;index.php?route=information/information&amp;amp;information_id=6&quot;&gt;المذابح الألماني&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;index.php?route=information/information&amp;amp;information_id=3&quot;&gt;عدد ثم تجهيز الجنرا&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li class=&quot;last&quot;&gt;&lt;a href=&quot;index.php?route=information/information&amp;amp;information_id=5&quot;&gt;المتّبعة استطاعوا, حول مكّن كردة&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\";}}', 0);

";


$query['pavmegamenu'][]  = "DELETE FROM `".DB_PREFIX ."setting` WHERE `group`='pavmegamenu' and `key` = 'pavmegamenu_module'";
$query['pavmegamenu'][] =  " 
INSERT INTO `".DB_PREFIX ."setting` (`setting_id`, `store_id`, `group`, `key`, `value`, `serialized`) VALUES
(0, 0, 'pavmegamenu', 'pavmegamenu_module', 'a:1:{i:0;a:4:{s:9:\"layout_id\";s:5:\"99999\";s:8:\"position\";s:8:\"mainmenu\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";i:1;}}', 1);";

$query['pavmegamenu'][]  = "DELETE FROM `".DB_PREFIX ."setting` WHERE `group`='pavmegamenu_params' and `key` = 'params'";
$query['pavmegamenu'][] =  " 
INSERT INTO `".DB_PREFIX ."setting` (`setting_id`, `store_id`, `group`, `key`, `value`, `serialized`) VALUES
(0, 0, 'pavmegamenu_params', 'params', '[{\"submenu\":1,\"cols\":3,\"group\":0,\"id\":2,\"rows\":[{\"cols\":[{\"colwidth\":6,\"type\":\"menu\"},{\"colwidth\":6,\"type\":\"menu\"},{\"colwidth\":6,\"type\":\"menu\"}]}]},{\"submenu\":1,\"cols\":1,\"group\":1,\"id\":8,\"rows\":[]},{\"submenu\":1,\"cols\":1,\"group\":1,\"id\":9,\"rows\":[]},{\"submenu\":1,\"subwidth\":730,\"cols\":1,\"group\":0,\"id\":5,\"rows\":[{\"cols\":[{\"widgets\":\"wid-7\",\"colwidth\":4},{\"widgets\":\"\",\"colwidth\":4},{\"widgets\":\"\",\"colwidth\":3},{\"widgets\":\"wid-8\",\"colwidth\":4},{\"widgets\":\"wid-2\",\"colwidth\":4}]}]},{\"submenu\":\"1\",\"id\":7,\"subwidth\":500,\"cols\":2,\"group\":0,\"rows\":[{\"cols\":[{\"colwidth\":6,\"widgets\":\"wid-3\"},{\"colwidth\":6,\"widgets\":\"wid-4\"}]}]},{\"submenu\":1,\"cols\":1,\"group\":0,\"id\":3,\"rows\":[{\"cols\":[{\"colwidth\":12,\"type\":\"menu\"}]}]}]', 0);";











$query['pavblog'][]  = "  DROP TABLE IF EXISTS `".DB_PREFIX ."pavblog_blog`; ";
$query['pavblog'][]  = "   DROP TABLE IF EXISTS `".DB_PREFIX ."pavblog_blog_description`; ";
$query['pavblog'][]  = "  DROP TABLE IF EXISTS `".DB_PREFIX ."pavblog_category`; ";
$query['pavblog'][]  = "   DROP TABLE IF EXISTS `".DB_PREFIX ."pavblog_category_description`; ";
$query['pavblog'][]  = "   DROP TABLE IF EXISTS `".DB_PREFIX ."pavblog_comment`; ";


$query['pavblog'][]  = "	
CREATE TABLE `".DB_PREFIX ."pavblog_blog` (
`blog_id` int(11) NOT NULL AUTO_INCREMENT,
`category_id` int(11) NOT NULL,
`position` int(11) NOT NULL,
`created` date NOT NULL,
`status` tinyint(1) NOT NULL,
`user_id` int(11) NOT NULL,
`hits` int(11) NOT NULL,
`image` varchar(255) NOT NULL,
`meta_keyword` varchar(255) NOT NULL,
`meta_description` varchar(255) NOT NULL,
`meta_title` varchar(255) NOT NULL,
`date_modified` date NOT NULL,
`video_code` varchar(255) NOT NULL,
`params` text NOT NULL,
`tags` varchar(255) NOT NULL,
`featured` tinyint(1) NOT NULL,
`keyword` varchar(255) NOT NULL,
PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;
";

$query['pavblog'][]  = "	
CREATE TABLE `".DB_PREFIX ."pavblog_blog_description` (
`blog_id` int(11) NOT NULL,
`language_id` int(11) NOT NULL,
`title` varchar(255) NOT NULL,
`description` text NOT NULL,
`content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";

$query['pavblog'][]  = "	
CREATE TABLE `".DB_PREFIX ."pavblog_category` (
`category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`image` varchar(255) NOT NULL DEFAULT '',
`parent_id` int(11) NOT NULL DEFAULT '0',
`is_group` smallint(6) NOT NULL DEFAULT '2',
`width` varchar(255) DEFAULT NULL,
`submenu_width` varchar(255) DEFAULT NULL,
`colum_width` varchar(255) DEFAULT NULL,
`submenu_colum_width` varchar(255) DEFAULT NULL,
`item` varchar(255) DEFAULT NULL,
`colums` varchar(255) DEFAULT '1',
`type` varchar(255) NOT NULL,
`is_content` smallint(6) NOT NULL DEFAULT '2',
`show_title` smallint(6) NOT NULL DEFAULT '1',
`meta_keyword` varchar(255) NOT NULL DEFAULT '1',
`level_depth` smallint(6) NOT NULL DEFAULT '0',
`published` smallint(6) NOT NULL DEFAULT '1',
`store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
`position` int(11) unsigned NOT NULL DEFAULT '0',
`show_sub` smallint(6) NOT NULL DEFAULT '0',
`url` varchar(255) DEFAULT NULL,
`target` varchar(25) DEFAULT NULL,
`privacy` smallint(5) unsigned NOT NULL DEFAULT '0',
`position_type` varchar(25) DEFAULT 'top',
`menu_class` varchar(25) DEFAULT NULL,
`description` text,
`meta_description` text,
`meta_title` varchar(255) DEFAULT NULL,
`level` int(11) NOT NULL,
`left` int(11) NOT NULL,
`right` int(11) NOT NULL,
`keyword` varchar(255) NOT NULL,
PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;  
";
			
$query['pavblog'][]  = "	
CREATE TABLE `".DB_PREFIX ."pavblog_category_description` (
`category_id` int(11) NOT NULL,
`language_id` int(11) NOT NULL,
`title` varchar(255) NOT NULL,
`description` text NOT NULL,
PRIMARY KEY (`category_id`,`language_id`),
KEY `name` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
";	
	
$query['pavblog'][]  = "	
CREATE TABLE `".DB_PREFIX ."pavblog_comment` (
`comment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`blog_id` int(11) unsigned NOT NULL,
`comment` text NOT NULL,
`status` tinyint(1) NOT NULL DEFAULT '0',
`created` datetime DEFAULT NULL,
`user` varchar(255) NOT NULL,
`email` varchar(255) NOT NULL,
PRIMARY KEY (`comment_id`),
KEY `FK_blog_comment` (`blog_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;
";
			

$query['pavblog'][] ="
INSERT INTO `".DB_PREFIX ."pavblog_blog` (`blog_id`, `category_id`, `position`, `created`, `status`, `user_id`, `hits`, `image`, `meta_keyword`, `meta_description`, `meta_title`, `date_modified`, `video_code`, `params`, `tags`, `featured`, `keyword`) VALUES
(7, 21, 2, '2013-03-09', 1, 1, 48, 'data/pavblog/pav-i1.jpg', 'ئرات عن. ا', 'ئرات عن. ا', 'ئرات عن. ا', '2013-10-11', '', '', 'magento. opencart. wordpress', 1, 'opencart'),
(9, 21, 1, '2013-03-09', 1, 1, 75, 'data/pavblog/pav-c5.jpg', '', '', '', '2013-10-11', '', '', 'prestashop, magento', 0, ''),
(10, 21, 4, '2013-03-09', 1, 1, 229, 'data/pavblog/pav-c1.jpg', 'test test', '', 'Custom SEO Titlte', '2013-10-11', '&lt;iframe width=&quot;600&quot; height=&quot;415&quot; src=&quot;//www.youtube.com/embed/wGCetsl-srk&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;', '', 'prestashop', 0, 'joomla, prestashop, magento, opencart'),
(11, 21, 5, '2013-03-11', 1, 1, 96, 'data/pavblog/pav-c4.jpg', 'opencart', 'opencart reponsive theme', 'Custom SEO Titlte', '2013-10-11', '', '', 'opencart', 1, 'joomla, prestashop, magento, opencart'),
(13, 1, 0, '2013-09-20', 1, 1, 157, 'data/pavblog/pav-c2.jpg', 'Custom SEO Titlte', 'Custom SEO Titlte', 'Custom SEO Titlte', '2013-10-11', '', '', 'joomla, prestashop, magento, opencart', 0, 'joomla, prestashop, magento, opencart'),
(14, 23, 0, '2013-10-10', 1, 1, 66, 'data/pavblog/pav-c6.jpg', '', '', '', '2013-10-12', '', '', 'magento. opencart. wordpress', 1, 'Awesome post with everything with it');
"; 	
		
$query['pavblog'][] ="
INSERT INTO `".DB_PREFIX ."pavblog_blog_description` (`blog_id`, `language_id`, `title`, `description`, `content`) VALUES
(10, 1, 'Gucci Heritege ', '&lt;p&gt;Sed a velit vitae nunc porta iaculis. Aliquam tempus rutrum diam, non tincidunt odio pharetra vitae. Nulla facilisi. Duis sed laoreet dui. Duis a risus nec erat tincidunt feugiat. Cras dui velit, luctus vitae volutpat vitae, vestibulum vitae orci.&lt;/p&gt;\r\n\r\n&lt;p&gt;Cras mattis consectetur purus sit amet fermentum. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec ullamcorper nulla non metus auctor fringilla.&lt;/p&gt;\r\n', '&lt;p&gt;Sed a velit vitae nunc porta iaculis. Aliquam tempus rutrum diam, non tincidunt odio pharetra vitae. Nulla facilisi. Duis sed laoreet dui. Duis a risus nec erat tincidunt feugiat. Cras dui velit, luctus vitae volutpat vitae, vestibulum vitae orci.&lt;/p&gt;\r\n\r\n&lt;p&gt;Cras mattis consectetur purus sit amet fermentum. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec ullamcorper nulla non metus auctor fringilla.&lt;/p&gt;\r\n\r\n&lt;p&gt;Cras mattis consectetur purus sit amet fermentum. Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Donec ullamcorper nulla non metus auctor fringilla.&lt;/p&gt;\r\n\r\n&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, illum, velit tenetur nesciunt dignissimos placeat laudantium quaerat ipsum aperiam officia repellendus magni in unde dolor ex deleniti accusantium nemo suscipit odit aliquam quos totam veritatis temporibus quae repudiandae voluptatum doloremque error voluptas sapiente incidunt obcaecati minima culpa ipsa ipsam est.&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n  &lt;li&gt;Cras mattis consectetur purus sit amet fermentum. Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam.&lt;/li&gt;\r\n  &lt;li&gt;Cras mattis consectetur purus sit amet fermentum. Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam.&lt;/li&gt;\r\n  &lt;li&gt;Cras mattis consectetur purus sit amet fermentum. Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam.&lt;/li&gt;\r\n&lt;/ul&gt;\r\n'),
(10, 4, 'ة دون, تلك أي بهيئة وأزيز الدنمارك. الحرب نورمبرغ كل حت', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n', '&lt;p&gt;مما خيار وهولندا، بريطانيا تم. لإعلان الشتاء لمّ أم. طوكيو للإمبراطورية في مدن, هذا جديدة المذابح الألماني إذ. تزامناً الإقتصادي حدة و, حتى بل وقبل أسلحته بريطانيا-فرنسا.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;مع قبضتهم والحزب الشتاء مكن, به، أسيا الطائرات عن. النفط الجنرال بال تم. يكن ودول التّحول بريطانيا في. برلين المدنيون لم ربع.&lt;/p&gt;\r\n\r\n&lt;p&gt;عدم بل عرفها للسيطرة وأكثرها, ثم أطراف الباهضة المؤلّفة دون, تلك أي بهيئة وأزيز الدنمارك. الحرب نورمبرغ كل حتى, حتى الأوضاع والجنود هو. مما العالم عسكرياً الأبرياء ثم, ان يونيو دنكيرك أوكيناوا بعد. بغزو برلين، السوفييتية بال عن, مكن شعار تطوير الأوروبية هو.&lt;/p&gt;\r\n'),
(7, 1, 'Ac tincidunt Suspendisse malesuada', '&lt;p&gt;Ac tincidunt Suspendisse malesuada velit in Nullam elit magnis netus Vestibulum. Praesent Nam adipiscing Aliquam elit accumsan wisi sit semper scelerisque convallis. Sed quisque cum velit&lt;/p&gt;\r\n', '&lt;div class=&quot;itemFullText&quot;&gt;\r\n&lt;p&gt;Commodo laoreet semper tincidunt lorem Vestibulum nunc at In Curabitur magna. Euismod euismod Suspendisse tortor ante adipiscing risus Aenean Lorem vitae id. Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor Aenean nulla lacinia Nullam elit vel vel. At risus pretium urna tortor metus fringilla interdum mauris tempor congue.&lt;/p&gt;\r\n\r\n&lt;p&gt;Donec tellus Nulla lorem Nullam elit id ut elit feugiat lacus. Congue eget dapibus congue tincidunt senectus nibh risus Phasellus tristique justo. Justo Pellentesque Donec lobortis faucibus Vestibulum Praesent mauris volutpat vitae metus. Ipsum cursus vestibulum at interdum Vivamus nunc fringilla Curabitur ac quis. Nam lacinia wisi tortor orci quis vitae.&lt;/p&gt;\r\n\r\n&lt;p&gt;Sed mauris Pellentesque elit Aliquam at lacus interdum nascetur elit ipsum. Enim ipsum hendrerit Suspendisse turpis laoreet fames tempus ligula pede ac. Et Lorem penatibus orci eu ultrices egestas Nam quam Vivamus nibh. Morbi condimentum molestie Nam enim odio sodales pretium eros sem pellentesque. Sit tellus Integer elit egestas lacus turpis id auctor nascetur ut. Ac elit vitae.&lt;/p&gt;\r\n\r\n&lt;p&gt;Mi vitae magnis Fusce laoreet nibh felis porttitor laoreet Vestibulum faucibus. At Nulla id tincidunt ut sed semper vel Lorem condimentum ornare. Laoreet Vestibulum lacinia massa a commodo habitasse velit Vestibulum tincidunt In. Turpis at eleifend leo mi elit Aenean porta ac sed faucibus. Nunc urna Morbi fringilla vitae orci convallis condimentum auctor sit dui. Urna pretium elit mauris cursus Curabitur at elit Vestibulum.&lt;/p&gt;\r\n&lt;/div&gt;\r\n'),
(7, 4, 'م يذكر تكتيكاً بوزيرها حول. وجزر تنفّس أفريقيا بلا', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n', '&lt;p&gt;مما خيار وهولندا، بريطانيا تم. لإعلان الشتاء لمّ أم. طوكيو للإمبراطورية في مدن, هذا جديدة المذابح الألماني إذ. تزامناً الإقتصادي حدة و, حتى بل وقبل أسلحته بريطانيا-فرنسا.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;مع قبضتهم والحزب الشتاء مكن, به، أسيا الطائرات عن. النفط الجنرال بال تم. يكن ودول التّحول بريطانيا في. برلين المدنيون لم ربع.&lt;/p&gt;\r\n\r\n&lt;p&gt;عدم بل عرفها للسيطرة وأكثرها, ثم أطراف الباهضة المؤلّفة دون, تلك أي بهيئة وأزيز الدنمارك. الحرب نورمبرغ كل حتى, حتى الأوضاع والجنود هو. مما العالم عسكرياً الأبرياء ثم, ان يونيو دنكيرك أوكيناوا بعد. بغزو برلين، السوفييتية بال عن, مكن شعار تطوير الأوروبية هو.&lt;/p&gt;\r\n'),
(11, 1, 'Designer Glasses', '&lt;p&gt;Sed a velit vitae nunc porta iaculis. Aliquam tempus rutrum diam, non tincidunt odio pharetra vitae. Nulla facilisi. Duis sed laoreet dui. Duis a risus nec erat tincidunt feugiat. Cras dui velit, luctus vitae volutpat vitae, vestibulum vitae orci.&lt;/p&gt;\r\n\r\n&lt;p&gt;Cras mattis consectetur purus sit amet fermentum. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec ullamcorper nulla non metus auctor fringilla.&lt;/p&gt;\r\n', '&lt;p&gt;Sed a velit vitae nunc porta iaculis. Aliquam tempus rutrum diam, non tincidunt odio pharetra vitae. Nulla facilisi. Duis sed laoreet dui. Duis a risus nec erat tincidunt feugiat. Cras dui velit, luctus vitae volutpat vitae, vestibulum vitae orci.&lt;/p&gt;\r\n\r\n&lt;p&gt;Cras mattis consectetur purus sit amet fermentum. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec ullamcorper nulla non metus auctor fringilla.&lt;/p&gt;\r\n\r\n&lt;p&gt;Cras mattis consectetur purus sit amet fermentum. Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Donec ullamcorper nulla non metus auctor fringilla.&lt;/p&gt;\r\n\r\n&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, illum, velit tenetur nesciunt dignissimos placeat laudantium quaerat ipsum aperiam officia repellendus magni in unde dolor ex deleniti accusantium nemo suscipit odit aliquam quos totam veritatis temporibus quae repudiandae voluptatum doloremque error voluptas sapiente incidunt obcaecati minima culpa ipsa ipsam est.&lt;/p&gt;\r\n\r\n&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, voluptas minima fugit saepe illo nihil ducimus ex deleniti! Vitae, voluptate, numquam, magni, deserunt provident ratione temporibus consectetur unde cumque laboriosam illo eos voluptates veritatis nemo aliquam quis qui optio expedita sit quod repellat. Quasi, possimus, officiis cum quaerat rem a atque facilis vel ipsam fugit optio blanditiis tempora eveniet velit odio repellendus sequi suscipit laudantium alias esse minima similique odit doloremque? Quae, repellat quisquam sit cumque laboriosam dolorem dolores rerum eveniet qui similique. Laboriosam, earum ex dicta quam praesentium ipsa culpa quaerat soluta. Molestiae, illo neque eligendi vel at hic?&lt;/p&gt;\r\n'),
(11, 4, 'لنفط الجنرال بال تم. يكن ودول التّحول بريطانيا في. برلين المدنيون لم ربع.', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n', '&lt;p&gt;مما خيار وهولندا، بريطانيا تم. لإعلان الشتاء لمّ أم. طوكيو للإمبراطورية في مدن, هذا جديدة المذابح الألماني إذ. تزامناً الإقتصادي حدة و, حتى بل وقبل أسلحته بريطانيا-فرنسا.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;مع قبضتهم والحزب الشتاء مكن, به، أسيا الطائرات عن. النفط الجنرال بال تم. يكن ودول التّحول بريطانيا في. برلين المدنيون لم ربع.&lt;/p&gt;\r\n\r\n&lt;p&gt;عدم بل عرفها للسيطرة وأكثرها, ثم أطراف الباهضة المؤلّفة دون, تلك أي بهيئة وأزيز الدنمارك. الحرب نورمبرغ كل حتى, حتى الأوضاع والجنود هو. مما العالم عسكرياً الأبرياء ثم, ان يونيو دنكيرك أوكيناوا بعد. بغزو برلين، السوفييتية بال عن, مكن شعار تطوير الأوروبية هو.&lt;/p&gt;\r\n'),
(13, 1, 'Splinter Cell: Blacklist', '&lt;p&gt;bisoft has released a new screenshot for Splinter Cell: Blacklist. This shot is a close-up of Sam Fisher as he’s about to infiltrate Jadid’s hide-out, and showcases the intricate details on his op suit, including the green light on his back, which indicates.&lt;/p&gt;\r\n', '&lt;p&gt;bisoft has released a new screenshot for Splinter Cell: Blacklist. This shot is a close-up of Sam Fisher as he’s about to infiltrate Jadid’s hide-out, and showcases the intricate details on his op suit, including the green light on his back, which indicates.&lt;/p&gt;\r\n\r\n&lt;p&gt;Cras mattis consectetur purus sit amet fermentum. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Donec ullamcorper nulla non metus auctor fringilla.&lt;/p&gt;\r\n'),
(13, 4, 'ي حدة. ما بحق الأثنان المقاومة الإمبراطورية.', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n'),
(9, 1, ' Best eyewear trends for man 2013', '&lt;p&gt;Commodo laoreet semper tincidunt lorem Vestibulum nunc at In Curabitur magna. Euismod euismod Suspendisse tortor ante adipiscing risus Aenean Lorem vitae id. Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla.&lt;/p&gt;\r\n\r\n&lt;p&gt;Accumsan morbi tristique auctor Aenean nulla lacinia Nullam elit vel vel. At risus pretium urna tortor metus fringilla interdum mauris tempor congue&lt;/p&gt;\r\n', '&lt;p&gt;Commodo laoreet semper tincidunt lorem Vestibulum nunc at In Curabitur magna. Euismod euismod Suspendisse tortor ante adipiscing risus Aenean Lorem vitae id. Odio ut pretium ligula quam Vestibulum consequat convallis fringilla Vestibulum nulla. Accumsan morbi tristique auctor Aenean nulla lacinia Nullam elit vel vel. At risus pretium urna tortor metus fringilla interdum mauris tempor congue.&lt;/p&gt;\r\n\r\n&lt;p&gt;Donec tellus Nulla lorem Nullam elit id ut elit feugiat lacus. Congue eget dapibus congue tincidunt senectus nibh risus Phasellus tristique justo. Justo Pellentesque Donec lobortis faucibus Vestibulum Praesent mauris volutpat vitae metus. Ipsum cursus vestibulum at interdum Vivamus nunc fringilla Curabitur ac quis. Nam lacinia wisi tortor orci quis vitae.&lt;/p&gt;\r\n\r\n&lt;p&gt;Sed mauris Pellentesque elit Aliquam at lacus interdum nascetur elit ipsum. Enim ipsum hendrerit Suspendisse turpis laoreet fames tempus ligula pede ac. Et Lorem penatibus orci eu ultrices egestas Nam quam Vivamus nibh. Morbi condimentum molestie Nam enim odio sodales pretium eros sem pellentesque. Sit tellus Integer elit egestas lacus turpis id auctor nascetur ut. Ac elit vitae.&lt;/p&gt;\r\n\r\n&lt;p&gt;Mi vitae magnis Fusce laoreet nibh felis porttitor laoreet Vestibulum faucibus. At Nulla id tincidunt ut sed semper vel Lorem condimentum ornare. Laoreet Vestibulum lacinia massa a commodo habitasse velit Vestibulum tincidunt In. Turpis at eleifend leo mi elit Aenean porta ac sed faucibus. Nunc urna Morbi fringilla vitae orci convallis condimentum auctor sit dui. Urna pretium elit mauris cursus Curabitur at elit Vestibulum.&lt;/p&gt;\r\n'),
(9, 4, 'لنفط الجنرال بال تم. يكن ودول التّحول بريطانيا في. برلين المدنيون لم ربع.', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n'),
(14, 1, 'Awesome post with everything with it', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, vitae, porro, tempore dolore aspernatur possimus sed assumenda ad tenetur minima rerum culpa magnam quaerat ducimus iure quam iusto quasi itaque accusantium explicabo. Omnis, doloribus similique ex poris nemo sequi iure suscipit inventore magnam harum velit?&lt;/p&gt;\r\n', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, vitae, porro, tempore dolore aspernatur possimus sed assumenda ad tenetur minima rerum culpa magnam quaerat ducimus iure quam iusto quasi itaque accusantium explicabo. Omnis, doloribus similique ex recusandae repellendus ea ut in tempora. Perferendis, repellat, pariatur, dolorem, minima ad eveniet eos impedit illum vitae repellendus quis aperiam saepe facilis consectetur dolores nam explicabo tempora excepturi maxime provident odit sint autem in eaque veritatis beatae blanditiis perspiciatis expedita quas ducimus fugit consequatur quisquam accusantium. Nihil, illum, impedit, quibusdam nam enim adipisci cumque non perferendis voluptates deleniti vero nostrum corporis ducimus magni molestiae dolores repudiandae nesciunt officia maiores saepe excepturi fuga consequuntur harum voluptatem iste ullam totam iusto dicta optio unde sapiente et repellendus ratione sunt quia distinctio accusamus? Quidem, consequuntur, recusandae, ratione, delectus quos blanditiis laudantium fugiat neque debitis labore vitae consectetur. Dolores, magni, quo, assumenda reiciendis hic vitae minima velit explicabo voluptatibus pariatur deleniti quidem fugiat recusandae enim placeat? Eum, doloribus at neque sed repellendus rem distinctio nam eius corporis. Dolores, nulla, rerum, exercitationem pariatur nostrum inventore optio maiores esse aspernatur accusantium ipsum aut quas facilis placeat eveniet dicta magnam neque! Vel, corporis nemo sequi iure suscipit inventore magnam harum velit?&lt;/p&gt;\r\n'),
(14, 4, 'م يذكر تكتيكاً بوزيرها حول. وجزر تنفّس أفريقيا بلا', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n', '&lt;p&gt;مما خيار وهولندا، بريطانيا تم. لإعلان الشتاء لمّ أم. طوكيو للإمبراطورية في مدن, هذا جديدة المذابح الألماني إذ. تزامناً الإقتصادي حدة و, حتى بل وقبل أسلحته بريطانيا-فرنسا.&lt;/p&gt;\r\n\r\n&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n\r\n&lt;p&gt;مع قبضتهم والحزب الشتاء مكن, به، أسيا الطائرات عن. النفط الجنرال بال تم. يكن ودول التّحول بريطانيا في. برلين المدنيون لم ربع.&lt;/p&gt;\r\n\r\n&lt;p&gt;عدم بل عرفها للسيطرة وأكثرها, ثم أطراف الباهضة المؤلّفة دون, تلك أي بهيئة وأزيز الدنمارك. الحرب نورمبرغ كل حتى, حتى الأوضاع والجنود هو. مما العالم عسكرياً الأبرياء ثم, ان يونيو دنكيرك أوكيناوا بعد. بغزو برلين، السوفييتية بال عن, مكن شعار تطوير الأوروبية هو.&lt;/p&gt;\r\n');
"; 		

$query['pavblog'][] ="
INSERT INTO `".DB_PREFIX ."pavblog_category` (`category_id`, `image`, `parent_id`, `is_group`, `width`, `submenu_width`, `colum_width`, `submenu_colum_width`, `item`, `colums`, `type`, `is_content`, `show_title`, `meta_keyword`, `level_depth`, `published`, `store_id`, `position`, `show_sub`, `url`, `target`, `privacy`, `position_type`, `menu_class`, `description`, `meta_description`, `meta_title`, `level`, `left`, `right`, `keyword`) VALUES
(1, '', 0, 2, NULL, NULL, NULL, NULL, NULL, '1', '', 2, 1, '1', 0, 1, 0, 0, 0, NULL, NULL, 0, 'top', NULL, NULL, NULL, NULL, -5, 34, 47, ''),
(20, 'data/pavblog/pav-c3.jpg', 22, 2, NULL, NULL, NULL, NULL, NULL, '1', '', 2, 1, '1', 0, 1, 0, 3, 0, NULL, NULL, 0, 'top', 'test test', NULL, '', '', 0, 0, 0, ''),
(21, 'data/pavblog/pav-c1.jpg', 22, 2, NULL, NULL, NULL, NULL, NULL, '1', '', 2, 1, '1', 0, 1, 0, 1, 0, NULL, NULL, 0, 'top', '', NULL, 'Optical Industry', 'Optical Industry', 0, 0, 0, 'Optical Industry'),
(22, 'data/pavblog/pav-c4.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL, '1', '', 2, 1, '1', 0, 1, 0, 1, 0, NULL, NULL, 0, 'top', '', NULL, 'Announcements', 'Announcements', 0, 0, 0, 'Announcements'),
(23, 'data/pavblog/pav-c2.jpg', 22, 2, NULL, NULL, NULL, NULL, NULL, '1', '', 2, 1, '1', 0, 1, 0, 2, 0, NULL, NULL, 0, 'top', '', NULL, 'Trends', 'Trends', 0, 0, 0, 'Trends'),
(24, 'data/pavblog/pav-i1.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL, '1', '', 2, 1, '1', 0, 1, 0, 2, 0, NULL, NULL, 0, 'top', '', NULL, 'Eye Health', 'Eye Health', 0, 0, 0, 'Eye Health'),
(25, 'data/pavblog/pav-c6.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL, '1', '', 2, 1, 'Archives', 0, 1, 0, 99, 0, NULL, NULL, 0, 'top', '', NULL, 'Archives', 'Archives', 0, 0, 0, 'Archives'),
(26, 'data/pavblog/pav-c1.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL, '1', '', 2, 1, 'Shortcodes', 0, 1, 0, 99, 0, NULL, NULL, 0, 'top', '', NULL, 'Shortcodes', 'Shortcodes', 0, 0, 0, 'Shortcodes'),
(27, 'data/pavblog/pav-c4.jpg', 1, 2, NULL, NULL, NULL, NULL, NULL, '1', '', 2, 1, 'Typography ', 0, 1, 0, 99, 0, NULL, NULL, 0, 'top', '', NULL, 'Typography ', 'Typography ', 0, 0, 0, 'Typography ');
"; 		

$query['pavblog'][] ="
INSERT INTO `".DB_PREFIX ."pavblog_category_description` (`category_id`, `language_id`, `title`, `description`) VALUES
(1, 1, 'ROOT', 'Menu Root'),
(22, 1, 'Announcements', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit&lt;/p&gt;\r\n'),
(26, 1, 'Shortcodes', '&lt;p&gt;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.&lt;/p&gt;\r\n'),
(22, 4, 'النفط الجنرال بال تم.', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n'),
(21, 1, 'Optical Industry', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit.&lt;/p&gt;\r\n'),
(21, 4, 'يرها حول. وجزر تنفّس أفريقيا بلا لم, عل هام', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n'),
(23, 1, 'Trends', '&lt;p&gt;Obcaecati, laboriosam, sint sed explicabo tenetur amet illo totam velit ab eligendi&lt;/p&gt;\r\n'),
(23, 4, 'ال تم. يكن ودول التّحول ب', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n'),
(20, 4, 'نتصر الجنرال كان. ذلك نهاية الأوروبي', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n'),
(27, 1, 'Typography ', '&lt;p&gt;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium&lt;/p&gt;\r\n'),
(27, 4, '. وجزر تنفّس أفريقيا بلا لم, عل ها', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n'),
(26, 4, 'ال تم. يكن ودول التّحول ب', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n'),
(25, 1, 'Archives', '&lt;p&gt;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.&lt;/p&gt;\r\n'),
(25, 4, 'مانية، أخذ كل. هو ضرب طوكيو للج', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n'),
(24, 4, ' بالمطالبة بالولايات, عن عرفها انته', '&lt;p&gt;بيرل البولندي ثم يتم, في تشكيل عملية وانتهاءً وصل. ولم ٣٠ وإيطالي بالمطالبة بالولايات, عن عرفها انتهت وحرمان دنو, بلاده بزوال الغربي بال بل. لم به، معقل برلين،, سحقت اقتصادية المناوشات ولم ما. بولندا لبولندا مع بحث, جوي إذ سلاح الخاصّة. من اعلان مقاطعة واندونيسيا، مما, حيث ما جزيرتي اليابانية, حتى نتيجة التبرعات الأمريكي تم.&lt;/p&gt;\r\n'),
(24, 1, 'Eye Health', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit.&lt;/p&gt;\r\n'),
(20, 1, 'Designer ', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit&lt;/p&gt;\r\n');
"; 	

$query['pavblog'][] ="
INSERT INTO `".DB_PREFIX ."pavblog_comment` (`comment_id`, `blog_id`, `comment`, `status`, `created`, `user`, `email`) VALUES
(6, 10, 'Commodo laoreet semper tincidunt lorem Vestibulum nunc at In Curabitur mag Commodo laoreet semper tincidunt lorem Vestibulum nunc at In Curabitur mag', 1, '2013-03-12 14:23:09', 'ha cong tien', 'hatuhn@gmail.com'),
(7, 10, 'Commodo laoreet semper tincidunt lorem Vestibulum nunc at In Curabitur mag', 1, '2013-03-12 14:25:19', 'ha cong tien', 'hatuhn@gmail.com'),
(8, 10, 'Commodo laoreet semper tincidunt lorem Vestibulum nunc at In Curabitur mag Commodo laoreet semper tincidunt lorem Vestibulum nunc at In Curabitur mag', 1, '2013-03-12 14:30:17', 'Test Test ', 'ngoisao@aa.com');
"; 		


$query['pavblog'][] ="

INSERT INTO `".DB_PREFIX ."setting` (`setting_id`, `store_id`, `group`, `key`, `value`, `serialized`) VALUES
(0, 0, 'pavblog', 'pavblog', 'a:42:{s:14:\"general_lwidth\";s:3:\"816\";s:15:\"general_lheight\";s:3:\"400\";s:14:\"general_swidth\";s:3:\"816\";s:15:\"general_sheight\";s:3:\"400\";s:14:\"general_xwidth\";s:2:\"80\";s:15:\"general_xheight\";s:2:\"80\";s:14:\"rss_limit_item\";s:2:\"12\";s:26:\"keyword_listing_blogs_page\";s:5:\"blogs\";s:16:\"children_columns\";s:1:\"3\";s:14:\"general_cwidth\";s:3:\"816\";s:15:\"general_cheight\";s:3:\"400\";s:22:\"cat_limit_leading_blog\";s:1:\"3\";s:24:\"cat_limit_secondary_blog\";s:1:\"3\";s:22:\"cat_leading_image_type\";s:1:\"l\";s:24:\"cat_secondary_image_type\";s:1:\"l\";s:24:\"cat_columns_leading_blog\";s:1:\"1\";s:27:\"cat_columns_secondary_blogs\";s:1:\"1\";s:14:\"cat_show_title\";s:1:\"1\";s:20:\"cat_show_description\";s:1:\"1\";s:17:\"cat_show_readmore\";s:1:\"1\";s:14:\"cat_show_image\";s:1:\"1\";s:15:\"cat_show_author\";s:1:\"1\";s:17:\"cat_show_category\";s:1:\"1\";s:16:\"cat_show_created\";s:1:\"0\";s:13:\"cat_show_hits\";s:1:\"1\";s:24:\"cat_show_comment_counter\";s:1:\"1\";s:15:\"blog_image_type\";s:1:\"l\";s:15:\"blog_show_title\";s:1:\"1\";s:15:\"blog_show_image\";s:1:\"1\";s:16:\"blog_show_author\";s:1:\"1\";s:18:\"blog_show_category\";s:1:\"1\";s:17:\"blog_show_created\";s:1:\"1\";s:25:\"blog_show_comment_counter\";s:1:\"1\";s:14:\"blog_show_hits\";s:1:\"1\";s:22:\"blog_show_comment_form\";s:1:\"1\";s:14:\"comment_engine\";s:5:\"local\";s:14:\"diquis_account\";s:10:\"pavothemes\";s:14:\"facebook_appid\";s:12:\"100858303516\";s:13:\"comment_limit\";s:2:\"10\";s:14:\"facebook_width\";s:3:\"600\";s:20:\"auto_publish_comment\";s:1:\"0\";s:16:\"enable_recaptcha\";s:1:\"1\";}', 1),
(0, 0, 'pavblog_frontmodules', 'pavblog_frontmodules', 'a:1:{s:7:\"modules\";a:3:{i:0;s:15:\"pavblogcategory\";i:1;s:14:\"pavblogcomment\";i:2;s:13:\"pavbloglatest\";}}', 1),
(0, 0, 'pavblogcategory', 'pavblogcategory_module', 'a:1:{i:1;a:5:{s:11:\"category_id\";s:1:\"1\";s:9:\"layout_id\";s:2:\"12\";s:8:\"position\";s:12:\"column_right\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";s:1:\"1\";}}', 1),
(0, 0, 'pavblogcomment', 'pavblogcomment_module', 'a:1:{i:1;a:5:{s:5:\"limit\";s:1:\"4\";s:9:\"layout_id\";s:2:\"12\";s:8:\"position\";s:12:\"column_right\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";s:1:\"2\";}}', 1),
(0, 0, 'pavbloglatest', 'pavbloglatest_module', 'a:1:{i:1;a:10:{s:11:\"description\";a:2:{i:1;s:219:\"&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, nesciunt, id possimus odit pariatur nulla minus totam nemo? Suscipit rerum non unde nihil expedita fuga rem aspernatur debitis ex at.&lt;/p&gt;\r\n\";i:4;s:328:\"&lt;p&gt;مما خيار وهولندا، بريطانيا تم. لإعلان الشتاء لمّ أم. طوكيو للإمبراطورية في مدن, هذا جديدة المذابح الألماني إذ. تزامناً الإقتصادي حدة و, حتى بل وقبل أسلحته بريطانيا-فرنسا.&lt;/p&gt;\r\n\";}s:4:\"tabs\";s:6:\"latest\";s:5:\"width\";s:3:\"225\";s:6:\"height\";s:3:\"110\";s:4:\"cols\";s:1:\"4\";s:5:\"limit\";s:1:\"4\";s:9:\"layout_id\";s:2:\"12\";s:8:\"position\";s:12:\"column_right\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";s:1:\"3\";}}', 1);

";






$query['pavverticalmenu'][]  = "DROP TABLE IF EXISTS `".DB_PREFIX ."verticalmenu`; ";
$query['pavverticalmenu'][]  = "DROP TABLE IF EXISTS `".DB_PREFIX ."verticalmenu_description`; "; 
$query['pavverticalmenu'][]  = "DROP TABLE IF EXISTS `".DB_PREFIX ."verticalmenu_widgets`; "; 
$query['pavverticalmenu'][]  = "  
CREATE TABLE `".DB_PREFIX ."verticalmenu` (
`verticalmenu_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `widget_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `is_group` smallint(6) NOT NULL DEFAULT '2',
  `width` varchar(255) DEFAULT NULL,
  `submenu_width` varchar(255) DEFAULT NULL,
  `colum_width` varchar(255) DEFAULT NULL,
  `submenu_colum_width` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `colums` varchar(255) DEFAULT '1',
  `type` varchar(255) NOT NULL,
  `is_content` smallint(6) NOT NULL DEFAULT '2',
  `show_title` smallint(6) NOT NULL DEFAULT '1',
  `type_submenu` varchar(10) NOT NULL DEFAULT '1',
  `level_depth` smallint(6) NOT NULL DEFAULT '0',
  `published` smallint(6) NOT NULL DEFAULT '1',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `position` int(11) unsigned NOT NULL DEFAULT '0',
  `show_sub` smallint(6) NOT NULL DEFAULT '0',
  `url` varchar(255) DEFAULT NULL,
  `target` varchar(25) DEFAULT NULL,
  `privacy` smallint(5) unsigned NOT NULL DEFAULT '0',
  `position_type` varchar(25) DEFAULT 'top',
  `menu_class` varchar(25) DEFAULT NULL,
  `description` text,
  `content_text` text,
  `submenu_content` text,
  `level` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `right` int(11) NOT NULL,
  PRIMARY KEY (`verticalmenu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

";


$query['pavverticalmenu'][]  = "  
CREATE TABLE `".DB_PREFIX ."verticalmenu_description` (
`verticalmenu_id` int(11) NOT NULL,
`language_id` int(11) NOT NULL,
`title` varchar(255) NOT NULL,
`description` text NOT NULL,
PRIMARY KEY (`verticalmenu_id`,`language_id`),
KEY `name` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

";


$query['pavverticalmenu'][]  = "  
CREATE TABLE `".DB_PREFIX ."verticalmenu_widgets` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(250) NOT NULL,
`type` varchar(255) NOT NULL,
`params` text NOT NULL,
`store_id` int(11) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

";


$query['pavverticalmenu'][] = " 
INSERT INTO `".DB_PREFIX ."verticalmenu` (`verticalmenu_id`, `widget_id`, `image`, `parent_id`, `is_group`, `width`, `submenu_width`, `colum_width`, `submenu_colum_width`, `item`, `colums`, `type`, `is_content`, `show_title`, `type_submenu`, `level_depth`, `published`, `store_id`, `position`, `show_sub`, `url`, `target`, `privacy`, `position_type`, `menu_class`, `description`, `content_text`, `submenu_content`, `level`, `left`, `right`) VALUES
(1, 0, '', 0, 2, NULL, NULL, NULL, NULL, NULL, '1', '', 2, 1, '1', 0, 1, 0, 0, 0, NULL, NULL, 0, 'top', NULL, NULL, NULL, NULL, -5, 34, 47),
(3, 1, '', 1, 0, NULL, NULL, NULL, '', '18', '1', 'category', 0, 1, 'menu', 0, 1, 0, 5, 0, '', NULL, 0, 'top', 'pav-parrent', NULL, '', '', 0, 0, 0),
(4, 0, '', 3, 0, NULL, NULL, NULL, '', '25', '1', 'category', 0, 1, 'menu', 0, 1, 0, 1, 0, '', NULL, 0, 'top', 'pav-parrent', NULL, '', '', 0, 0, 0),
(8, 1, '', 2, 1, NULL, NULL, NULL, '', '27', '1', 'category', 0, 1, 'menu', 0, 1, 0, 1, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '&lt;p&gt;test&lt;/p&gt;\r\n', 0, 0, 0),
(9, 1, '', 2, 1, NULL, NULL, NULL, '', '26', '1', 'category', 0, 1, 'menu', 0, 1, 0, 2, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(10, 1, '', 8, 0, NULL, NULL, NULL, '', '59', '1', 'category', 0, 1, 'menu', 0, 1, 0, 1, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(11, 1, '', 8, 0, NULL, NULL, NULL, '', '60', '1', 'category', 0, 1, 'menu', 0, 1, 0, 2, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(12, 1, '', 8, 0, NULL, NULL, NULL, '', '61', '1', 'category', 0, 1, 'menu', 0, 1, 0, 3, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(13, 0, '', 8, 0, NULL, NULL, NULL, '', '62', '1', 'category', 0, 1, 'menu', 0, 1, 0, 4, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(14, 0, '', 8, 0, NULL, NULL, NULL, '', '63', '1', 'category', 0, 1, 'menu', 0, 1, 0, 5, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(15, 0, '', 8, 0, NULL, NULL, NULL, '', '64', '1', 'category', 0, 1, 'menu', 0, 1, 0, 6, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(16, 0, '', 8, 0, NULL, NULL, NULL, '', '65', '1', 'category', 0, 1, 'menu', 0, 1, 0, 7, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(17, 0, '', 9, 0, NULL, NULL, NULL, '', '66', '1', 'category', 0, 1, 'menu', 0, 1, 0, 1, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(18, 0, '', 9, 0, NULL, NULL, NULL, '', '67', '1', 'category', 0, 1, 'menu', 0, 1, 0, 2, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(19, 0, '', 9, 0, NULL, NULL, NULL, '', '68', '1', 'category', 0, 1, 'menu', 0, 1, 0, 3, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(20, 0, '', 9, 0, NULL, NULL, NULL, '', '71', '1', 'category', 0, 1, 'menu', 0, 1, 0, 4, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(21, 0, '', 9, 0, NULL, NULL, NULL, '', '72', '1', 'category', 0, 1, 'menu', 0, 1, 0, 5, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(22, 0, '', 9, 0, NULL, NULL, NULL, '', '69', '1', 'category', 0, 1, 'menu', 0, 1, 0, 6, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(23, 0, '', 9, 0, NULL, NULL, NULL, '', '70', '1', 'category', 0, 1, 'menu', 0, 1, 0, 7, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '', '', 0, 0, 0),
(24, 1, '', 2, 0, NULL, NULL, NULL, '', '', '1', 'html', 1, 1, 'menu', 0, 1, 0, 3, 0, '', NULL, 0, 'top', 'pav-menu-child', NULL, '&lt;div class=&quot;pav-menu-video&quot;&gt;&lt;embed height=&quot;157&quot; src=&quot;http://www.youtube.com/v/EoydBE9LNm0&quot; type=&quot;application/x-shockwave-flash&quot; width=&quot;280&quot;&gt;&lt;/embed&gt;\r\n\r\n&lt;h3&gt;Lorem ipsum dolor sit&lt;/h3&gt;\r\n\r\n&lt;p&gt;Dorem ipsum dolor sit amet consectetur adipiscing elit congue sit amet erat roin tincidunt vehicula lorem in adipiscing urna iaculis vel.&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 0, 0, 0),
(25, 1, '', 3, 0, NULL, NULL, NULL, '', '79', '1', 'category', 0, 1, 'menu', 0, 1, 0, 2, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(26, 1, '', 3, 0, NULL, NULL, NULL, '', '74', '1', 'category', 0, 1, 'menu', 0, 1, 0, 3, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(27, 1, '', 3, 0, NULL, NULL, NULL, '', '73', '1', 'category', 0, 1, 'menu', 0, 1, 0, 4, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(28, 1, '', 3, 0, NULL, NULL, NULL, '', '80', '1', 'category', 0, 1, 'menu', 0, 1, 0, 5, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(29, 1, '', 3, 0, NULL, NULL, NULL, '', '', '1', 'category', 0, 1, 'menu', 0, 1, 0, 6, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(30, 1, '', 3, 0, NULL, NULL, NULL, '', '46', '1', 'category', 0, 1, 'menu', 0, 1, 0, 7, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(31, 1, '', 3, 0, NULL, NULL, NULL, '', '75', '1', 'category', 0, 1, 'menu', 0, 1, 0, 8, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(32, 1, '', 3, 0, NULL, NULL, NULL, '', '78', '1', 'category', 0, 1, 'menu', 0, 1, 0, 9, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(33, 1, '', 3, 0, NULL, NULL, NULL, '', '77', '1', 'category', 0, 1, 'menu', 0, 1, 0, 10, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(34, 1, '', 3, 0, NULL, NULL, NULL, '', '77', '1', 'category', 0, 1, 'menu', 0, 1, 0, 11, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(35, 1, '', 3, 0, NULL, NULL, NULL, '', '45', '1', 'category', 0, 1, 'menu', 0, 1, 0, 12, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(36, 1, '', 3, 0, NULL, NULL, NULL, '', '45', '1', 'category', 0, 1, 'menu', 0, 1, 0, 13, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(37, 1, '', 1, 0, NULL, NULL, NULL, '', '25', '1', 'category', 0, 1, 'menu', 0, 1, 0, 6, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(38, 1, '', 1, 0, NULL, NULL, NULL, '', '57', '1', 'category', 0, 1, 'menu', 0, 1, 0, 7, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(40, 1, '', 1, 0, NULL, NULL, NULL, '', '', '1', 'url', 0, 1, 'menu', 0, 1, 0, 1, 0, '?route=common/home', NULL, 0, 'top', 'home', NULL, '', '', 0, 0, 0),
(41, 1, '', 1, 0, NULL, NULL, NULL, '', '20', '1', 'category', 0, 1, 'menu', 0, 1, 0, 99, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(42, 1, '', 1, 0, NULL, NULL, NULL, '', '', '1', 'url', 0, 1, 'menu', 0, 1, 0, 99, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(43, 1, '', 1, 0, NULL, NULL, NULL, '', '', '1', 'url', 0, 1, 'menu', 0, 1, 0, 99, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0),
(44, 1, '', 1, 0, NULL, NULL, NULL, '', '', '1', 'url', 0, 1, 'menu', 0, 1, 0, 99, 0, '', NULL, 0, 'top', '', NULL, '', '', 0, 0, 0);

";


$query['pavverticalmenu'][] = " 
INSERT INTO `".DB_PREFIX ."verticalmenu_description` (`verticalmenu_id`, `language_id`, `title`, `description`) VALUES
(42, 4, 'جنرال بال تم. ', ''),
(3, 1, 'Home, Garden &amp; Tools', ''),
(4, 2, 'Watches', ''),
(4, 1, 'Watches', ''),
(43, 4, 'لعدم ويكيبيديا, في أو', ''),
(37, 1, 'Vehicle, GPS &amp; Navigation', ''),
(10, 1, 'Office Products', ''),
(11, 1, 'Software', ''),
(12, 1, 'Video Games &amp; Consoles', ''),
(13, 2, 'Condimentum eu', ''),
(13, 1, 'Condimentum eu', ''),
(14, 2, 'Lehicula lorem', ''),
(14, 1, 'Lehicula lorem', ''),
(15, 2, 'Integer semper', ''),
(15, 1, 'Integer semper', ''),
(16, 2, 'Sollicitudin lacus', ''),
(16, 1, 'Sollicitudin lacus', ''),
(9, 1, 'Books &amp; Magazines', ''),
(17, 2, 'Nam ipsum ', ''),
(17, 1, 'Nam ipsum ', ''),
(18, 2, 'Curabitur turpis ', ''),
(18, 1, 'Curabitur turpis ', ''),
(19, 1, 'Molestie eu mattis ', ''),
(19, 2, 'Molestie eu mattis ', ''),
(20, 1, 'Suspendisse eu ', ''),
(20, 2, 'Suspendisse eu ', ''),
(21, 1, 'Nunc imperdiet ', ''),
(21, 2, 'Nunc imperdiet ', ''),
(22, 1, 'Mauris mattis', ''),
(22, 2, 'Mauris mattis', ''),
(23, 1, 'Lacus sed iaculis ', ''),
(23, 2, 'Lacus sed iaculis ', ''),
(24, 4, 'نتاج', ''),
(25, 1, 'Computers &amp; Tablets', ''),
(26, 1, 'TV &amp; Home Theater', ''),
(27, 1, 'Home &amp; Portable Audio', ''),
(28, 1, 'Cell Phones &amp; Services', ''),
(29, 1, 'Camera &amp; Video', ''),
(30, 1, 'Vehicle, GPS &amp; Navigation', ''),
(31, 1, 'Musical Instruments', ''),
(32, 1, 'Video Games &amp; Consoles', ''),
(33, 4, 'لفرنسا استطاعوا, هو سبتمبر', ''),
(33, 1, 'Software', ''),
(34, 1, 'Office Products', ''),
(35, 1, 'Electronics Accessories', ''),
(36, 1, 'Trade In Electronics', ''),
(38, 1, 'Musical Instruments', ''),
(40, 1, 'Audible Audiobooks', ''),
(25, 4, 'لبولندا مع بحث, جوي إذ سلاح ', ''),
(26, 4, 'المقاومة الإمبراطورية.', ''),
(27, 4, 'الإمبراطورية. من أما المتّبعة ', ''),
(28, 4, 'بحق الأثنان المقاومة', ''),
(29, 4, 'الإمبراطورية. من أما المتّبعة', ''),
(30, 4, ' تمهيد الغربي الأمريكي يبق ', ''),
(31, 4, 'تكتيكاً بوزيرها حول. ', ''),
(32, 4, 'تكتيكاً بوزيرها حول.', ''),
(34, 4, 'ويكيبيديا, في أواخر', ''),
(35, 4, 'غير. بـ وتردي برلين', ''),
(36, 4, 'بال تم. يكن ودول', ''),
(37, 4, 'هو سبتمبر المنتصر ', ''),
(38, 4, ' بال تم. يكن ودول ', ''),
(12, 4, 'بوزيرها حول. وجزر تنفّس', ''),
(11, 4, ' تكتيكاً بوزيرها حول.', ''),
(10, 4, 'تكتيكاً بوزيرها حول.', ''),
(8, 4, 'ويكيبيديا, في أواخر كُلفة ', ''),
(8, 1, 'Accessories', ''),
(42, 1, 'Clothing, Shoes &amp; Jewelry', ''),
(40, 4, 'حدة. ما بحق الأثنان المقاومة', ''),
(9, 4, 'لإمبراطورية. من أما المتّبعة', ''),
(41, 4, 'حادثة دول, ان مئات الجو مشقّة ', ''),
(41, 1, 'Automotive &amp; Industrialnes', ''),
(3, 4, 'حادثة دول, ان مئات الجو مشقّة ', ''),
(24, 1, 'Lorem ipsum dolor sit ', ''),
(43, 1, 'Toys, Kids &amp; Baby', ''),
(44, 1, 'Clothing, Shoes &amp; Jewelry    ', ''),
(44, 4, 'حادثة دول, ان مئات الجو مشقّة ', '');

";


$query['pavverticalmenu'][] = "
INSERT INTO `".DB_PREFIX ."verticalmenu_widgets` (`id`, `name`, `type`, `params`, `store_id`) VALUES
(1, 'Video Opencart Installation', 'video_code', 'a:1:{s:10:\"video_code\";s:168:\"&lt;iframe width=&quot;300&quot; height=&quot;315&quot; src=&quot;//www.youtube.com/embed/cUhPA5qIxDQ&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;\";}', 0),
(2, 'Demo HTML Sample', 'html', 'a:1:{s:4:\"html\";a:2:{i:1;s:325:\"&lt;p&gt;\r\n	Dorem ipsum dolor sit amet consectetur adipiscing elit congue sit amet erat roin tincidunt vehicula lorem in adipiscing urna iaculis vel. Dorem ipsum dolor \r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n	sit amet consectetur adipiscing elit congue sit amet erat roin tincidunt vehicula lorem in adipiscing urna iaculis vel.\r\n&lt;/p&gt;\";i:4;s:664:\"&lt;p&gt;\r\n	مما خيار وهولندا، بريطانيا تم. لإعلان الشتاء لمّ أم. طوكيو للإمبراطورية في مدن, هذا جديدة المذابح الألماني إذ. تزامناً الإقتصادي حدة و, حتى بل وقبل أسلحته بريطانيا-فرنسا.\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n	مما خيار وهولندا، بريطانيا تم. لإعلان الشتاء لمّ أم. طوكيو للإمبراطورية في مدن, هذا جديدة المذابح الألماني إذ. تزامناً الإقتصادي حدة و, حتى بل وقبل أسلحته بريطانيا-فرنسا.\r\n&lt;/p&gt;\";}}', 0),
(3, 'Products Latest', 'product_list', 'a:4:{s:9:\"list_type\";s:6:\"newest\";s:5:\"limit\";s:1:\"4\";s:11:\"image_width\";s:3:\"100\";s:12:\"image_height\";s:3:\"100\";}', 0),
(4, 'Products In Cat 20', 'product_category', 'a:4:{s:11:\"category_id\";s:2:\"20\";s:5:\"limit\";s:1:\"4\";s:11:\"image_width\";s:3:\"100\";s:12:\"image_height\";s:3:\"100\";}', 0),
(5, 'Manufactures', 'banner', 'a:4:{s:8:\"group_id\";s:1:\"8\";s:11:\"image_width\";s:3:\"130\";s:12:\"image_height\";s:2:\"60\";s:5:\"limit\";s:2:\"12\";}', 0),
(6, 'PavoThemes Feed', 'feed', 'a:1:{s:8:\"feed_url\";s:55:\"http://www.pavothemes.com/opencart-themes.feed?type=rss\";}', 0),
(7, 'Electronics &amp; Computer', 'html', 'a:1:{s:4:\"html\";a:2:{i:1;s:531:\"&lt;ul&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Computers &amp; Tablets&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;TV &amp; Home Theater&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Cell Phones &amp; Services&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Camera &amp; Video&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Vehicle, GPS &amp; Navigation&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Musical Instruments&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\";i:4;s:531:\"&lt;ul&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Computers &amp; Tablets&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;TV &amp; Home Theater&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Cell Phones &amp; Services&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Camera &amp; Video&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Vehicle, GPS &amp; Navigation&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Musical Instruments&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\";}}', 0),
(8, 'Camera &amp; Videos', 'html', 'a:1:{s:4:\"html\";a:2:{i:1;s:353:\"&lt;ul&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Vehicle, GPS &amp; Navigation&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Musical Instruments&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Video Games &amp; Consoles&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Software&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\";i:4;s:353:\"&lt;ul&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Vehicle, GPS &amp; Navigation&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Musical Instruments&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Video Games &amp; Consoles&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Software&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\";}}', 0),
(9, 'Home &amp; Portable Audio', 'html', 'a:1:{s:4:\"html\";a:2:{i:1;s:267:\"&lt;ul&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Office Products&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Electronics Accessories&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Trade In Electronics&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\";i:4;s:267:\"&lt;ul&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Office Products&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Electronics Accessories&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;Trade In Electronics&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\";}}', 0),
(10, 'Image Sub Verticalmenu', 'image', 'a:3:{s:10:\"image_path\";s:31:\"data/demo/sub-vertical-menu.jpg\";s:11:\"image_width\";s:3:\"300\";s:12:\"image_height\";s:3:\"250\";}', 0);

";


$query['pavverticalmenu'][]  = "DELETE FROM `".DB_PREFIX ."setting` WHERE `group`='pavverticalmenu' and `key` = 'pavverticalmenu_module'";
$query['pavverticalmenu'][] =  " 
INSERT INTO `".DB_PREFIX ."setting` (`setting_id`, `store_id`, `group`, `key`, `value`, `serialized`) VALUES
(0, 0, 'pavverticalmenu', 'pavverticalmenu_module', 'a:1:{i:0;a:4:{s:9:\"layout_id\";s:5:\"99999\";s:8:\"position\";s:16:\"called_framework\";s:6:\"status\";s:1:\"1\";s:10:\"sort_order\";s:0:\"\";}}', 1);";


$query['pavverticalmenu'][]  = "DELETE FROM `".DB_PREFIX ."setting` WHERE `group`='pavverticalmenu_params' and `key` = 'params'";
$query['pavverticalmenu'][] =  " 
INSERT INTO `".DB_PREFIX ."setting` (`setting_id`, `store_id`, `group`, `key`, `value`, `serialized`) VALUES
(0, 0, 'pavverticalmenu_params', 'params', '[{\"submenu\":1,\"cols\":1,\"group\":0,\"id\":3,\"rows\":[{\"cols\":[{\"colwidth\":12,\"type\":\"menu\"}]}]},{\"submenu\":1,\"subwidth\":700,\"cols\":1,\"group\":0,\"id\":37,\"rows\":[{\"cols\":[{\"widgets\":\"wid-7|wid-8\",\"colwidth\":6},{\"widgets\":\"wid-9|wid-10\",\"colwidth\":6}]}]},{\"submenu\":1,\"subwidth\":530,\"id\":38,\"cols\":1,\"group\":0,\"rows\":[{\"cols\":[{\"widgets\":\"wid-5\",\"colwidth\":12}]}]},{\"submenu\":1,\"subwidth\":400,\"id\":42,\"cols\":1,\"group\":0,\"rows\":[{\"cols\":[{\"widgets\":\"wid-3\",\"colwidth\":12}]}]}]', 0);";



?>