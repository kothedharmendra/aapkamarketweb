<?php
class ControllerReportAdvCustomerProfit extends Controller {
	public function index() {  
            
		$this->data['home'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
                
		// Insert DB columns
		$query = $this->db->query("DESC " . DB_PREFIX . "product cost_additional");
			if (!$query->num_rows) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD `cost_additional` decimal(15,4) NOT NULL DEFAULT '0.0000' AFTER `price`;");
			}

		$query = $this->db->query("DESC " . DB_PREFIX . "product cost_percentage");
			if (!$query->num_rows) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD `cost_percentage` decimal(15,2) NOT NULL DEFAULT '0.00' AFTER `price`;");
			}

		$query = $this->db->query("DESC " . DB_PREFIX . "product cost_amount");
			if (!$query->num_rows) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD `cost_amount` decimal(15,4) NOT NULL DEFAULT '0.0000' AFTER `price`;");
			}
			
		$query = $this->db->query("DESC " . DB_PREFIX . "product cost");
			if (!$query->num_rows) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD `cost` decimal(15,4) NOT NULL DEFAULT '0.0000' AFTER `price`;");
			}

		$query = $this->db->query("DESC " . DB_PREFIX . "product_option_value cost_prefix");
			if (!$query->num_rows) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "product_option_value` ADD `cost_prefix` varchar(1) COLLATE utf8_bin NOT NULL AFTER `price`;");
			}	
			
		$query = $this->db->query("DESC " . DB_PREFIX . "product_option_value cost");
			if (!$query->num_rows) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "product_option_value` ADD `cost` decimal(15,4) NOT NULL DEFAULT '0.0000' AFTER `price`;");
			}	
			
		$query = $this->db->query("DESC " . DB_PREFIX . "order_product cost");
			if (!$query->num_rows) {
				$this->db->query("ALTER TABLE `" . DB_PREFIX . "order_product` ADD `cost` decimal(15,4) NOT NULL DEFAULT '0.0000';");
			}

		$this->load->language('report/adv_customer_profit');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('report/adv_customer_profit');
		
		if (isset($this->request->post['filter_date_start'])) {
			$filter_date_start = $this->request->post['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->post['filter_date_end'])) {
			$filter_date_end = $this->request->post['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		$this->data['ranges'] = array();
		
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_custom'),
			'value' => 'custom',
		);			
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_week'),
			'value' => 'week',
		);		
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_month'),
			'value' => 'month',
		);					
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_quarter'),
			'value' => 'quarter',
		);
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_year'),
			'value' => 'year',
		);
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_current_week'),
			'value' => 'current_week',
		);
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_current_month'),
			'value' => 'current_month',
		);	
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_current_quarter'),
			'value' => 'current_quarter',
		);			
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_current_year'),
			'value' => 'current_year',
		);			
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_last_week'),
			'value' => 'last_week',
		);
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_last_month'),
			'value' => 'last_month',
		);	
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_last_quarter'),
			'value' => 'last_quarter',
		);			
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_last_year'),
			'value' => 'last_year',
		);			
		$this->data['ranges'][] = array(
			'text'  => $this->language->get('stat_all_time'),
			'value' => 'all_time',
		);

		if (isset($this->request->post['filter_range'])) {
			$filter_range = $this->request->post['filter_range'];
		} else {
			$filter_range = 'current_year'; //show Current Xear in Statistics Range by default
		}

		$this->data['groups'] = array();

		$this->data['groups'][] = array(
			'text'  => $this->language->get('text_no_group'),
			'value' => 'no_group',
		);
		$this->data['groups'][] = array(
			'text'  => $this->language->get('text_year'),
			'value' => 'year',
		);
		$this->data['groups'][] = array(
			'text'  => $this->language->get('text_quarter'),
			'value' => 'quarter',
		);
		$this->data['groups'][] = array(
			'text'  => $this->language->get('text_month'),
			'value' => 'month',
		);
		$this->data['groups'][] = array(
			'text'  => $this->language->get('text_week'),
			'value' => 'week',
		);
		$this->data['groups'][] = array(
			'text'  => $this->language->get('text_day'),
			'value' => 'day',
		);
		
		if (isset($this->request->post['filter_group'])) {
			$filter_group = $this->request->post['filter_group'];
		} else {
			$filter_group = 'no_group'; //show No Grouping in Group By default
		}	
		
		if (isset($this->request->post['filter_types'])) {
			$filter_types = $this->request->post['filter_types'];
		} else {
			$filter_types = NULL;
		}	

		if (isset($this->request->post['filter_sort'])) {
			$filter_sort = $this->request->post['filter_sort'];
		} else {
			$filter_sort = 'orders';
		}	

		if (isset($this->request->post['filter_details'])) {
			$filter_details = $this->request->post['filter_details'];
		} else {
			$filter_details = 0;
		}	
		
		if (isset($this->request->post['filter_limit'])) {
			$filter_limit = $this->request->post['filter_limit'];
		} else {
			$filter_limit = 25;
		}
 	
		$this->data['order_statuses'] = $this->model_report_adv_customer_profit->getOrderStatuses();
		if (isset($this->request->post['filter_order_status_id']) && is_array($this->request->post['filter_order_status_id'])) {
			$filter_order_status_id = array_flip($this->request->post['filter_order_status_id']);
		} else {
			$filter_order_status_id = '';
		}

		$this->data['stores'] = $this->model_report_adv_customer_profit->getOrderStores();	
		if (isset($this->request->post['filter_store_id']) && is_array($this->request->post['filter_store_id'])) {
			$filter_store_id = array_flip($this->request->post['filter_store_id']);
		} else {
			$filter_store_id = '';			
		}
		
		$this->data['currencies'] = $this->model_report_adv_customer_profit->getOrderCurrencies();
		if (isset($this->request->post['filter_currency']) && is_array($this->request->post['filter_currency'])) {
			$filter_currency = array_flip($this->request->post['filter_currency']);
		} else {
			$filter_currency = '';		
		}

		$this->data['taxes'] = $this->model_report_adv_customer_profit->getOrderTaxes();
		if (isset($this->request->post['filter_taxes']) && is_array($this->request->post['filter_taxes'])) {
			$filter_taxes = array_flip($this->request->post['filter_taxes']);
		} else {
			$filter_taxes = '';		
		}
		
		$this->data['customer_groups'] = $this->model_report_adv_customer_profit->getOrderCustomerGroups();
		if (isset($this->request->post['filter_customer_group_id']) && is_array($this->request->post['filter_customer_group_id'])) {
			$filter_customer_group_id = array_flip($this->request->post['filter_customer_group_id']);
		} else {
			$filter_customer_group_id = '';
		}

		$this->data['statuses'] = $this->model_report_adv_customer_profit->getOrderCustomerStatuses();
		if (isset($this->request->post['filter_status']) && is_array($this->request->post['filter_status'])) {
			$filter_status = array_flip($this->request->post['filter_status']);
		} else {
			$filter_status = '';
		}
		
		if (isset($this->request->post['filter_company'])) {
			$filter_company = $this->request->post['filter_company'];
		} else {
			$filter_company = '';
		}
		
		if (isset($this->request->post['filter_customer_id'])) {
			$filter_customer_id = $this->request->post['filter_customer_id'];
		} else {
			$filter_customer_id = '';
		}

		if (isset($this->request->post['filter_email'])) {
			$filter_email = $this->request->post['filter_email'];
		} else {
			$filter_email = '';
		}	

		if (isset($this->request->post['filter_ip'])) {
			$filter_ip = $this->request->post['filter_ip'];
		} else {
			$filter_ip = '';
		}
		
		if (isset($this->request->post['filter_product_id'])) {
			$filter_product_id = $this->request->post['filter_product_id'];
		} else {
			$filter_product_id = '';
		}

		$this->data['product_options'] = $this->model_report_adv_customer_profit->getProductOptions();
		if (isset($this->request->post['filter_option']) && is_array($this->request->post['filter_option'])) {
			$filter_option = array_flip($this->request->post['filter_option']);
		} else {
			$filter_option = '';
		}

		$this->data['locations'] = $this->model_report_adv_customer_profit->getProductLocation();			
		if (isset($this->request->post['filter_location']) && is_array($this->request->post['filter_location'])) {
			$filter_location = array_flip($this->request->post['filter_location']);
		} else {
			$filter_location = '';		
		}
		
		$this->data['affiliates'] = $this->model_report_adv_customer_profit->getOrderAffiliate();
		if (isset($this->request->post['filter_affiliate']) && is_array($this->request->post['filter_affiliate'])) {
			$filter_affiliate = array_flip($this->request->post['filter_affiliate']);
		} else {
			$filter_affiliate = '';
		}
		
		$this->data['shippings'] = $this->model_report_adv_customer_profit->getOrderShipping();
		if (isset($this->request->post['filter_shipping']) && is_array($this->request->post['filter_shipping'])) {
			$filter_shipping = array_flip($this->request->post['filter_shipping']);
		} else {
			$filter_shipping = '';		
		}

		$this->data['payments'] = $this->model_report_adv_customer_profit->getOrderPayment();
		if (isset($this->request->post['filter_payment']) && is_array($this->request->post['filter_payment'])) {
			$filter_payment = array_flip($this->request->post['filter_payment']);
		} else {
			$filter_payment = '';		
		}

		$this->data['shipping_zones'] = $this->model_report_adv_customer_profit->getShippingZones();			
		if (isset($this->request->post['filter_shipping_zone']) && is_array($this->request->post['filter_shipping_zone'])) {
			$filter_shipping_zone = array_flip($this->request->post['filter_shipping_zone']);
		} else {
			$filter_shipping_zone = '';		
		}
		
		$this->data['shipping_countries'] = $this->model_report_adv_customer_profit->getShippingCountries();
		if (isset($this->request->post['filter_shipping_country']) && is_array($this->request->post['filter_shipping_country'])) {
			$filter_shipping_country = array_flip($this->request->post['filter_shipping_country']);
		} else {
			$filter_shipping_country = '';		
		}
		
		$this->data['payment_countries'] = $this->model_report_adv_customer_profit->getPaymentCountries();	
		if (isset($this->request->post['filter_payment_country']) && is_array($this->request->post['filter_payment_country'])) {
			$filter_payment_country = array_flip($this->request->post['filter_payment_country']);
		} else {
			$filter_payment_country = '';		
		}
		
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/adv_customer_profit', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);		

		$this->load->model('report/adv_customer_profit');
		
		$this->data['customers'] = array();
		
		$data = array(
			'filter_date_start'	     	=> $filter_date_start, 
			'filter_date_end'	     	=> $filter_date_end, 
			'filter_range'           	=> $filter_range,
			'filter_group'           	=> $filter_group,			
			'filter_order_status_id' 	=> $filter_order_status_id,
			'filter_store_id' 	 	 	=> $filter_store_id,
			'filter_currency' 	 	 	=> $filter_currency,
			'filter_taxes' 	 	 		=> $filter_taxes,
			'filter_customer_group_id'  => $filter_customer_group_id,
			'filter_status'   		 	=> $filter_status,			
			'filter_company' 	 		=> $filter_company,
			'filter_customer_id' 	 	=> $filter_customer_id,			
			'filter_email' 	 			=> $filter_email,
			'filter_ip' 	 			=> $filter_ip,				
			'filter_product_id' 	 	=> $filter_product_id,
			'filter_option'  			=> $filter_option,
			'filter_location'  			=> $filter_location,
			'filter_affiliate'  		=> $filter_affiliate,			
			'filter_shipping'  			=> $filter_shipping,
			'filter_payment'  			=> $filter_payment,	
			'filter_shipping_zone'  	=> $filter_shipping_zone,				
			'filter_shipping_country'  	=> $filter_shipping_country,
			'filter_payment_country'  	=> $filter_payment_country,	
			'filter_types'  			=> $filter_types,
			'filter_sort'  				=> $filter_sort,	
			'filter_details'  			=> $filter_details,				
			'filter_limit'  			=> $filter_limit
		);
				  
		$results = $this->model_report_adv_customer_profit->getCustomerProfit($data);
	
		foreach ($results as $result) {

			if ($result['prod_costs']) {
				$profit_margin_percent = ($result['sub_total']+$result['handling']+$result['low_order_fee']+$result['reward']+$result['coupon']+$result['credit']+$result['voucher']-$result['commission']) > 0 ? round(100 * ($result['sub_total']-$result['prod_costs']-$result['commission']+$result['handling']+$result['low_order_fee']+$result['reward']+$result['coupon']+$result['credit']+$result['voucher']) / ($result['sub_total']+$result['handling']+$result['low_order_fee']+$result['reward']+$result['coupon']+$result['credit']+$result['voucher']-$result['commission']), 2) . '%' : '0%';
				$profit_margin_total_percent = ($result['sub_total_total']+$result['handling_total']+$result['low_order_fee_total']+$result['reward_total']+$result['coupon_total']+$result['credit_total']+$result['voucher_total']-$result['commission_total']) > 0 ? round(100 * ($result['sub_total_total']-$result['prod_costs_total']-$result['commission_total']+$result['handling_total']+$result['low_order_fee_total']+$result['reward_total']+$result['coupon_total']+$result['credit_total']+$result['voucher_total']) / ($result['sub_total_total']+$result['handling_total']+$result['low_order_fee_total']+$result['reward_total']+$result['coupon_total']+$result['credit_total']+$result['voucher_total']-$result['commission_total']), 2) . '%' : '0%';						
			} else {
				$profit_margin_percent = '100%';
				$profit_margin_total_percent = '100%';				
			}
						
			$this->data['customers'][] = array(	
				'year'		       				=> $result['year'],
				'quarter'		       			=> 'Q' . $result['quarter'],				
				'month'		       				=> $result['month'],											   
				'date_start' 					=> date($this->language->get('date_format_short'), strtotime($result['date_start'])),
				'date_end'   					=> date($this->language->get('date_format_short'), strtotime($result['date_end'])),	
				'order_id'   					=> $result['order_id'],
				'customer_id'     				=> $result['customer_id'],			
				'cust_name'    					=> $result['cust_name'],
				'cust_email'   					=> $result['cust_email'],
				'cust_company'   				=> $result['cust_company'],					
				'cust_group_reg' 				=> $result['cust_group_reg'],
				'cust_group_guest' 				=> $result['cust_group_guest'],					
				'cust_status'         			=> ($result['cust_status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),	
				'cust_ip'   					=> $result['cust_ip'],
				'mostrecent'					=> date($this->language->get('date_format_short'), strtotime($result['mostrecent'])),				
				'orders'  						=> $result['orders'],
				'products'      				=> $result['products'],
				'sales'        					=> $this->currency->format($result['sub_total']+$result['handling']+$result['low_order_fee'], $this->config->get('config_currency')),	
				'value'      					=> $this->currency->format($result['total'], $this->config->get('config_currency')),
				'costs'      					=> $this->currency->format('-' . ($result['prod_costs']+$result['commission']-$result['reward']-$result['coupon']-$result['credit']-$result['voucher']), $this->config->get('config_currency')),				
				'netprofit'      				=> $this->currency->format($result['sub_total']-$result['prod_costs']-$result['commission']+$result['handling']+$result['low_order_fee']+$result['reward']+$result['coupon']+$result['credit']+$result['voucher'], $this->config->get('config_currency')),				
				'profit_margin_percent' 		=> $profit_margin_percent,				
				'order_ord_id'     				=> $filter_details == 1 ? $result['order_ord_id'] : '',
				'order_ord_idc'     			=> $filter_details == 1 ? $result['order_ord_idc'] : '',					
				'order_order_date'    			=> $filter_details == 1 ? $result['order_order_date'] : '',
				'order_inv_no'     				=> $filter_details == 1 ? $result['order_inv_no'] : '',
				'order_name'   					=> $filter_details == 1 ? $result['order_name'] : '',
				'order_email'   				=> $filter_details == 1 ? $result['order_email'] : '',
				'order_group'   				=> $filter_details == 1 ? $result['order_group'] : '',
				'order_shipping_method' 		=> $filter_details == 1 ? $result['order_shipping_method'] : '',
				'order_payment_method'  		=> $filter_details == 1 ? strip_tags($result['order_payment_method'], '<br>') : '',
				'order_status'  				=> $filter_details == 1 ? $result['order_status'] : '',
				'order_store'      				=> $filter_details == 1 ? $result['order_store'] : '',	
				'order_currency' 				=> $filter_details == 1 ? $result['order_currency'] : '',				
				'order_products' 				=> $filter_details == 1 ? $result['order_products'] : '',
				'order_sub_total'  				=> $filter_details == 1 ? $result['order_sub_total'] : '',
				'order_hf'  					=> $filter_details == 1 ? $result['order_hf'] : '',
				'order_lof'  					=> $filter_details == 1 ? $result['order_lof'] : '',				
				'order_shipping'  				=> $filter_details == 1 ? $result['order_shipping'] : '',
				'order_tax'  					=> $filter_details == 1 ? $result['order_tax'] : '',					
				'order_value'  					=> $filter_details == 1 ? $result['order_value'] : '',
				'order_costs'   				=> $filter_details == 1 ? $result['order_costs'] : '',
				'order_profit'   				=> $filter_details == 1 ? $result['order_profit'] : '',	
				'order_profit_margin_percent' 	=> $filter_details == 1 ? $result['order_profit_margin_percent'] : '',				
				'product_ord_id'  				=> $filter_details == 2 ? $result['product_ord_id'] : '',
				'product_ord_idc'  				=> $filter_details == 2 ? $result['product_ord_idc'] : '',
				'product_order_date'    		=> $filter_details == 2 ? $result['product_order_date'] : '',
				'product_inv_no'     			=> $filter_details == 2 ? $result['product_inv_no'] : '',					
				'product_pid'  					=> $filter_details == 2 ? $result['product_pid'] : '',	
				'product_pidc'  				=> $filter_details == 2 ? $result['product_pidc'] : '',	
				'product_sku'  					=> $filter_details == 2 ? $result['product_sku'] : '',					
				'product_name'  				=> $filter_details == 2 ? $result['product_name'] : '',	
				'product_option'  				=> $filter_details == 2 ? $result['product_option'] : '',					
				'product_model'  				=> $filter_details == 2 ? $result['product_model'] : '',					
				'product_manu'  				=> $filter_details == 2 ? $result['product_manu'] : '',
				'product_currency'  			=> $filter_details == 2 ? $result['product_currency'] : '',
				'product_price'  				=> $filter_details == 2 ? $result['product_price'] : '',
				'product_quantity'  			=> $filter_details == 2 ? $result['product_quantity'] : '',				
				'product_total'  				=> $filter_details == 2 ? $result['product_total'] : '',
				'product_tax'  					=> $filter_details == 2 ? $result['product_tax'] : '',
				'product_costs'   				=> $filter_details == 2 ? $result['product_costs'] : '',
				'product_profit'   				=> $filter_details == 2 ? $result['product_profit'] : '',
				'product_profit_margin_percent' => $filter_details == 2 ? $result['product_profit_margin_percent'] : '',
				'orders_total'      			=> $result['orders_total'],	
				'products_total'      			=> $result['products_total'],
				'sales_total'      				=> $this->currency->format($result['sub_total_total']+$result['handling_total']+$result['low_order_fee_total'], $this->config->get('config_currency')),	
				'value_total'      				=> $this->currency->format($result['value_total'], $this->config->get('config_currency')),
				'costs_total' 					=> $this->currency->format('-' . ($result['prod_costs_total']+$result['commission_total']-$result['reward_total']-$result['coupon_total']-$result['credit_total']-$result['voucher_total']), $this->config->get('config_currency')),			
				'netprofit_total'      			=> $this->currency->format($result['sub_total_total']-$result['prod_costs_total']-$result['commission_total']+$result['handling_total']+$result['low_order_fee_total']+$result['reward_total']+$result['coupon_total']+$result['credit_total']+$result['voucher_total'], $this->config->get('config_currency')),
				'profit_margin_total_percent' 	=> $profit_margin_total_percent,				
				'href' 							=> $this->url->link('sale/customer/update', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'], 'SSL')
			);
		}

		if (($filter_range != 'all_time' && ($filter_group == 'year' or $filter_group == 'quarter' or $filter_group == 'month')) or ($filter_range == 'all_time' && $filter_group == 'year')) {
			
		$this->data['sales'] = array();
		
		$sales_results = $this->model_report_adv_customer_profit->getCustomerSale($data);
		
		foreach ($sales_results as $sales_result) {
			
			$this->data['sales'][] = array(
				'gyear'		       					=> $sales_result['gyear'],
				'gyear_quarter'		       			=> 'Q' . $sales_result['gquarter']. ' ' . $sales_result['gyear'],
				'gyear_month'		       			=> substr($sales_result['gmonth'],0,3) . ' ' . $sales_result['gyear'],
				'gorders'    						=> $sales_result['gorders'],
				'gcustomers'    					=> $sales_result['gcustomers'],
				'gproducts'    						=> $sales_result['gproducts'],
				'gsales'    						=> $sales_result['gsub_total']+$sales_result['ghandling']+$sales_result['glow_order_fee'],				
				'gcosts'      						=> $sales_result['gprod_costs']+$sales_result['gcommission']-$sales_result['greward']-$sales_result['gcoupon']-$sales_result['gcredit']-$sales_result['gvoucher'],
				'gnetprofit'      					=> $sales_result['gsub_total']-$sales_result['gprod_costs']-$sales_result['gcommission']+$sales_result['ghandling']+$sales_result['glow_order_fee']+$sales_result['greward']+$sales_result['gcoupon']+$sales_result['gcredit']+$sales_result['gvoucher']
			);
		}
		
		}
		
		$this->data['text_guest'] = $this->language->get('text_guest');
		$this->data['text_registered'] = $this->language->get('text_registered');
		$this->data['text_no_details'] = $this->language->get('text_no_details');
		$this->data['text_order_list'] = $this->language->get('text_order_list');
		$this->data['text_product_list'] = $this->language->get('text_product_list');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');		
		$this->data['text_all_stores'] = $this->language->get('text_all_stores');
		$this->data['text_all_currencies'] = $this->language->get('text_all_currencies');
		$this->data['text_all_taxes'] = $this->language->get('text_all_taxes');	
		$this->data['text_all_groups'] = $this->language->get('text_all_groups');
		$this->data['text_all_options'] = $this->language->get('text_all_options');	
		$this->data['text_all_locations'] = $this->language->get('text_all_locations');	
		$this->data['text_all_affiliates'] = $this->language->get('text_all_affiliates');			
		$this->data['text_all_shippings'] = $this->language->get('text_all_shippings');
		$this->data['text_all_payments'] = $this->language->get('text_all_payments');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');			
		$this->data['text_all_countries'] = $this->language->get('text_all_countries');	
		$this->data['text_all_cust_types'] = $this->language->get('text_all_cust_types');		
		$this->data['text_none_selected'] = $this->language->get('text_none_selected');
		$this->data['text_selected'] = $this->language->get('text_selected');		
		$this->data['text_detail'] = $this->language->get('text_detail');
		$this->data['text_export_no_details'] = $this->language->get('text_export_no_details');
		$this->data['text_export_order_list'] = $this->language->get('text_export_order_list');
		$this->data['text_export_product_list'] = $this->language->get('text_export_product_list');	
		$this->data['text_export_all_details'] = $this->language->get('text_export_all_details');
		$this->data['text_filter_total'] = $this->language->get('text_filter_total');
		$this->data['text_profit_help'] = $this->language->get('text_profit_help');	
		$this->data['text_filtering_options'] = $this->language->get('text_filtering_options');
		$this->data['text_mv_columns'] = $this->language->get('text_mv_columns');		
		$this->data['text_ol_columns'] = $this->language->get('text_ol_columns');	
		$this->data['text_pl_columns'] = $this->language->get('text_pl_columns');
		$this->data['text_pagin_page'] = $this->language->get('text_pagin_page');
		$this->data['text_pagin_of'] = $this->language->get('text_pagin_of');
		$this->data['text_pagin_results'] = $this->language->get('text_pagin_results');			

		$this->data['column_date'] = $this->language->get('column_date');
		$this->data['column_date_start'] = $this->language->get('column_date_start');
		$this->data['column_date_end'] = $this->language->get('column_date_end');
		$this->data['column_id'] = $this->language->get('column_id');
		$this->data['column_customer'] = $this->language->get('column_customer');
		$this->data['column_email'] = $this->language->get('column_email');
		$this->data['column_customer_group'] = $this->language->get('column_customer_group');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_ip'] = $this->language->get('column_ip');			
		$this->data['column_mostrecent'] = $this->language->get('column_mostrecent');
		$this->data['column_orders'] = $this->language->get('column_orders');
		$this->data['column_products'] = $this->language->get('column_products');
		$this->data['column_customers'] = $this->language->get('column_customers');		
		$this->data['column_sales'] = $this->language->get('column_sales');		
		$this->data['column_value'] = $this->language->get('column_value');
		$this->data['column_costs'] = $this->language->get('column_costs');		
		$this->data['column_net_profit'] = $this->language->get('column_net_profit');	
		$this->data['column_profit_margin'] = $this->language->get('column_profit_margin');			
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['column_order_date_added'] = $this->language->get('column_order_date_added');
		$this->data['column_order_order_id'] = $this->language->get('column_order_order_id');
		$this->data['column_order_inv_date'] = $this->language->get('column_order_inv_date');
		$this->data['column_order_inv_no'] = $this->language->get('column_order_inv_no');
		$this->data['column_order_customer'] = $this->language->get('column_order_customer');		
		$this->data['column_order_email'] = $this->language->get('column_order_email');		
		$this->data['column_order_customer_group'] = $this->language->get('column_order_customer_group');		
		$this->data['column_order_shipping_method'] = $this->language->get('column_order_shipping_method');
		$this->data['column_order_payment_method'] = $this->language->get('column_order_payment_method');		
		$this->data['column_order_status'] = $this->language->get('column_order_status');
		$this->data['column_order_store'] = $this->language->get('column_order_store');
		$this->data['column_order_currency'] = $this->language->get('column_order_currency');		
		$this->data['column_order_quantity'] = $this->language->get('column_order_quantity');	
		$this->data['column_order_sub_total'] = $this->language->get('column_order_sub_total');
		$this->data['column_order_hf'] = $this->language->get('column_order_hf');	
		$this->data['column_order_lof'] = $this->language->get('column_order_lof');		
		$this->data['column_order_shipping'] = $this->language->get('column_order_shipping');
		$this->data['column_order_tax'] = $this->language->get('column_order_tax');			
		$this->data['column_order_value'] = $this->language->get('column_order_value');	
		$this->data['column_order_costs'] = $this->language->get('column_order_costs');
		$this->data['column_order_profit'] = $this->language->get('column_order_profit');
		$this->data['column_prod_order_id'] = $this->language->get('column_prod_order_id');		
		$this->data['column_prod_date_added'] = $this->language->get('column_prod_date_added');	
		$this->data['column_prod_inv_no'] = $this->language->get('column_prod_inv_no');			
		$this->data['column_prod_sku'] = $this->language->get('column_prod_sku');
		$this->data['column_prod_id'] = $this->language->get('column_prod_id');		
		$this->data['column_prod_model'] = $this->language->get('column_prod_model');	
		$this->data['column_prod_name'] = $this->language->get('column_prod_name');	
		$this->data['column_prod_option'] = $this->language->get('column_prod_option');			
		$this->data['column_prod_manu'] = $this->language->get('column_prod_manu');
		$this->data['column_prod_currency'] = $this->language->get('column_prod_currency');
		$this->data['column_prod_price'] = $this->language->get('column_prod_price');
		$this->data['column_prod_quantity'] = $this->language->get('column_prod_quantity');
		$this->data['column_prod_total'] = $this->language->get('column_prod_total');
		$this->data['column_prod_tax'] = $this->language->get('column_prod_tax');
		$this->data['column_prod_costs'] = $this->language->get('column_prod_costs');
		$this->data['column_prod_profit'] = $this->language->get('column_prod_profit');		

		$this->data['column_year'] = $this->language->get('column_year');
		$this->data['column_quarter'] = $this->language->get('column_quarter');
		$this->data['column_month'] = $this->language->get('column_month');
		$this->data['column_sales'] = $this->language->get('column_sales');		
		$this->data['column_total_costs'] = $this->language->get('column_total_costs');
		$this->data['column_total_profit'] = $this->language->get('column_total_profit');
		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		$this->data['entry_range'] = $this->language->get('entry_range'); 		
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_currency'] = $this->language->get('entry_currency');	
		$this->data['entry_tax'] = $this->language->get('entry_tax');
		$this->data['entry_customer_group'] = $this->language->get('entry_customer_group'); 
		$this->data['entry_customer_status'] = $this->language->get('entry_customer_status');		
		$this->data['entry_company'] = $this->language->get('entry_company');
		$this->data['entry_customer'] = $this->language->get('entry_customer');		
		$this->data['entry_email'] = $this->language->get('entry_email'); 
		$this->data['entry_ip'] = $this->language->get('entry_ip'); 		
		$this->data['entry_product'] = $this->language->get('entry_product');
		$this->data['entry_option'] = $this->language->get('entry_option');
		$this->data['entry_location'] = $this->language->get('entry_location');
		$this->data['entry_affiliate'] = $this->language->get('entry_affiliate');
		$this->data['entry_shipping'] = $this->language->get('entry_shipping');
		$this->data['entry_payment'] = $this->language->get('entry_payment');
		$this->data['entry_zone'] = $this->language->get('entry_zone');		
		$this->data['entry_shipping_country'] = $this->language->get('entry_shipping_country');
		$this->data['entry_payment_country'] = $this->language->get('entry_payment_country');	
		$this->data['entry_customer_type'] = $this->language->get('entry_customer_type');	
		$this->data['entry_group'] = $this->language->get('entry_group');			
		$this->data['entry_sort_by'] = $this->language->get('entry_sort_by');
		$this->data['entry_show_details'] = $this->language->get('entry_show_details');	
		$this->data['entry_limit'] = $this->language->get('entry_limit');		
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_chart'] = $this->language->get('button_chart');			
		$this->data['button_export'] = $this->language->get('button_export');		
		$this->data['button_settings'] = $this->language->get('button_settings');
		
 		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['heading_version'] = $this->language->get('heading_version');			
		 
		$this->data['token'] = $this->session->data['token'];
		
		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_range'] = $filter_range;
		$this->data['filter_order_status_id'] = $filter_order_status_id;
		$this->data['filter_store_id'] = $filter_store_id;
		$this->data['filter_currency'] = $filter_currency; 
		$this->data['filter_taxes'] = $filter_taxes;
		$this->data['filter_customer_group_id'] = $filter_customer_group_id;
		$this->data['filter_status'] = $filter_status;			
		$this->data['filter_company'] = $filter_company; 
		$this->data['filter_customer_id'] = $filter_customer_id; 		
		$this->data['filter_email'] = $filter_email; 
		$this->data['filter_ip'] = $filter_ip;		
		$this->data['filter_product_id'] = $filter_product_id; 
		$this->data['filter_option'] = $filter_option; 
		$this->data['filter_location'] = $filter_location;
		$this->data['filter_affiliate'] = $filter_affiliate; 		
		$this->data['filter_shipping'] = $filter_shipping;
		$this->data['filter_payment'] = $filter_payment;
		$this->data['filter_shipping_zone'] = $filter_shipping_zone;
		$this->data['filter_shipping_country'] = $filter_shipping_country;
		$this->data['filter_payment_country'] = $filter_payment_country;
		$this->data['filter_types'] = $filter_types;
		$this->data['filter_group'] = $filter_group;		
		$this->data['filter_sort'] = $filter_sort;	
		$this->data['filter_details'] = $filter_details;
		$this->data['filter_limit'] = $filter_limit;		
				 
		$this->template = 'report/adv_customer_profit.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
				
		$this->response->setOutput($this->render());
			
    	if (isset($this->request->post['export']) && $this->request->post['export'] == 1) { // export_xls
			$this->load->model('report/adv_customer_profit_export');
    		$results = $this->model_report_adv_customer_profit_export->getCustomerProfitExport($data);
			include(DIR_APPLICATION."controller/report/adv_reports/cp_export_xls.inc.php");
			
		} elseif (isset($this->request->post['export']) && $this->request->post['export'] == 2) { // export_xls_order_list
			$this->load->model('report/adv_customer_profit_export');
    		$results = $this->model_report_adv_customer_profit_export->getCustomerProfitExport($data);
			include(DIR_APPLICATION."controller/report/adv_reports/cp_export_xls_order_list.inc.php");

		} elseif (isset($this->request->post['export']) && $this->request->post['export'] == 3) { // export_xls_product_list
			$this->load->model('report/adv_customer_profit_export');
    		$results = $this->model_report_adv_customer_profit_export->getCustomerProfitExport($data);
			include(DIR_APPLICATION."controller/report/adv_reports/cp_export_xls_product_list.inc.php");

		} elseif (isset($this->request->post['export']) && $this->request->post['export'] == 5) { // export_xls_all_details
			$this->load->model('report/adv_customer_profit_export_all');
    		$results = $this->model_report_adv_customer_profit_export_all->getCustomerProfitExportAll($data);
			include(DIR_APPLICATION."controller/report/adv_reports/cp_export_xls_all_details.inc.php");
				
		} elseif (isset($this->request->post['export']) && $this->request->post['export'] == 6) { // export_html
			$this->load->model('report/adv_customer_profit_export');
    		$results = $this->model_report_adv_customer_profit_export->getCustomerProfitExport($data);
			include(DIR_APPLICATION."controller/report/adv_reports/cp_export_html.inc.php");
			
		} elseif (isset($this->request->post['export']) && $this->request->post['export'] == 7) { // export_html_order_list
			$this->load->model('report/adv_customer_profit_export');
    		$results = $this->model_report_adv_customer_profit_export->getCustomerProfitExport($data);
			include(DIR_APPLICATION."controller/report/adv_reports/cp_export_html_order_list.inc.php");
				
		} elseif (isset($this->request->post['export']) && $this->request->post['export'] == 8) { // export_html_product_list
			$this->load->model('report/adv_customer_profit_export');
    		$results = $this->model_report_adv_customer_profit_export->getCustomerProfitExport($data);
			include(DIR_APPLICATION."controller/report/adv_reports/cp_export_html_product_list.inc.php");
							
		} elseif (isset($this->request->post['export']) && $this->request->post['export'] == 10) { // export_html_all_details
			$this->load->model('report/adv_customer_profit_export_all');
    		$results = $this->model_report_adv_customer_profit_export_all->getCustomerProfitExportAll($data);
			include(DIR_APPLICATION."controller/report/adv_reports/cp_export_html_all_details.inc.php");

		} elseif (isset($this->request->post['export']) && $this->request->post['export'] == 11) { // export_pdf
			$this->load->model('report/adv_customer_profit_export');
    		$results = $this->model_report_adv_customer_profit_export->getCustomerProfitExport($data);
			require_once(DIR_SYSTEM . 'library/dompdf/dompdf_config.inc.php');
			include(DIR_APPLICATION."controller/report/adv_reports/cp_export_pdf.inc.php");
		
		} elseif (isset($this->request->post['export']) && $this->request->post['export'] == 12) { // export_pdf_order_list
			$this->load->model('report/adv_customer_profit_export');
    		$results = $this->model_report_adv_customer_profit_export->getCustomerProfitExport($data);
			require_once(DIR_SYSTEM . 'library/dompdf/dompdf_config.inc.php');
			include(DIR_APPLICATION."controller/report/adv_reports/cp_export_pdf_order_list.inc.php");
			
		} elseif (isset($this->request->post['export']) && $this->request->post['export'] == 13) { // export_pdf_product_list
			$this->load->model('report/adv_customer_profit_export');
    		$results = $this->model_report_adv_customer_profit_export->getCustomerProfitExport($data);
			require_once(DIR_SYSTEM . 'library/dompdf/dompdf_config.inc.php');
			include(DIR_APPLICATION."controller/report/adv_reports/cp_export_pdf_product_list.inc.php");
			
		} elseif (isset($this->request->post['export']) && $this->request->post['export'] == 15) { // export_pdf_all_details
			$this->load->model('report/adv_customer_profit_export_all');
    		$results = $this->model_report_adv_customer_profit_export_all->getCustomerProfitExportAll($data);
			require_once(DIR_SYSTEM . 'library/dompdf/dompdf_config.inc.php');
			include(DIR_APPLICATION."controller/report/adv_reports/cp_export_pdf_all_details.inc.php");			
		}			
	}
	
	public function customer_autocomplete() {
		$json = array();

		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->request->get['filter_customer_id']) or isset($this->request->get['filter_email']) or isset($this->request->get['filter_company'])) {
			$this->load->model('report/adv_customer_profit');
					
		if (isset($this->request->get['filter_company'])) {
			$filter_company = $this->request->get['filter_company'];
		} else {
			$filter_company = '';
		}
		
		if (isset($this->request->get['filter_customer_id'])) {
			$filter_customer_id = $this->request->get['filter_customer_id'];
		} else {
			$filter_customer_id = '';
		}

		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = '';
		}	
		
		$data = array(		
			'filter_company' 	 		=> $filter_company,
			'filter_customer_id' 	 	=> $filter_customer_id,			
			'filter_email' 	 			=> $filter_email			
		);
						
		$results = $this->model_report_adv_customer_profit->getCustomerAutocomplete($data);
			
			foreach ($results as $result) {
				$json[] = array(
					'customer_id'     		=> $result['customer_id'],
					'cust_company'     		=> html_entity_decode($result['cust_company'], ENT_QUOTES, 'UTF-8'),					
					'cust_name'     		=> html_entity_decode($result['cust_name'], ENT_QUOTES, 'UTF-8'),
					'cust_email'     		=> $result['cust_email']					
				);
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}	

	public function ip_autocomplete() {
		$json = array();

		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->request->get['filter_ip'])) {
			$this->load->model('report/adv_customer_profit');

		if (isset($this->request->get['filter_ip'])) {
			$filter_ip = $this->request->get['filter_ip'];
		} else {
			$filter_ip = '';
		}
		
		$data = array(		
			'filter_ip' 	 			=> $filter_ip			
		);
						
		$results = $this->model_report_adv_customer_profit->getIPAutocomplete($data);
			
			foreach ($results as $result) {
				$json[] = array(
					'customer_id'     		=> $result['customer_id'],
					'cust_ip'     			=> $result['cust_ip']					
				);
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function product_autocomplete() {
		$json = array();

		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->request->get['filter_product_id'])) {
			$this->load->model('report/adv_customer_profit');
					
		if (isset($this->request->get['filter_product_id'])) {
			$filter_product_id = $this->request->get['filter_product_id'];
		} else {
			$filter_product_id = '';
		}
		
		$data = array(				
			'filter_product_id' 	 	=> $filter_product_id			
		);
						
		$results = $this->model_report_adv_customer_profit->getProductAutocomplete($data);
			
			foreach ($results as $result) {
				$json[] = array(
					'product_id'     		=> $result['product_id'],
					'prod_name'     		=> html_entity_decode($result['prod_name'], ENT_QUOTES, 'UTF-8')					
				);
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}	
}
?>