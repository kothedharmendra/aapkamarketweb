<?php
ini_set("memory_limit","1024M");
set_time_limit( 180000 );
			
	$export_pdf_cat ="<html><head>";
	$export_pdf_cat .="</head>";
	$export_pdf_cat .="<body>";
	$export_pdf_cat .="<style type='text/css'>
	.list_main {
		width: 100%;
		border-top: 1px solid #DDDDDD;
		border-left: 1px solid #DDDDDD;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 10px;
	}
	.list_main td {
		border-right: 1px solid #DDDDDD;
		border-bottom: 1px solid #DDDDDD;	
	}
	.list_main thead td {
		background-color: #E5E5E5;
		padding: 3px;
		font-weight: bold;
	}
	.list_main tbody a {
		text-decoration: none;
	}
	.list_main tbody td {
		vertical-align: middle;
		padding: 3px;
	}
	.list_main .left {
		text-align: left;
		padding: 7px;
	}
	.list_main .right {
		text-align: right;
		padding: 7px;
	}
	.list_main .center {
		text-align: center;
		padding: 3px;
	}
	</style>";
	$export_pdf_cat .="<table class='list_main'>";
	$export_pdf_cat .="<thead>";
	$export_pdf_cat .="<tr>";
	if ($filter_group == 'year') {				
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_year')."</td>";
	} elseif ($filter_group == 'quarter') {
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_year')."</td>";
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_quarter')."</td>";				
	} elseif ($filter_group == 'month') {
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_year')."</td>";
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_month')."</td>";
	} else {
	$export_pdf_cat .= "<td align='left' width='80' nowrap='nowrap'>".$this->language->get('column_date_start')."</td>";
	$export_pdf_cat .= "<td align='left' width='80' nowrap='nowrap'>".$this->language->get('column_date_end')."</td>";	
	}	
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_category')."</td>";
	$export_pdf_cat .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_sold_quantity')."</td>";
	$export_pdf_cat .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_sold_percent')."</td>";	
	$export_pdf_cat .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_total')."</td>";				
	$export_pdf_cat .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_tax')."</td>";
	$export_pdf_cat .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_costs')."</td>";
	$export_pdf_cat .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_profit')."</td>";	
	$export_pdf_cat .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_profit_margin')."</td>";				
	$export_pdf_cat .="</tr>";
	$export_pdf_cat .="</thead><tbody>";
	foreach ($results as $result) {	
			
	$this->load->model('catalog/product');
	$cat =  $this->model_catalog_product->getProductCategories($result['product_id']);
	$categories = $this->model_report_adv_product_profit->getProductsCategories(0); 
			
	$export_pdf_cat .="<tr>";
	if ($filter_group == 'year') {				
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".$result['year']."</td>";
	} elseif ($filter_group == 'quarter') {
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".$result['year']."</td>";	
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".'Q' . $result['quarter']."</td>";						
	} elseif ($filter_group == 'month') {
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".$result['year']."</td>";	
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".$result['month']."</td>";	
	} else {
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".date($this->language->get('date_format_short'), strtotime($result['date_start']))."</td>";
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>".date($this->language->get('date_format_short'), strtotime($result['date_end']))."</td>";
	}			
	$export_pdf_cat .= "<td align='left' nowrap='nowrap'>";
		foreach ($categories as $category) {
			if (in_array($category['category_id'], $cat)) {
				$export_pdf_cat .= "".$category['name']."<br>";
			}
		}
	$export_pdf_cat .= "</td>";	
	$export_pdf_cat .= "<td align='right' nowrap='nowrap'>".$result['sold_quantity']."</td>";
	if (!is_null($result['sold_quantity'])) {		
	$export_pdf_cat .= "<td align='right' nowrap='nowrap'>".round(100 * ($result['sold_quantity'] / $result['sold_quantity_total']), 2) . '%'."</td>";
	} else {	
	$export_pdf_cat .= "<td align='right' nowrap='nowrap'>".'0'."</td>";
	}
	$export_pdf_cat .= "<td align='right' nowrap='nowrap' style='background-color:#DCFFB9;'>".$this->currency->format($result['total'], $this->config->get('config_currency'))."</td>";				
	$export_pdf_cat .= "<td align='right' nowrap='nowrap'>".$this->currency->format($result['tax'], $this->config->get('config_currency'))."</td>";			
	$export_pdf_cat .= "<td align='right' nowrap='nowrap' style='background-color:#ffd7d7;'>".$this->currency->format('-' . ($result['prod_costs']), $this->config->get('config_currency'))."</td>";
	$export_pdf_cat .= "<td align='right' nowrap='nowrap' style='background-color:#BCD5ED; font-weight:bold;'>".$this->currency->format($result['prod_profit'], $this->config->get('config_currency'))."</td>";	
	if (($result['prod_costs']+$result['prod_profit']) > 0) {
	$export_pdf_cat .= "<td align='right' nowrap='nowrap' style='background-color:#BCD5ED; font-weight:bold;'>".round(100 * ($result['prod_profit']) / ($result['prod_costs']+$result['prod_profit']), 2) . '%'."</td>";				
	} else {
	$export_pdf_cat .= "<td align='right' nowrap='nowrap'>".'0%'."</td>";
	}		
	$export_pdf_cat .="</tr>";				
	}
	$export_pdf_cat .="</tbody></table>";
	$export_pdf_cat .="</body></html>";

$dompdf = new DOMPDF();
$dompdf->load_html($export_pdf_cat);
$dompdf->set_paper("a3", "landscape");
$dompdf->render();
$dompdf->stream("categories_profit_report_".date("Y-m-d",time()).".pdf");
?>