<?php
ini_set("memory_limit","1024M");
set_time_limit( 180000 );
			
	$export_xls_prod ="<html><head>";
	$export_xls_prod .="<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>";
	$export_xls_prod .="</head>";
	$export_xls_prod .="<body>";					
	$export_xls_prod .="<table border='1'>";	
	$export_xls_prod .="<tr>";
	if ($filter_group == 'year') {				
	$export_xls_prod .= "<td colspan='2' align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_year')."</td>";
	} elseif ($filter_group == 'quarter') {
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_year')."</td>";					
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_quarter')."</td>";				
	} elseif ($filter_group == 'month') {
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_year')."</td>";					
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_month')."</td>";
	} else {
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_date_start')."</td>";				
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_date_end')."</td>";	
	}
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_name')."</td>";	
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_options')."</td>";
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_sku')."</td>";					
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_model')."</td>";
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_category')."</td>";
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_manufacturer')."</td>";
	$export_xls_prod .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_status')."</td>";
	$export_xls_prod .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_sold_quantity')."</td>";
	$export_xls_prod .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_sold_percent')."</td>";	
	$export_xls_prod .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_total')."</td>";				
	$export_xls_prod .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_tax')."</td>";				
	$export_xls_prod .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_prod_costs')."</td>";
	$export_xls_prod .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_prod_profit')."</td>";
	$export_xls_prod .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_profit_margin')."</td>";	
	$export_xls_prod .="</tr>";
	foreach ($results as $result) {		

	$this->load->model('catalog/product');
	$cat =  $this->model_catalog_product->getProductCategories($result['product_id']);
	$manu = $this->model_report_adv_product_profit->getProductManufacturers($result['manufacturer_id']);	
	$manufacturers = $this->model_report_adv_product_profit->getProductsManufacturers();
	$categories = $this->model_report_adv_product_profit->getProductsCategories(0); 
			
	$export_xls_prod .="<tr>";
	if ($filter_group == 'year') {				
	$export_xls_prod .= "<td colspan='2' align='left' valign='top'>".$result['year']."</td>";
	} elseif ($filter_group == 'quarter') {
	$export_xls_prod .= "<td align='left' valign='top'>".$result['year']."</td>";
	$export_xls_prod .= "<td align='left' valign='top'>".'Q' . $result['quarter']."</td>";					
	} elseif ($filter_group == 'month') {
	$export_xls_prod .= "<td align='left' valign='top'>".$result['year']."</td>";
	$export_xls_prod .= "<td align='left' valign='top'>".$result['month']."</td>";	
	} else {
	$export_xls_prod .= "<td align='left' valign='top'>".date($this->language->get('date_format_short'), strtotime($result['date_start']))."</td>";
	$export_xls_prod .= "<td align='left' valign='top'>".date($this->language->get('date_format_short'), strtotime($result['date_end']))."</td>";	
	}
	$export_xls_prod .= "<td align='left' valign='top'>".$result['name']."</td>";					
	$export_xls_prod .= "<td align='left'>";
	if ($filter_ogrouping) {
	if ($result['oovalue']) {
	$export_xls_prod .= "<table border='0' cellpadding='0' cellspacing='0'><tr>";	
	$export_xls_prod .= "<td>".$result['ooname'].":</td>";	
	$export_xls_prod .= "<td>".$result['oovalue']."</td>";	
	$export_xls_prod .= "</tr></table>";	
	}
	}
	$export_xls_prod .= "</td>";
	$export_xls_prod .= "<td align='left' valign='top'>".$result['sku']."</td>";
	$export_xls_prod .= "<td align='left' valign='top'>".$result['model']."</td>";
	$export_xls_prod .= "<td align='left' valign='top'>";
		foreach ($categories as $category) {
			if (in_array($category['category_id'], $cat)) {
				$export_xls_prod .= "".$category['name']."<br>";
			}
		}
	$export_xls_prod .= "</td>";	
	$export_xls_prod .= "<td align='left' valign='top'>";
		foreach ($manufacturers as $manufacturer) {
			if (in_array($manufacturer['manufacturer_id'], $manu)) {
				$export_xls_prod .= "".$manufacturer['name']."";
			}
		}
	$export_xls_prod .= "</td>";
	$export_xls_prod .= "<td align='left' valign='top'>".($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'))."</td>";
	$export_xls_prod .= "<td align='right' valign='top'>".$result['sold_quantity']."</td>";
	if (!is_null($result['sold_quantity'])) {
	$export_xls_prod .= "<td align='right' valign='top'>".round(100 * ($result['sold_quantity'] / $result['sold_quantity_total']), 2) . '%'."</td>";
	} else {
	$export_xls_prod .= "<td align='right' valign='top'>".'0'."</td>";
	}						
	$export_xls_prod .= "<td align='right' valign='top'>".$this->currency->format($result['total'], $this->config->get('config_currency'))."</td>";					
	$export_xls_prod .= "<td align='right' valign='top'>".$this->currency->format($result['tax'], $this->config->get('config_currency'))."</td>";			
	$export_xls_prod .= "<td align='right' valign='top'>".$this->currency->format('-' . ($result['prod_costs']), $this->config->get('config_currency'))."</td>";
	$export_xls_prod .= "<td align='right' valign='top'>".$this->currency->format($result['prod_profit'], $this->config->get('config_currency'))."</td>";
	if (($result['prod_costs']+$result['prod_profit']) > 0) {				
	$export_xls_prod .= "<td align='right' valign='top'>".round(100 * ($result['prod_profit']) / ($result['prod_costs']+$result['prod_profit']), 2) . '%'."</td>";	
	} else {
	$export_xls_prod .= "<td align='right' valign='top'>".'0%'."</td>";
	}	
	$export_xls_prod .="</tr>";				
	}
	$export_xls_prod .="</table>";		
	$export_xls_prod .="</body></html>";

$filename = "products_profit_report_".date("Y-m-d",time());
header('Expires: 0');
header('Cache-control: private');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Content-Description: File Transfer');			
header('Content-Type: application/vnd.ms-excel; charset=UTF-8; encoding=UTF-8');			
header('Content-Disposition: attachment; filename='.$filename.".xls");
header('Content-Transfer-Encoding: UTF-8');	
print $export_xls_prod;			
exit;	
?>