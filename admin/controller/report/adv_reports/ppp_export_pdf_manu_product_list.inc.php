<?php
ini_set("memory_limit","1024M");
set_time_limit( 180000 );
			
	$export_pdf_manu_product_list ="<html><head>";
	$export_pdf_manu_product_list .="</head>";
	$export_pdf_manu_product_list .="<body>";
	$export_pdf_manu_product_list .="<style type='text/css'>
	.list_main {
		border-top: 1px solid #DDDDDD;
		border-left: 1px solid #DDDDDD;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 10px;
	}
	.list_main td {
		border-right: 1px solid #DDDDDD;
		border-bottom: 1px solid #DDDDDD;	
	}
	.list_main thead td {
		background-color: #E5E5E5;
		padding: 3px;
		font-weight: bold;
	}
	.list_main tbody a {
		text-decoration: none;
	}
	.list_main tbody td {
		vertical-align: middle;
		padding: 3px;
	}
	.list_main .left {
		text-align: left;
		padding: 7px;
	}
	.list_main .right {
		text-align: right;
		padding: 7px;
	}
	.list_main .center {
		text-align: center;
		padding: 3px;
	}

	.list_detail {
		width: 100%;
		border-top: 1px solid #DDDDDD;
		border-left: 1px solid #DDDDDD;
		font-family: Arial, Helvetica, sans-serif;	
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.list_detail td {
		border-right: 1px solid #DDDDDD;
		border-bottom: 1px solid #DDDDDD;
	}
	.list_detail thead td {
		background-color: #F0F0F0;
		padding: 0px 3px;
		font-size: 9px;
		font-weight: bold;	
	}
	.list_detail tbody td {
		padding: 0px 3px;
		font-size: 9px;	
	}
	.list_detail .left {
		text-align: left;
		padding: 3px;
	}
	.list_detail .right {
		text-align: right;
		padding: 3px;
	}
	.list_detail .center {
		text-align: center;
		padding: 3px;
	}
	</style>";
	foreach ($results as $result) {		
	$export_pdf_manu_product_list .="<table class='list_main'>";
	$export_pdf_manu_product_list .="<thead>";
	$export_pdf_manu_product_list .="<tr>";
	if ($filter_group == 'year') {				
	$export_pdf_manu_product_list .= "<td colspan='2' align='left' nowrap='nowrap'>".$this->language->get('column_year')."</td>";
	} elseif ($filter_group == 'quarter') {
	$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_year')."</td>";
	$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_quarter')."</td>";				
	} elseif ($filter_group == 'month') {
	$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_year')."</td>";
	$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_month')."</td>";
	} else {
	$export_pdf_manu_product_list .= "<td align='left' width='80' nowrap='nowrap'>".$this->language->get('column_date_start')."</td>";
	$export_pdf_manu_product_list .= "<td align='left' width='80' nowrap='nowrap'>".$this->language->get('column_date_end')."</td>";	
	}	
	$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_manufacturer')."</td>";
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_sold_quantity')."</td>";
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_sold_percent')."</td>";	
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_total')."</td>";				
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_tax')."</td>";
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_costs')."</td>";
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_profit')."</td>";	
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_profit_margin')."</td>";				
	$export_pdf_manu_product_list .="</tr>";
	$export_pdf_manu_product_list .="</thead><tbody>";
			
	$this->load->model('catalog/product');
	$manu = $this->model_report_adv_product_profit->getProductManufacturers($result['manufacturer_id']);	
	$manufacturers = $this->model_report_adv_product_profit->getProductsManufacturers();
			
	$export_pdf_manu_product_list .="<tr>";
	if ($filter_group == 'year') {				
	$export_pdf_manu_product_list .= "<td colspan='2' align='left' nowrap='nowrap'>".$result['year']."</td>";
	} elseif ($filter_group == 'quarter') {
	$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$result['year']."</td>";	
	$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".'Q' . $result['quarter']."</td>";						
	} elseif ($filter_group == 'month') {
	$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$result['year']."</td>";	
	$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$result['month']."</td>";	
	} else {
	$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".date($this->language->get('date_format_short'), strtotime($result['date_start']))."</td>";
	$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".date($this->language->get('date_format_short'), strtotime($result['date_end']))."</td>";
	}			
	$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>";
		foreach ($manufacturers as $manufacturer) {
			if (in_array($manufacturer['manufacturer_id'], $manu)) {
				$export_pdf_manu_product_list .= "".$manufacturer['name']."";
			}
		}
	$export_pdf_manu_product_list .= "</td>";
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$result['sold_quantity']."</td>";
	if (!is_null($result['sold_quantity'])) {		
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".round(100 * ($result['sold_quantity'] / $result['sold_quantity_total']), 2) . '%'."</td>";
	} else {	
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".'0'."</td>";
	}
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap' style='background-color:#DCFFB9;'>".$this->currency->format($result['total'], $this->config->get('config_currency'))."</td>";				
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->currency->format($result['tax'], $this->config->get('config_currency'))."</td>";			
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap' style='background-color:#ffd7d7;'>".$this->currency->format('-' . ($result['prod_costs']), $this->config->get('config_currency'))."</td>";
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap' style='background-color:#BCD5ED; font-weight:bold;'>".$this->currency->format($result['prod_profit'], $this->config->get('config_currency'))."</td>";	
	if (($result['prod_costs']+$result['prod_profit']) > 0) {
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap' style='background-color:#BCD5ED; font-weight:bold;'>".round(100 * ($result['prod_profit']) / ($result['prod_costs']+$result['prod_profit']), 2) . '%'."</td>";				
	} else {
	$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".'0%'."</td>";
	}		
	$export_pdf_manu_product_list .="</tr>";
	$export_pdf_manu_product_list .="<tr>";
	$export_pdf_manu_product_list .= "<td colspan='10' align='center'>";
		$export_pdf_manu_product_list .="<table class='list_detail'>";
		$export_pdf_manu_product_list .="<thead>";
		$export_pdf_manu_product_list .="<tr>";
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_prod_order_id')."</td>";					
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_prod_date_added')."</td>";
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_prod_inv_no')."</td>";									
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_prod_id')."</td>";
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_prod_sku')."</td>";
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_prod_name')."</td>";
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_prod_option')."</td>";					
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_prod_model')."</td>";
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_prod_manu')."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_currency')."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_price')."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_quantity')."</td>";					
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_total')."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_tax')."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_costs')."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_profit')."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_profit_margin')."</td>";					
		$export_pdf_manu_product_list .="</tr>";
		$export_pdf_manu_product_list .="</thead><tbody>";
		$export_pdf_manu_product_list .="<tr>";
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$result['product_ord_idc']."</td>";					
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$result['product_order_date']."</td>";
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$result['product_inv_no']."</td>";				
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$result['product_pidc']."</td>";
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$result['product_sku']."</td>";
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$result['product_name']."</td>";
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$result['product_option']."</td>";					
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$result['product_model']."</td>";
		$export_pdf_manu_product_list .= "<td align='left' nowrap='nowrap'>".$result['product_manu']."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$result['product_currency']."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$result['product_price']."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$result['product_quantity']."</td>";					
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap' style='background-color:#DCFFB9;'>".$result['product_total']."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap'>".$result['product_tax']."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap' style='background-color:#ffd7d7;'>-".$result['product_costs']."</td>";					
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap' style='background-color:#BCD5ED; font-weight:bold;'>".$result['product_profit']."</td>";
		$export_pdf_manu_product_list .= "<td align='right' nowrap='nowrap' style='background-color:#BCD5ED; font-weight:bold;'>".$result['product_profit_margin_percent'] . '%'."</td>";					
		$export_pdf_manu_product_list .="</tr>";					
		$export_pdf_manu_product_list .="</tbody></table>";
	$export_pdf_manu_product_list .="</td>";
	$export_pdf_manu_product_list .="</tr></tbody>";
	$export_pdf_manu_product_list .="</table>";						
	}
	$export_pdf_manu_product_list .="</body></html>";

$dompdf = new DOMPDF();
$dompdf->load_html($export_pdf_manu_product_list);
$dompdf->set_paper("a3", "landscape");
$dompdf->render();
$dompdf->stream("manufacturers_profit_report_product_list_".date("Y-m-d",time()).".pdf");
?>