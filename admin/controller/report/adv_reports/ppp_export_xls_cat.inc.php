<?php
ini_set("memory_limit","1024M");
set_time_limit( 180000 );
			
	$export_xls_cat ="<html><head>";
	$export_xls_cat .="<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>";
	$export_xls_cat .="</head>";
	$export_xls_cat .="<body>";					
	$export_xls_cat .="<table border='1'>";	
	$export_xls_cat .="<tr>";
	if ($filter_group == 'year') {				
	$export_xls_cat .= "<td colspan='2' align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_year')."</td>";
	} elseif ($filter_group == 'quarter') {
	$export_xls_cat .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_year')."</td>";					
	$export_xls_cat .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_quarter')."</td>";				
	} elseif ($filter_group == 'month') {
	$export_xls_cat .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_year')."</td>";					
	$export_xls_cat .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_month')."</td>";
	} else {
	$export_xls_cat .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_date_start')."</td>";				
	$export_xls_cat .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_date_end')."</td>";	
	}
	$export_xls_cat .= "<td align='left' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_category')."</td>";
	$export_xls_cat .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_sold_quantity')."</td>";
	$export_xls_cat .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_sold_percent')."</td>";	
	$export_xls_cat .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_total')."</td>";				
	$export_xls_cat .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_tax')."</td>";				
	$export_xls_cat .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_prod_costs')."</td>";
	$export_xls_cat .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_prod_profit')."</td>";
	$export_xls_cat .= "<td align='right' style='background-color:#D8D8D8; font-weight:bold;'>".$this->language->get('column_profit_margin')."</td>";	
	$export_xls_cat .="</tr>";
	foreach ($results as $result) {		

	$this->load->model('catalog/product');
	$cat =  $this->model_catalog_product->getProductCategories($result['product_id']);
	$categories = $this->model_report_adv_product_profit->getProductsCategories(0); 
			
	$export_xls_cat .="<tr>";
	if ($filter_group == 'year') {				
	$export_xls_cat .= "<td colspan='2' align='left' valign='top'>".$result['year']."</td>";
	} elseif ($filter_group == 'quarter') {
	$export_xls_cat .= "<td align='left' valign='top'>".$result['year']."</td>";
	$export_xls_cat .= "<td align='left' valign='top'>".'Q' . $result['quarter']."</td>";					
	} elseif ($filter_group == 'month') {
	$export_xls_cat .= "<td align='left' valign='top'>".$result['year']."</td>";
	$export_xls_cat .= "<td align='left' valign='top'>".$result['month']."</td>";	
	} else {
	$export_xls_cat .= "<td align='left' valign='top'>".date($this->language->get('date_format_short'), strtotime($result['date_start']))."</td>";
	$export_xls_cat .= "<td align='left' valign='top'>".date($this->language->get('date_format_short'), strtotime($result['date_end']))."</td>";	
	}
	$export_xls_cat .= "<td align='left' valign='top'>";
		foreach ($categories as $category) {
			if (in_array($category['category_id'], $cat)) {
				$export_xls_cat .= "".$category['name']."<br>";
			}
		}
	$export_xls_cat .= "</td>";	
	$export_xls_cat .= "<td align='right' valign='top'>".$result['sold_quantity']."</td>";
	if (!is_null($result['sold_quantity'])) {
	$export_xls_cat .= "<td align='right' valign='top'>".round(100 * ($result['sold_quantity'] / $result['sold_quantity_total']), 2) . '%'."</td>";
	} else {
	$export_xls_cat .= "<td align='right' valign='top'>".'0'."</td>";
	}						
	$export_xls_cat .= "<td align='right' valign='top'>".$this->currency->format($result['total'], $this->config->get('config_currency'))."</td>";					
	$export_xls_cat .= "<td align='right' valign='top'>".$this->currency->format($result['tax'], $this->config->get('config_currency'))."</td>";			
	$export_xls_cat .= "<td align='right' valign='top'>".$this->currency->format('-' . ($result['prod_costs']), $this->config->get('config_currency'))."</td>";
	$export_xls_cat .= "<td align='right' valign='top'>".$this->currency->format($result['prod_profit'], $this->config->get('config_currency'))."</td>";
	if (($result['prod_costs']+$result['prod_profit']) > 0) {				
	$export_xls_cat .= "<td align='right' valign='top'>".round(100 * ($result['prod_profit']) / ($result['prod_costs']+$result['prod_profit']), 2) . '%'."</td>";	
	} else {
	$export_xls_cat .= "<td align='right' valign='top'>".'0%'."</td>";
	}	
	$export_xls_cat .="</tr>";				
	}
	$export_xls_cat .="</table>";		
	$export_xls_cat .="</body></html>";

$filename = "categories_profit_report_".date("Y-m-d",time());
header('Expires: 0');
header('Cache-control: private');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Content-Description: File Transfer');			
header('Content-Type: application/vnd.ms-excel; charset=UTF-8; encoding=UTF-8');			
header('Content-Disposition: attachment; filename='.$filename.".xls");
header('Content-Transfer-Encoding: UTF-8');	
print $export_xls_cat;			
exit;	
?>