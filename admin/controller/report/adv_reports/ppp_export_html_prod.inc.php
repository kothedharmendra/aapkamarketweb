<?php
ini_set("memory_limit","1024M");
set_time_limit( 180000 );
			
	$export_html_prod ="<html><head>";
	$export_html_prod .="<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
	$export_html_prod .="</head>";
	$export_html_prod .="<body>";
	$export_html_prod .="<style type='text/css'>
	.list_main {
		border-collapse: collapse;
		width: 100%;
		border-top: 1px solid #DDDDDD;
		border-left: 1px solid #DDDDDD;	
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}
	.list_main td {
		border-right: 1px solid #DDDDDD;
		border-bottom: 1px solid #DDDDDD;	
	}
	.list_main thead td {
		background-color: #E5E5E5;
		padding: 3px;
		font-weight: bold;
	}
	.list_main tbody a {
		text-decoration: none;
	}
	.list_main tbody td {
		vertical-align: middle;
		padding: 3px;
	}
	.list_main .left {
		text-align: left;
		padding: 7px;
	}
	.list_main .right {
		text-align: right;
		padding: 7px;
	}
	.list_main .center {
		text-align: center;
		padding: 3px;
	}
	</style>";
	$export_html_prod .="<table class='list_main'>";
	$export_html_prod .="<thead>";
	$export_html_prod .="<tr>";
	if ($filter_group == 'year') {				
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_year')."</td>";
	} elseif ($filter_group == 'quarter') {
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_year')."</td>";
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_quarter')."</td>";				
	} elseif ($filter_group == 'month') {
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_year')."</td>";
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_month')."</td>";
	} else {
	$export_html_prod .= "<td align='left' width='80' nowrap='nowrap'>".$this->language->get('column_date_start')."</td>";
	$export_html_prod .= "<td align='left' width='80' nowrap='nowrap'>".$this->language->get('column_date_end')."</td>";	
	}	
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_sku')."</td>";
	$export_html_prod .= "<td align='left'>".$this->language->get('column_name')."</td>";				
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_options')."</td>";
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_model')."</td>";
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_category')."</td>";
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_manufacturer')."</td>";
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$this->language->get('column_status')."</td>";
	$export_html_prod .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_sold_quantity')."</td>";
	$export_html_prod .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_sold_percent')."</td>";	
	$export_html_prod .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_total')."</td>";				
	$export_html_prod .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_tax')."</td>";
	$export_html_prod .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_costs')."</td>";
	$export_html_prod .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_prod_profit')."</td>";	
	$export_html_prod .= "<td align='right' nowrap='nowrap'>".$this->language->get('column_profit_margin')."</td>";				
	$export_html_prod .="</tr>";
	$export_html_prod .="</thead><tbody>";
	foreach ($results as $result) {	
			
	$this->load->model('catalog/product');
	$cat =  $this->model_catalog_product->getProductCategories($result['product_id']);
	$manu = $this->model_report_adv_product_profit->getProductManufacturers($result['manufacturer_id']);	
	$manufacturers = $this->model_report_adv_product_profit->getProductsManufacturers();
	$categories = $this->model_report_adv_product_profit->getProductsCategories(0); 
			
	$export_html_prod .="<tr>";
	if ($filter_group == 'year') {				
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$result['year']."</td>";
	} elseif ($filter_group == 'quarter') {
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$result['year']."</td>";	
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".'Q' . $result['quarter']."</td>";						
	} elseif ($filter_group == 'month') {
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$result['year']."</td>";	
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$result['month']."</td>";	
	} else {
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".date($this->language->get('date_format_short'), strtotime($result['date_start']))."</td>";
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".date($this->language->get('date_format_short'), strtotime($result['date_end']))."</td>";
	}			
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$result['sku']."</td>";
	$export_html_prod .= "<td align='left'>".$result['name']."</td>";				
	$export_html_prod .= "<td align='left' nowrap='nowrap'>";
	if ($filter_ogrouping) {
	if ($result['oovalue']) {
	$export_html_prod .= "<table cellpadding='0' cellspacing='0' style='border:none; font-family:Arial, Helvetica, sans-serif; font-size:12px;'><tbody><tr>";	
	$export_html_prod .= "<td style='border:none;' nowrap='nowrap'>".$result['ooname'].":</td>";	
	$export_html_prod .= "<td style='border:none;' nowrap='nowrap'>".$result['oovalue']."</td>";	
	$export_html_prod .= "</tr></tbody></table>";	
	}
	}
	$export_html_prod .= "</td>";
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".$result['model']."</td>";	
	$export_html_prod .= "<td align='left' nowrap='nowrap'>";
		foreach ($categories as $category) {
			if (in_array($category['category_id'], $cat)) {
				$export_html_prod .= "".$category['name']."<br>";
			}
		}
	$export_html_prod .= "</td>";	
	$export_html_prod .= "<td align='left' nowrap='nowrap'>";
		foreach ($manufacturers as $manufacturer) {
			if (in_array($manufacturer['manufacturer_id'], $manu)) {
				$export_html_prod .= "".$manufacturer['name']."";
			}
		}
	$export_html_prod .= "</td>";
	$export_html_prod .= "<td align='left' nowrap='nowrap'>".($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'))."</td>";
	$export_html_prod .= "<td align='right' nowrap='nowrap'>".$result['sold_quantity']."</td>";
	if (!is_null($result['sold_quantity'])) {		
	$export_html_prod .= "<td align='right' nowrap='nowrap'>".round(100 * ($result['sold_quantity'] / $result['sold_quantity_total']), 2) . '%'."</td>";
	} else {	
	$export_html_prod .= "<td align='right' nowrap='nowrap'>".'0'."</td>";
	}
	$export_html_prod .= "<td align='right' nowrap='nowrap' style='background-color:#DCFFB9;'>".$this->currency->format($result['total'], $this->config->get('config_currency'))."</td>";				
	$export_html_prod .= "<td align='right' nowrap='nowrap'>".$this->currency->format($result['tax'], $this->config->get('config_currency'))."</td>";			
	$export_html_prod .= "<td align='right' nowrap='nowrap' style='background-color:#ffd7d7;'>".$this->currency->format('-' . ($result['prod_costs']), $this->config->get('config_currency'))."</td>";
	$export_html_prod .= "<td align='right' nowrap='nowrap' style='background-color:#BCD5ED; font-weight:bold;'>".$this->currency->format($result['prod_profit'], $this->config->get('config_currency'))."</td>";	
	if (($result['prod_costs']+$result['prod_profit']) > 0) {
	$export_html_prod .= "<td align='right' nowrap='nowrap' style='background-color:#BCD5ED; font-weight:bold;'>".round(100 * ($result['prod_profit']) / ($result['prod_costs']+$result['prod_profit']), 2) . '%'."</td>";				
	} else {
	$export_html_prod .= "<td align='right' nowrap='nowrap'>".'0%'."</td>";
	}		
	$export_html_prod .="</tr>";				
	}
	$export_html_prod .="</tbody></table>";
	$export_html_prod .="</body></html>";

$filename = "products_profit_report_".date("Y-m-d",time());
header('Expires: 0');
header('Cache-control: private');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Content-Description: File Transfer');			
header('Content-Disposition: attachment; filename='.$filename.".html");
print $export_html_prod;			
exit;
?>