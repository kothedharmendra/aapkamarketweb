<?php

class ControllerModuledailyshopin extends Controller {
    
    private $error = array(); 
    
    public function index() {   
        //Load the language file for this module
        $language = $this->load->language('module/dailyshopin');
        $this->data = array_merge($this->data, $language);

        //Set the title from the language file $_['heading_title'] string
        $this->document->setTitle($this->language->get('heading_title'));
        
        //Load the settings model. You can also add any other models you want to load here.
        $this->load->model('setting/setting');
        
        $this->load->model('tool/image');    
        
        //Save the settings if the user has submitted the admin form (ie if someone has pressed save).
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('dailyshopin', $this->request->post);    

                
                    
            $this->session->data['success'] = $this->language->get('text_success');
        
                        
            $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }
        
            $this->data['text_image_manager'] = 'Image manager';
                    $this->data['token'] = $this->session->data['token'];       
        
        $text_strings = array(
                'heading_title',
                'text_enabled',
                'text_disabled',
                'text_content_top',
                'text_content_bottom',
                'text_column_left',
                'text_column_right',
                'entry_status',
                'entry_sort_order',
                'button_save',
                'button_cancel',
        );
        
        foreach ($text_strings as $text) {
            $this->data[$text] = $this->language->get($text);
        }
        

        // store config data
        
        $config_data = array(

        'dailyshopin_status',
		
		//====================================================================================================== BODY
		//-------------------------------------------------------------- body
        'dailyshopin_background_color',
		'dailyshopin_pattern_overlay',		
        'dailyshopin_custom_image',		
        'dailyshopin_custom_pattern',		
        'dailyshopin_image_preview',		
        'dailyshopin_pattern_preview',	
		
		//====================================================================================================== CONTAINER
		//------------------------------------------------------------- container
		'dailyshopin_background_color_cont',
		'dailyshopin_custom_image_cont',
		'dailyshopin_custom_pattern_cont',
		'dailyshopin_pattern_cont',
		
		//====================================================================================================== COLORS
		
		//------------------------------------------------------------- header
		'dailyshopin_thickbar_color',
		'dailyshopin_welcome_text_color',
		
		//------------------------------------------------------------- blocks
		'dailyshopin_block_title_color',
		'dailyshopin_block_desc_color',
		
		//------------------------------------------------------------- Menu
		'dailyshopin_sub_menu_heading',
		'dailyshopin_menu_bg_links_hover',
		'dailyshopin_menu_background',
		'dailyshopin_menu_border',
		'dailyshopin_menu_color',
		'dailyshopin_smenu_color',
		'dailyshopin_smenuh_color',
		
		//------------------------------------------------------------ cart
		'dailyshopin_cart_title_color',
		'dailyshopin_cart_desc_color',
		
		//------------------------------------------------------------- Search
		'dailyshopin_search_bg',
		'dailyshopin_search_border',
		
		//------------------------------------------------------------- Category
		'dailyshopin_category_heading_bg',
		'dailyshopin_category_heading_border',
		'dailyshopin_category_heading_color',
		'dailyshopin_category_link_color',
		'dailyshopin_category_link_color_hover',
		'dailyshopin_category_title',
		
		//------------------------------------------------------------- Box
		'dailyshopin_heading_bg',
		'dailyshopin_heading_border',
		'dailyshopin_heading_color',
		'dailyshopin_heading_tabs_bg',
		'dailyshopin_heading_tabs_border',
		'dailyshopin_prodbcat_title',
		'dailyshopin_prodbcat_viewall',
		'dailyshopin_prodbcat_tab_bg',
		'dailyshopin_prodbcat_tab_bg_select',
		'dailyshopin_prodbcat_tab_link',
		
		//------------------------------------------------------------- product
		'dailyshopin_product_border',
		'dailyshopin_sale_bg',
		'dailyshopin_price_color',
		'dailyshopin_other_oldprice_color',
		'dailyshopin_product_name_color',
		
		//------------------------------------------------------------- general
		'dailyshopin_title_color',
		'dailyshopin_links_color',
		
		//------------------------------------------------------------- footer
		'dailyshopin_footerheaders_color',
		'dailyshopin_footerheaderswid_color',
		'dailyshopin_footerheaderswid_bg',
		'dailyshopin_footerheaderswid_border',
		'dailyshopin_first_border_color',
		'dailyshopin_second_border_color',
		'dailyshopin_footer_info_color',
		'dailyshopin_footerlinks_color',
		'dailyshopin_footer_bg',
		'dailyshopin_footer_bottom_bg',
		'footer_info_title',
		'facebook_bg_color',
		
		//====================================================================================================== FONTS
		'dailyshopin_title_font',
		'dailyshopin_title_font_family',
		'dailyshopin_title_font_size',
		
        'dailyshopin_body_font',
		'dailyshopin_body_font_family',
		'dailyshopin_body_font_size',
		
		'dailyshopin_button_font',
		'dailyshopin_button_font_family',
		'dailyshopin_button_font_size',
		
        'dailyshopin_category_font',
		'dailyshopin_category_font_family',
		'dailyshopin_category_font_size',
		
		'dailyshopin_menu_font',
		'dailyshopin_menu_font_family',
		'dailyshopin_menu_font_size',
		
		'dailyshopin_product_font',
		'dailyshopin_product_font_family',
		'dailyshopin_product_font_size',
		
		'dailyshopin_price_font',
		'dailyshopin_price_font_family',
		'dailyshopin_price_font_size',
		
		'dailyshopin_boxheading_font',
		'dailyshopin_boxheading_font_family',
		'dailyshopin_boxheading_font_size',
		
		//====================================================================================================== BUTTON
		'dailyshopin_details_addtocart',
		'dailyshopin_details_addtocart_hover',
		'dailyshopin_details_button_color',
		
		//======================================================================================================= CUSTOM Settings
		//top
		'dailyshopin_facebook_id',
        'dailyshopin_twitter_username',
		'consumer_keyy',
		'consumer_secrett',
		'access_token',
		'token_secret',
		'custom_css',
		//social icons
		'dailyshopin_facebook_link',
		'dailyshopin_twitter_link',
		'dailyshopin_google_link',
		'dailyshopin_youtube_link',
		'dailyshopin_linkedin_link',
		'dailyshopin_digg_link',
		'dailyshopin_dribbble_link',
		'dailyshopin_flickr_link',
		'dailyshopin_pinterest_link',
		'dailyshopin_skype_link',
		'dailyshopin_vimeo_link',
		'dailyshopin_rss_link',
		//
		'dailyshopin_time_text',
		//blocks
		'dailyshopin_firstblock_img',
		'dailyshopin_firstblock_title',
		'dailyshopin_firstblock_desc',
		'dailyshopin_secondblock_img',
		'dailyshopin_secondblock_title',
		'dailyshopin_secondblock_desc',
		'dailyshopin_thirdblock_img',
		'dailyshopin_thirdblock_title',
		'dailyshopin_thirdblock_desc',
		'dailyshopin_fourthblock_img',
		'dailyshopin_fourthblock_title',
		'dailyshopin_fourthblock_desc',
		//contact
		'dailyshopin_contact_img',
		'dailyshopin_address',
		'dailyshopin_phone',
		'dailyshopin_phone_second',
		'dailyshopin_email',
		'dailyshopin_email_second',
		//custom widget
		'dailyshopin_custom_widget_title',
		'dailyshopin_footer_info_text',
		'dailyshopin_shipping_text',
		'dailyshopin_shipping_first_text',
		'dailyshopin_shipping_last_text',
		'dailyshopin_cus_img',
		//payment
		'dailyshopin_pay1_img',
		'dailyshopin_pay2_img',
		'dailyshopin_pay3_img',
		'dailyshopin_pay4_img',
		'dailyshopin_pay5_img',
		'dailyshopin_pay6_img',
		'dailyshopin_pay7_img',
		'dailyshopin_pay8_img',
		'dailyshopin_pay9_img',
		'dailyshopin_pay10_img',
		'dailyshopin_pay1_link',
		'dailyshopin_pay2_link',
		'dailyshopin_pay3_link',
		'dailyshopin_pay4_link',
		'dailyshopin_pay5_link',
		'dailyshopin_pay6_link',
		'dailyshopin_pay7_link',
		'dailyshopin_pay8_link',
		'dailyshopin_pay9_link',
		'dailyshopin_pay10_link',
		'dailyshopin_pay1_preview',
		'dailyshopin_pay2_preview',
		'dailyshopin_pay3_preview',
		'dailyshopin_pay4_preview',
		'dailyshopin_pay5_preview',
		'dailyshopin_pay6_preview',
		'dailyshopin_pay7_preview',
		'dailyshopin_pay8_preview',
		'dailyshopin_pay9_preview',
		'dailyshopin_pay10_preview',
		//others
		'option_slideshow',
		
		
		
		'dailyshopin_breadcrumb_color',
		'dailyshopin_second_color',
		'dailyshopin_lines_color',
		
		'dailyshopin_toplinks_color',
        
		'dailyshopin_title_color_hover',
        'dailyshopin_bodytext_color',
        'dailyshopin_lighttext_color',
        
        
		
		'dailyshopin_cart_text_color',
		
		
		
		'dailyshopin_all_addtocart',
		'dailyshopin_all_addtocart_hover',
	    
		'dailyshopin_menu_color_hover',
		


		'dailyshopin_language_text_color',
		'dailyshopin_currency_text_color',
        
		'dailyshopin_all_buttons_color',
		'dailyshopin_heading_bgcolor',
        'dailyshopin_copyright',


        );
        
        foreach ($config_data as $conf) {
            if (isset($this->request->post[$conf])) {
                $this->data[$conf] = $this->request->post[$conf];
            } else {
                $this->data[$conf] = $this->config->get($conf);
            }
        }
    
        //This creates an error message. The error['warning'] variable is set by the call to function validate() in this controller (below)
        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        
        //SET UP BREADCRUMB TRAIL. YOU WILL NOT NEED TO MODIFY THIS UNLESS YOU CHANGE YOUR MODULE NAME.
        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
        
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/dailyshopin', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
        
        $this->data['action'] = $this->url->link('module/dailyshopin', 'token=' . $this->session->data['token'], 'SSL');
        
        $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

    
        //This code handles the situation where you have multiple instances of this module, for different layouts.
        if (isset($this->request->post['dailyshopin_module'])) {
            $modules = explode(',', $this->request->post['dailyshopin_module']);
        } elseif ($this->config->get('dailyshopin_module') != '') {
            $modules = explode(',', $this->config->get('dailyshopin_module'));
        } else {
            $modules = array();
        }           
                
        $this->load->model('design/layout');
        
        $this->data['layouts'] = $this->model_design_layout->getLayouts();
                
        foreach ($modules as $module) {
            if (isset($this->request->post['dailyshopin_' . $module . '_layout_id'])) {
                $this->data['dailyshopin_' . $module . '_layout_id'] = $this->request->post['dailyshopin_' . $module . '_layout_id'];
            } else {
                $this->data['dailyshopin_' . $module . '_layout_id'] = $this->config->get('dailyshopin_' . $module . '_layout_id');
            }   
            
            if (isset($this->request->post['dailyshopin_' . $module . '_position'])) {
                $this->data['dailyshopin_' . $module . '_position'] = $this->request->post['dailyshopin_' . $module . '_position'];
            } else {
                $this->data['dailyshopin_' . $module . '_position'] = $this->config->get('dailyshopin_' . $module . '_position');
            }   
            
            if (isset($this->request->post['dailyshopin_' . $module . '_status'])) {
                $this->data['dailyshopin_' . $module . '_status'] = $this->request->post['dailyshopin_' . $module . '_status'];
            } else {
                $this->data['dailyshopin_' . $module . '_status'] = $this->config->get('dailyshopin_' . $module . '_status');
            }   
                        
            if (isset($this->request->post['dailyshopin_' . $module . '_sort_order'])) {
                $this->data['dailyshopin_' . $module . '_sort_order'] = $this->request->post['dailyshopin_' . $module . '_sort_order'];
            } else {
                $this->data['dailyshopin_' . $module . '_sort_order'] = $this->config->get('dailyshopin_' . $module . '_sort_order');
            }               
        }
        

        
        $this->data['modules'] = $modules;
        
        if (isset($this->request->post['dailyshopin_module'])) {
            $this->data['dailyshopin_module'] = $this->request->post['dailyshopin_module'];
        } else {
            $this->data['dailyshopin_module'] = $this->config->get('dailyshopin_module');
        }

        //Choose which template file will be used to display this request.
        $this->template = 'module/dailyshopin.tpl';
        $this->children = array(
            'common/header',
            'common/footer',
        );

        if (isset($this->data['dailyshopin_custom_pattern']) && $this->data['dailyshopin_custom_pattern'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_custom_pattern'])) {
            $this->data['dailyshopin_pattern_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_custom_pattern'], 70, 70);
        } else {
            $this->data['dailyshopin_pattern_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }
        
        
        if (isset($this->data['dailyshopin_custom_image']) && $this->data['dailyshopin_custom_image'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_custom_image'])) {
            $this->data['dailyshopin_image_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_custom_image'], 70, 70);
        } else {
            $this->data['dailyshopin_image_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		
		
		
/////////////////// header //////////////////	
		
			
		 if (isset($this->data['dailyshopin_custom_pattern_head']) && $this->data['dailyshopin_custom_pattern_head'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_custom_pattern_head'])) {
            $this->data['dailyshopin_pattern_preview_head'] = $this->model_tool_image->resize($this->data['dailyshopin_custom_pattern_head'], 70, 70);
        } else {
            $this->data['dailyshopin_pattern_preview_head'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }
        
			
        
        if (isset($this->data['dailyshopin_custom_image_head']) && $this->data['dailyshopin_custom_image_head'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_custom_image'])) {
            $this->data['dailyshopin_image_preview_head'] = $this->model_tool_image->resize($this->data['dailyshopin_custom_image_head'], 70, 70);
        } else {
            $this->data['dailyshopin_image_preview_head'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		
		
		
		

/////////////////// footerTop //////////////////	
		
			
		 if (isset($this->data['dailyshopin_custom_pattern_footerTop']) && $this->data['dailyshopin_custom_pattern_footerTop'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_custom_pattern_footerTop'])) {
            $this->data['dailyshopin_pattern_preview_footerTop'] = $this->model_tool_image->resize($this->data['dailyshopin_custom_pattern_footerTop'], 70, 70);
        } else {
            $this->data['dailyshopin_pattern_preview_footerTop'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }
        
		
        
        if (isset($this->data['dailyshopin_custom_image_footerTop']) && $this->data['dailyshopin_custom_image_footerTop'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_custom_image'])) {
            $this->data['dailyshopin_image_preview_footerTop'] = $this->model_tool_image->resize($this->data['dailyshopin_custom_image_footerTop'], 70, 70);
        } else {
            $this->data['dailyshopin_image_preview_footerTop'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);		




/////////////////// footer //////////////////	
		
			
		 if (isset($this->data['dailyshopin_custom_pattern_footer']) && $this->data['dailyshopin_custom_pattern_footer'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_custom_pattern_footer'])) {
            $this->data['dailyshopin_pattern_preview_footer'] = $this->model_tool_image->resize($this->data['dailyshopin_custom_pattern_footer'], 70, 70);
        } else {
            $this->data['dailyshopin_pattern_preview_footer'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }
        
		
        
        if (isset($this->data['dailyshopin_custom_image_footer']) && $this->data['dailyshopin_custom_image_footer'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_custom_image'])) {
            $this->data['dailyshopin_image_preview_footer'] = $this->model_tool_image->resize($this->data['dailyshopin_custom_image_footer'], 70, 70);
        } else {
            $this->data['dailyshopin_image_preview_footer'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);				
		
		
		
		
/////////////////// container //////////////////		
		
		
		 if (isset($this->data['dailyshopin_custom_pattern_cont']) && $this->data['dailyshopin_custom_pattern_cont'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_custom_pattern_cont'])) {
            $this->data['dailyshopin_pattern_preview_cont'] = $this->model_tool_image->resize($this->data['dailyshopin_custom_pattern_cont'], 70, 70);
        } else {
            $this->data['dailyshopin_pattern_preview_cont'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }
        
        
        if (isset($this->data['dailyshopin_custom_image_cont']) && $this->data['dailyshopin_custom_image_cont'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_custom_image'])) {
            $this->data['dailyshopin_image_preview_cont'] = $this->model_tool_image->resize($this->data['dailyshopin_custom_image_cont'], 70, 70);
        } else {
            $this->data['dailyshopin_image_preview_cont'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);


		
		/////////////////// custom settings //////////////////	

		//first block img
		
		if (isset($this->data['dailyshopin_firstblock_img']) && $this->data['dailyshopin_firstblock_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_firstblock_img'])) {
            $this->data['dailyshopin_firstblock_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_firstblock_img'], 70, 70);
        } else {
            $this->data['dailyshopin_firstblock_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		//second block img
		
		if (isset($this->data['dailyshopin_secondblock_img']) && $this->data['dailyshopin_secondblock_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_secondblock_img'])) {
            $this->data['dailyshopin_secondblock_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_secondblock_img'], 70, 70);
        } else {
            $this->data['dailyshopin_secondblock_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		//third block img
		
		if (isset($this->data['dailyshopin_thirdblock_img']) && $this->data['dailyshopin_thirdblock_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_thirdblock_img'])) {
            $this->data['dailyshopin_thirdblock_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_thirdblock_img'], 70, 70);
        } else {
            $this->data['dailyshopin_thirdblock_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		//fourth block img
		
		if (isset($this->data['dailyshopin_fourthblock_img']) && $this->data['dailyshopin_fourthblock_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_fourthblock_img'])) {
            $this->data['dailyshopin_fourthblock_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_fourthblock_img'], 70, 70);
        } else {
            $this->data['dailyshopin_fourthblock_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		
		//contact img
		
		if (isset($this->data['dailyshopin_contact_img']) && $this->data['dailyshopin_contact_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_contact_img'])) {
            $this->data['dailyshopin_contact_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_contact_img'], 70, 70);
        } else {
            $this->data['dailyshopin_contact_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		
		//cus img
		
		if (isset($this->data['dailyshopin_cus_img']) && $this->data['dailyshopin_cus_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_cus_img'])) {
            $this->data['dailyshopin_cus_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_cus_img'], 70, 70);
        } else {
            $this->data['dailyshopin_cus_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);

		
		//pay1
		
		if (isset($this->data['dailyshopin_pay1_img']) && $this->data['dailyshopin_pay1_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_pay1_img'])) {
            $this->data['dailyshopin_pay1_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_pay1_img'], 70, 70);
        } else {
            $this->data['dailyshopin_pay1_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		//pay2
		
		if (isset($this->data['dailyshopin_pay2_img']) && $this->data['dailyshopin_pay2_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_pay2_img'])) {
            $this->data['dailyshopin_pay2_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_pay2_img'], 70, 70);
        } else {
            $this->data['dailyshopin_pay2_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		//pay3
		
		if (isset($this->data['dailyshopin_pay3_img']) && $this->data['dailyshopin_pay3_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_pay3_img'])) {
            $this->data['dailyshopin_pay3_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_pay3_img'], 70, 70);
        } else {
            $this->data['dailyshopin_pay3_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		//pay4
		
		if (isset($this->data['dailyshopin_pay4_img']) && $this->data['dailyshopin_pay4_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_pay4_img'])) {
            $this->data['dailyshopin_pay4_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_pay4_img'], 70, 70);
        } else {
            $this->data['dailyshopin_pay4_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		//pay5
		
		if (isset($this->data['dailyshopin_pay5_img']) && $this->data['dailyshopin_pay5_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_pay5_img'])) {
            $this->data['dailyshopin_pay5_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_pay5_img'], 70, 70);
        } else {
            $this->data['dailyshopin_pay5_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		//pay6
		
		if (isset($this->data['dailyshopin_pay6_img']) && $this->data['dailyshopin_pay6_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_pay6_img'])) {
            $this->data['dailyshopin_pay6_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_pay6_img'], 70, 70);
        } else {
            $this->data['dailyshopin_pay6_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		//pay7
		
		if (isset($this->data['dailyshopin_pay7_img']) && $this->data['dailyshopin_pay7_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_pay7_img'])) {
            $this->data['dailyshopin_pay7_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_pay7_img'], 70, 70);
        } else {
            $this->data['dailyshopin_pay7_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		//pay8
		
		if (isset($this->data['dailyshopin_pay8_img']) && $this->data['dailyshopin_pay8_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_pay8_img'])) {
            $this->data['dailyshopin_pay8_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_pay8_img'], 70, 70);
        } else {
            $this->data['dailyshopin_pay8_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		//pay9
		
		if (isset($this->data['dailyshopin_pay9_img']) && $this->data['dailyshopin_pay9_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_pay9_img'])) {
            $this->data['dailyshopin_pay9_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_pay9_img'], 70, 70);
        } else {
            $this->data['dailyshopin_pay9_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		//pay10
		
		if (isset($this->data['dailyshopin_pay10_img']) && $this->data['dailyshopin_pay10_img'] != "" && file_exists(DIR_IMAGE . $this->data['dailyshopin_pay10_img'])) {
            $this->data['dailyshopin_pay10_preview'] = $this->model_tool_image->resize($this->data['dailyshopin_pay10_img'], 70, 70);
        } else {
            $this->data['dailyshopin_pay10_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		
		
		
        //Send the output.
        $this->response->setOutput($this->render());
    }
    
    /*
     * 
     * This function is called to ensure that the settings chosen by the admin user are allowed/valid.
     * You can add checks in here of your own.
     * 
     */
    
    private function validate() {
        if (!$this->user->hasPermission('modify', 'module/dailyshopin')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        
        if (!$this->error) {
            return TRUE;
        } else {
            return FALSE;
        }   
    }


}
?>