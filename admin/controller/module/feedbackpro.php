<?php
class ControllerModuleFeedbackpro extends Controller {
	private $error = array(); 
	public function index() {   
		$this->load->language('module/feedbackpro');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {

			$fb = array();
			$all = array();

			foreach($this->request->post['feedback'] as $keys => $modinfo) {
				if(!empty($modinfo)) {
					$fb['feedback_'.$keys] = $modinfo;
					$all[] = $keys;
				}
			}
			$fb['feedback_all'] = $all;
			$this->model_setting_setting->editSetting('feedback',$fb);			
			$this->session->data['success'] = $this->language->get('text_success');	
			$this->redirect($this->url->link('module/feedbackpro', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_namefield'] = $this->language->get('text_namefield');
		$this->data['text_namefieldfaq'] = $this->language->get('text_namefieldfaq');
		$this->data['text_addmask'] = $this->language->get('text_addmask');
		$this->data['text_dellmask'] = $this->language->get('text_dellmask');
		$this->data['text_maskfaq'] = $this->language->get('text_maskfaq');
		$this->data['text_dellfield'] = $this->language->get('text_dellfield');
		$this->data['text_addfield'] = $this->language->get('text_addfield');
		$this->data['text_namemodule'] = $this->language->get('text_namemodule');
		$this->data['text_namemodulefaq'] = $this->language->get('text_namemodulefaq');
		$this->data['text_namelink'] = $this->language->get('text_namelink');
		$this->data['text_namelinkfaq'] = $this->language->get('text_namelinkfaq');
		$this->data['text_nameemail'] = $this->language->get('text_nameemail');
		$this->data['text_nameemailfaq'] = $this->language->get('text_nameemailfaq');
		$this->data['text_namecomment'] = $this->language->get('text_namecomment');
		$this->data['text_namecommentfaq'] = $this->language->get('text_namecommentfaq');
		$this->data['text_nameonlytext'] = $this->language->get('text_nameonlytext');
		$this->data['text_nameonlytextfaq'] = $this->language->get('text_nameonlytextfaq');
		$this->data['text_namedatatime'] = $this->language->get('text_namedatatime');
		$this->data['text_namedatatimefaq'] = $this->language->get('text_namedatatimefaq');
		$this->data['text_namerequired'] = $this->language->get('text_namerequired');
		$this->data['text_namerequiredfaq'] = $this->language->get('text_namerequiredfaq');
		$this->data['text_namemainfield'] = $this->language->get('text_namemainfield');
		$this->data['text_namemainfieldfaq'] = $this->language->get('text_namemainfieldfaq');
		$this->data['text_nameaction'] = $this->language->get('text_nameaction');
		$this->data['text_copyright'] = $this->language->get('text_copyright');
				
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
		$this->data['tab_module'] = $this->language->get('tab_module');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/feedbackpro', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/feedbackpro', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['modules'] = array();
		
		if (isset($this->request->post['feedback'])) {
			$this->data['modules'] = $this->request->post['feedback'];
		} elseif ($this->config->get('feedback_all')) { 
			foreach($this->config->get('feedback_all') as $key) {
				$this->data['modules']['feedback'][$key] = $this->config->get('feedback_'.$key);
			}
		}	
		$this->load->model('design/layout');
		$this->data['layouts'] = $this->model_design_layout->getLayouts();		
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$this->template = 'module/feedbackpro.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);		
		$this->response->setOutput($this->render());
	}
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/feedbackpro')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	public function install() {
		$this->load->model('catalog/feedbackpro');
		$this->model_catalog_feedbackpro->createDatabaseTables();
	}
	public function uninstall() {
		$this->load->model('catalog/feedbackpro');
		$this->model_catalog_feedbackpro->dropDatabaseTables();
	}
}
?>