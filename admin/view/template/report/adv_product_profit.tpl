<?php echo $header; ?>

<div class="report-logo">
    <a href="<?php echo $home; ?>">
        <img src="view/image/admin_theme/base5builder_impulsepro/logo.png" />
    </a>
</div><!-- report-logo -->

<script type="text/javascript">
$(document).ready(function() { 
  $("#pagination_content").hide(); 
  $(window).load(function() { 
    $("#pagination_content").show(); 
    $("#content-loading").hide(); 
  }) 
}) 
</script>
<div id="content-loading" style="position: absolute; background-color:white; layer-background-color:white; height:100%; width:100%; text-align:center;"><img src="view/image/page_loading.gif" border="0"></div>
<style type="text/css">
.box > .content_report {
	padding: 10px;
	border-left: 1px solid #CCCCCC;
	border-right: 1px solid #CCCCCC;
	border-bottom: 1px solid #CCCCCC;
	min-height: 300px;
}
.list_main {
	border-collapse: collapse;
	width: 100%;
	border-top: 1px solid #DDDDDD;
	border-left: 1px solid #DDDDDD;	
	margin-bottom: 20px;
}
.list_main td {
	border-right: 1px solid #DDDDDD;
	border-bottom: 1px solid #DDDDDD;	
}
.list_main thead td {
	background-color: #E5E5E5;
	padding: 0px 5px;
	font-weight: bold;	
}
.list_main tbody td {
	vertical-align: middle;
	padding: 0px 5px;
}
.list_main .left {
	text-align: left;
	padding: 7px;
}
.list_main .right {
	text-align: right;
	padding: 7px;
}
.list_main .center {
	text-align: center;
	padding: 3px;
}
.list_main .noresult {
	text-align: center;
	padding: 7px;
}

.list_detail {
	border-collapse: collapse;
	width: 100%;
	border-top: 1px solid #DDDDDD;
	border-left: 1px solid #DDDDDD;
	margin-top: 10px;
	margin-bottom: 10px;
}
.list_detail td {
	border-right: 1px solid #DDDDDD;
	border-bottom: 1px solid #DDDDDD;
}
.list_detail thead td {
	background-color: #F0F0F0;
	padding: 0px 3px;
	font-size: 11px;
}
.list_detail tbody td {
	padding: 0px 3px;
	font-size: 11px;	
}
.list_detail .left {
	text-align: left;
	padding: 3px;
}
.list_detail .right {
	text-align: right;
	padding: 3px;
}
.list_detail .center {
	text-align: center;
	padding: 3px;
}

#mask {
	position: absolute;
	left: 0;
	top: 0;
	z-index: 9000;
	background-color: #000000;
	display: none;
}
#boxes .window {
	position: fixed;
	left: 0;
	top: 0;
	display: none;
	z-index: 9999;
}
#boxes #dialog {
	background:#FFFFFF; 
	border: 2px solid #ff9f00; 
	padding: 10px;
}

.export_item {
  text-decoration: none;
  cursor: pointer;
}
.export_item a {
  text-decoration: none;
}
.export_item :hover {
  opacity: 0.7;
  -moz-opacity: 0.7;
  -ms-filter: "alpha(opacity=70)"; /* IE 8 */
  filter: alpha(opacity=70); /* IE < 8 */
} 
.noexport_item {
  opacity: 0.5;
  -moz-opacity: 0.5;
  -ms-filter: "alpha(opacity=50)"; /* IE 8 */
  filter: alpha(opacity=50); /* IE < 8 */
} 
a.cbutton {
	text-decoration: none;
	color: #FFF;
	display: inline-block;
	padding: 5px 15px 5px 15px;
	-webkit-border-radius: 5px 5px 5px 5px;
	-moz-border-radius: 5px 5px 5px 5px;
	-khtml-border-radius: 5px 5px 5px 5px;
	border-radius: 5px 5px 5px 5px;
}

.pagination_report {
	padding:3px;
	margin:3px;
	text-align:right;
}
.pagination_report a {
	padding: 4px 8px 4px 8px;
	margin-right: 2px;
	border: 1px solid #ddd;
	text-decoration: none; 
	color: #666;
}
.pagination_report a:hover, .pagination_report a:active {
	padding: 4px 8px 4px 8px;
	margin-right: 2px;
	border: 1px solid #c0c0c0;
}
.pagination_report span.current {
	padding: 4px 8px 4px 8px;
	margin-right: 2px;
	border: 1px solid #a0a0a0;
	font-weight: bold;
	background-color: #f0f0f0;
	color: #666;
}
.pagination_report span.disabled {
	padding: 4px 8px 4px 8px;
	margin-right: 2px;
	border: 1px solid #f3f3f3;
	color: #ccc;
}

#content{
  margin: 0px !important;
}
#left-column{
   display: none;
}
thead td {
  font-weight: normal !important;
}

#content .heading h1{
    left: 210px;
}
#content .heading h1 img{
    display: none;
}
</style>
<link href="view/stylesheet/jquery.multiSelect.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function getSelectOptions() {
	rep=document.forms.report;
	switch(rep.filter_report.value) {
	
	case "products":
		rep.filter_sort.options[0].value='date';
		rep.filter_sort.options[0].text="<?php echo $column_date; ?>";		
		rep.filter_sort.options[1].value='sku';
		rep.filter_sort.options[1].text="<?php echo $column_sku; ?>";
		rep.filter_sort.options[1].disabled=false;
		rep.filter_sort.options[1].style.color="#000000";			
		rep.filter_sort.options[2].value='name';
		rep.filter_sort.options[2].text="<?php echo $column_name; ?>";
		rep.filter_sort.options[2].disabled=false;
		rep.filter_sort.options[2].style.color="#000000";			
		rep.filter_sort.options[3].value='model';
		rep.filter_sort.options[3].text="<?php echo $column_model; ?>";
		rep.filter_sort.options[3].disabled=false;
		rep.filter_sort.options[3].style.color="#000000";			
		rep.filter_sort.options[4].value='category';
		rep.filter_sort.options[4].text="<?php echo $column_category; ?>";
		rep.filter_sort.options[4].disabled=false;
		rep.filter_sort.options[4].style.color="#000000";			
		rep.filter_sort.options[5].value='manufacturer';
		rep.filter_sort.options[5].text="<?php echo $column_manufacturer; ?>";
		rep.filter_sort.options[5].disabled=false;
		rep.filter_sort.options[5].style.color="#000000";			
		rep.filter_sort.options[6].value='status';
		rep.filter_sort.options[6].text="<?php echo $column_status; ?>";
		rep.filter_sort.options[6].disabled=false;
		rep.filter_sort.options[6].style.color="#000000";			
		rep.filter_sort.options[7].value='quantity';
		rep.filter_sort.options[7].text="<?php echo $column_sold_quantity; ?>";
		rep.filter_sort.options[8].value='total';
		rep.filter_sort.options[8].text="<?php echo $column_total; ?>";
		rep.filter_sort.options[9].value='tax';
		rep.filter_sort.options[9].text="<?php echo $column_tax; ?>";
		rep.filter_sort.options[10].value='prod_costs';
		rep.filter_sort.options[10].text="<?php echo $column_prod_costs; ?>";
		rep.filter_sort.options[11].value='prod_profit';
		rep.filter_sort.options[11].text="<?php echo $column_prod_profit; ?>"; 
		
		rep.filter_details.options[0].value=0;
		rep.filter_details.options[0].text="<?php echo $text_no_details; ?>";		
		rep.filter_details.options[1].value=1;
		rep.filter_details.options[1].text="<?php echo $text_order_list; ?>";
		rep.filter_details.options[1].disabled=false;
		rep.filter_details.options[1].style.color="#000000";			
		rep.filter_details.options[2].value=3;
		rep.filter_details.options[2].text="<?php echo $text_product_list; ?>";
		rep.filter_details.options[2].disabled=true;
		rep.filter_details.options[2].style.color="#999999";	
		rep.filter_details.options[3].value=2;
		rep.filter_details.options[3].text="<?php echo $text_customer_list; ?>";
		rep.filter_details.options[3].disabled=false;
		rep.filter_details.options[3].style.color="#000000";			
	break;
 
	case "manufacturers":
		rep.filter_sort.options[0].value='date';
		rep.filter_sort.options[0].text="<?php echo $column_date; ?>";		
		rep.filter_sort.options[1].value='sku';
		rep.filter_sort.options[1].text="<?php echo $column_sku; ?>";
		rep.filter_sort.options[1].disabled=true;
		rep.filter_sort.options[1].style.color="#999999";			
		rep.filter_sort.options[2].value='name';
		rep.filter_sort.options[2].text="<?php echo $column_name; ?>";
		rep.filter_sort.options[2].disabled=true;
		rep.filter_sort.options[2].style.color="#999999";			
		rep.filter_sort.options[3].value='model';
		rep.filter_sort.options[3].text="<?php echo $column_model; ?>";
		rep.filter_sort.options[3].disabled=true;
		rep.filter_sort.options[3].style.color="#999999";			
		rep.filter_sort.options[4].value='category';
		rep.filter_sort.options[4].text="<?php echo $column_category; ?>";
		rep.filter_sort.options[4].disabled=true;
		rep.filter_sort.options[4].style.color="#999999";			
		rep.filter_sort.options[5].value='manufacturer';
		rep.filter_sort.options[5].text="<?php echo $column_manufacturer; ?>";
		rep.filter_sort.options[5].disabled=false;
		rep.filter_sort.options[5].style.color="#000000";	
		rep.filter_sort.options[6].value='status';
		rep.filter_sort.options[6].text="<?php echo $column_status; ?>";
		rep.filter_sort.options[6].disabled=true;
		rep.filter_sort.options[6].style.color="#999999";			
		rep.filter_sort.options[7].value='quantity';
		rep.filter_sort.options[7].text="<?php echo $column_sold_quantity; ?>";
		rep.filter_sort.options[8].value='total';
		rep.filter_sort.options[8].text="<?php echo $column_total; ?>";
		rep.filter_sort.options[9].value='tax';
		rep.filter_sort.options[9].text="<?php echo $column_tax; ?>";
		rep.filter_sort.options[10].value='prod_costs';
		rep.filter_sort.options[10].text="<?php echo $column_prod_costs; ?>";
		rep.filter_sort.options[11].value='prod_profit';
		rep.filter_sort.options[11].text="<?php echo $column_prod_profit; ?>";
		
		rep.filter_details.options[0].value=0;
		rep.filter_details.options[0].text="<?php echo $text_no_details; ?>";		
		rep.filter_details.options[1].value=1;
		rep.filter_details.options[1].text="<?php echo $text_order_list; ?>";
		rep.filter_details.options[1].disabled=true;
		rep.filter_details.options[1].style.color="#999999";			
		rep.filter_details.options[2].value=3;
		rep.filter_details.options[2].text="<?php echo $text_product_list; ?>";
		rep.filter_details.options[2].disabled=false;
		rep.filter_details.options[2].style.color="#000000";	
		rep.filter_details.options[3].value=2;
		rep.filter_details.options[3].text="<?php echo $text_customer_list; ?>";
		rep.filter_details.options[3].disabled=true;
		rep.filter_details.options[3].style.color="#999999";			
	break;
 
	case "categories":
		rep.filter_sort.options[0].value='date';
		rep.filter_sort.options[0].text="<?php echo $column_date; ?>";			
		rep.filter_sort.options[1].value='sku';
		rep.filter_sort.options[1].text="<?php echo $column_sku; ?>";
		rep.filter_sort.options[1].disabled=true;
		rep.filter_sort.options[1].style.color="#999999";			
		rep.filter_sort.options[2].value='name';
		rep.filter_sort.options[2].text="<?php echo $column_name; ?>";
		rep.filter_sort.options[2].disabled=true;
		rep.filter_sort.options[2].style.color="#999999";			
		rep.filter_sort.options[3].value='model';
		rep.filter_sort.options[3].text="<?php echo $column_model; ?>";
		rep.filter_sort.options[3].disabled=true;
		rep.filter_sort.options[3].style.color="#999999";			
		rep.filter_sort.options[4].value='category';
		rep.filter_sort.options[4].text="<?php echo $column_category; ?>";
		rep.filter_sort.options[4].disabled=false;
		rep.filter_sort.options[4].style.color="#000000";	
		rep.filter_sort.options[5].value='manufacturer';
		rep.filter_sort.options[5].text="<?php echo $column_manufacturer; ?>";
		rep.filter_sort.options[5].disabled=true;
		rep.filter_sort.options[5].style.color="#999999";			
		rep.filter_sort.options[6].value='status';
		rep.filter_sort.options[6].text="<?php echo $column_status; ?>";
		rep.filter_sort.options[6].disabled=true;
		rep.filter_sort.options[6].style.color="#999999";			
		rep.filter_sort.options[7].value='quantity';
		rep.filter_sort.options[7].text="<?php echo $column_sold_quantity; ?>";
		rep.filter_sort.options[8].value='total';
		rep.filter_sort.options[8].text="<?php echo $column_total; ?>";
		rep.filter_sort.options[9].value='tax';
		rep.filter_sort.options[9].text="<?php echo $column_tax; ?>";
		rep.filter_sort.options[10].value='prod_costs';
		rep.filter_sort.options[10].text="<?php echo $column_prod_costs; ?>";
		rep.filter_sort.options[11].value='prod_profit';
		rep.filter_sort.options[11].text="<?php echo $column_prod_profit; ?>";
		
		rep.filter_details.options[0].value=0;
		rep.filter_details.options[0].text="<?php echo $text_no_details; ?>";		
		rep.filter_details.options[1].value=1;
		rep.filter_details.options[1].text="<?php echo $text_order_list; ?>";
		rep.filter_details.options[1].disabled=true;
		rep.filter_details.options[1].style.color="#999999";			
		rep.filter_details.options[2].value=3;
		rep.filter_details.options[2].text="<?php echo $text_product_list; ?>";
		rep.filter_details.options[2].disabled=false;
		rep.filter_details.options[2].style.color="#000000";	
		rep.filter_details.options[3].value=2;
		rep.filter_details.options[3].text="<?php echo $text_customer_list; ?>";
		rep.filter_details.options[3].disabled=true;
		rep.filter_details.options[3].style.color="#999999";		
	break;
	}
}
</script>
<form method="post" action="index.php?route=report/adv_product_profit&token=<?php echo $token; ?>" id="report" name="report"> 
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1><span class="vtip" title="<?php echo $text_profit_help; ?>"><img style="padding-top:10px; padding-left:5px;" src="view/image/profit_info.png" alt="" /></span><span style="float:right; padding-top:5px; padding-right:5px; font-size:11px; color:#666; text-align:right;"><?php echo $heading_version; ?></span></div>
      <div align="right" style="height:38px; background-color:#F0F0F0; border: 1px solid #DDDDDD; margin-top:5px;">
      <div style="padding-top: 7px; margin-right: 5px;"><?php echo $entry_report; ?>
          <select name="filter_report" id="filter_report" onchange="$('#report').submit();" style="background-color:#ffcc99; border-width:thin; border-color:#333;"> 
              <?php foreach ($report as $report) { ?>
              <?php if ($report['value'] == $filter_report) { ?>
              <option value="<?php echo $report['value']; ?>" title="<?php echo $report['text']; ?>" selected="selected"><?php echo $report['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $report['value']; ?>" title="<?php echo $report['text']; ?>"><?php echo $report['text']; ?></option>
              <?php } ?>
              <?php } ?>
          </select>&nbsp;&nbsp; 
      	  <label <?php echo ($filter_report == 'manufacturers' or $filter_report == 'categories') ? 'style="color:#999; display: inline-block;cursor:default;"' : 'style="display: inline-block;color:#000; cursor:auto;"' ?>><?php echo $entry_option_grouping; ?></label>
          <select <?php echo ($filter_report == 'manufacturers' or $filter_report == 'categories') ? 'disabled="disabled"' : '' ?> name="filter_ogrouping" style="background-color:#E7EFEF; border-width:thin; border-color:#333;"> 
          <?php if ($filter_report == 'products') { ?> 
            <?php if ($filter_ogrouping && $filter_ogrouping == '1') { ?>
            <option value="1" selected="selected"><?php echo $text_yes; ?></option>
            <?php } else { ?>
            <option value="1"><?php echo $text_yes; ?></option>
            <?php } ?>
            <?php if (!$filter_ogrouping) { ?>
            <option value="0" selected="selected"><?php echo $text_no; ?></option>
            <?php } else { ?>
            <option value="0"><?php echo $text_no; ?></option>
            <?php } ?>
          <?php } elseif ($filter_report != 'products') { ?> 
          <option value="1">----</option>
          <?php } ?>
          </select>&nbsp;&nbsp; 
		  <?php echo $entry_group; ?>
          <select name="filter_group" id="filter_group" style="background-color:#E7EFEF; border-width:thin; border-color:#333;"> 
              <?php foreach ($groups as $groups) { ?>
              <?php if ($groups['value'] == $filter_group) { ?>
              <option value="<?php echo $groups['value']; ?>" selected="selected"><?php echo $groups['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $groups['value']; ?>"><?php echo $groups['text']; ?></option>
              <?php } ?>
              <?php } ?>
          </select>&nbsp;&nbsp;
          <?php echo $entry_sort_by; ?>
		  <select name="filter_sort" style="width: 100px;background-color:#E7EFEF; border-width:thin; border-color:#333;"> 
            <?php if ($filter_sort == 'date') { ?>
            <option value="date" selected="selected"><?php echo $column_date; ?></option>
            <?php } else { ?>
            <option value="date"><?php echo $column_date; ?></option>
            <?php } ?>                                   
            <?php if ($filter_report == 'products' && $filter_sort == 'sku') { ?>
            <option value="sku" selected="selected"><?php echo $column_sku; ?></option>         
            <?php } else { ?>
            <option value="sku" <?php echo ($filter_report == 'manufacturers' or $filter_report == 'categories') ? 'disabled="disabled" style="color:#999"' : '' ?>><?php echo $column_sku; ?></option>
            <?php } ?>
            <?php if ($filter_report == 'products' && $filter_sort == 'name') { ?>
            <option value="name" selected="selected"><?php echo $column_name; ?></option>            
            <?php } else { ?>
            <option value="name" <?php echo ($filter_report == 'manufacturers' or $filter_report == 'categories') ? 'disabled="disabled" style="color:#999"' : '' ?>><?php echo $column_name; ?></option>
            <?php } ?>
            <?php if ($filter_report == 'products' && $filter_sort == 'model') { ?>
            <option value="model" selected="selected"><?php echo $column_model; ?></option>            
            <?php } else { ?>
            <option value="model" <?php echo ($filter_report == 'manufacturers' or $filter_report == 'categories') ? 'disabled="disabled" style="color:#999"' : '' ?>><?php echo $column_model; ?></option>
            <?php } ?>                                
            <?php if ($filter_report == 'products' && $filter_sort == 'category' or $filter_report == 'categories' && $filter_sort == 'category') { ?>
            <option value="category" selected="selected"><?php echo $column_category; ?></option>            
            <?php } else { ?>
            <option value="category" <?php echo ($filter_report == 'manufacturers') ? 'disabled="disabled" style="color:#999"' : '' ?>><?php echo $column_category; ?></option>
            <?php } ?>                       
            <?php if ($filter_report == 'products' && $filter_sort == 'manufacturer' or $filter_report == 'manufacturers' && $filter_sort == 'manufacturer') { ?>
            <option value="manufacturer" selected="selected"><?php echo $column_manufacturer; ?></option>           
            <?php } else { ?>
            <option value="manufacturer" <?php echo ($filter_report == 'categories') ? 'disabled="disabled" style="color:#999"' : '' ?>><?php echo $column_manufacturer; ?></option>
            <?php } ?>                     
            <?php if ($filter_report == 'products' && $filter_sort == 'status') { ?>
            <option value="status" selected="selected"><?php echo $column_status; ?></option>           
            <?php } else { ?>
            <option value="status" <?php echo ($filter_report == 'manufacturers' or $filter_report == 'categories') ? 'disabled="disabled" style="color:#999"' : '' ?>><?php echo $column_status; ?></option>
            <?php } ?>           
            <?php if (!$filter_sort or $filter_sort == 'quantity' or ($filter_report == 'manufacturers' && $filter_sort == 'category') or ($filter_report == 'categories' && $filter_sort == 'manufacturer')) { ?>
            <option value="quantity" selected="selected"><?php echo $column_sold_quantity; ?></option>
            <?php } else { ?>
            <option value="quantity"><?php echo $column_sold_quantity; ?></option>
            <?php } ?>            
            <?php if ($filter_sort == 'total') { ?>
            <option value="total" selected="selected"><?php echo $column_total; ?></option>
            <?php } else { ?>
            <option value="total"><?php echo $column_total; ?></option>
            <?php } ?>
            <?php if ($filter_sort == 'tax') { ?>
            <option value="tax" selected="selected"><?php echo $column_tax; ?></option>
            <?php } else { ?>
            <option value="tax"><?php echo $column_tax; ?></option>
            <?php } ?>            
            <?php if ($filter_sort == 'prod_costs') { ?>
            <option value="prod_costs" selected="selected"><?php echo $column_prod_costs; ?></option>
            <?php } else { ?>
            <option value="prod_costs"><?php echo $column_prod_costs; ?></option>
            <?php } ?>            
            <?php if ($filter_sort == 'prod_profit') { ?>
            <option value="prod_profit" selected="selected"><?php echo $column_prod_profit; ?></option>
            <?php } else { ?>
            <option value="prod_profit"><?php echo $column_prod_profit; ?></option>
            <?php } ?>
          </select>&nbsp;&nbsp; 
          <?php echo $entry_show_details; ?>
		  <select name="filter_details" style="background-color:#E7EFEF; border-width:thin; border-color:#333;">                           
            <?php if (!$filter_details or $filter_details == '0' or ($filter_report == 'products' && $filter_details == '3') or ($filter_report == 'manufacturers' && filter_details == '1' or filter_details == '2') or ($filter_report == 'categories' && filter_details == '1' or filter_details == '2')) { ?>
            <option value="0" selected="selected"><?php echo $text_no_details; ?></option>
            <?php } else { ?>
            <option value="0"><?php echo $text_no_details; ?></option>
            <?php } ?>
            <?php if ($filter_report == 'products' && $filter_details == '1') { ?>
            <option value="1" selected="selected"><?php echo $text_order_list; ?></option>
            <?php } else { ?>
            <option value="1" <?php echo ($filter_report == 'manufacturers' or $filter_report == 'categories') ? 'disabled="disabled" style="color:#999"' : '' ?>><?php echo $text_order_list; ?></option>
            <?php } ?>
            <?php if ($filter_report == 'manufacturers' && $filter_details == '3' or $filter_report == 'categories' && $filter_details == '3') { ?>
            <option value="3" selected="selected"><?php echo $text_product_list; ?></option>
            <?php } else { ?>
            <option value="3" <?php echo ($filter_report == 'products') ? 'disabled="disabled" style="color:#999"' : '' ?>><?php echo $text_product_list; ?></option>
            <?php } ?>            
            <?php if ($filter_report == 'products' && $filter_details == '2') { ?>
            <option value="2" selected="selected"><?php echo $text_customer_list; ?></option>
            <?php } else { ?>
            <option value="2" <?php echo ($filter_report == 'manufacturers' or $filter_report == 'categories') ? 'disabled="disabled" style="color:#999"' : '' ?>><?php echo $text_customer_list; ?></option>
            <?php } ?>                        
          </select>&nbsp;&nbsp; 
          <?php echo $entry_limit; ?>
		  <select name="filter_limit" style="background-color:#E7EFEF; border-width:thin; border-color:#333;"> 
            <?php if ($filter_limit == '10') { ?>
            <option value="10" selected="selected">10</option>
            <?php } else { ?>
            <option value="10">10</option>
            <?php } ?>                                
            <?php if (!$filter_limit or $filter_limit == '25') { ?>
            <option value="25" selected="selected">25</option>
            <?php } else { ?>
            <option value="25">25</option>
            <?php } ?>
            <?php if ($filter_limit == '50') { ?>
            <option value="50" selected="selected">50</option>
            <?php } else { ?>
            <option value="50">50</option>
            <?php } ?>
            <?php if ($filter_limit == '100') { ?>
            <option value="100" selected="selected">100</option>
            <?php } else { ?>
            <option value="100">100</option>
            <?php } ?>                        
          </select>&nbsp; <a id="button" onclick="$('#report').submit();" class="cbutton" style="background:#069;"><span><?php echo $button_filter; ?></span></a>&nbsp;<?php if ($products) { ?><?php if (($filter_range != 'all_time' && ($filter_group == 'year' or $filter_group == 'quarter' or $filter_group == 'month')) or ($filter_range == 'all_time' && $filter_group == 'year')) { ?><a id="show_tab_chart" class="cbutton" style="background:#930;"><span><?php echo $button_chart; ?></span></a><?php } ?><?php } ?>&nbsp;<a id="show_tab_export" class="cbutton" style="background:#699;"><span><?php echo $button_export; ?></span></a>&nbsp;<a href="#dialog" name="modal" class="cbutton" style="background:#666;"><span><?php echo $button_settings; ?></span></a></div>
    </div>
    <div class="content_report">
<script type="text/javascript"><!--
$(document).ready(function() {
var prev = {start: 0, stop: 0},
    cont = $('#pagination_content .element');
	
$(".pagination_report").paging(cont.length, {
	format: '[< ncnnn! >]',
	perpage: '<?php echo $filter_limit; ?>',	
	lapping: 0,
	page: null, // we await hashchange() event
			onSelect: function() {

				var data = this.slice;

				cont.slice(prev[0], prev[1]).css('display', 'none');
				cont.slice(data[0], data[1]).fadeIn(0);

				prev = data;

				return true; // locate!
			},
			onFormat: function (type) {

				switch (type) {

					case 'block':

						if (!this.active)
							return '<span class="disabled">' + this.value + '</span>';
						else if (this.value != this.page)
							return '<em><a href="index.php?route=report/adv_product_profit&token=<?php echo $token; ?>#' + this.value + '">' + this.value + '</a></em>';
						return '<span class="current">' + this.value + '</span>';

					case 'next':

						if (this.active) {
							return '<a href="index.php?route=report/adv_product_profit&token=<?php echo $token; ?>#' + this.value + '" class="next">Next &gt;</a>';
						}
						return '';						

					case 'prev':

						if (this.active) {
							return '<a href="index.php?route=report/adv_product_profit&token=<?php echo $token; ?>#' + this.value + '" class="prev">&lt; Previous</a>';
						}	
						return '';						

					case 'first':

						if (this.active) {
							return '<?php echo $text_pagin_page; ?> ' + this.page + ' <?php echo $text_pagin_of; ?> ' + this.pages + '&nbsp;&nbsp;<a href="index.php?route=report/adv_product_profit&token=<?php echo $token; ?>#' + this.value + '" class="first">|&lt;</a>';
						}	
						return '<?php echo $text_pagin_page; ?> ' + this.page + ' <?php echo $text_pagin_of; ?> ' + this.pages + '&nbsp;&nbsp';
							
					case 'last':

						if (this.active) {
							return '<a href="index.php?route=report/adv_product_profit&token=<?php echo $token; ?>#' + this.value + '" class="prev">&gt;|</a>&nbsp;&nbsp;(' + cont.length + ' <?php echo $text_pagin_results; ?>)';
						}
						return '&nbsp;&nbsp;(' + cont.length + ' <?php echo $text_pagin_results; ?>)';					

				}
				return ''; // return nothing for missing branches
			}
});
});		
//--></script>     
<script type="text/javascript"><!--
function getStorage(key_prefix) {
    // this function will return us an object with a "set" and "get" method
    if (window.localStorage) {
        // use localStorage:
        return {
            set: function(id, data) {
                localStorage.setItem(key_prefix+id, data);
            },
            get: function(id) {
                return localStorage.getItem(key_prefix+id);
            }
        };
    }
}

$(document).ready(function() {
    // a key must is used for the cookie/storage
    var storedData = getStorage('com_mysite_checkboxes_'); 
    
    $('div.check input:checkbox').bind('change',function(){
        $('#'+this.id+'_filter').toggle($(this).is(':checked'));
        $('#'+this.id+'_title').toggle($(this).is(':checked'));
			<?php if ($products) {
					foreach ($products as $key => $product) {
						echo "$('#'+this.id+'_" . $product['order_product_id'] . "_title').toggle($(this).is(':checked')); ";
						echo "$('#'+this.id+'_" . $product['order_product_id'] . "').toggle($(this).is(':checked')); ";						
					}			
			} 
			;?>		
        $('#'+this.id+'_total').toggle($(this).is(':checked'));			
        // save the data on change
        storedData.set(this.id, $(this).is(':checked')?'checked':'not');
    }).each(function() {
        // on load, set the value to what we read from storage:
        var val = storedData.get(this.id);
        if (val == 'checked') $(this).attr('checked', 'checked');
        if (val == 'not') $(this).removeAttr('checked');
        if (val) $(this).trigger('change');
    });
});
//--></script>
<script type="text/javascript">
$(document).ready(function() {
	//select all the a tag with name equal to modal
	$('a[name=modal]').click(function(e) {
		//Cancel the link behavior
		e.preventDefault();
		
		//Get the A tag
		var id = $(this).attr('href');
	
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeTo("fast",0.15);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
              
		//Set the popup window to center
		$(id).css('top',  winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);
	
		//transition effect
		$(id).fadeIn(500); 
	});
	
	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		
		$('#mask').hide();
		$('.window').hide();
	});		
	
	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});			
	
	$(window).resize(function () {
 		var box = $('#boxes .window');
 
        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
      
        //Set height and width to mask to fill up the whole screen
        $('#mask').css({'width':maskWidth,'height':maskHeight});
               
        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        //Set the popup window to center
        box.css('top',  winH/2 - box.height()/2);
        box.css('left', winW/2 - box.width()/2);
	});	
});
</script>
<div id="boxes">
<div id="dialog" class="window">
<div align="right"><a href="#" class="close" style="text-decoration:none;">[Close]</a></div>
    <div class="check">  
	<div style="float:left; padding-right:10px; padding-top:5px;">     
      &nbsp;<b><?php echo $text_filtering_options; ?></b><br />        
      <table cellspacing="0" cellpadding="3" style="background:#E7EFEF; border: 1px solid #DDDDDD; padding-right:10px;">
        <tr>
          <td>          
			<input id="ppp1" checked="checked" type="checkbox"><label for="ppp1"><?php echo substr($entry_status,0,-1); ?></label><br />
			<input id="ppp2" checked="checked" type="checkbox"><label for="ppp2"><?php echo substr($entry_store,0,-1); ?></label><br />
			<input id="ppp3" checked="checked" type="checkbox"><label for="ppp3"><?php echo substr($entry_currency,0,-1); ?></label><br />
			<input id="ppp4" checked="checked" type="checkbox"><label for="ppp4"><?php echo substr($entry_tax,0,-1); ?></label><br />
			<input id="ppp5" checked="checked" type="checkbox"><label for="ppp5"><?php echo substr($entry_customer_group,0,-1); ?></label><br />   
			<input id="ppp6" checked="checked" type="checkbox"><label for="ppp6"><?php echo substr($entry_company,0,-1); ?></label><br />
			<input id="ppp7" checked="checked" type="checkbox"><label for="ppp7"><?php echo substr($entry_customer,0,-1); ?></label><br />
			<input id="ppp8" checked="checked" type="checkbox"><label for="ppp8"><?php echo substr($entry_email,0,-1); ?></label><br />
			<input id="ppp9" checked="checked" type="checkbox"><label for="ppp9"><?php echo substr($entry_category,0,-1); ?></label><br />
            <input id="ppp10" checked="checked" type="checkbox"><label for="ppp10"><?php echo substr($entry_manufacturer,0,-1); ?></label><br />
            <input id="ppp11" checked="checked" type="checkbox"><label for="ppp11"><?php echo substr($entry_sku,0,-1); ?></label><br />
            <input id="ppp12" checked="checked" type="checkbox"><label for="ppp12"><?php echo substr($entry_product,0,-1); ?></label><br />
            <input id="ppp13" checked="checked" type="checkbox"><label for="ppp13"><?php echo substr($entry_model,0,-1); ?></label><br />
            <input id="ppp14" checked="checked" type="checkbox"><label for="ppp14"><?php echo substr($entry_option,0,-1); ?></label><br />
            <input id="ppp15" checked="checked" type="checkbox"><label for="ppp15"><?php echo substr($entry_attributes,0,-1); ?></label><br />
            <input id="ppp16" checked="checked" type="checkbox"><label for="ppp16"><?php echo substr($entry_location,0,-1); ?></label><br />
            <input id="ppp17" checked="checked" type="checkbox"><label for="ppp17"><?php echo substr($entry_affiliate,0,-1); ?></label><br />              
            <input id="ppp18" checked="checked" type="checkbox"><label for="ppp18"><?php echo substr($entry_prod_status,0,-1); ?></label>                                     	
          </td>                                                                                                                        
        </tr>
      </table>  
    </div>
	<div style="float:left; padding-right:10px; padding-top:5px;">          
      &nbsp;<b><?php echo $text_mv_columns; ?></b><br />
      <table cellspacing="0" cellpadding="3" style="background:#E5E5E5; border: 1px solid #DDDDDD; padding-right:10px;">
        <tr>
          <td>            
			<input id="ppp20" checked="checked" type="checkbox"><label for="ppp20"><?php echo $column_image; ?></label><br />
			<input id="ppp21" checked="checked" type="checkbox"><label for="ppp21"><?php echo $column_sku; ?></label><br />
			<input id="ppp22" checked="checked" type="checkbox"><label for="ppp22"><?php echo $column_name; ?></label><br />
			<input id="ppp23" checked="checked" type="checkbox"><label for="ppp23"><?php echo $column_model; ?></label><br />
			<input id="ppp24" checked="checked" type="checkbox"><label for="ppp24"><?php echo $column_category; ?></label><br />
			<input id="ppp25" checked="checked" type="checkbox"><label for="ppp25"><?php echo $column_manufacturer; ?></label><br />
			<input id="ppp26" checked="checked" type="checkbox"><label for="ppp26"><?php echo $column_status; ?></label><br />
			<input id="ppp27" checked="checked" type="checkbox"><label for="ppp27"><?php echo $column_sold_quantity; ?></label><br />
            <input id="ppp28" checked="checked" type="checkbox"><label for="ppp28"><?php echo $column_sold_percent; ?></label><br />
            <input id="ppp29" checked="checked" type="checkbox"><label for="ppp29"><?php echo $column_total; ?></label><br />
            <input id="ppp30" checked="checked" type="checkbox"><label for="ppp30"><?php echo $column_tax; ?></label><br />
            <input id="ppp31" checked="checked" type="checkbox"><label for="ppp31"><?php echo $column_prod_costs; ?></label><br />
            <input id="ppp32" checked="checked" type="checkbox"><label for="ppp32"><?php echo $column_prod_profit; ?></label><br />
            <input id="ppp33" checked="checked" type="checkbox"><label for="ppp33"><?php echo $column_profit_margin; ?></label> 
          </td>                                                                                                                        
        </tr>
      </table>
    </div>
	<div style="float:left; padding-right:10px; padding-top:5px;">        
      &nbsp;<b><?php echo $text_ol_columns; ?></b><br />
      <table cellspacing="0" cellpadding="3" style="background:#F0F0F0; border: 1px solid #DDDDDD; padding-right:10px;">
        <tr>
          <td>           
			<input id="ppp40" checked="checked" type="checkbox"><label for="ppp40"><?php echo $column_order_prod_order_id; ?></label><br />
			<input id="ppp41" checked="checked" type="checkbox"><label for="ppp41"><?php echo $column_order_prod_date_added; ?></label><br />
			<input id="ppp42" checked="checked" type="checkbox"><label for="ppp42"><?php echo $column_order_prod_inv_no; ?></label><br />
			<input id="ppp43" checked="checked" type="checkbox"><label for="ppp43"><?php echo $column_order_prod_customer; ?></label><br />
			<input id="ppp44" checked="checked" type="checkbox"><label for="ppp44"><?php echo $column_order_prod_email; ?></label><br />
			<input id="ppp45" checked="checked" type="checkbox"><label for="ppp45"><?php echo $column_order_prod_customer_group; ?></label><br />
            <input id="ppp46" checked="checked" type="checkbox"><label for="ppp46"><?php echo $column_order_prod_shipping_method; ?></label><br />
            <input id="ppp47" checked="checked" type="checkbox"><label for="ppp47"><?php echo $column_order_prod_payment_method; ?></label><br />
            <input id="ppp48" checked="checked" type="checkbox"><label for="ppp48"><?php echo $column_order_prod_status; ?></label><br />
            <input id="ppp49" checked="checked" type="checkbox"><label for="ppp49"><?php echo $column_order_prod_store; ?></label><br />
            <input id="ppp50" checked="checked" type="checkbox"><label for="ppp50"><?php echo $column_order_prod_currency; ?></label><br />
            <input id="ppp51" checked="checked" type="checkbox"><label for="ppp51"><?php echo $column_order_prod_price; ?></label><br />
            <input id="ppp52" checked="checked" type="checkbox"><label for="ppp52"><?php echo $column_order_prod_quantity; ?></label><br />
            <input id="ppp53" checked="checked" type="checkbox"><label for="ppp53"><?php echo $column_order_prod_total; ?></label><br />
            <input id="ppp54" checked="checked" type="checkbox"><label for="ppp54"><?php echo $column_order_prod_tax; ?></label><br />
            <input id="ppp55" checked="checked" type="checkbox"><label for="ppp55"><?php echo $column_order_prod_costs; ?></label><br />
            <input id="ppp56" checked="checked" type="checkbox"><label for="ppp56"><?php echo $column_order_prod_profit; ?></label><br />
            <input id="ppp57" checked="checked" type="checkbox"><label for="ppp57"><?php echo $column_profit_margin; ?></label>
          </td>                                                                                                                        
        </tr>
      </table>  
    </div>
	<div style="float:left; padding-right:10px; padding-top:5px;">         
      &nbsp;<b><?php echo $text_pl_columns; ?></b><br />
      <table cellspacing="0" cellpadding="3" style="background:#F0F0F0; border: 1px solid #DDDDDD; padding-right:10px;">
        <tr>
          <td>    
			<input id="ppp60" checked="checked" type="checkbox"><label for="ppp60"><?php echo $column_prod_order_id; ?></label><br />
			<input id="ppp61" checked="checked" type="checkbox"><label for="ppp61"><?php echo $column_prod_date_added; ?></label><br />
			<input id="ppp62" checked="checked" type="checkbox"><label for="ppp62"><?php echo $column_prod_inv_no; ?></label><br />
			<input id="ppp63" checked="checked" type="checkbox"><label for="ppp63"><?php echo $column_prod_id; ?></label><br />
			<input id="ppp64" checked="checked" type="checkbox"><label for="ppp64"><?php echo $column_prod_sku; ?></label><br />
			<input id="ppp65" checked="checked" type="checkbox"><label for="ppp65"><?php echo $column_prod_model; ?></label><br />
			<input id="ppp66" checked="checked" type="checkbox"><label for="ppp66"><?php echo $column_prod_name; ?></label><br />
			<input id="ppp67" checked="checked" type="checkbox"><label for="ppp67"><?php echo $column_prod_option; ?></label><br />
            <input id="ppp68" checked="checked" type="checkbox"><label for="ppp68"><?php echo $column_prod_manu; ?></label><br />
            <input id="ppp69" checked="checked" type="checkbox"><label for="ppp69"><?php echo $column_prod_currency; ?></label><br />
            <input id="ppp70" checked="checked" type="checkbox"><label for="ppp70"><?php echo $column_prod_price; ?></label><br />
            <input id="ppp71" checked="checked" type="checkbox"><label for="ppp71"><?php echo $column_prod_quantity; ?></label><br />
            <input id="ppp72" checked="checked" type="checkbox"><label for="ppp72"><?php echo $column_prod_total; ?></label><br />
            <input id="ppp73" checked="checked" type="checkbox"><label for="ppp73"><?php echo $column_prod_tax; ?></label><br />
            <input id="ppp74" checked="checked" type="checkbox"><label for="ppp74"><?php echo $column_prod_costs; ?></label><br />
            <input id="ppp75" checked="checked" type="checkbox"><label for="ppp75"><?php echo $column_prod_profit; ?></label><br />
            <input id="ppp76" checked="checked" type="checkbox"><label for="ppp76"><?php echo $column_profit_margin; ?></label>
          </td>                                                                                                                        
        </tr>
      </table> 
    </div>         
	<div style="float:left; padding-right:10px; padding-top:5px;">        
      &nbsp;<b><?php echo $text_cl_columns; ?></b><br />
      <table cellspacing="0" cellpadding="3" style="background:#F0F0F0; border: 1px solid #DDDDDD; padding-right:10px;">
        <tr>
          <td>          
			<input id="ppp80" checked="checked" type="checkbox"><label for="ppp80"><?php echo $column_customer_order_id; ?></label><br />
			<input id="ppp81" checked="checked" type="checkbox"><label for="ppp81"><?php echo $column_customer_date_added; ?></label><br />
			<input id="ppp82" checked="checked" type="checkbox"><label for="ppp82"><?php echo $column_customer_inv_no; ?></label><br />
			<input id="ppp83" checked="checked" type="checkbox"><label for="ppp83"><?php echo $column_customer_cust_id; ?></label><br />
			<input id="ppp84" checked="checked" type="checkbox"><label for="ppp84"><?php echo strip_tags($column_billing_name); ?></label><br />
			<input id="ppp85" checked="checked" type="checkbox"><label for="ppp85"><?php echo strip_tags($column_billing_company); ?></label><br />
			<input id="ppp86" checked="checked" type="checkbox"><label for="ppp86"><?php echo strip_tags($column_billing_address_1); ?></label><br />
			<input id="ppp87" checked="checked" type="checkbox"><label for="ppp87"><?php echo strip_tags($column_billing_address_2); ?></label><br />			
            <input id="ppp88" checked="checked" type="checkbox"><label for="ppp88"><?php echo strip_tags($column_billing_city); ?></label><br />
            <input id="ppp89" checked="checked" type="checkbox"><label for="ppp89"><?php echo strip_tags($column_billing_zone); ?></label><br />
            <input id="ppp90" checked="checked" type="checkbox"><label for="ppp90"><?php echo strip_tags($column_billing_postcode); ?></label><br />
            <input id="ppp91" checked="checked" type="checkbox"><label for="ppp91"><?php echo strip_tags($column_billing_country); ?></label><br />
            <input id="ppp92" checked="checked" type="checkbox"><label for="ppp92"><?php echo $column_customer_telephone; ?></label><br />
			<input id="ppp93" checked="checked" type="checkbox"><label for="ppp93"><?php echo strip_tags($column_shipping_name); ?></label><br />
			<input id="ppp94" checked="checked" type="checkbox"><label for="ppp94"><?php echo strip_tags($column_shipping_company); ?></label><br />
			<input id="ppp95" checked="checked" type="checkbox"><label for="ppp95"><?php echo strip_tags($column_shipping_address_1); ?></label><br />
			<input id="ppp96" checked="checked" type="checkbox"><label for="ppp96"><?php echo strip_tags($column_shipping_address_2); ?></label><br />
            <input id="ppp97" checked="checked" type="checkbox"><label for="ppp97"><?php echo strip_tags($column_shipping_city); ?></label><br />
            <input id="ppp98" checked="checked" type="checkbox"><label for="ppp98"><?php echo strip_tags($column_shipping_zone); ?></label><br />
            <input id="ppp99" checked="checked" type="checkbox"><label for="ppp99"><?php echo strip_tags($column_shipping_postcode); ?></label><br />
            <input id="ppp100" checked="checked" type="checkbox"><label for="ppp100"><?php echo strip_tags($column_shipping_country); ?></label>
          </td>                                                                                                                        
        </tr>
      </table>                
    </div>                    
    </div> 
</div>
<div id="mask"></div>
</div>      
    <div style="background: #E7EFEF; border: 1px solid #C6D7D7; margin-bottom: 15px;">
	<table width="100%" cellspacing="0" cellpadding="3">
	<tr>
	<td>
	 <table border="0" cellspacing="0" cellpadding="0">
  	 <tr>
      <td width="220" valign="top" nowrap="nowrap" style="background: #C6D7D7; border: 1px solid #CCCCCC; padding: 5px;">
      <table cellpadding="0" cellspacing="0" style="float:left;">
        <tr><td><?php echo $entry_date_start; ?><br />
          <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="12" style="margin-top: 4px;" />
          </td><td width="10"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>
      <table cellpadding="0" cellspacing="0" style="float:left;">
        <tr><td><?php echo $entry_date_end; ?><br />
          <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="12" style="margin-top: 4px;" />
          </td><td></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>
      <table cellpadding="0" cellspacing="0">
        <tr><td><?php echo $entry_range; ?><br />
            <select name="filter_range" style="width: 215px !important; margin-top: 4px;">
              <?php foreach ($ranges as $range) { ?>
              <?php if ($range['value'] == $filter_range) { ?>
              <option value="<?php echo $range['value']; ?>" title="<?php echo $range['text']; ?>" selected="selected"><?php echo $range['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $range['value']; ?>" title="<?php echo $range['text']; ?>"><?php echo $range['text']; ?></option>
              <?php } ?>
              <?php } ?>
            </select></td><td></td></tr>
        <tr><td>&nbsp;</td><td></td>
        </tr></table>    
      </td>
    <td valign="top" style="padding: 5px;">
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp1_filter">
        <tr><td><?php echo $entry_status; ?><br />
          <span <?php echo (!$filter_order_status_id) ? '' : 'class="vtip"' ?> title="<?php foreach ($order_statuses as $order_status) { ?><?php if (isset($filter_order_status_id[$order_status['order_status_id']])) { ?><?php echo $order_status['name']; ?><br /><?php } ?><?php } ?>">
          <select name="filter_order_status_id" id="filter_order_status_id" multiple="multiple" size="1">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if (isset($filter_order_status_id[$order_status['order_status_id']])) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table> 
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp2_filter">
        <tr><td><?php echo $entry_store; ?><br />
          <span <?php echo (!$filter_store_id) ? '' : 'class="vtip"' ?> title="<?php foreach ($stores as $store) { ?><?php if (isset($filter_store_id[$store['store_id']])) { ?><?php echo $store['store_name']; ?><br /><?php } ?><?php } ?>">
          <select name="filter_store_id" id="filter_store_id" multiple="multiple" size="1">
            <?php foreach ($stores as $store) { ?>
            <?php if (isset($filter_store_id[$store['store_id']])) { ?>            
            <option value="<?php echo $store['store_id']; ?>" selected="selected"><?php echo $store['store_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $store['store_id']; ?>"><?php echo $store['store_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>    
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp3_filter">
        <tr><td><?php echo $entry_currency; ?><br />
          <span <?php echo (!$filter_currency) ? '' : 'class="vtip"' ?> title="<?php foreach ($currencies as $currency) { ?><?php if (isset($filter_currency[$currency['currency_id']])) { ?><?php echo $currency['title']; ?> (<?php echo $currency['code']; ?>)<br /><?php } ?><?php } ?>">
          <select name="filter_currency" id="filter_currency" multiple="multiple" size="1">
            <?php foreach ($currencies as $currency) { ?>
            <?php if (isset($filter_currency[$currency['currency_id']])) { ?>
            <option value="<?php echo $currency['currency_id']; ?>" selected="selected"><?php echo $currency['title']; ?> (<?php echo $currency['code']; ?>)</option>
            <?php } else { ?>
            <option value="<?php echo $currency['currency_id']; ?>"><?php echo $currency['title']; ?> (<?php echo $currency['code']; ?>)</option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>          
          </tr></table>
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp4_filter">
        <tr><td><?php echo $entry_tax; ?><br />
          <span <?php echo (!$filter_taxes) ? '' : 'class="vtip"' ?> title="<?php foreach ($taxes as $tax) { ?><?php if (isset($filter_taxes[$tax['tax']])) { ?><?php echo $tax['tax_title']; ?><br /><?php } ?><?php } ?>">
		  <select name="filter_taxes" id="filter_taxes" multiple="multiple" size="1">
            <?php foreach ($taxes as $tax) { ?>
            <?php if (isset($filter_taxes[$tax['tax']])) { ?>              
            <option value="<?php echo $tax['tax']; ?>" selected="selected"><?php echo $tax['tax_title']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $tax['tax']; ?>"><?php echo $tax['tax_title']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table> 
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp5_filter">
        <tr><td><?php echo $entry_customer_group; ?><br />
          <span <?php echo (!$filter_customer_group_id) ? '' : 'class="vtip"' ?> title="<?php foreach ($customer_groups as $customer_group) { ?><?php if (isset($filter_customer_group_id[$customer_group['customer_group_id']])) { ?><?php echo $customer_group['name']; ?><br /><?php } ?><?php } ?>">
          <select name="filter_customer_group_id" id="filter_customer_group_id" multiple="multiple" size="1">
            <?php foreach ($customer_groups as $customer_group) { ?>
            <?php if (isset($filter_customer_group_id[$customer_group['customer_group_id']])) { ?>              
            <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>                   
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp6_filter">
        <tr><td> <?php echo $entry_company; ?><br />
        <input type="text" name="filter_company" value="<?php echo $filter_company; ?>" size="18" style="margin-top:4px; height:16px; border:solid 1px #BBB; color:#003A88;" onclick="this.value = '';">
		</td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>                   
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp7_filter">
        <tr><td> <?php echo $entry_customer; ?><br />
        <input type="text" name="filter_customer_id" value="<?php echo $filter_customer_id; ?>" size="18" style="margin-top:4px; height:16px; border:solid 1px #BBB; color:#003A88;" onclick="this.value = '';">
        </td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table> 
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp8_filter">
        <tr><td> <?php echo $entry_email; ?><br />
        <input type="text" name="filter_email" value="<?php echo $filter_email; ?>" size="18" style="margin-top:4px; height:16px; border:solid 1px #BBB; color:#003A88;" onclick="this.value = '';">
        </td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp9_filter">
        <tr><td><?php echo $entry_category; ?><br />
          <span <?php echo (!$filter_category) ? '' : 'class="vtip"' ?> title="<?php foreach ($categories as $category) { ?><?php if (isset($filter_category[$category['category_id']])) { ?><?php echo $category['name']; ?><br /><?php } ?><?php } ?>">
          <select name="filter_category" id="filter_category" multiple="multiple" size="1">
            <?php foreach ($categories as $category) { ?>
            <?php if (isset($filter_category[$category['category_id']])) { ?>               
            <option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option> 
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>                               
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp10_filter">
        <tr><td><?php echo $entry_manufacturer; ?><br />
          <span <?php echo (!$filter_manufacturer) ? '' : 'class="vtip"' ?> title="<?php foreach ($manufacturers as $manufacturer) { ?><?php if (isset($filter_manufacturer[$manufacturer['manufacturer_id']])) { ?> <?php echo $manufacturer['name']; ?><br /><?php } ?><?php } ?>">
          <select name="filter_manufacturer" id="filter_manufacturer" multiple="multiple" size="1">
            <?php foreach ($manufacturers as $manufacturer) { ?>
            <?php if (isset($filter_manufacturer[$manufacturer['manufacturer_id']])) { ?>               
            <option value="<?php echo $manufacturer['manufacturer_id']; ?>" selected="selected"><?php echo $manufacturer['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></option> 
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>  
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp11_filter">
        <tr><td> <?php echo $entry_sku; ?><br />
        <input type="text" name="filter_sku" value="<?php echo $filter_sku; ?>" size="18" style="margin-top:4px; height:16px; border:solid 1px #BBB; color:#003A88;" onclick="this.value = '';">
        </td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table> 
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp12_filter">
        <tr><td> <?php echo $entry_product; ?><br />
        <input type="text" name="filter_product_id" value="<?php echo $filter_product_id; ?>" size="30" style="margin-top:4px; height:16px; border:solid 1px #BBB; color:#003A88;" onclick="this.value = '';">
        </td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table> 
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp13_filter">
        <tr><td> <?php echo $entry_model; ?><br />
        <input type="text" name="filter_model" value="<?php echo $filter_model; ?>" size="18" style="margin-top:4px; height:16px; border:solid 1px #BBB; color:#003A88;" onclick="this.value = '';">
        </td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>                     
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp14_filter">
        <tr><td><label <?php echo ($filter_ogrouping or $filter_report != 'products') ? 'style="color:#000;display: inline-block; cursor:auto;"' : 'style="color:#999;display: inline-block; cursor:default;"' ?>><?php echo $entry_option; ?></label><br />
          <span <?php echo (($filter_option && $filter_ogrouping && $filter_report == 'products') or ($filter_option && $filter_report != 'products')) ? 'class="vtip"' : '' ?> title="<?php foreach ($product_options as $product_option) { ?><?php if ((isset($filter_option[$product_option['options']]) && $filter_ogrouping && $filter_report == 'products') or (isset($filter_option[$product_option['options']]) && $filter_report != 'products')) { ?><?php echo $product_option['option_name']; ?>: <?php echo $product_option['option_value']; ?><br /><?php } ?><?php } ?>">        
          <select name="filter_option" id="filter_option" multiple="multiple" size="1">
          <?php if ($filter_ogrouping && $filter_report == 'products') { ?>          
            <?php foreach ($product_options as $product_option) { ?>
            <?php if (isset($filter_option[$product_option['options']]) && $filter_ogrouping && $filter_report == 'products') { ?>              
            <option value="<?php echo $product_option['options']; ?>" selected="selected"><?php echo $product_option['option_name']; ?>: <?php echo $product_option['option_value']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $product_option['options']; ?>"><?php echo $product_option['option_name']; ?>: <?php echo $product_option['option_value']; ?></option>
            <?php } ?>
            <?php } ?>
          <?php } elseif ($filter_report != 'products') { ?>
            <?php foreach ($product_options as $product_option) { ?>
            <?php if (isset($filter_option[$product_option['options']]) && $filter_report != 'products') { ?>              
            <option value="<?php echo $product_option['options']; ?>" selected="selected"><?php echo $product_option['option_name']; ?>: <?php echo $product_option['option_value']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $product_option['options']; ?>"><?php echo $product_option['option_name']; ?>: <?php echo $product_option['option_value']; ?></option>
            <?php } ?>
            <?php } ?>            
          <?php } ?>  
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp15_filter">
        <tr><td><?php echo $entry_attributes; ?><br />
          <span <?php echo (!$filter_attribute) ? '' : 'class="vtip"' ?> title="<?php foreach ($attributes as $attribute) { ?><?php if (isset($filter_attribute[$attribute['attribute_title']])) { ?><?php echo $attribute['attribute_name']; ?><br /><?php } ?><?php } ?>">
		  <select name="filter_attribute" id="filter_attribute" multiple="multiple" size="1">
            <?php foreach ($attributes as $attribute) { ?>
            <?php if (isset($filter_attribute[$attribute['attribute_title']])) { ?>              
            <option value="<?php echo $attribute['attribute_title']; ?>" selected="selected"><?php echo $attribute['attribute_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $attribute['attribute_title']; ?>"><?php echo $attribute['attribute_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>           
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp16_filter">
        <tr><td><?php echo $entry_location; ?><br />
          <span <?php echo (!$filter_location) ? '' : 'class="vtip"' ?> title="<?php foreach ($locations as $location) { ?><?php if (isset($filter_location[$location['location_title']])) { ?><?php echo $location['location_name']; ?><br /><?php } ?><?php } ?>">
		  <select name="filter_location" id="filter_location" multiple="multiple" size="1">
            <?php foreach ($locations as $location) { ?>
            <?php if (isset($filter_location[$location['location_title']])) { ?>              
            <option value="<?php echo $location['location_title']; ?>" selected="selected"><?php echo $location['location_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $location['location_title']; ?>"><?php echo $location['location_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>   
	  <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp17_filter">
        <tr><td><?php echo $entry_affiliate; ?><br />
          <span <?php echo (!$filter_affiliate) ? '' : 'class="vtip"' ?> title="<?php foreach ($affiliates as $affiliate) { ?><?php if (isset($filter_affiliate[$affiliate['affiliate_id']])) { ?><?php echo $affiliate['affiliate_name']; ?>: <?php echo $affiliate['option_value']; ?><br /><?php } ?><?php } ?>">        
          <select name="filter_affiliate" id="filter_affiliate" multiple="multiple" size="1">
            <?php foreach ($affiliates as $affiliate) { ?>
            <?php if (isset($filter_affiliate[$affiliate['affiliate_id']])) { ?>              
            <option value="<?php echo $affiliate['affiliate_id']; ?>" selected="selected"><?php echo $affiliate['affiliate_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $affiliate['affiliate_id']; ?>"><?php echo $affiliate['affiliate_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>                      
      <table cellpadding="0" cellspacing="0" style="float:left;" id="ppp18_filter">
        <tr><td><?php echo $entry_prod_status; ?><br />
          <span <?php echo (!$filter_status) ? '' : 'class="vtip"' ?> title="<?php foreach ($statuses as $status) { ?><?php if (isset($filter_status[$status['status']]) && $status['status'] == 1) { ?><?php echo $text_enabled; ?><br /><?php } ?><?php if (isset($filter_status[$status['status']]) && $status['status'] == 0) { ?><?php echo $text_disabled; ?><br /><?php } ?><?php } ?>">         
          <select name="filter_status" id="filter_status" multiple="multiple" size="1">
            <?php foreach ($statuses as $status) { ?>
            <?php if (isset($filter_status[$status['status']]) && $status['status'] == 1) { ?>
            <option value="<?php echo $status['status']; ?>" selected="selected"><?php echo $text_enabled; ?></option>
            <?php } elseif (!isset($filter_status[$status['status']]) && $status['status'] == 1) { ?>
            <option value="<?php echo $status['status']; ?>"><?php echo $text_enabled; ?></option>
            <?php } ?>
            <?php if (isset($filter_status[$status['status']]) && $status['status'] == 0) { ?>
            <option value="<?php echo $status['status']; ?>" selected="selected"><?php echo $text_disabled; ?></option>
            <?php } elseif (!isset($filter_status[$status['status']]) && $status['status'] == 0) { ?>
            <option value="<?php echo $status['status']; ?>"><?php echo $text_disabled; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>                               
	   </td>
	  </tr>
	 </table>
	</td>
	</tr>
	</table>      
    </div>
<script type="text/javascript">$(function(){ 
$('#show_tab_export').click(function() {
		$('#tab_export').slideToggle('fast');
	});
});
</script>    
  <div id="tab_export" style="background:#E7EFEF; border:1px solid #C6D7D7; padding:3px; margin-bottom:15px; display:none">
      <table width="100%" cellspacing="0" cellpadding="3">
        <tr>
          <td width="3%">&nbsp;</td>
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'manufacturers' or $filter_report == 'categories') { ?>
          <span class="noexport_item"><img src="view/image/XLS.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/HTML.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/PDF.png" width="48" height="48" border="0" /></span>
          <?php } else { ?>          
          <span id="export_xls_prod" class="export_item"><img src="view/image/XLS.png" width="48" height="48" border="0" title="XLS" /></span><span id="export_html_prod" class="export_item"><img src="view/image/HTML.png" width="48" height="48" border="0" title="HTML" /></span><span id="export_pdf_prod" class="export_item"><img src="view/image/PDF.png" width="48" height="48" border="0" title="PDF" /></span>
          <?php } ?></td>   
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'manufacturers' or $filter_report == 'categories') { ?>
          <span class="noexport_item"><img src="view/image/XLS.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/HTML.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/PDF.png" width="48" height="48" border="0" /></span>
          <?php } else { ?>            
          <span id="export_xls_prod_order_list" class="export_item"><img src="view/image/XLS.png" width="48" height="48" border="0" title="XLS" /></span><span id="export_html_prod_order_list" class="export_item"><img src="view/image/HTML.png" width="48" height="48" border="0" title="HTML" /></span><span id="export_pdf_prod_order_list" class="export_item"><img src="view/image/PDF.png" width="48" height="48" border="0" title="PDF" /></span>
          <?php } ?></td>   
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'manufacturers' or $filter_report == 'categories') { ?>
          <span class="noexport_item"><img src="view/image/XLS.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/HTML.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/PDF.png" width="48" height="48" border="0" /></span>
          <?php } else { ?>            
          <span id="export_xls_prod_customer_list" class="export_item"><img src="view/image/XLS.png" width="48" height="48" border="0" title="XLS" /></span><span id="export_html_prod_customer_list" class="export_item"><img src="view/image/HTML.png" width="48" height="48" border="0" title="HTML" /></span><span id="export_pdf_prod_customer_list" class="export_item"><img src="view/image/PDF.png" width="48" height="48" border="0" title="PDF" /></span>
          <?php } ?></td>   
          <td width="5%">&nbsp;</td>
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'products' or $filter_report == 'categories') { ?>
          <span class="noexport_item"><img src="view/image/XLS.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/HTML.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/PDF.png" width="48" height="48" border="0" /></span>
          <?php } else { ?>
          <span id="export_xls_manu" class="export_item"><img src="view/image/XLS.png" width="48" height="48" border="0" title="XLS" /></span><span id="export_html_manu" class="export_item"><img src="view/image/HTML.png" width="48" height="48" border="0" title="HTML" /></span><span id="export_pdf_manu" class="export_item"><img src="view/image/PDF.png" width="48" height="48" border="0" title="PDF" /></span>
          <?php } ?></td>                    
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'products' or $filter_report == 'categories') { ?>
          <span class="noexport_item"><img src="view/image/XLS.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/HTML.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/PDF.png" width="48" height="48" border="0" /></span>
          <?php } else { ?>          
          <span id="export_xls_manu_product_list" class="export_item"><img src="view/image/XLS.png" width="48" height="48" border="0" title="XLS" /></span><span id="export_html_manu_product_list" class="export_item"><img src="view/image/HTML.png" width="48" height="48" border="0" title="HTML" /></span><span id="export_pdf_manu_product_list" class="export_item"><img src="view/image/PDF.png" width="48" height="48" border="0" title="PDF" /></span>
          <?php } ?></td> 
          <td width="5%">&nbsp;</td>
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'products' or $filter_report == 'manufacturers') { ?>
          <span class="noexport_item"><img src="view/image/XLS.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/HTML.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/PDF.png" width="48" height="48" border="0" /></span>
          <?php } else { ?>          
          <span id="export_xls_cat" class="export_item"><img src="view/image/XLS.png" width="48" height="48" border="0" title="XLS" /></span><span id="export_html_cat" class="export_item"><img src="view/image/HTML.png" width="48" height="48" border="0" title="HTML" /></span><span id="export_pdf_cat" class="export_item"><img src="view/image/PDF.png" width="48" height="48" border="0" title="PDF" /></span>
          <?php } ?></td>                  
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'products' or $filter_report == 'manufacturers') { ?>
          <span class="noexport_item"><img src="view/image/XLS.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/HTML.png" width="48" height="48" border="0" /></span><span class="noexport_item"><img src="view/image/PDF.png" width="48" height="48" border="0" /></span>
          <?php } else { ?>          
          <span id="export_xls_cat_product_list" class="export_item"><img src="view/image/XLS.png" width="48" height="48" border="0" title="XLS" /></span><span id="export_html_cat_product_list" class="export_item"><img src="view/image/HTML.png" width="48" height="48" border="0" title="HTML" /></span><span id="export_pdf_cat_product_list" class="export_item"><img src="view/image/PDF.png" width="48" height="48" border="0" title="PDF" /></span>
          <?php } ?></td> 
          <td width="3%">&nbsp;</td>
        </tr>
        <tr>
          <td width="3%">&nbsp;</td>
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'manufacturers' or $filter_report == 'categories') { ?>          
          <label style="color:#999; cursor:default;"><?php echo $text_export_prod_no_details; ?></label>
          <?php } else { ?>  
          <?php echo $text_export_prod_no_details; ?>          
          <?php } ?></td>          
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'manufacturers' or $filter_report == 'categories') { ?>          
          <label style="color:#999; cursor:default;"><?php echo $text_export_prod_order_list; ?></label>
          <?php } else { ?>  
          <?php echo $text_export_prod_order_list; ?>          
          <?php } ?></td>  
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'manufacturers' or $filter_report == 'categories') { ?>          
          <label style="color:#999; cursor:default;"><?php echo $text_export_prod_customer_list; ?></label>
          <?php } else { ?>  
          <?php echo $text_export_prod_customer_list; ?>          
          <?php } ?></td>  
          <td width="5%">&nbsp;</td>          
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'products' or $filter_report == 'categories') { ?>          
          <label style="color:#999; cursor:default;"><?php echo $text_export_manu_no_details; ?></label>
          <?php } else { ?>  
          <?php echo $text_export_manu_no_details; ?>          
          <?php } ?></td>    
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'products' or $filter_report == 'categories') { ?>          
          <label style="color:#999; cursor:default;"><?php echo $text_export_manu_product_list; ?></label>
          <?php } else { ?>            
          <?php echo $text_export_manu_product_list; ?>
          <?php } ?></td>  
          <td width="5%">&nbsp;</td>          
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'products' or $filter_report == 'manufacturers') { ?>          
          <label style="color:#999; cursor:default;"><?php echo $text_export_cat_no_details; ?></label>
          <?php } else { ?>           
          <?php echo $text_export_cat_no_details; ?>
          <?php } ?></td>  
          <td width="12%" align="center" nowrap="nowrap">
          <?php if ($filter_report == 'products' or $filter_report == 'manufacturers') { ?>          
          <label style="color:#999; cursor:default;"><?php echo $text_export_cat_product_list; ?></label>
          <?php } else { ?>           
          <?php echo $text_export_cat_product_list; ?>
          <?php } ?></td>                                
          <td width="3%">&nbsp;</td>                                                                                                                       
        </tr>        
      </table> 
      <input type="hidden" id="export" name="export" value="" />
    </div>   
<?php if ($products) { ?>
<?php if (($filter_range != 'all_time' && ($filter_group == 'year' or $filter_group == 'quarter' or $filter_group == 'month')) or ($filter_range == 'all_time' && $filter_group == 'year')) { ?>   
<script type="text/javascript">$(function(){ 
$('#show_tab_chart').click(function() {
		$('#tab_chart').slideToggle('slow');
	});
});
</script>  
    <div id="tab_chart" style="display:none">
      <table align="center" cellspacing="0" cellpadding="0">
        <tr>
          <td><div style="float:left;" id="chart1_div"></div><div style="float:left;" id="chart2_div"></div></td>
        </tr>              
      </table>
      <br />
    </div>
<?php } ?> 
<?php } ?>     
    <div id="pagination_content"> 
    <table class="list_main">
      <thead>
        <tr>
		  <?php if ($filter_group == 'year') { ?>           
          <td class="left" colspan="2" nowrap="nowrap"><?php echo $column_year; ?></td>
		  <?php } elseif ($filter_group == 'quarter') { ?> 
          <td class="left" nowrap="nowrap"><?php echo $column_year; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $column_quarter; ?></td>       
		  <?php } elseif ($filter_group == 'month') { ?> 
          <td class="left" nowrap="nowrap"><?php echo $column_year; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $column_month; ?></td> 
		  <?php } else { ?>    
          <td class="left" width="70" nowrap="nowrap"><?php echo $column_date_start; ?></td>
          <td class="left" width="70" nowrap="nowrap"><?php echo $column_date_end; ?></td>           
		  <?php } ?> 
		  <?php if ($filter_report == 'products') { ?>          
          <td id="ppp20_title" class="center" nowrap="nowrap"><?php echo $column_image; ?></td> 
          <td id="ppp21_title" class="left"><?php echo $column_sku; ?></td>           <!--  nowrap="nowrap" -->
          <td id="ppp22_title" class="left"><?php echo $column_name; ?></td>                    
          <td id="ppp23_title" class="left" nowrap="nowrap"><?php echo $column_model; ?></td>  
          <td id="ppp24_title" class="left" nowrap="nowrap"><?php echo $column_category; ?></td>            
          <td id="ppp25_title" class="left" nowrap="nowrap"><?php echo $column_manufacturer; ?></td>
          <td id="ppp26_title" class="left" nowrap="nowrap"><?php echo $column_status; ?></td> 
		  <?php } elseif ($filter_report == 'manufacturers') { ?>    
          <td id="ppp25_title" class="left" nowrap="nowrap"><?php echo $column_manufacturer; ?></td>
		  <?php } elseif ($filter_report == 'categories') { ?>
          <td id="ppp24_title" class="left" nowrap="nowrap"><?php echo $column_category; ?></td>  
		  <?php } ?>
          <td id="ppp27_title" class="right" nowrap="nowrap"><?php echo $column_sold_quantity; ?></td>  
          <td id="ppp28_title" class="right" nowrap="nowrap"><?php echo $column_sold_percent; ?></td>                
          <td id="ppp29_title" class="right" nowrap="nowrap"><?php echo $column_total; ?></td>
          <td id="ppp30_title" class="right" nowrap="nowrap"><?php echo $column_tax; ?></td>           
          <td id="ppp31_title" class="right"><?php echo $column_prod_costs; ?></td>        
          <td id="ppp32_title" class="right" nowrap="nowrap"><?php echo $column_prod_profit; ?></td>
          <td id="ppp33_title" class="right" nowrap="nowrap"><?php echo $column_profit_margin; ?></td>
          <?php if (($filter_report == 'products' && $filter_details == 1 OR $filter_details == 2) OR ($filter_report == 'manufacturers' && $filter_details == 3) OR ($filter_report == 'categories' && $filter_details == 3)) { ?><td class="right" nowrap="nowrap"><?php echo $column_action; ?></td><?php } ?>
        </tr>
      </thead>
        <?php if ($products) { ?>
        <?php foreach ($products as $product) { ?>
      <tbody class="element">        
        <tr>
		  <?php if ($filter_group == 'year') { ?>           
          <td class="left" colspan="2" nowrap="nowrap"><?php echo $product['year']; ?></td>
		  <?php } elseif ($filter_group == 'quarter') { ?> 
          <td class="left" nowrap="nowrap"><?php echo $product['year']; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $product['quarter']; ?></td>  
		  <?php } elseif ($filter_group == 'month') { ?> 
          <td class="left" nowrap="nowrap"><?php echo $product['year']; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $product['month']; ?></td>
		  <?php } else { ?>    
          <td class="left" nowrap="nowrap"><?php echo $product['date_start']; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $product['date_end']; ?></td>         
		  <?php } ?>
		  <?php if ($filter_report == 'products') { ?>           
          <td id="ppp20_<?php echo $product['order_product_id']; ?>" class="center"><img src="<?php echo $product['image']; ?>" style="padding: 1px; border: 1px solid #DDDDDD;" /></td> 
          <td id="ppp21_<?php echo $product['order_product_id']; ?>" class="left"><?php echo $product['sku']; ?></td>           <!--  nowrap="nowrap" -->
          <td id="ppp22_<?php echo $product['order_product_id']; ?>" class="left">
          <?php if ($product['status'] != NULL) { ?>
          <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
          <?php } else { ?>
          <?php echo $product['name']; ?>
          <?php } ?>
          <?php if ($filter_ogrouping) { ?>
          <?php if ($product['oovalue']) { ?>
          <table cellpadding="0" cellspacing="0" style="border:hidden;">
          <tr>
		  <td nowrap="nowrap"><span style="font-size:11px; border:hidden;"><?php echo $product['ooname']; ?>:</span></td>
          <td nowrap="nowrap"><span style="font-size:11px; border:hidden;"><?php echo $product['oovalue']; ?></span></td>
          </tr>
          </table>
          <?php } ?><?php } ?></td>
          <td id="ppp23_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['model']; ?></td>             
          <td id="ppp24_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php foreach ($categories as $category) { ?>
                <?php if (in_array($category['category_id'], $product['category'])) { ?>
                <?php echo $category['name'];?><br />
                <?php } ?> <?php } ?></td>          
          <td id="ppp25_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php foreach ($manufacturers as $manufacturer) { ?>
                <?php if (in_array($manufacturer['manufacturer_id'], $product['manufacturer'])) { ?>
                <?php echo $manufacturer['name'];?>
                <?php } ?> <?php } ?></td>
		  <td id="ppp26_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap">
          <?php if ($product['status'] == '1') { ?>
          <?php echo $text_enabled; ?>
          <?php } else if ($product['status'] == '0') { ?>
          <?php echo $text_disabled; ?>
          <?php } ?></td>
		  <?php } elseif ($filter_report == 'manufacturers') { ?>
          <td id="ppp25_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php foreach ($manufacturers as $manufacturer) { ?>
                <?php if (in_array($manufacturer['manufacturer_id'], $product['manufacturer'])) { ?>
                <?php echo $manufacturer['name'];?>
                <?php } ?> <?php } ?></td>
		  <?php } elseif ($filter_report == 'categories') { ?>
          <td id="ppp24_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php foreach ($categories as $category) { ?>
                <?php if (in_array($category['category_id'], $product['category'])) { ?>
                <?php echo $category['name'];?><br />
                <?php } ?> <?php } ?></td> 
		  <?php } ?>
          <td id="ppp27_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap"><?php echo $product['sold_quantity']; ?></td> 
          <td id="ppp28_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap"><?php echo $product['sold_percent']; ?></td>                   
          <td id="ppp29_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap" style="background-color:#DCFFB9;"><?php echo $product['total']; ?></td>
          <td id="ppp30_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap"><?php echo $product['tax']; ?></td>           
          <td id="ppp31_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap" style="background-color:#ffd7d7;"><?php echo $product['prod_costs']; ?></td>         
          <td id="ppp32_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap" style="background-color:#BCD5ED; font-weight:bold;"><?php echo $product['prod_profit']; ?></td> 
          <td id="ppp33_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap" style="background-color:#BCD5ED; font-weight:bold;"><?php echo $product['profit_margin_percent']; ?></td>
          <?php if (($filter_report == 'products' && $filter_details == 1 OR $filter_details == 2) OR ($filter_report == 'manufacturers' && $filter_details == 3) OR ($filter_report == 'categories' && $filter_details == 3)) { ?><td class="right" nowrap="nowrap">[ <a id="show_details_<?php echo $product['order_product_id']; ?>"><?php echo $text_detail; ?></a> ]</td><?php } ?>
        </tr>
<tr class="detail">
<td colspan="17" class="center">
<?php if ($filter_report == 'products' && $filter_details == 1) { ?>
<script type="text/javascript">$(function(){ 
$('#show_details_<?php echo $product['order_product_id']; ?>').click(function() {
		$('#tab_details_<?php echo $product['order_product_id']; ?>').slideToggle('slow');
	});
});
</script>
<div id="tab_details_<?php echo $product['order_product_id']; ?>" style="display:none">
    <table class="list_detail">
      <thead>
        <tr>
          <td id="ppp40_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_prod_order_id; ?></td>        
          <td id="ppp41_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_prod_date_added; ?></td>
          <td id="ppp42_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_prod_inv_no; ?></td>            
          <td id="ppp43_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_prod_customer; ?></td>
          <td id="ppp44_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_prod_email; ?></td>
          <td id="ppp45_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_prod_customer_group; ?></td>
          <td id="ppp46_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_prod_shipping_method; ?></td>
          <td id="ppp47_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_prod_payment_method; ?></td>          
          <td id="ppp48_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_prod_status; ?></td>
          <td id="ppp49_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_prod_store; ?></td>
          <td id="ppp50_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_prod_currency; ?></td>
          <td id="ppp51_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_prod_price; ?></td> 
          <td id="ppp52_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_prod_quantity; ?></td>                    
          <td id="ppp53_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_prod_total; ?></td>
          <td id="ppp54_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_prod_tax; ?></td> 
          <td id="ppp55_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_prod_costs; ?></td> 
          <td id="ppp56_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_prod_profit; ?></td>  
          <td id="ppp57_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_profit_margin; ?></td>                             
        </tr>
      </thead>
        <tr bgcolor="#FFFFFF">
          <td id="ppp40_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['order_prod_ord_id']; ?></td>        
          <td id="ppp41_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['order_prod_order_date']; ?></td>
          <td id="ppp42_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['order_prod_inv_no']; ?></td>
          <td id="ppp43_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['order_prod_name']; ?></td>
          <td id="ppp44_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['order_prod_email']; ?></td>
          <td id="ppp45_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['order_prod_group']; ?></td>
          <td id="ppp46_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['order_prod_shipping_method']; ?></td>
          <td id="ppp47_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['order_prod_payment_method']; ?></td>           
          <td id="ppp48_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['order_prod_status']; ?></td>
          <td id="ppp49_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['order_prod_store']; ?></td>           
          <td id="ppp50_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap"><?php echo $product['order_prod_currency']; ?></td>  
          <td id="ppp51_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap"><?php echo $product['order_prod_price']; ?></td> 
          <td id="ppp52_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap"><?php echo $product['order_prod_quantity']; ?></td>                            
          <td id="ppp53_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap" style="background-color:#DCFFB9;"><?php echo $product['order_prod_total']; ?></td>
          <td id="ppp54_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap"><?php echo $product['order_prod_tax']; ?></td>
          <td id="ppp55_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap" style="background-color:#ffd7d7;">-<?php echo $product['order_prod_costs']; ?></td>                    
          <td id="ppp56_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap" style="background-color:#BCD5ED; font-weight:bold;"><?php echo $product['order_prod_profit']; ?></td>
          <td id="ppp57_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap" style="background-color:#BCD5ED; font-weight:bold;"><?php echo $product['order_prod_profit_margin_percent']; ?>%</td>
         </tr>
    </table>
</div>
<?php } ?>
<?php if ($filter_report == 'manufacturers' && $filter_details == 3 or $filter_report == 'categories' && $filter_details == 3) { ?>
<script type="text/javascript">$(function(){ 
$('#show_details_<?php echo $product['order_product_id']; ?>').click(function() {
		$('#tab_details_<?php echo $product['order_product_id']; ?>').slideToggle('slow');
	});
});
</script>
<div id="tab_details_<?php echo $product['order_product_id']; ?>" style="display:none">
    <table class="list_detail">
      <thead>
        <tr>
          <td id="ppp60_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_order_id; ?></td>  
          <td id="ppp61_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_date_added; ?></td>
          <td id="ppp62_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_inv_no; ?></td> 
          <td id="ppp63_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_id; ?></td>                                          
          <td id="ppp64_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_sku; ?></td>          
          <td id="ppp65_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_name; ?></td> 
          <td id="ppp66_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_option; ?></td>           
          <td id="ppp67_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_model; ?></td>  
          <?php if ($filter_report == 'categories') { ?>          
          <td id="ppp68_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_manu; ?></td> 
          <?php } ?>
          <td id="ppp69_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_currency; ?></td>   
          <td id="ppp70_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_price; ?></td>                     
          <td id="ppp71_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_quantity; ?></td>                  
          <td id="ppp72_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_total; ?></td>   
          <td id="ppp73_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_tax; ?></td> 
          <td id="ppp74_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_costs; ?></td> 
          <td id="ppp75_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_profit; ?></td>
          <td id="ppp76_<?php echo $product['order_product_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_profit_margin; ?></td>                                                                      
        </tr>
      </thead>
        <tr bgcolor="#FFFFFF">
          <td id="ppp60_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><a><?php echo $product['product_ord_id']; ?></a></td>  
          <td id="ppp61_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['product_order_date']; ?></td>
          <td id="ppp62_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['product_inv_no']; ?></td>
          <td id="ppp63_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['product_pid']; ?></td>  
          <td id="ppp64_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['product_sku']; ?></td>                    
          <td id="ppp65_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['product_name']; ?></td> 
          <td id="ppp66_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['product_option']; ?></td>           
          <td id="ppp67_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['product_model']; ?></td>
          <?php if ($filter_report == 'categories') { ?>        
          <td id="ppp68_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['product_manu']; ?></td> 
          <?php } ?>
          <td id="ppp69_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap"><?php echo $product['product_currency']; ?></td> 
          <td id="ppp70_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap"><?php echo $product['product_price']; ?></td> 
          <td id="ppp71_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap"><?php echo $product['product_quantity']; ?></td> 
          <td id="ppp72_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap" style="background-color:#DCFFB9;"><?php echo $product['product_total']; ?></td>    
          <td id="ppp73_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap"><?php echo $product['product_tax']; ?></td>  
          <td id="ppp74_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap" style="background-color:#ffd7d7;">-<?php echo $product['product_costs']; ?></td>
          <td id="ppp75_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap" style="background-color:#c4d9ee; font-weight:bold;"><?php echo $product['product_profit']; ?></td>
          <td id="ppp76_<?php echo $product['order_product_id']; ?>" class="right" nowrap="nowrap" style="background-color:#c4d9ee; font-weight:bold;"><?php echo $product['product_profit_margin_percent']; ?>%</td>   
         </tr>       
    </table>
</div> 
<?php } ?>  
<?php if ($filter_report == 'products' && $filter_details == 2) { ?>
<script type="text/javascript">$(function(){ 
$('#show_details_<?php echo $product['order_product_id']; ?>').click(function() {
		$('#tab_details_<?php echo $product['order_product_id']; ?>').slideToggle('slow');
	});
});
</script>
<div id="tab_details_<?php echo $product['order_product_id']; ?>" style="display:none">
    <table class="list_detail">
      <thead>
        <tr>
          <td id="ppp80_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_customer_order_id; ?></td>        
          <td id="ppp81_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_customer_date_added; ?></td>
          <td id="ppp82_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_customer_inv_no; ?></td>           
          <td id="ppp83_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_customer_cust_id; ?></td>           
          <td id="ppp84_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_billing_name; ?></td> 
          <td id="ppp85_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_billing_company; ?></td> 
          <td id="ppp86_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_billing_address_1; ?></td> 
          <td id="ppp87_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_billing_address_2; ?></td> 
          <td id="ppp88_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_billing_city; ?></td>
          <td id="ppp89_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_billing_zone; ?></td> 
          <td id="ppp90_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_billing_postcode; ?></td>
          <td id="ppp91_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_billing_country; ?></td>
          <td id="ppp92_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_customer_telephone; ?></td>
          <td id="ppp93_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_shipping_name; ?></td> 
          <td id="ppp94_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_shipping_company; ?></td> 
          <td id="ppp95_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_shipping_address_1; ?></td> 
          <td id="ppp96_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_shipping_address_2; ?></td> 
          <td id="ppp97_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_shipping_city; ?></td>
          <td id="ppp98_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_shipping_zone; ?></td> 
          <td id="ppp99_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_shipping_postcode; ?></td>
          <td id="ppp100_<?php echo $product['order_product_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_shipping_country; ?></td>          
        </tr>
      </thead>
        <tr bgcolor="#FFFFFF">
          <td id="ppp80_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['customer_ord_id']; ?></td>        
          <td id="ppp81_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['customer_order_date']; ?></td>
          <td id="ppp82_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['customer_inv_no']; ?></td>
          <td id="ppp83_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['customer_cust_id']; ?></td>             
          <td id="ppp84_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['billing_name']; ?></td>         
          <td id="ppp85_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['billing_company']; ?></td> 
          <td id="ppp86_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['billing_address_1']; ?></td> 
          <td id="ppp87_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['billing_address_2']; ?></td> 
          <td id="ppp88_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['billing_city']; ?></td> 
          <td id="ppp89_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['billing_zone']; ?></td> 
          <td id="ppp90_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['billing_postcode']; ?></td>                    
          <td id="ppp91_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['billing_country']; ?></td>
          <td id="ppp92_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['customer_telephone']; ?></td> 
          <td id="ppp93_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['shipping_name']; ?></td>         
          <td id="ppp94_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['shipping_company']; ?></td> 
          <td id="ppp95_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['shipping_address_1']; ?></td> 
          <td id="ppp96_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['shipping_address_2']; ?></td> 
          <td id="ppp97_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['shipping_city']; ?></td> 
          <td id="ppp98_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['shipping_zone']; ?></td> 
          <td id="ppp99_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['shipping_postcode']; ?></td>                    
          <td id="ppp100_<?php echo $product['order_product_id']; ?>" class="left" nowrap="nowrap"><?php echo $product['shipping_country']; ?></td>                                                         
         </tr>
    </table>
</div> 
<?php } ?>  
</td>
</tr>       
        <?php } ?>
        <tr>
          <td colspan="17"></td>
        </tr>
      </tbody>                 
        <tr>
          <td colspan="2" class="right" style="background-color:#E7EFEF;"><strong><?php echo $text_filter_total; ?></strong></td>
		  <?php if ($filter_report == 'products') { ?>            
          <td id="ppp20_total" style="background-color:#DDDDDD;"></td>
          <td id="ppp21_total" style="background-color:#DDDDDD;"></td>
          <td id="ppp22_total" style="background-color:#DDDDDD;"></td>
          <td id="ppp23_total" style="background-color:#DDDDDD;"></td>
          <td id="ppp24_total" style="background-color:#DDDDDD;"></td>
          <td id="ppp25_total" style="background-color:#DDDDDD;"></td>
		  <?php } ?>            
          <td id="ppp26_total" style="background-color:#DDDDDD;"></td>           
          <td id="ppp27_total" class="right" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $product['sold_quantity_total']; ?></strong></td> 
          <td id="ppp28_total" class="right" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $product['sold_percent_total']; ?></strong></td>            
          <td id="ppp29_total" class="right" style="background-color:#DCFFB9; color:#003A88;"><strong><?php echo $product['value_total']; ?></strong></td> 
          <td id="ppp30_total" class="right" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $product['tax_total']; ?></strong></td>           
          <td id="ppp31_total" class="right" style="background-color:#ffd7d7; color:#003A88;"><strong><?php echo $product['costs_total']; ?></strong></td>
          <td id="ppp32_total" class="right" style="background-color:#BCD5ED; color:#003A88;"><strong><?php echo $product['profit_total']; ?></strong></td> 
          <td id="ppp33_total" class="right" style="background-color:#BCD5ED; color:#003A88;"><strong><?php echo $product['profit_margin_total_percent']; ?></strong></td>                    
          <?php if (($filter_report == 'products' && $filter_details == 1 OR $filter_details == 2) OR ($filter_report == 'manufacturers' && $filter_details == 3) OR ($filter_report == 'categories' && $filter_details == 3)) { ?><td class="right" style="background-color:#E7EFEF;"></td><?php } ?>                  
        </tr>         
        <?php } else { ?>
        <tr>
        <?php if ($filter_report == 'products') { ?> 
          <td class="noresult" colspan="17"><?php echo $text_no_results; ?></td>
        <?php } elseif ($filter_report == 'manufacturers' or $filter_report == 'categories') { ?> 
          <td class="noresult" colspan="11"><?php echo $text_no_results; ?></td>
        <?php } ?>      
        </tr>       
        <?php } ?>
    </table>
    </div>
      <?php if ($products) { ?>    
      <div class="pagination_report"></div>
      <?php } ?>
    </div>
  </div>
</div>  
</form>
<script type="text/javascript" src="view/javascript/jquery/jquery.multiSelect.js"></script>
<script type="text/javascript" src="view/javascript/jquery/jquery.paging.min.js"></script>
<script type="text/javascript" src="view/javascript/jquery/vtip.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});

	$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});
	
    $('#filter_order_status_id').multiSelect({
      selectAllText:'<?php echo $text_all_status; ?>', noneSelected:'<?php echo $text_all_status; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#filter_store_id').multiSelect({
      selectAllText:'<?php echo $text_all_stores; ?>', noneSelected:'<?php echo $text_all_stores; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#filter_currency').multiSelect({
      selectAllText:'<?php echo $text_all_currencies; ?>', noneSelected:'<?php echo $text_all_currencies; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });

    $('#filter_taxes').multiSelect({
      selectAllText:'<?php echo $text_all_taxes; ?>', noneSelected:'<?php echo $text_all_taxes; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#filter_customer_group_id').multiSelect({
      selectAllText:'<?php echo $text_all_groups; ?>', noneSelected:'<?php echo $text_all_groups; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });

    $('#filter_category').multiSelect({
      selectAllText:'<?php echo $text_all_categories; ?>', noneSelected:'<?php echo $text_all_categories; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });

    $('#filter_manufacturer').multiSelect({
      selectAllText:'<?php echo $text_all_manufacturers; ?>', noneSelected:'<?php echo $text_all_manufacturers; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#filter_option').multiSelect({
      selectAllText:'<?php if ($filter_ogrouping or $filter_report != 'products') { ?><?php echo $text_all_options; ?><?php } else { ?><span style="color:#999"><?php echo $text_all_options; ?></span><?php } ?>', noneSelected:'<?php if ($filter_ogrouping or $filter_report != 'products') { ?><?php echo $text_all_options; ?><?php } else { ?><span style="color:#999"><?php echo $text_all_options; ?></span><?php } ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });

    $('#filter_attribute').multiSelect({
      selectAllText:'<?php echo $text_all_attributes; ?>', noneSelected:'<?php echo $text_all_attributes; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#filter_location').multiSelect({
      selectAllText:'<?php echo $text_all_locations; ?>', noneSelected:'<?php echo $text_all_locations; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });

    $('#filter_affiliate').multiSelect({
      selectAllText:'<?php echo $text_all_affiliates; ?>', noneSelected:'<?php echo $text_all_affiliates; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#filter_status').multiSelect({
      selectAllText:'<?php echo $text_all_status; ?>', noneSelected:'<?php echo $text_all_status; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#button').click(function() {
      $('#report').submit() ;
      return(false)
    });	
	
    $('#export_xls_prod').click(function() {
      $('#export').val('1') ; // export_xls_prod: #1
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_xls_prod_order_list').click(function() {
      $('#export').val('2') ; // export_xls_prod_order_list: #2
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	

    $('#export_xls_prod_customer_list').click(function() {
      $('#export').val('3') ; // export_xls_prod_customer_list: #3
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	

    $('#export_xls_manu').click(function() {
      $('#export').val('4') ; // export_xls_manu: #4
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	

    $('#export_xls_manu_product_list').click(function() {
      $('#export').val('5') ; // export_xls_manu_product_list: #5
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	

    $('#export_xls_cat').click(function() {
      $('#export').val('6') ; // export_xls_cat: #6
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	

    $('#export_xls_cat_product_list').click(function() {
      $('#export').val('7') ; // export_xls_cat_product_list: #7
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_html_prod').click(function() {
      $('#export').val('8') ; // export_html_prod: #8
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_html_prod_order_list').click(function() {
      $('#export').val('9') ; // export_html_prod_order_list: #9
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
		
    $('#export_html_prod_customer_list').click(function() {
      $('#export').val('10') ; // export_html_prod_customer_list: #10
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_html_manu').click(function() {
      $('#export').val('11') ; // export_html_manu: #11
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	

    $('#export_html_manu_product_list').click(function() {
      $('#export').val('12') ; // export_html_manu_product_list: #12
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });		

    $('#export_html_cat').click(function() {
      $('#export').val('13') ; // export_html_cat: #13
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	

    $('#export_html_cat_product_list').click(function() {
      $('#export').val('14') ; // export_html_cat_product_list: #14
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });
	
    $('#export_pdf_prod').click(function() {
      $('#export').val('15') ; // export_pdf_prod: #15
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_pdf_prod_order_list').click(function() {
      $('#export').val('16') ; // export_pdf_prod_order_list: #16
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_pdf_prod_customer_list').click(function() {
      $('#export').val('17') ; // export_pdf_prod_customer_list: #17
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_pdf_manu').click(function() {
      $('#export').val('18') ; // export_pdf_manu: #18
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_pdf_manu_product_list').click(function() {
      $('#export').val('19') ; // export_pdf_manu_product_list: #19
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });		

    $('#export_pdf_cat').click(function() {
      $('#export').val('20') ; // export_pdf_cat: #20
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_pdf_cat_product_list').click(function() {
      $('#export').val('21') ; // export_pdf_cat_product_list: #21
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
});
</script>  
<script type="text/javascript"><!--
$('input[name=\'filter_company\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_product_profit/customer_autocomplete&token=<?php echo $token; ?>&filter_company=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.cust_company,
						value: item.customer_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_company\']').val(ui.item.label);
						
		return false;
	}
});

$('input[name=\'filter_customer_id\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_product_profit/customer_autocomplete&token=<?php echo $token; ?>&filter_customer_id=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.cust_name,
						value: item.customer_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_customer_id\']').val(ui.item.label);
						
		return false;
	}
});

$('input[name=\'filter_email\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_product_profit/customer_autocomplete&token=<?php echo $token; ?>&filter_email=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.cust_email,
						value: item.customer_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_email\']').val(ui.item.label);
						
		return false;
	}
});

$('input[name=\'filter_sku\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_product_profit/product_autocomplete&token=<?php echo $token; ?>&filter_sku=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.prod_sku,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_sku\']').val(ui.item.label);
						
		return false;
	}
});

$('input[name=\'filter_product_id\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_product_profit/product_autocomplete&token=<?php echo $token; ?>&filter_product_id=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.prod_name,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_product_id\']').val(ui.item.label);
						
		return false;
	}
});

$('input[name=\'filter_model\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_product_profit/product_autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.prod_model,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_model\']').val(ui.item.label);
						
		return false;
	}
});
//--></script>
<?php if ($products) { ?>    
<?php if (($filter_range != 'all_time' && ($filter_group == 'year' or $filter_group == 'quarter' or $filter_group == 'month')) or ($filter_range == 'all_time' && $filter_group == 'year')) { ?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript"><!--
	google.load('visualization', '1', {packages: ['corechart']});
      google.setOnLoadCallback(drawChart);      
	  function drawChart() {        
	  	var data = google.visualization.arrayToDataTable([
			<?php if ($sales && $filter_group == 'month') {
				echo "['" . $column_month . "','". $column_orders . "','" . $column_customers . "','" . $column_products . "'],";
					foreach ($sales as $key => $sale) {
						if (count($sales)==($key+1)) {
							echo "['" . $sale['gyear_month'] . "',". $sale['gorders'] . ",". $sale['gcustomers'] . ",". $sale['gproducts'] . "]";
						} else {
							echo "['" . $sale['gyear_month'] . "',". $sale['gorders'] . ",". $sale['gcustomers'] . ",". $sale['gproducts'] . "],";
						}
					}	
			} elseif ($sales && $filter_group == 'quarter') {
				echo "['" . $column_quarter . "','". $column_orders . "','" . $column_customers . "','" . $column_products . "'],";
					foreach ($sales as $key => $sale) {
						if (count($sales)==($key+1)) {
							echo "['" . $sale['gyear_quarter'] . "',". $sale['gorders'] . ",". $sale['gcustomers'] . ",". $sale['gproducts'] . "]";
						} else {
							echo "['" . $sale['gyear_quarter'] . "',". $sale['gorders'] . ",". $sale['gcustomers'] . ",". $sale['gproducts'] . "],";
						}
					}	
			} elseif ($sales && $filter_group == 'year') {
				echo "['" . $column_year . "','". $column_orders . "','" . $column_customers . "','" . $column_products . "'],";
					foreach ($sales as $key => $sale) {
						if (count($sales)==($key+1)) {
							echo "['" . $sale['gyear'] . "',". $sale['gorders'] . ",". $sale['gcustomers'] . ",". $sale['gproducts'] . "]";
						} else {
							echo "['" . $sale['gyear'] . "',". $sale['gorders'] . ",". $sale['gcustomers'] . ",". $sale['gproducts'] . "],";
						}
					}	
			} 
			;?>
		]);

        var options = {
			width: 630,	
			height: 266,  
			colors: ['#edc240', '#9dc7e8', '#CCCCCC'],
			chartArea: {left:30, top:30, width:"75%", height:"70%"},
			pointSize: '4',
			legend: {position: 'right', alignment: 'start', textStyle: {color: '#666666', fontSize: 12}}
		};

			var chart = new google.visualization.LineChart(document.getElementById('chart1_div'));
			chart.draw(data, options);
	}
//--></script>
<script type="text/javascript"><!--
	google.load('visualization', '1', {packages: ['corechart']});
	function drawVisualization() {
   		var data = google.visualization.arrayToDataTable([
			<?php if ($sales && $filter_group == 'month') {
				echo "['" . $column_month . "','". $column_total . "','" . $column_prod_costs . "','" . $column_prod_profit . "'],";
					foreach ($sales as $key => $sale) {
						if (count($sales)==($key+1)) {
							echo "['" . $sale['gyear_month'] . "',". $sale['gsales'] . ",". $sale['gcosts'] . ",". $sale['gprofit'] . "]";
						} else {
							echo "['" . $sale['gyear_month'] . "',". $sale['gsales'] . ",". $sale['gcosts'] . ",". $sale['gprofit'] . "],";
						}
					}	
			} elseif ($sales && $filter_group == 'quarter') {
				echo "['" . $column_quarter . "','". $column_total . "','" . $column_prod_costs . "','" . $column_prod_profit . "'],";
					foreach ($sales as $key => $sale) {
						if (count($sales)==($key+1)) {
							echo "['" . $sale['gyear_quarter'] . "',". $sale['gsales'] . ",". $sale['gcosts'] . ",". $sale['gprofit'] . "]";
						} else {
							echo "['" . $sale['gyear_quarter'] . "',". $sale['gsales'] . ",". $sale['gcosts'] . ",". $sale['gprofit'] . "],";
						}
					}	
			} elseif ($sales && $filter_group == 'year') {
				echo "['" . $column_year . "','". $column_total . "','" . $column_prod_costs . "','" . $column_prod_profit . "'],";
					foreach ($sales as $key => $sale) {
						if (count($sales)==($key+1)) {
							echo "['" . $sale['gyear'] . "',". $sale['gsales'] . ",". $sale['gcosts'] . ",". $sale['gprofit'] . "]";
						} else {
							echo "['" . $sale['gyear'] . "',". $sale['gsales'] . ",". $sale['gcosts'] . ",". $sale['gprofit'] . "],";
						}
					}	
			} 
			;?>
		]);

        var options = {
			width: 630,	
			height: 266,  
			colors: ['#b5e08b', '#ed9999', '#739cc3'],
			chartArea: {left:45, top:30, width:"74%", height:"70%"},
			legend: {position: 'right', alignment: 'start', textStyle: {color: '#666666', fontSize: 12}},				
			seriesType: "bars",
			series: {2: {type: "line", lineWidth: '3', pointSize: '5'}}
		};

			var chart = new google.visualization.ComboChart(document.getElementById('chart2_div'));
			chart.draw(data, options);
	}
	
	google.setOnLoadCallback(drawVisualization);
//--></script>
<?php } ?>
<?php } ?>
<?php echo $footer; ?>