<?php echo $header; ?>

<div class="report-logo">
    <a href="<?php echo $home; ?>">
        <img src="view/image/admin_theme/base5builder_impulsepro/logo.png" />
    </a>
</div><!-- report-logo -->

<script type="text/javascript">
$(document).ready(function() { 
  $("#pagination_content").hide(); 
  $(window).load(function() { 
    $("#pagination_content").show(); 
    $("#content-loading").hide(); 
  }) 
}) 
</script>
<div id="content-loading" style="position: absolute; background-color:white; layer-background-color:white; height:100%; width:100%; text-align:center;"><img src="view/image/page_loading.gif" border="0"></div>
<style type="text/css">
.box > .content_report {
	padding: 10px;
	border-left: 1px solid #CCCCCC;
	border-right: 1px solid #CCCCCC;
	border-bottom: 1px solid #CCCCCC;
	min-height: 300px;
}
.list_main {
	border-collapse: collapse;
	width: 100%;
	border-top: 1px solid #DDDDDD;
	border-left: 1px solid #DDDDDD;	
	margin-bottom: 20px;
}
.list_main td {
	border-right: 1px solid #DDDDDD;
	border-bottom: 1px solid #DDDDDD;	
}
.list_main thead td {
	background-color: #E5E5E5;
	padding: 0px 5px;
	font-weight: bold;	
}
.list_main tbody td {
	vertical-align: middle;
	padding: 0px 5px;
}
.list_main .left {
	text-align: left;
	padding: 7px;
}
.list_main .right {
	text-align: right;
	padding: 7px;
}
.list_main .center {
	text-align: center;
	padding: 3px;
}
.list_main .noresult {
	text-align: center;
	padding: 7px;
}

.list_detail {
	border-collapse: collapse;
	width: 100%;
	border-top: 1px solid #DDDDDD;
	border-left: 1px solid #DDDDDD;
	margin-top: 10px;
	margin-bottom: 10px;
}
.list_detail td {
	border-right: 1px solid #DDDDDD;
	border-bottom: 1px solid #DDDDDD;
}
.list_detail thead td {
	background-color: #F0F0F0;
	padding: 0px 3px;
	font-size: 11px;
}
.list_detail tbody td {
	padding: 0px 3px;
	font-size: 11px;	
}
.list_detail .left {
	text-align: left;
	padding: 3px;
}
.list_detail .right {
	text-align: right;
	padding: 3px;
}
.list_detail .center {
	text-align: center;
	padding: 3px;
}

#mask {
	position: absolute;
	left: 0;
	top: 0;
	z-index: 9000;
	background-color: #000000;
	display: none;
}
#boxes .window {
	position: fixed;
	left: 0;
	top: 0;
	display: none;
	z-index: 9999;
}
#boxes #dialog {
	background:#FFFFFF; 
	border: 2px solid #ff9f00; 
	padding: 10px;
}

.export_item {
  text-decoration: none;
  cursor: pointer;
}
.export_item a {
  text-decoration: none;
}
.export_item :hover {
  opacity: 0.7;
  -moz-opacity: 0.7;
  -ms-filter: "alpha(opacity=70)"; /* IE 8 */
  filter: alpha(opacity=70); /* IE < 8 */
} 
a.cbutton {
	text-decoration: none;
	color: #FFF;
	display: inline-block;
	padding: 5px 15px 5px 15px;
	-webkit-border-radius: 5px 5px 5px 5px;
	-moz-border-radius: 5px 5px 5px 5px;
	-khtml-border-radius: 5px 5px 5px 5px;
	border-radius: 5px 5px 5px 5px;
}

.pagination_report {
	padding:3px;
	margin:3px;
	text-align:right;
}
.pagination_report a {
	padding: 4px 8px 4px 8px;
	margin-right: 2px;
	border: 1px solid #ddd;
	text-decoration: none; 
	color: #666;
}
.pagination_report a:hover, .pagination_report a:active {
	padding: 4px 8px 4px 8px;
	margin-right: 2px;
	border: 1px solid #c0c0c0;
}
.pagination_report span.current {
	padding: 4px 8px 4px 8px;
	margin-right: 2px;
	border: 1px solid #a0a0a0;
	font-weight: bold;
	background-color: #f0f0f0;
	color: #666;
}
.pagination_report span.disabled {
	padding: 4px 8px 4px 8px;
	margin-right: 2px;
	border: 1px solid #f3f3f3;
	color: #ccc;
}

#content{
  margin: 0px !important;
}
#left-column{
   display: none;
}
thead td {
  font-weight: normal !important;
}

#content .heading h1{
    left: 210px;
}
#content .heading h1 img{
    display: none;
}
</style>
<link href="view/stylesheet/jquery.multiSelect.css" rel="stylesheet" type="text/css" />
<form method="post" action="index.php?route=report/adv_customer_profit&token=<?php echo $token; ?>" id="report" name="report"> 
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1><span class="vtip" title="<?php echo $text_profit_help; ?>"><img style="padding-top:10px; padding-left:5px;" src="view/image/profit_info.png" alt="" /></span><span style="float:right; padding-top:5px; padding-right:5px; font-size:11px; color:#666; text-align:right;"><?php echo $heading_version; ?></span></div>
      <div align="right" style="height:38px; background-color:#F0F0F0; border: 1px solid #DDDDDD; margin-top:5px;">
      <div style="padding-top: 7px; margin-right: 5px;"><?php echo $entry_customer_type; ?>
		<select name="filter_types" style="background-color:#E7EFEF; border-width:thin; border-color:#333;"> 
            <?php if (!$filter_types) { ?>        
            <option value="3" selected="selected"><?php echo $text_all_cust_types; ?></option> 
            <?php } else { ?>
            <option value="3"><?php echo $text_all_cust_types; ?></option> 
            <?php } ?>                      
            <?php if ($filter_types == '1') { ?>
            <option value="1" selected="selected"><?php echo $text_registered; ?></option>
            <?php } else { ?>
            <option value="1"><?php echo $text_registered; ?></option>
            <?php } ?>
            <?php if ($filter_types == '0') { ?>
            <option value="0" selected="selected"><?php echo $text_guest; ?></option>
            <?php } else { ?>
            <option value="0"><?php echo $text_guest; ?></option>
            <?php } ?>
          </select>&nbsp;&nbsp; 
          <select name="filter_group" style="background-color:#E7EFEF; border-width:thin; border-color:#333;"> 
            <?php foreach ($groups as $groups) { ?>
            <?php if ($groups['value'] == $filter_group) { ?>
            <option value="<?php echo $groups['value']; ?>" selected="selected"><?php echo $groups['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $groups['value']; ?>"><?php echo $groups['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>&nbsp;&nbsp;           
          <?php echo $entry_sort_by; ?>
		  <select name="filter_sort" style="width: 100px;background-color:#E7EFEF; border-width:thin; border-color:#333;">
            <?php if ($filter_sort == 'date') { ?>
            <option value="date" selected="selected"><?php echo $column_date; ?></option>
            <?php } else { ?>
            <option value="date"><?php echo $column_date; ?></option>
            <?php } ?>          
            <?php if ($filter_sort == 'customer_id') { ?>
            <option value="customer_id" selected="selected"><?php echo $column_id; ?></option>
            <?php } else { ?>
            <option value="customer_id"><?php echo $column_id; ?></option>
            <?php } ?>
            <?php if ($filter_sort == 'cust_name') { ?>
            <option value="cust_name" selected="selected"><?php echo $column_customer; ?></option>
            <?php } else { ?>
            <option value="cust_name"><?php echo $column_customer; ?></option>
            <?php } ?>
            <?php if ($filter_sort == 'cust_email') { ?>
            <option value="cust_email" selected="selected"><?php echo $column_email; ?></option>
            <?php } else { ?>
            <option value="cust_email"><?php echo $column_email; ?></option>
            <?php } ?>
            <?php if ($filter_sort == 'cust_group_reg') { ?>
            <option value="cust_group_reg" selected="selected"><?php echo $column_customer_group; ?></option>
            <?php } else { ?>
            <option value="cust_group_reg"><?php echo $column_customer_group; ?></option>
            <?php } ?>
            <?php if ($filter_sort == 'cust_status') { ?>
            <option value="cust_status" selected="selected"><?php echo $column_status; ?></option>
            <?php } else { ?>
            <option value="cust_status"><?php echo $column_status; ?></option>
            <?php } ?>
            <?php if ($filter_sort == 'cust_ip') { ?>
            <option value="cust_ip" selected="selected"><?php echo $column_ip; ?></option>
            <?php } else { ?>
            <option value="cust_ip"><?php echo $column_ip; ?></option>
            <?php } ?>
            <?php if ($filter_sort == 'mostrecent') { ?>
            <option value="mostrecent" selected="selected"><?php echo $column_mostrecent; ?></option>
            <?php } else { ?>
            <option value="mostrecent"><?php echo $column_mostrecent; ?></option>
            <?php } ?>                                                                                                        
            <?php if (!$filter_sort or $filter_sort == 'orders') { ?>
            <option value="orders" selected="selected"><?php echo $column_orders; ?></option>
            <?php } else { ?>
            <option value="orders"><?php echo $column_orders; ?></option>
            <?php } ?>
            <?php if ($filter_sort == 'products') { ?>
            <option value="products" selected="selected"><?php echo $column_products; ?></option>
            <?php } else { ?>
            <option value="products"><?php echo $column_products; ?></option>
            <?php } ?>
            <?php if ($filter_sort == 'sales') { ?>
            <option value="sales" selected="selected"><?php echo $column_sales; ?></option>
            <?php } else { ?>
            <option value="sales"><?php echo $column_sales; ?></option>
            <?php } ?>            
            <?php if ($filter_sort == 'total') { ?>
            <option value="total" selected="selected"><?php echo $column_value; ?></option>
            <?php } else { ?>
            <option value="total"><?php echo $column_value; ?></option>
            <?php } ?>
            <?php if ($filter_sort == 'costs') { ?>
            <option value="costs" selected="selected"><?php echo $column_costs; ?></option>
            <?php } else { ?>
            <option value="costs"><?php echo $column_costs; ?></option>
            <?php } ?>              
            <?php if ($filter_sort == 'profit') { ?>
            <option value="profit" selected="selected"><?php echo $column_net_profit; ?></option>
            <?php } else { ?>
            <option value="profit"><?php echo $column_net_profit; ?></option>
            <?php } ?>                        
          </select>&nbsp;&nbsp; 
          <?php echo $entry_show_details; ?>
		  <select name="filter_details" style="background-color:#E7EFEF; border-width:thin; border-color:#333;">                      
            <?php if (!$filter_details or $filter_details == '0') { ?>
            <option value="0" selected="selected"><?php echo $text_no_details; ?></option>
            <?php } else { ?>
            <option value="0"><?php echo $text_no_details; ?></option>
            <?php } ?>
            <?php if ($filter_details == '1') { ?>
            <option value="1" selected="selected"><?php echo $text_order_list; ?></option>
            <?php } else { ?>
            <option value="1"><?php echo $text_order_list; ?></option>
            <?php } ?>
            <?php if ($filter_details == '2') { ?>
            <option value="2" selected="selected"><?php echo $text_product_list; ?></option>
            <?php } else { ?>
            <option value="2"><?php echo $text_product_list; ?></option>
            <?php } ?>                      
          </select>&nbsp;&nbsp; 
          <?php echo $entry_limit; ?>
		  <select name="filter_limit" style="background-color:#E7EFEF; border-width:thin; border-color:#333;"> 
            <?php if ($filter_limit == '10') { ?>
            <option value="10" selected="selected">10</option>
            <?php } else { ?>
            <option value="10">10</option>
            <?php } ?>                                
            <?php if (!$filter_limit or $filter_limit == '25') { ?>
            <option value="25" selected="selected">25</option>
            <?php } else { ?>
            <option value="25">25</option>
            <?php } ?>
            <?php if ($filter_limit == '50') { ?>
            <option value="50" selected="selected">50</option>
            <?php } else { ?>
            <option value="50">50</option>
            <?php } ?>
            <?php if ($filter_limit == '100') { ?>
            <option value="100" selected="selected">100</option>
            <?php } else { ?>
            <option value="100">100</option>
            <?php } ?>                        
          </select>&nbsp; <a id="button" onclick="$('#report').submit();" class="cbutton" style="background:#069;"><span><?php echo $button_filter; ?></span></a>&nbsp;<?php if ($customers) { ?><?php if (($filter_range != 'all_time' && ($filter_group == 'year' or $filter_group == 'quarter' or $filter_group == 'month')) or ($filter_range == 'all_time' && $filter_group == 'year')) { ?><a id="show_tab_chart" class="cbutton" style="background:#930;"><span><?php echo $button_chart; ?></span></a><?php } ?><?php } ?>&nbsp;<a id="show_tab_export" class="cbutton" style="background:#699;"><span><?php echo $button_export; ?></span></a>&nbsp;<a href="#dialog" name="modal" class="cbutton" style="background:#666;"><span><?php echo $button_settings; ?></span></a></div>
    </div>
    <div class="content_report">
<script type="text/javascript"><!--
$(document).ready(function() {
var prev = {start: 0, stop: 0},
    cont = $('#pagination_content .element');
	
$(".pagination_report").paging(cont.length, {
	format: '[< ncnnn! >]',
	perpage: '<?php echo $filter_limit; ?>',	
	lapping: 0,
	page: null, // we await hashchange() event
			onSelect: function() {

				var data = this.slice;

				cont.slice(prev[0], prev[1]).css('display', 'none');
				cont.slice(data[0], data[1]).fadeIn(0);

				prev = data;

				return true; // locate!
			},
			onFormat: function (type) {

				switch (type) {

					case 'block':

						if (!this.active)
							return '<span class="disabled">' + this.value + '</span>';
						else if (this.value != this.page)
							return '<em><a href="index.php?route=report/adv_customer_profit&token=<?php echo $token; ?>#' + this.value + '">' + this.value + '</a></em>';
						return '<span class="current">' + this.value + '</span>';

					case 'next':

						if (this.active) {
							return '<a href="index.php?route=report/adv_customer_profit&token=<?php echo $token; ?>#' + this.value + '" class="next">Next &gt;</a>';
						}
						return '';						

					case 'prev':

						if (this.active) {
							return '<a href="index.php?route=report/adv_customer_profit&token=<?php echo $token; ?>#' + this.value + '" class="prev">&lt; Previous</a>';
						}	
						return '';						

					case 'first':

						if (this.active) {
							return '<?php echo $text_pagin_page; ?> ' + this.page + ' <?php echo $text_pagin_of; ?> ' + this.pages + '&nbsp;&nbsp;<a href="index.php?route=report/adv_customer_profit&token=<?php echo $token; ?>#' + this.value + '" class="first">|&lt;</a>';
						}	
						return '<?php echo $text_pagin_page; ?> ' + this.page + ' <?php echo $text_pagin_of; ?> ' + this.pages + '&nbsp;&nbsp';
							
					case 'last':

						if (this.active) {
							return '<a href="index.php?route=report/adv_customer_profit&token=<?php echo $token; ?>#' + this.value + '" class="prev">&gt;|</a>&nbsp;&nbsp;(' + cont.length + ' <?php echo $text_pagin_results; ?>)';
						}
						return '&nbsp;&nbsp;(' + cont.length + ' <?php echo $text_pagin_results; ?>)';					

				}
				return ''; // return nothing for missing branches
			}
});
});		
//--></script>         
<script type="text/javascript"><!--
function getStorage(key_prefix) {
    // this function will return us an object with a "set" and "get" method
    if (window.localStorage) {
        // use localStorage:
        return {
            set: function(id, data) {
                localStorage.setItem(key_prefix+id, data);
            },
            get: function(id) {
                return localStorage.getItem(key_prefix+id);
            }
        };
    }
}

$(document).ready(function() {
    // a key must is used for the cookie/storage
    var storedData = getStorage('com_mysite_checkboxes_'); 
    
    $('div.check input:checkbox').bind('change',function(){
        $('#'+this.id+'_filter').toggle($(this).is(':checked'));
        $('#'+this.id+'_title').toggle($(this).is(':checked'));
			<?php if ($customers) {
					foreach ($customers as $key => $customer) {
						echo "$('#'+this.id+'_" . $customer['order_id'] . "_title').toggle($(this).is(':checked')); ";
						echo "$('#'+this.id+'_" . $customer['order_id'] . "').toggle($(this).is(':checked')); ";						
					}			
			} 
			;?>		
        $('#'+this.id+'_total').toggle($(this).is(':checked'));			
        // save the data on change
        storedData.set(this.id, $(this).is(':checked')?'checked':'not');
    }).each(function() {
        // on load, set the value to what we read from storage:
        var val = storedData.get(this.id);
        if (val == 'checked') $(this).attr('checked', 'checked');
        if (val == 'not') $(this).removeAttr('checked');
        if (val) $(this).trigger('change');
    });
});
//--></script>
<script type="text/javascript">
$(document).ready(function() {
	//select all the a tag with name equal to modal
	$('a[name=modal]').click(function(e) {
		//Cancel the link behavior
		e.preventDefault();
		
		//Get the A tag
		var id = $(this).attr('href');
	
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeTo("fast",0.15);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
              
		//Set the popup window to center
		$(id).css('top',  winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);
	
		//transition effect
		$(id).fadeIn(500); 
	});
	
	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		
		$('#mask').hide();
		$('.window').hide();
	});		
	
	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});			
	
	$(window).resize(function () {
 		var box = $('#boxes .window');
 
        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
      
        //Set height and width to mask to fill up the whole screen
        $('#mask').css({'width':maskWidth,'height':maskHeight});
               
        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        //Set the popup window to center
        box.css('top',  winH/2 - box.height()/2);
        box.css('left', winW/2 - box.width()/2);
	});	
});
</script>
<div id="boxes">
<div id="dialog" class="window">
<div align="right"><a href="#" class="close" style="text-decoration:none;">[Close]</a></div>
    <div class="check">  
	<div style="float:left; padding-right:10px; padding-top:5px;">     
      &nbsp;<b><?php echo $text_filtering_options; ?></b><br />        
      <table cellspacing="0" cellpadding="3" style="background:#E7EFEF; border: 1px solid #DDDDDD; padding-right:10px;">
        <tr>
          <td>          
			<input id="cp1" checked="checked" type="checkbox"><label for="cp1"><?php echo substr($entry_status,0,-1); ?></label><br />
			<input id="cp2" checked="checked" type="checkbox"><label for="cp2"><?php echo substr($entry_store,0,-1); ?></label><br />
			<input id="cp3" checked="checked" type="checkbox"><label for="cp3"><?php echo substr($entry_currency,0,-1); ?></label><br />
			<input id="cp4" checked="checked" type="checkbox"><label for="cp4"><?php echo substr($entry_tax,0,-1); ?></label><br />
			<input id="cp5" checked="checked" type="checkbox"><label for="cp5"><?php echo substr($entry_customer_group,0,-1); ?></label><br />
			<input id="cp6" checked="checked" type="checkbox"><label for="cp6"><?php echo substr($entry_customer_status,0,-1); ?></label><br />
			<input id="cp7" checked="checked" type="checkbox"><label for="cp7"><?php echo substr($entry_company,0,-1); ?></label><br />
			<input id="cp8" checked="checked" type="checkbox"><label for="cp8"><?php echo substr($entry_customer,0,-1); ?></label><br />
			<input id="cp9" checked="checked" type="checkbox"><label for="cp9"><?php echo substr($entry_email,0,-1); ?></label><br />
			<input id="cp10" checked="checked" type="checkbox"><label for="cp10"><?php echo substr($entry_ip,0,-1); ?></label><br />
            <input id="cp11" checked="checked" type="checkbox"><label for="cp11"><?php echo substr($entry_product,0,-1); ?></label><br />
            <input id="cp12" checked="checked" type="checkbox"><label for="cp12"><?php echo substr($entry_option,0,-1); ?></label><br />  
            <input id="cp13" checked="checked" type="checkbox"><label for="cp13"><?php echo substr($entry_location,0,-1); ?></label><br />
            <input id="cp14" checked="checked" type="checkbox"><label for="cp14"><?php echo substr($entry_affiliate,0,-1); ?></label><br />                       
            <input id="cp15" checked="checked" type="checkbox"><label for="cp15"><?php echo substr($entry_shipping,0,-1); ?></label><br />
            <input id="cp16" checked="checked" type="checkbox"><label for="cp16"><?php echo substr($entry_payment,0,-1); ?></label><br />
            <input id="cp17" checked="checked" type="checkbox"><label for="cp17"><?php echo substr($entry_zone,0,-1); ?></label><br />            
            <input id="cp18" checked="checked" type="checkbox"><label for="cp18"><?php echo substr($entry_shipping_country,0,-1); ?></label><br />
            <input id="cp19" checked="checked" type="checkbox"><label for="cp19"><?php echo substr($entry_payment_country,0,-1); ?></label> 	         		
          </td>                                                                                                                        
        </tr>
      </table>  
    </div>
	<div style="float:left; padding-right:10px; padding-top:5px;">          
      &nbsp;<b><?php echo $text_mv_columns; ?></b><br />
      <table cellspacing="0" cellpadding="3" style="background:#E5E5E5; border: 1px solid #DDDDDD; padding-right:10px;">
        <tr>
          <td>         
			<input id="cp20" checked="checked" type="checkbox"><label for="cp20"><?php echo $column_id; ?></label><br />
			<input id="cp21" checked="checked" type="checkbox"><label for="cp21"><?php echo $column_customer; ?></label><br />
			<input id="cp22" checked="checked" type="checkbox"><label for="cp22"><?php echo $column_email; ?></label><br />
			<input id="cp23" checked="checked" type="checkbox"><label for="cp23"><?php echo $column_customer_group; ?></label><br />
			<input id="cp24" checked="checked" type="checkbox"><label for="cp24"><?php echo $column_status; ?></label><br />
			<input id="cp25" checked="checked" type="checkbox"><label for="cp25"><?php echo $column_ip; ?></label><br />
			<input id="cp26" checked="checked" type="checkbox"><label for="cp26"><?php echo $column_mostrecent; ?></label><br />
			<input id="cp27" checked="checked" type="checkbox"><label for="cp27"><?php echo $column_orders; ?></label><br />
            <input id="cp28" checked="checked" type="checkbox"><label for="cp28"><?php echo $column_products; ?></label><br />
            <input id="cp29" checked="checked" type="checkbox"><label for="cp29"><?php echo $column_sales; ?></label><br />
            <input id="cp30" checked="checked" type="checkbox"><label for="cp30"><?php echo $column_value; ?></label><br />
            <input id="cp31" checked="checked" type="checkbox"><label for="cp31"><?php echo $column_costs; ?></label><br />
            <input id="cp32" checked="checked" type="checkbox"><label for="cp32"><?php echo $column_net_profit; ?></label><br />
            <input id="cp33" checked="checked" type="checkbox"><label for="cp33"><?php echo $column_profit_margin; ?></label>       
          </td>                                                                                                                        
        </tr>
      </table>
    </div>
	<div style="float:left; padding-right:10px; padding-top:5px;">        
      &nbsp;<b><?php echo $text_ol_columns; ?></b><br />
      <table cellspacing="0" cellpadding="3" style="background:#F0F0F0; border: 1px solid #DDDDDD; padding-right:10px;">
        <tr>
          <td>      
			<input id="cp40" checked="checked" type="checkbox"><label for="cp40"><?php echo $column_order_order_id; ?></label><br />
			<input id="cp41" checked="checked" type="checkbox"><label for="cp41"><?php echo $column_order_date_added; ?></label><br />
			<input id="cp42" checked="checked" type="checkbox"><label for="cp42"><?php echo $column_order_inv_no; ?></label><br />
			<input id="cp43" checked="checked" type="checkbox"><label for="cp43"><?php echo $column_order_customer; ?></label><br />
			<input id="cp44" checked="checked" type="checkbox"><label for="cp44"><?php echo $column_order_email; ?></label><br />
			<input id="cp45" checked="checked" type="checkbox"><label for="cp45"><?php echo $column_order_customer_group; ?></label><br />
			<input id="cp46" checked="checked" type="checkbox"><label for="cp46"><?php echo $column_order_shipping_method; ?></label><br />
            <input id="cp47" checked="checked" type="checkbox"><label for="cp47"><?php echo $column_order_payment_method; ?></label><br />
            <input id="cp48" checked="checked" type="checkbox"><label for="cp48"><?php echo $column_order_status; ?></label><br />
            <input id="cp49" checked="checked" type="checkbox"><label for="cp49"><?php echo $column_order_store; ?></label><br />
            <input id="cp50" checked="checked" type="checkbox"><label for="cp50"><?php echo $column_order_currency; ?></label><br />
            <input id="cp51" checked="checked" type="checkbox"><label for="cp51"><?php echo $column_order_quantity; ?></label><br />
            <input id="cp52" checked="checked" type="checkbox"><label for="cp52"><?php echo $column_order_sub_total; ?></label><br />
            <input id="cp531" checked="checked" type="checkbox"><label for="cp531"><?php echo $column_order_hf; ?></label><br />
            <input id="cp532" checked="checked" type="checkbox"><label for="cp532"><?php echo $column_order_lof; ?></label><br />
            <input id="cp54" checked="checked" type="checkbox"><label for="cp54"><?php echo $column_order_shipping; ?></label><br />
            <input id="cp55" checked="checked" type="checkbox"><label for="cp55"><?php echo $column_order_tax; ?></label><br />
            <input id="cp56" checked="checked" type="checkbox"><label for="cp56"><?php echo $column_order_value; ?></label><br />
            <input id="cp57" checked="checked" type="checkbox"><label for="cp57"><?php echo $column_order_costs; ?></label><br />
            <input id="cp58" checked="checked" type="checkbox"><label for="cp58"><?php echo $column_order_profit; ?></label><br />
            <input id="cp59" checked="checked" type="checkbox"><label for="cp59"><?php echo $column_profit_margin; ?></label>
          </td>                                                                                                                        
        </tr>
      </table>  
    </div>
	<div style="float:left; padding-right:10px; padding-top:5px;">        
      &nbsp;<b><?php echo $text_pl_columns; ?></b><br />
      <table cellspacing="0" cellpadding="3" style="background:#F0F0F0; border: 1px solid #DDDDDD; padding-right:10px;">
        <tr>
          <td>     
			<input id="cp60" checked="checked" type="checkbox"><label for="cp60"><?php echo $column_prod_order_id; ?></label><br />
			<input id="cp61" checked="checked" type="checkbox"><label for="cp61"><?php echo $column_prod_date_added; ?></label><br />
			<input id="cp62" checked="checked" type="checkbox"><label for="cp62"><?php echo $column_prod_inv_no; ?></label><br />
			<input id="cp63" checked="checked" type="checkbox"><label for="cp63"><?php echo $column_prod_id; ?></label><br />
			<input id="cp64" checked="checked" type="checkbox"><label for="cp64"><?php echo $column_prod_sku; ?></label><br />
			<input id="cp65" checked="checked" type="checkbox"><label for="cp65"><?php echo $column_prod_model; ?></label><br />
			<input id="cp66" checked="checked" type="checkbox"><label for="cp66"><?php echo $column_prod_name; ?></label><br />
			<input id="cp67" checked="checked" type="checkbox"><label for="cp67"><?php echo $column_prod_option; ?></label><br />
            <input id="cp68" checked="checked" type="checkbox"><label for="cp68"><?php echo $column_prod_manu; ?></label><br />
            <input id="cp69" checked="checked" type="checkbox"><label for="cp69"><?php echo $column_prod_currency; ?></label><br />
            <input id="cp70" checked="checked" type="checkbox"><label for="cp70"><?php echo $column_prod_price; ?></label><br />
            <input id="cp71" checked="checked" type="checkbox"><label for="cp71"><?php echo $column_prod_quantity; ?></label><br />
            <input id="cp72" checked="checked" type="checkbox"><label for="cp72"><?php echo $column_prod_total; ?></label><br />
            <input id="cp73" checked="checked" type="checkbox"><label for="cp73"><?php echo $column_prod_tax; ?></label><br />
            <input id="cp74" checked="checked" type="checkbox"><label for="cp74"><?php echo $column_prod_costs; ?></label><br />
            <input id="cp75" checked="checked" type="checkbox"><label for="cp75"><?php echo $column_prod_profit; ?></label><br />
            <input id="cp76" checked="checked" type="checkbox"><label for="cp76"><?php echo $column_profit_margin; ?></label>
          </td>                                                                                                                        
        </tr>
      </table>                
    </div>                    
    </div> 
</div>
<div id="mask"></div>
</div>     
    <div style="background: #E7EFEF; border: 1px solid #C6D7D7; margin-bottom: 15px;">
	<table width="100%" cellspacing="0" cellpadding="3">
	<tr>
	<td>
	 <table border="0" cellspacing="0" cellpadding="0">
  	 <tr>
      <td width="220" valign="top" nowrap="nowrap" style="background: #C6D7D7; border: 1px solid #CCCCCC; padding: 5px;">
      <table cellpadding="0" cellspacing="0" style="float:left;">
        <tr><td><?php echo $entry_date_start; ?><br />
          <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="12" style="margin-top: 4px;" />
          </td><td width="10"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>
      <table cellpadding="0" cellspacing="0" style="float:left;">
        <tr><td><?php echo $entry_date_end; ?><br />
          <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="12" style="margin-top: 4px;" />
          </td><td></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>
      <table cellpadding="0" cellspacing="0">
        <tr><td><?php echo $entry_range; ?><br />
            <select name="filter_range" style="width:215px !important;margin-top: 4px;">
              <?php foreach ($ranges as $range) { ?>
              <?php if ($range['value'] == $filter_range) { ?>
              <option value="<?php echo $range['value']; ?>" title="<?php echo $range['text']; ?>" selected="selected"><?php echo $range['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $range['value']; ?>" title="<?php echo $range['text']; ?>"><?php echo $range['text']; ?></option>
              <?php } ?>
              <?php } ?>
            </select></td><td></td></tr>
        <tr><td>&nbsp;</td><td></td>
        </tr></table>    
      </td>
    <td valign="top" style="padding: 5px;">
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp1_filter">
        <tr><td><?php echo $entry_status; ?><br />
          <span <?php echo (!$filter_order_status_id) ? '' : 'class="vtip"' ?> title="<?php foreach ($order_statuses as $order_status) { ?><?php if (isset($filter_order_status_id[$order_status['order_status_id']])) { ?><?php echo $order_status['name']; ?><br /><?php } ?><?php } ?>">
          <select name="filter_order_status_id" id="filter_order_status_id" multiple="multiple" size="1">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if (isset($filter_order_status_id[$order_status['order_status_id']])) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table> 
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp2_filter">
        <tr><td><?php echo $entry_store; ?><br />
          <span <?php echo (!$filter_store_id) ? '' : 'class="vtip"' ?> title="<?php foreach ($stores as $store) { ?><?php if (isset($filter_store_id[$store['store_id']])) { ?><?php echo $store['store_name']; ?><br /><?php } ?><?php } ?>">
          <select name="filter_store_id" id="filter_store_id" multiple="multiple" size="1">
            <?php foreach ($stores as $store) { ?>
            <?php if (isset($filter_store_id[$store['store_id']])) { ?>            
            <option value="<?php echo $store['store_id']; ?>" selected="selected"><?php echo $store['store_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $store['store_id']; ?>"><?php echo $store['store_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>    
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp3_filter">
        <tr><td><?php echo $entry_currency; ?><br />
          <span <?php echo (!$filter_currency) ? '' : 'class="vtip"' ?> title="<?php foreach ($currencies as $currency) { ?><?php if (isset($filter_currency[$currency['currency_id']])) { ?><?php echo $currency['title']; ?> (<?php echo $currency['code']; ?>)<br /><?php } ?><?php } ?>">
          <select name="filter_currency" id="filter_currency" multiple="multiple" size="1">
            <?php foreach ($currencies as $currency) { ?>
            <?php if (isset($filter_currency[$currency['currency_id']])) { ?>
            <option value="<?php echo $currency['currency_id']; ?>" selected="selected"><?php echo $currency['title']; ?> (<?php echo $currency['code']; ?>)</option>
            <?php } else { ?>
            <option value="<?php echo $currency['currency_id']; ?>"><?php echo $currency['title']; ?> (<?php echo $currency['code']; ?>)</option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>          
          </tr></table>
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp4_filter">
        <tr><td><?php echo $entry_tax; ?><br />
          <span <?php echo (!$filter_taxes) ? '' : 'class="vtip"' ?> title="<?php foreach ($taxes as $tax) { ?><?php if (isset($filter_taxes[$tax['tax']])) { ?><?php echo $tax['tax_title']; ?><br /><?php } ?><?php } ?>">
		  <select name="filter_taxes" id="filter_taxes" multiple="multiple" size="1">
            <?php foreach ($taxes as $tax) { ?>
            <?php if (isset($filter_taxes[$tax['tax']])) { ?>              
            <option value="<?php echo $tax['tax']; ?>" selected="selected"><?php echo $tax['tax_title']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $tax['tax']; ?>"><?php echo $tax['tax_title']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table> 
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp5_filter">
        <tr><td><?php echo $entry_customer_group; ?><br />
          <span <?php echo (!$filter_customer_group_id) ? '' : 'class="vtip"' ?> title="<?php foreach ($customer_groups as $customer_group) { ?><?php if (isset($filter_customer_group_id[$customer_group['customer_group_id']])) { ?><?php echo $customer_group['name']; ?><br /><?php } ?><?php } ?>">
          <select name="filter_customer_group_id" id="filter_customer_group_id" multiple="multiple" size="1">
            <?php foreach ($customer_groups as $customer_group) { ?>
            <?php if (isset($filter_customer_group_id[$customer_group['customer_group_id']])) { ?>              
            <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>  
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp6_filter">
        <tr><td><?php echo $entry_customer_status; ?><br />
          <span <?php echo (!$filter_status) ? '' : 'class="vtip"' ?> title="<?php foreach ($statuses as $status) { ?><?php if (isset($filter_status[$status['status']]) && $status['status'] == 1) { ?><?php echo $text_enabled; ?><br /><?php } ?><?php if (isset($filter_status[$status['status']]) && $status['status'] == 0) { ?><?php echo $text_disabled; ?><br /><?php } ?><?php } ?>">         
          <select name="filter_status" id="filter_status" multiple="multiple" size="1">
            <?php foreach ($statuses as $status) { ?>
            <?php if (isset($filter_status[$status['status']]) && $status['status'] == 1) { ?>
            <option value="<?php echo $status['status']; ?>" selected="selected"><?php echo $text_enabled; ?></option>
            <?php } elseif (!isset($filter_status[$status['status']]) && $status['status'] == 1) { ?>
            <option value="<?php echo $status['status']; ?>"><?php echo $text_enabled; ?></option>
            <?php } ?>
            <?php if (isset($filter_status[$status['status']]) && $status['status'] == 0) { ?>
            <option value="<?php echo $status['status']; ?>" selected="selected"><?php echo $text_disabled; ?></option>
            <?php } elseif (!isset($filter_status[$status['status']]) && $status['status'] == 0) { ?>
            <option value="<?php echo $status['status']; ?>"><?php echo $text_disabled; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>   
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp7_filter">
        <tr><td> <?php echo $entry_company; ?><br />
        <input type="text" name="filter_company" value="<?php echo $filter_company; ?>" size="18" style="margin-top:4px; height:16px; border:solid 1px #BBB; color:#003A88;" onclick="this.value = '';">
		</td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>                   
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp8_filter">
        <tr><td> <?php echo $entry_customer; ?><br />
        <input type="text" name="filter_customer_id" value="<?php echo $filter_customer_id; ?>" size="18" style="margin-top:4px; height:16px; border:solid 1px #BBB; color:#003A88;" onclick="this.value = '';">
        </td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table> 
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp9_filter">
        <tr><td> <?php echo $entry_email; ?><br />
        <input type="text" name="filter_email" value="<?php echo $filter_email; ?>" size="18" style="margin-top:4px; height:16px; border:solid 1px #BBB; color:#003A88;" onclick="this.value = '';">
        </td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>   
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp10_filter">
        <tr><td> <?php echo $entry_ip; ?><br />
        <input type="text" name="filter_ip" value="<?php echo $filter_ip; ?>" size="18" style="margin-top:4px; height:16px; border:solid 1px #BBB; color:#003A88;" onclick="this.value = '';">
        </td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>           
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp11_filter">
        <tr><td> <?php echo $entry_product; ?><br />
        <input type="text" name="filter_product_id" value="<?php echo $filter_product_id; ?>" size="30" style="margin-top:4px; height:16px; border:solid 1px #BBB; color:#003A88;" onclick="this.value = '';">
        </td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>  
	  <table cellpadding="0" cellspacing="0" style="float:left;" id="cp12_filter">
        <tr><td><?php echo $entry_option; ?><br />
          <span <?php echo (!$filter_option) ? '' : 'class="vtip"' ?> title="<?php foreach ($product_options as $product_option) { ?><?php if (isset($filter_option[$product_option['options']])) { ?><?php echo $product_option['option_name']; ?>: <?php echo $product_option['option_value']; ?><br /><?php } ?><?php } ?>">        
          <select name="filter_option" id="filter_option" multiple="multiple" size="1">
            <?php foreach ($product_options as $product_option) { ?>
            <?php if (isset($filter_option[$product_option['options']])) { ?>              
            <option value="<?php echo $product_option['options']; ?>" selected="selected"><?php echo $product_option['option_name']; ?>: <?php echo $product_option['option_value']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $product_option['options']; ?>"><?php echo $product_option['option_name']; ?>: <?php echo $product_option['option_value']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table> 
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp13_filter">
        <tr><td><?php echo $entry_location; ?><br />
          <span <?php echo (!$filter_location) ? '' : 'class="vtip"' ?> title="<?php foreach ($locations as $location) { ?><?php if (isset($filter_location[$location['location_title']])) { ?><?php echo $location['location_name']; ?><br /><?php } ?><?php } ?>">
		  <select name="filter_location" id="filter_location" multiple="multiple" size="1">
            <?php foreach ($locations as $location) { ?>
            <?php if (isset($filter_location[$location['location_title']])) { ?>              
            <option value="<?php echo $location['location_title']; ?>" selected="selected"><?php echo $location['location_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $location['location_title']; ?>"><?php echo $location['location_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>
	  <table cellpadding="0" cellspacing="0" style="float:left;" id="cp14_filter">
        <tr><td><?php echo $entry_affiliate; ?><br />
          <span <?php echo (!$filter_affiliate) ? '' : 'class="vtip"' ?> title="<?php foreach ($affiliates as $affiliate) { ?><?php if (isset($filter_affiliate[$affiliate['affiliate_id']])) { ?><?php echo $affiliate['affiliate_name']; ?>: <?php echo $affiliate['option_value']; ?><br /><?php } ?><?php } ?>">        
          <select name="filter_affiliate" id="filter_affiliate" multiple="multiple" size="1">
            <?php foreach ($affiliates as $affiliate) { ?>
            <?php if (isset($filter_affiliate[$affiliate['affiliate_id']])) { ?>              
            <option value="<?php echo $affiliate['affiliate_id']; ?>" selected="selected"><?php echo $affiliate['affiliate_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $affiliate['affiliate_id']; ?>"><?php echo $affiliate['affiliate_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>                               
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp15_filter">
        <tr><td><?php echo $entry_shipping; ?><br />
          <span <?php echo (!$filter_shipping) ? '' : 'class="vtip"' ?> title="<?php foreach ($shippings as $shipping) { ?><?php if (isset($filter_shipping[$shipping['shipping_title']])) { ?><?php echo $shipping['shipping_name']; ?><br /><?php } ?><?php } ?>">
		  <select name="filter_shipping" id="filter_shipping" multiple="multiple" size="1">
            <?php foreach ($shippings as $shipping) { ?>
            <?php if (isset($filter_shipping[$shipping['shipping_title']])) { ?>              
            <option value="<?php echo $shipping['shipping_title']; ?>" selected="selected"><?php echo $shipping['shipping_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $shipping['shipping_title']; ?>"><?php echo $shipping['shipping_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table> 
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp16_filter">
        <tr><td><?php echo $entry_payment; ?><br />
          <span <?php echo (!$filter_payment) ? '' : 'class="vtip"' ?> title="<?php foreach ($payments as $payment) { ?><?php if (isset($filter_payment[$payment['payment_title']])) { ?><?php echo $payment['payment_name']; ?><br /><?php } ?><?php } ?>">
		  <select name="filter_payment" id="filter_payment" multiple="multiple" size="1">
            <?php foreach ($payments as $payment) { ?>
            <?php if (isset($filter_payment[$payment['payment_title']])) { ?>              
            <option value="<?php echo $payment['payment_title']; ?>" selected="selected"><?php echo $payment['payment_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $payment['payment_title']; ?>"><?php echo $payment['payment_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table> 
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp17_filter">
        <tr><td><?php echo $entry_zone; ?><br />
          <span <?php echo (!$filter_shipping_zone) ? '' : 'class="vtip"' ?> title="<?php foreach ($shipping_zones as $shipping_zone) { ?><?php if (isset($filter_shipping_zone[$shipping_zone['shipping_zone']])) { ?><?php echo $shipping_zone['zone_name']; ?><br /><?php } ?><?php } ?>">
		  <select name="filter_shipping_zone" id="filter_shipping_zone" multiple="multiple" size="1">
            <?php foreach ($shipping_zones as $shipping_zone) { ?>
            <?php if (isset($filter_shipping_zone[$shipping_zone['shipping_zone']])) { ?>              
            <option value="<?php echo $shipping_zone['shipping_zone']; ?>" selected="selected"><?php echo $shipping_zone['zone_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $shipping_zone['shipping_zone']; ?>"><?php echo $shipping_zone['zone_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>             
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp18_filter">
        <tr><td><?php echo $entry_shipping_country; ?><br />
          <span <?php echo (!$filter_shipping_country) ? '' : 'class="vtip"' ?> title="<?php foreach ($shipping_countries as $shipping_country) { ?><?php if (isset($filter_shipping_country[$shipping_country['shipping_country']])) { ?><?php echo $shipping_country['country_name']; ?><br /><?php } ?><?php } ?>">
		  <select name="filter_shipping_country" id="filter_shipping_country" multiple="multiple" size="1">
            <?php foreach ($shipping_countries as $shipping_country) { ?>
            <?php if (isset($filter_shipping_country[$shipping_country['shipping_country']])) { ?>              
            <option value="<?php echo $shipping_country['shipping_country']; ?>" selected="selected"><?php echo $shipping_country['country_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $shipping_country['shipping_country']; ?>"><?php echo $shipping_country['country_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>                                       
      <table cellpadding="0" cellspacing="0" style="float:left;" id="cp19_filter">
        <tr><td><?php echo $entry_payment_country; ?><br />
          <span <?php echo (!$filter_payment_country) ? '' : 'class="vtip"' ?> title="<?php foreach ($payment_countries as $payment_country) { ?><?php if (isset($filter_payment_country[$payment_country['payment_country']])) { ?><?php echo $payment_country['country_name']; ?><br /><?php } ?><?php } ?>">
		  <select name="filter_payment_country" id="filter_payment_country" multiple="multiple" size="1">
            <?php foreach ($payment_countries as $payment_country) { ?>
            <?php if (isset($filter_payment_country[$payment_country['payment_country']])) { ?>              
            <option value="<?php echo $payment_country['payment_country']; ?>" selected="selected"><?php echo $payment_country['country_name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $payment_country['payment_country']; ?>"><?php echo $payment_country['country_name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></span></td><td width="20"></td></tr>
        <tr><td>&nbsp;</td><td></td>
          </tr></table>             
	   </td>
	  </tr>
	 </table>
	</td>
	</tr>
	</table>      
    </div>
<script type="text/javascript">$(function(){ 
$('#show_tab_export').click(function() {
		$('#tab_export').slideToggle('fast');
	});
});
</script>    
  <div id="tab_export" style="background:#E7EFEF; border:1px solid #C6D7D7; padding:3px; margin-bottom:15px; display:none">
      <table width="100%" cellspacing="0" cellpadding="3">
        <tr>
          <td width="10%">&nbsp;</td>
          <td width="20%" align="center" nowrap="nowrap"><span id="export_xls" class="export_item"><img src="view/image/XLS.png" width="48" height="48" border="0" title="XLS" /></span><span id="export_html" class="export_item"><img src="view/image/HTML.png" width="48" height="48" border="0" title="HTML" /></span><span id="export_pdf" class="export_item"><img src="view/image/PDF.png" width="48" height="48" border="0" title="PDF" /></span></td>
          <td width="20%" align="center" nowrap="nowrap"><span id="export_xls_order_list" class="export_item"><img src="view/image/XLS.png" width="48" height="48" border="0" title="XLS" /></span><span id="export_html_order_list" class="export_item"><img src="view/image/HTML.png" width="48" height="48" border="0" title="HTML" /></span><span id="export_pdf_order_list" class="export_item"><img src="view/image/PDF.png" width="48" height="48" border="0" title="PDF" /></span></td>
          <td width="20%" align="center" nowrap="nowrap"><span id="export_xls_product_list" class="export_item"><img src="view/image/XLS.png" width="48" height="48" border="0" title="XLS" /></span><span id="export_html_product_list" class="export_item"><img src="view/image/HTML.png" width="48" height="48" border="0" title="HTML" /></span><span id="export_pdf_product_list" class="export_item"><img src="view/image/PDF.png" width="48" height="48" border="0" title="PDF" /></span></td>  
          <td width="20%" align="center" nowrap="nowrap"><span id="export_xls_all_details" class="export_item"><img src="view/image/XLS.png" width="48" height="48" border="0" title="XLS" /></span><span id="export_html_all_details" class="export_item"><img src="view/image/HTML.png" width="48" height="48" border="0" title="HTML" /></span><span id="export_pdf_all_details" class="export_item"><img src="view/image/PDF.png" width="48" height="48" border="0" title="PDF" /></span></td>                            
          <td width="10%">&nbsp;</td>                                                                                                                       
        </tr>
        <tr>
          <td width="10%">&nbsp;</td>
          <td width="20%" align="center" nowrap="nowrap"><?php echo $text_export_no_details; ?></td>
          <td width="20%" align="center" nowrap="nowrap"><?php echo $text_export_order_list; ?></td>
          <td width="20%" align="center" nowrap="nowrap"><?php echo $text_export_product_list; ?></td>   
          <td width="20%" align="center" nowrap="nowrap"><?php echo $text_export_all_details; ?></td>                            
          <td width="10%">&nbsp;</td>                                                                                                                       
        </tr>        
      </table> 
      <input type="hidden" id="export" name="export" value="" />
    </div> 
<?php if ($customers) { ?>
<?php if (($filter_range != 'all_time' && ($filter_group == 'year' or $filter_group == 'quarter' or $filter_group == 'month')) or ($filter_range == 'all_time' && $filter_group == 'year')) { ?>      
<script type="text/javascript">$(function(){ 
$('#show_tab_chart').click(function() {
		$('#tab_chart').slideToggle('slow');
	});
});
</script>  
    <div id="tab_chart" style="display:none">
      <table align="center" cellspacing="0" cellpadding="3">
        <tr>
          <td><div style="float:left;" id="chart1_div"></div><div style="float:left;" id="chart2_div"></div></td>
        </tr>
      </table>
    </div>
<?php } ?> 
<?php } ?>     
    <div id="pagination_content">     
      <table class="list_main">
        <thead>
          <tr>
		  <?php if ($filter_group == 'year') { ?>           
          <td class="left" colspan="2" nowrap="nowrap"><?php echo $column_year; ?></td>
		  <?php } elseif ($filter_group == 'quarter') { ?> 
          <td class="left" nowrap="nowrap"><?php echo $column_year; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $column_quarter; ?></td>       
		  <?php } elseif ($filter_group == 'month') { ?> 
          <td class="left" nowrap="nowrap"><?php echo $column_year; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $column_month; ?></td> 
		  <?php } else { ?>    
          <td class="left" width="70" nowrap="nowrap"><?php echo $column_date_start; ?></td>
          <td class="left" width="70" nowrap="nowrap"><?php echo $column_date_end; ?></td>           
		  <?php } ?> 
          <td id="cp20_title" class="right" nowrap="nowrap"><?php echo $column_id; ?></td>
          <td id="cp21_title" class="left"><?php echo $column_customer; ?></td>         
          <td id="cp22_title" class="left" nowrap="nowrap"><?php echo $column_email; ?></td>
          <td id="cp23_title" class="left" nowrap="nowrap"><?php echo $column_customer_group; ?></td> 
          <td id="cp24_title" class="left" nowrap="nowrap"><?php echo $column_status; ?></td>  
          <td id="cp25_title" class="left" nowrap="nowrap"><?php echo $column_ip; ?></td>           
          <td id="cp26_title" class="left" nowrap="nowrap"><?php echo $column_mostrecent; ?></td>                           
          <td id="cp27_title" class="right" nowrap="nowrap"><?php echo $column_orders; ?></td> 
          <td id="cp28_title" class="right" nowrap="nowrap"><?php echo $column_products; ?></td> 
          <td id="cp29_title" class="right" nowrap="nowrap"><?php echo $column_sales; ?></td>                          
          <td id="cp30_title" class="right" nowrap="nowrap"><?php echo $column_value; ?></td>
          <td id="cp31_title" class="right" nowrap="nowrap"><?php echo $column_costs; ?></td>            
          <td id="cp32_title" class="right" nowrap="nowrap"><?php echo $column_net_profit; ?></td>
          <td id="cp33_title" class="right" nowrap="nowrap"><?php echo $column_profit_margin; ?></td>          
          <?php if ($filter_details == 1 OR $filter_details == 2) { ?><td class="right" nowrap="nowrap"><?php echo $column_action; ?></td><?php } ?>
          </tr>
      	  </thead>
          <?php if ($customers) { ?>
          <?php foreach ($customers as $customer) { ?>
      	  <tbody class="element">
          <tr>
		  <?php if ($filter_group == 'year') { ?>           
          <td class="left" colspan="2" nowrap="nowrap"><?php echo $customer['year']; ?></td>
		  <?php } elseif ($filter_group == 'quarter') { ?> 
          <td class="left" nowrap="nowrap"><?php echo $customer['year']; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $customer['quarter']; ?></td>  
		  <?php } elseif ($filter_group == 'month') { ?> 
          <td class="left" nowrap="nowrap"><?php echo $customer['year']; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $customer['month']; ?></td>
		  <?php } else { ?>    
          <td class="left" nowrap="nowrap"><?php echo $customer['date_start']; ?></td>
          <td class="left" nowrap="nowrap"><?php echo $customer['date_end']; ?></td>         
		  <?php } ?>  
	  	  <td id="cp20_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap">
          <?php if ($customer['customer_id'] == 0) { ?>
          <?php echo $text_guest; ?>          
          <?php } else { ?>
          <?php echo $customer['customer_id']; ?>
          <?php } ?></td>           
	  	  <td id="cp21_<?php echo $customer['order_id']; ?>" class="left">
          <?php if ($customer['customer_id'] == 0) { ?>
          <?php echo $customer['cust_name']; ?>          
          <?php } else { ?>
          <a href="<?php echo $customer['href']; ?>"><?php echo $customer['cust_name']; ?></a>
          <?php } ?></td>
      	  <td id="cp22_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['cust_email']; ?></td> 
	  	  <td id="cp23_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap">
          <?php if ($customer['customer_id'] == 0) { ?>
          <?php echo $customer['cust_group_guest']; ?>         
          <?php } else { ?>
          <?php echo $customer['cust_group_reg']; ?>
          <?php } ?></td>                                
          <td id="cp24_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php if (!$customer['customer_id'] == 0) { ?><?php echo $customer['cust_status']; ?><?php } ?></td>
      	  <td id="cp25_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['cust_ip']; ?></td>            
          <td id="cp26_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['mostrecent']; ?></td>          
          <td id="cp27_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap"><?php echo $customer['orders']; ?></td>
          <td id="cp28_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap"><?php echo $customer['products']; ?></td>  
          <td id="cp29_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#DCFFB9;"><?php echo $customer['sales']; ?></td>                  
          <td id="cp30_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap"><?php echo $customer['value']; ?></td>
          <td id="cp31_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#ffd7d7;"><?php echo $customer['costs']; ?></td>          
          <td id="cp32_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#c4d9ee; font-weight:bold;"><?php echo $customer['netprofit']; ?></td>
          <td id="cp33_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#c4d9ee; font-weight:bold;"><?php echo $customer['profit_margin_percent']; ?></td>          
          <?php if ($filter_details == 1 OR $filter_details == 2) { ?><td class="right" nowrap="nowrap">[ <a id="show_details_<?php echo $customer['order_id']; ?>"><?php echo $text_detail; ?></a> ]</td><?php } ?>
          </tr>
<tr class="detail">         
<td colspan="17" class="center">
<?php if ($filter_details == 1) { ?> 
<script type="text/javascript">$(function(){ 
$('#show_details_<?php echo $customer['order_id']; ?>').click(function() {
		$('#tab_details_<?php echo $customer['order_id']; ?>').slideToggle('slow');
	});
});
</script>
<div id="tab_details_<?php echo $customer['order_id']; ?>" style="display:none">
    <table class="list_detail">
      <thead>
        <tr>
          <td id="cp40_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_order_id; ?></td>        
          <td id="cp41_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_date_added; ?></td>
          <td id="cp42_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_inv_no; ?></td>                  
          <td id="cp43_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_customer; ?></td>
          <td id="cp44_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_email; ?></td>
          <td id="cp45_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_customer_group; ?></td>
          <td id="cp46_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_shipping_method; ?></td>
          <td id="cp47_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_payment_method; ?></td>          
          <td id="cp48_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_status; ?></td>
          <td id="cp49_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_order_store; ?></td>
          <td id="cp50_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_currency; ?></td>
          <td id="cp51_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_quantity; ?></td>  
          <td id="cp52_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_sub_total; ?></td>  
          <td id="cp531_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_hf; ?></td>  
          <td id="cp532_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_lof; ?></td>                   
          <td id="cp54_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_shipping; ?></td>         
          <td id="cp55_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_tax; ?></td>
          <td id="cp56_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_value; ?></td>          
          <td id="cp57_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_costs; ?></td> 
          <td id="cp58_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_order_profit; ?></td>
          <td id="cp59_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_profit_margin; ?></td>             
        </tr>
      </thead>
        <tr bgcolor="#FFFFFF">
          <td id="cp40_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><a><?php echo $customer['order_ord_id']; ?></a></td>        
          <td id="cp41_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['order_order_date']; ?></td>
          <td id="cp42_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['order_inv_no']; ?></td>
          <td id="cp43_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['order_name']; ?></td>
          <td id="cp44_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['order_email']; ?></td>
          <td id="cp45_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['order_group']; ?></td>
          <td id="cp46_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['order_shipping_method']; ?></td>
          <td id="cp47_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['order_payment_method']; ?></td>          
          <td id="cp48_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['order_status']; ?></td>
          <td id="cp49_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['order_store']; ?></td> 
          <td id="cp50_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap"><?php echo $customer['order_currency']; ?></td>          
          <td id="cp51_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap"><?php echo $customer['order_products']; ?></td> 
          <td id="cp52_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#DCFFB9;"><?php echo $customer['order_sub_total']; ?></td> 
          <td id="cp531_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#DCFFB9;"><?php echo $customer['order_hf']; ?></td> 
          <td id="cp532_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#DCFFB9;"><?php echo $customer['order_lof']; ?></td>          
          <td id="cp54_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap"><?php echo $customer['order_shipping']; ?></td>           
          <td id="cp55_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap"><?php echo $customer['order_tax']; ?></td>                              
          <td id="cp56_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap"><?php echo $customer['order_value']; ?></td>
          <td id="cp57_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#ffd7d7;">-<?php echo $customer['order_costs']; ?></td>
          <td id="cp58_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#c4d9ee; font-weight:bold;"><?php echo $customer['order_profit']; ?></td> 
          <td id="cp59_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#c4d9ee; font-weight:bold;"><?php echo $customer['order_profit_margin_percent']; ?>%</td>      
         </tr>
    </table> 
</div>  
<?php } ?> 
<?php if ($filter_details == 2) { ?>
<script type="text/javascript">$(function(){ 
$('#show_details_<?php echo $customer['order_id']; ?>').click(function() {
		$('#tab_details_<?php echo $customer['order_id']; ?>').slideToggle('slow');
	});
});
</script>
<div id="tab_details_<?php echo $customer['order_id']; ?>" style="display:none">
    <table class="list_detail">
      <thead>
        <tr>
          <td id="cp60_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_order_id; ?></td>  
          <td id="cp61_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_date_added; ?></td>
          <td id="cp62_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_inv_no; ?></td> 
          <td id="cp63_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_id; ?></td>                                          
          <td id="cp64_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_sku; ?></td>          
          <td id="cp65_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_name; ?></td> 
          <td id="cp66_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_option; ?></td>           
          <td id="cp67_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_model; ?></td>            
          <td id="cp68_<?php echo $customer['order_id']; ?>_title" class="left" nowrap="nowrap"><?php echo $column_prod_manu; ?></td> 
          <td id="cp69_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_currency; ?></td>   
          <td id="cp70_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_price; ?></td>                     
          <td id="cp71_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_quantity; ?></td>                  
          <td id="cp72_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_total; ?></td>   
          <td id="cp73_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_tax; ?></td> 
          <td id="cp74_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_costs; ?></td> 
          <td id="cp75_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_prod_profit; ?></td>
          <td id="cp76_<?php echo $customer['order_id']; ?>_title" class="right" nowrap="nowrap"><?php echo $column_profit_margin; ?></td>                                                                    
        </tr>
      </thead>
        <tr bgcolor="#FFFFFF">
          <td id="cp60_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><a><?php echo $customer['product_ord_id']; ?></a></td>  
          <td id="cp61_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['product_order_date']; ?></td>
          <td id="cp62_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['product_inv_no']; ?></td>
          <td id="cp63_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['product_pid']; ?></td>  
          <td id="cp64_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['product_sku']; ?></td>                    
          <td id="cp65_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['product_name']; ?></td> 
          <td id="cp66_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['product_option']; ?></td>           
          <td id="cp67_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['product_model']; ?></td>           
          <td id="cp68_<?php echo $customer['order_id']; ?>" class="left" nowrap="nowrap"><?php echo $customer['product_manu']; ?></td> 
          <td id="cp69_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap"><?php echo $customer['product_currency']; ?></td> 
          <td id="cp70_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap"><?php echo $customer['product_price']; ?></td> 
          <td id="cp71_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap"><?php echo $customer['product_quantity']; ?></td> 
          <td id="cp72_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#DCFFB9;"><?php echo $customer['product_total']; ?></td>    
          <td id="cp73_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap"><?php echo $customer['product_tax']; ?></td>  
          <td id="cp74_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#ffd7d7;">-<?php echo $customer['product_costs']; ?></td>
          <td id="cp75_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#c4d9ee; font-weight:bold;"><?php echo $customer['product_profit']; ?></td>
          <td id="cp76_<?php echo $customer['order_id']; ?>" class="right" nowrap="nowrap" style="background-color:#c4d9ee; font-weight:bold;"><?php echo $customer['product_profit_margin_percent']; ?>%</td>
         </tr>       
    </table>
</div> 
<?php } ?>   
</td>
</tr>           
          <?php } ?>
        <tr>
          <td colspan="17"></td>
        </tr>
      </tbody>          
        <tr>
          <td colspan="2" class="right" style="background-color:#E7EFEF;"><strong><?php echo $text_filter_total; ?></strong></td>        
          <td id="cp20_total" style="background-color:#DDDDDD;"></td>
          <td id="cp21_total" style="background-color:#DDDDDD;"></td>
          <td id="cp22_total" style="background-color:#DDDDDD;"></td>
          <td id="cp23_total" style="background-color:#DDDDDD;"></td>
          <td id="cp24_total" style="background-color:#DDDDDD;"></td>
          <td id="cp25_total" style="background-color:#DDDDDD;"></td>
          <td id="cp26_total" style="background-color:#DDDDDD;"></td>        
          <td id="cp27_total" class="right" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $customer['orders_total']; ?></strong></td>  
          <td id="cp28_total" class="right" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $customer['products_total']; ?></strong></td> 
          <td id="cp29_total" class="right" style="background-color:#DCFFB9; color:#003A88;"><strong><?php echo $customer['sales_total']; ?></strong></td> 
          <td id="cp30_total" class="right" style="background-color:#E7EFEF; color:#003A88;"><strong><?php echo $customer['value_total']; ?></strong></td>           
          <td id="cp31_total" class="right" style="background-color:#ffd7d7; color:#003A88;"><strong><?php echo $customer['costs_total']; ?></strong></td>           
          <td id="cp32_total" class="right" style="background-color:#c4d9ee; color:#003A88;"><strong><?php echo $customer['netprofit_total']; ?></strong></td>
          <td id="cp33_total" class="right" style="background-color:#c4d9ee; color:#003A88;"><strong><?php echo $customer['profit_margin_total_percent']; ?></strong></td>          
          <?php if ($filter_details == 1 OR $filter_details == 2) { ?><td style="background-color:#E7EFEF;"></td><?php } ?>                  
        </tr>        
          <?php } else { ?>
          <tr>
            <td class="noresult" colspan="17"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
      <?php if ($customers) { ?>    
      <div class="pagination_report"></div>
      <?php } ?>        
    </div>
  </div>
</div>  
</form>  
<script type="text/javascript" src="view/javascript/jquery/jquery.multiSelect.js"></script>
<script type="text/javascript" src="view/javascript/jquery/jquery.paging.min.js"></script>
<script type="text/javascript" src="view/javascript/jquery/vtip.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});

	$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});
	
    $('#filter_order_status_id').multiSelect({
      selectAllText:'<?php echo $text_all_status; ?>', noneSelected:'<?php echo $text_all_status; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#filter_store_id').multiSelect({
      selectAllText:'<?php echo $text_all_stores; ?>', noneSelected:'<?php echo $text_all_stores; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#filter_currency').multiSelect({
      selectAllText:'<?php echo $text_all_currencies; ?>', noneSelected:'<?php echo $text_all_currencies; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });

    $('#filter_taxes').multiSelect({
      selectAllText:'<?php echo $text_all_taxes; ?>', noneSelected:'<?php echo $text_all_taxes; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });

    $('#filter_customer_group_id').multiSelect({
      selectAllText:'<?php echo $text_all_groups; ?>', noneSelected:'<?php echo $text_all_groups; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });

    $('#filter_status').multiSelect({
      selectAllText:'<?php echo $text_all_status; ?>', noneSelected:'<?php echo $text_all_status; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });	

    $('#filter_option').multiSelect({
      selectAllText:'<?php echo $text_all_options; ?>', noneSelected:'<?php echo $text_all_options; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });

    $('#filter_location').multiSelect({
      selectAllText:'<?php echo $text_all_locations; ?>', noneSelected:'<?php echo $text_all_locations; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });

    $('#filter_affiliate').multiSelect({
      selectAllText:'<?php echo $text_all_affiliates; ?>', noneSelected:'<?php echo $text_all_affiliates; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#filter_shipping').multiSelect({
      selectAllText:'<?php echo $text_all_shippings; ?>', noneSelected:'<?php echo $text_all_shippings; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });

    $('#filter_payment').multiSelect({
      selectAllText:'<?php echo $text_all_payments; ?>', noneSelected:'<?php echo $text_all_payments; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });

    $('#filter_shipping_zone').multiSelect({
      selectAllText:'<?php echo $text_all_zones; ?>', noneSelected:'<?php echo $text_all_zones; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#filter_shipping_country').multiSelect({
      selectAllText:'<?php echo $text_all_countries; ?>', noneSelected:'<?php echo $text_all_countries; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#filter_payment_country').multiSelect({
      selectAllText:'<?php echo $text_all_countries; ?>', noneSelected:'<?php echo $text_all_countries; ?>', oneOrMoreSelected:'<?php echo $text_selected; ?>'
      });
	
    $('#button').click(function() {
      $('#report').submit() ;
      return(false)
    });	
	
    $('#export_xls').click(function() {
      $('#export').val('1') ; // export_xls: #1
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_xls_order_list').click(function() {
      $('#export').val('2') ; // export_xls_order_list: #2
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	

    $('#export_xls_product_list').click(function() {
      $('#export').val('3') ; // export_xls_product_list: #3
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	

    $('#export_xls_all_details').click(function() {
      $('#export').val('5') ; // export_xls_all_details: #5
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_html').click(function() {
      $('#export').val('6') ; // export_html: #6
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_html_order_list').click(function() {
      $('#export').val('7') ; // export_html_order_list: #7
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_html_product_list').click(function() {
      $('#export').val('8') ; // export_html_product_list: #8
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });		
		
    $('#export_html_all_details').click(function() {
      $('#export').val('10') ; // export_html_all_details: #10
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_pdf').click(function() {
      $('#export').val('11') ; // export_pdf: #11
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_pdf_order_list').click(function() {
      $('#export').val('12') ; // export_pdf_order_list: #12
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
	
    $('#export_pdf_product_list').click(function() {
      $('#export').val('13') ; // export_pdf_product_list: #13
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });		
		
    $('#export_pdf_all_details').click(function() {
      $('#export').val('15') ; // export_pdf_all_details: #15
      $('#report').attr('target', '_blank'); // opening file in a new window
      $('#report').submit() ;
      $('#report').attr('target', '_self'); // preserve current form      
      $('#export').val('') ; 
      return(false)
    });	
});
</script>  
<script type="text/javascript"><!--
$('input[name=\'filter_company\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_customer_profit/customer_autocomplete&token=<?php echo $token; ?>&filter_company=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.cust_company,
						value: item.customer_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_company\']').val(ui.item.label);
						
		return false;
	}
});

$('input[name=\'filter_customer_id\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_customer_profit/customer_autocomplete&token=<?php echo $token; ?>&filter_customer_id=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.cust_name,
						value: item.customer_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_customer_id\']').val(ui.item.label);
						
		return false;
	}
});

$('input[name=\'filter_email\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_customer_profit/customer_autocomplete&token=<?php echo $token; ?>&filter_email=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.cust_email,
						value: item.customer_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_email\']').val(ui.item.label);
						
		return false;
	}
});

$('input[name=\'filter_ip\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_customer_order/ip_autocomplete&token=<?php echo $token; ?>&filter_ip=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.cust_ip,
						value: item.customer_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_ip\']').val(ui.item.label);
						
		return false;
	}
});

$('input[name=\'filter_product_id\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/adv_customer_profit/product_autocomplete&token=<?php echo $token; ?>&filter_product_id=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.prod_name,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_product_id\']').val(ui.item.label);
						
		return false;
	}
});
//--></script> 
<?php if ($customers) { ?>    
<?php if (($filter_range != 'all_time' && ($filter_group == 'year' or $filter_group == 'quarter' or $filter_group == 'month')) or ($filter_range == 'all_time' && $filter_group == 'year')) { ?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript"><!--
	google.load('visualization', '1', {packages: ['corechart']});
      google.setOnLoadCallback(drawChart);      
	  function drawChart() {        
	  	var data = google.visualization.arrayToDataTable([
			<?php if ($sales && $filter_group == 'month') {
				echo "['" . $column_month . "','". $column_orders . "','" . $column_customers . "','" . $column_products . "'],";
					foreach ($sales as $key => $sale) {
						if (count($sales)==($key+1)) {
							echo "['" . $sale['gyear_month'] . "',". $sale['gorders'] . ",". $sale['gcustomers'] . ",". $sale['gproducts'] . "]";
						} else {
							echo "['" . $sale['gyear_month'] . "',". $sale['gorders'] . ",". $sale['gcustomers'] . ",". $sale['gproducts'] . "],";
						}
					}	
			} elseif ($sales && $filter_group == 'quarter') {
				echo "['" . $column_quarter . "','". $column_orders . "','" . $column_customers . "','" . $column_products . "'],";
					foreach ($sales as $key => $sale) {
						if (count($sales)==($key+1)) {
							echo "['" . $sale['gyear_quarter'] . "',". $sale['gorders'] . ",". $sale['gcustomers'] . ",". $sale['gproducts'] . "]";
						} else {
							echo "['" . $sale['gyear_quarter'] . "',". $sale['gorders'] . ",". $sale['gcustomers'] . ",". $sale['gproducts'] . "],";
						}
					}	
			} elseif ($sales && $filter_group == 'year') {
				echo "['" . $column_year . "','". $column_orders . "','" . $column_customers . "','" . $column_products . "'],";
					foreach ($sales as $key => $sale) {
						if (count($sales)==($key+1)) {
							echo "['" . $sale['gyear'] . "',". $sale['gorders'] . ",". $sale['gcustomers'] . ",". $sale['gproducts'] . "]";
						} else {
							echo "['" . $sale['gyear'] . "',". $sale['gorders'] . ",". $sale['gcustomers'] . ",". $sale['gproducts'] . "],";
						}
					}	
			} 
			;?>
		]);

        var options = {
			width: 630,	
			height: 266,  
			colors: ['#edc240', '#9dc7e8', '#CCCCCC'],
			chartArea: {left: 30, top: 30, width: "75%", height: "70%"},
			pointSize: '4',
			legend: {position: 'right', alignment: 'start', textStyle: {color: '#666666', fontSize: 12}}
		};

			var chart = new google.visualization.LineChart(document.getElementById('chart1_div'));
			chart.draw(data, options);
	}
//--></script>
<script type="text/javascript"><!--
	google.load('visualization', '1', {packages: ['corechart']});
	function drawVisualization() {
   		var data = google.visualization.arrayToDataTable([
			<?php if ($sales && $filter_group == 'month') {
				echo "['" . $column_month . "','". $column_sales . "','" . $column_total_costs . "','" . $column_total_profit . "'],";
					foreach ($sales as $key => $sale) {
						if (count($sales)==($key+1)) {
							echo "['" . $sale['gyear_month'] . "',". $sale['gsales'] . ",". $sale['gcosts'] . ",". $sale['gnetprofit'] . "]";
						} else {
							echo "['" . $sale['gyear_month'] . "',". $sale['gsales'] . ",". $sale['gcosts'] . ",". $sale['gnetprofit'] . "],";
						}
					}	
			} elseif ($sales && $filter_group == 'quarter') {
				echo "['" . $column_quarter . "','". $column_sales . "','" . $column_total_costs . "','" . $column_total_profit . "'],";
					foreach ($sales as $key => $sale) {
						if (count($sales)==($key+1)) {
							echo "['" . $sale['gyear_quarter'] . "',". $sale['gsales'] . ",". $sale['gcosts'] . ",". $sale['gnetprofit'] . "]";
						} else {
							echo "['" . $sale['gyear_quarter'] . "',". $sale['gsales'] . ",". $sale['gcosts'] . ",". $sale['gnetprofit'] . "],";
						}
					}	
			} elseif ($sales && $filter_group == 'year') {
				echo "['" . $column_year . "','". $column_sales . "','" . $column_total_costs . "','" . $column_total_profit . "'],";
					foreach ($sales as $key => $sale) {
						if (count($sales)==($key+1)) {
							echo "['" . $sale['gyear'] . "',". $sale['gsales'] . ",". $sale['gcosts'] . ",". $sale['gnetprofit'] . "]";
						} else {
							echo "['" . $sale['gyear'] . "',". $sale['gsales'] . ",". $sale['gcosts'] . ",". $sale['gnetprofit'] . "],";
						}
					}	
			} 
			;?>
		]);

        var options = {
			width: 630,	
			height: 266,  
			colors: ['#b5e08b', '#ed9999', '#739cc3'],
			chartArea: {left: 45, top: 30, width: "75%", height: "70%"},
			legend: {position: 'right', alignment: 'start', textStyle: {color: '#666666', fontSize: 12}},				
			seriesType: "bars",
			series: {2: {type: "line", lineWidth: '3', pointSize: '5'}}
		};

			var chart = new google.visualization.ComboChart(document.getElementById('chart2_div'));
			chart.draw(data, options);
	}
	
	google.setOnLoadCallback(drawVisualization);
//--></script>
<?php } ?>
<?php } ?>
<?php echo $footer; ?>