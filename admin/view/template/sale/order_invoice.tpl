<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $language; ?>" xml:lang="<?php echo $language; ?>">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<?php foreach ($orders as $order) { ?>
<div style="page-break-after: always;">
  <h1><?php echo $text_invoice; ?></h1>
  <table class="store">
    <tr>
      <td><?php echo $order['store_name']; ?><br />
        <?php //echo $order['store_address']; ?>
        <?php echo $text_telephone; ?> <?php echo $order['store_telephone']; ?><br />
        <?php if ($order['store_fax']) { ?>
        <?php echo $text_fax; ?> <?php echo $order['store_fax']; ?><br />
        <?php } ?>
        <?php echo $order['store_email']; ?><br />
        <?php echo $order['store_url']; ?></td>
      <td align="right" valign="top"><table>
          
          <?php if ($order['invoice_no']) { ?>
          <tr>
            <td><b><?php echo $text_invoice_no; ?></b></td>
            <td><?php echo $order['invoice_no']; ?></td>
          </tr>
          <?php } ?>
          <tr>
            <td><b><?php echo $text_date_added; ?></b></td>
            <td><?php echo $order['date_added']; ?></td>
          </tr>
          <tr>
            <td><b><?php echo $text_order_id; ?></b></td>
            <td><?php echo $order['order_id']; ?></td>
          </tr>
          <tr>
            <td><b><?php echo $text_payment_method; ?></b></td>
            <td><?php echo $order['payment_method']; ?></td>
          </tr>
          <?php if ($order['shipping_method']) { ?>
          <tr>
            <td><b><?php echo $text_shipping_method; ?></b></td>
            <td><?php echo $order['shipping_method']; ?></td>
          </tr>
          <?php } ?>
        </table></td>
    </tr>
  </table>
  <table class="address">
    <tr class="heading">
      <td width="50%"><b><?php echo $text_to; ?></b></td>
      <td width="50%"><b><?php echo $text_ship_to; ?></b></td>
    </tr>
    <tr>
      <td><?php echo $order['payment_address']; ?><br/>
        <?php echo $order['email']; ?><br/>
        <?php echo $order['telephone']; ?>
        <?php if ($order['payment_company_id']) { ?>
        <br/>
        <br/>
        <?php echo $text_company_id; ?> <?php echo $order['payment_company_id']; ?>
        <?php } ?>
        <?php if ($order['payment_tax_id']) { ?>
        <br/>
        <?php echo $text_tax_id; ?> <?php echo $order['payment_tax_id']; ?>
        <?php } ?></td>
      <td><?php echo $order['shipping_address']; ?></td>
    </tr>
  </table>
  <table class="product">
    <tr class="heading">
      <td><b><?php echo $column_product; ?></b></td>
      <!--<td colspan="2"><b><?php //echo $column_model; ?></b></td>-->
      <td align="right"><b><?php echo $column_quantity; ?></b></td>
      <td align="right"><b><?php echo $column_price; ?></b></td>
      <td align="right" colspan="2"><b><?php echo $column_total; ?></b></td>
    </tr>
    <?php $fca=0; ?>
    <?php foreach ($order['product'] as $product) { ?>
    <tr>
      <td><?php echo $product['name']; ?>
        <?php foreach ($product['option'] as $option) { ?>
        <br />
        &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
        <?php } ?></td>
      <!--<td colspan="2"><?php //echo $product['model']; ?></td>-->
      <td align="right"><?php echo $product['quantity']; ?></td>
      <td align="right"><?php echo $product['price']; ?></td>
      <td align="right" colspan="2"><?php echo $product['total']; ?></td>
    </tr>
    <?php } ?>
    <?php foreach ($order['voucher'] as $voucher) { ?>
    <tr>
      <td align="left"><?php echo $voucher['description']; ?></td>
      <td align="left"></td>
      <td align="right">1</td>
      <td align="right"><?php echo $voucher['amount']; ?></td>
      <td align="right"><?php echo $voucher['amount']; ?></td>
    </tr>
    <?php } ?>
    <?php foreach ($order['total'] as $total) { ?>
    <tr>
      <td align="right" colspan="4"><b><?php echo $total['title']; ?>:</b></td>
      <td align="right"><?php echo $total['text']; ?></td>
    </tr>
    <?php    
            if(strpos($total['title'],'Reward') !== false){
            $fca+=str_replace("Rs ","",$total['text']);
        }
        
        if(strpos($total['title'],'Coupon') !== false){
            $fca+=str_replace("Rs ","",$total['text']);
        } 
        
        ?>
    <?php } ?>
    <!--<tr>
      <td align="right" colspan="6"><b>Max Food Coupons Allowed:</b></td>
      <td align="right">Rs <?php echo ($fca < 0 ? 0 : $fca); ?></td>
    </tr>-->
  </table>
  
  <table width="100%" class="comment product">
    <tr class="heading">
      <!--<td><b><?php if ($order['comment']) { ?><?php echo $column_comment; ?></b> <?php } ?></td>-->
      <td width="25%"><b>Delivery Date</b></td>
      <td width="25%"><b>Time Slot</b></td>
    </tr>
    <tr>
      <!--<td><?php //if($order['comment']) { ?><?php //echo $order['comment']; ?> <?php //} ?></td>-->
      <td><?php if($order['datepicker']!='0000-00-00'  && $order['datepicker']!='1970-01-01') { echo date('d/m/Y',strtotime($order['datepicker'])); } ?></td>
      <td><?php echo str_replace('<br />','',nl2br($order['timeslot'])); ?></td>
    </tr>
  </table>
 
  <!--<table width="50%" class="product">
    <tr class="heading">
      <td><b>Tax Category</b></td>
      <td><b>A</b></td>
      <td align="right"><b>B</b></td>
      <td align="right"><b>C</b></td>
      <td align="right"><b>D</b></td>
      <td align="right"><b>E</b></td>
    </tr>
     <tr>
      <td>Tax %</td>
      <td>0%</td>
      <td align="right">17.0%</td>
      <td align="right">5.5%</td>
      <td align="right">14.5%</td>
      <td align="right">2%</td>
    </tr>
 </table>-->
  <hr>
  <table width="100%">
  <tr><td align="left" valign="top">Email us at cutomercare@aapkamarket.com or  call at +91-9373366660 in case of any query.
<!--<br>You will earn <?php echo floor(str_replace("Rs ", "",$total['text'])/100)*10; ?> points  for this order on delivery
</td>--><td align="right" valign="top"></td></tr>
</table>
<a href="javascript:window.print();"><img src="http://www.ayc.ltd.uk/Professionals/professional/professional/images/INVOICE_icon.gif" id="printbtn"></a>
<style type="text/css">
@media print {
    #printbtn {
        display :  none;
    }
}
</style>
</div>
<?php } ?>
</body>
</html>