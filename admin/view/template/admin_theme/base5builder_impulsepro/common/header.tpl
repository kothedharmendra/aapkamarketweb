<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>
	<meta charset="utf-8" />
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?>
	<meta name="description" content="<?php echo $description; ?>" />
	<?php } ?>
	<?php if ($keywords) { ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
	<?php } ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>

	<!-- Le styles -->
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/bootstrap.css" rel="stylesheet" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style.css" rel="stylesheet" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/bootstrap-responsive.css" rel="stylesheet" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-responsive.css" rel="stylesheet" />
    <!--<link rel="stylesheet" href="view/stylesheet/admin_theme/base5builder_impulsepro/bootstrap.min.css">-->
    <!--<link rel="stylesheet" href="view/stylesheet/admin_theme/base5builder_impulsepro/theme_light.css" class="style_set">-->
	<link type="text/css" href="http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet" />
	<link type="text/css" href="view/javascript/admin_theme/base5builder_impulsepro/ui/themes/custom-theme/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />
	  <!--[if IE 7]>
	  <link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-ie7.css" rel="stylesheet">
	  <![endif]-->
	  <!--[if IE 8]>
	  <link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-ie8.css" rel="stylesheet">
	  <![endif]-->
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/jquery.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/ui/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/ui/external/jquery.bgiframe-3.0.0.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/tabs.js"></script>
	<?php foreach ($styles as $style) { ?>
	<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>
	<?php foreach ($scripts as $script) { ?>
	<script type="text/javascript" src="<?php echo $script; ?>"></script>
	<?php } ?>
	<?php if($this->user->getUserName()){ ?>
		<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/jquery.flot.min.js"></script>
		<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/jquery.flot.pie.min.js"></script>
		<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/curvedLines.min.js"></script>
		<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/jquery.flot.tooltip.min.js"></script>
		<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/modernizr.js"></script>
        <!--<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/jquery-1.7.2.min.js"></script>-->
        <!--<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/bootstrap.min.js"></script>--> 
		<!--[if lte IE 8]>
		<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/excanvas.min.js"></script>
		<![endif]-->
	<?php } ?>
	<script type="text/javascript">
	$(document).ready(function(){

		// Signin - Button

		$(".form-signin-body-right input").click(function(){
			$(".form-signin").submit();
		});

		// Signin - Enter Key

		$('.form-signin input').keydown(function(e) {
			if (e.keyCode == 13) {
				$('.form-signin').submit();
			}
		});

	    // Confirm Delete
	    $('#form').submit(function(){
	    	if ($(this).attr('action').indexOf('delete',1) != -1) {
	    		if (!confirm('<?php echo $text_confirm; ?>')) {
	    			return false;
	    		}
	    	}
	    });

		// Confirm Uninstall
		$('a').click(function(){
			if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall', 1) != -1) {
				if (!confirm('<?php echo $text_confirm; ?>')) {
					return false;
				}
			}
		});
	});
	</script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="shortcut icon" href="view/image/admin_theme/base5builder_impulsepro/favicon.png" />
</head>

<body>

	<div class="container-fluid">

		<?php if ($logged) { ?>

		<div id="left-column">
			<div class="sidebar-logo">
				<a href="<?php echo $home; ?>">
					<img src="view/image/admin_theme/base5builder_impulsepro/logo.png" />
				</a>
			</div>
			<div id="mainnav">
				<ul class="mainnav">
					<li id="menu-control">
						<div class="menu-control-outer">
							<div class="menu-control-inner">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</div>
						</div>
					</li>
					<li id="dashboard"><a href="<?php echo $home; ?>" class="top"><?php echo $text_dashboard; ?></a></li>
                                        
                                        <li id="category">
                                            <a class="top">
                                                <?php echo $text_category; ?>
                                                &nbsp;<b>(<?php echo $total_cat; ?>)</b>
                                            </a>
                                            <ul>
                                                <li>
                                                    <a href="<?php echo $add_category; ?>"><?php echo $text_add_category; ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo $category; ?>"><?php echo $text_list_category; ?></a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                         
                                        <li id="product">
                                            <a class="top">
                                                <?php echo $text_product; ?>&nbsp;<b>(<?php echo $total_product; ?>)</b>
                                            </a>
                                            <ul>
                                                <li>
                                                    <a href="<?php echo $add_product; ?>"><?php echo $text_add_product; ?></a>
                                                </li>
                                                <li>
                                                    <li><a href="<?php echo $product; ?>"><?php echo $text_list_product; ?></a>
                                                </li>
                                                <li><a href="<?php echo $low_stock_product; ?>"><?php echo $text_low_stock_product; ?></a></li>           
                                                <li>
                                                    <a class="parent"><?php echo $text_attribute; ?></a>
                                                    <ul>
                                                        <li><a href="<?php echo $attribute; ?>"><?php echo $text_attribute; ?>&nbsp;<b>(<?php echo $total_attribute; ?>)</b></a></li>
                                                        <li><a href="<?php echo $attribute_group; ?>"><?php echo $text_attribute_group; ?>&nbsp;<b>(<?php echo $total_attribute_group; ?>)</b></a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="<?php echo $option; ?>"><?php echo $text_option; ?>&nbsp;<b>(<?php echo $total_option; ?>)</b></a></li>
                                                <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?>&nbsp;<b>(<?php echo $total_brand; ?>)</b></a></li>
                                                <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?>&nbsp;<b>(<?php echo $total_download; ?>)</b></a></li>
                                                <li><a href="<?php echo $review; ?>"><?php echo $text_review; ?>&nbsp;<b>(<?php echo $total_review; ?>)</b></a></li>    
                                            </ul>
                                        </li>
                                               
                                        <li id="order">
                                             <a class="top"><?php echo $text_order; ?>&nbsp;<b>(<?php echo $total_order; ?>)</b></a>        
                                             <ul>
                                                 <li><a href="<?php echo $order; ?>"><?php echo $text_list_order; ?></a></li>     
                                                 <li><a href="<?php echo $pending_order; ?>"><?php echo $text_pending_order; ?></a></li>     
                                                 <li><a href="<?php echo $complete_order; ?>"><?php echo $text_complete_order; ?></a></li>     
                                                 <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?>&nbsp;<b>(<?php echo $total_return; ?>)</b></a></li>
                                             </ul>
                                        </li>
                                        
                                        <li id="customer">
                                            <a class="top"><?php echo $text_customer; ?>&nbsp;<b>(<?php echo $total_customer; ?>)</b></a><!-- JIPL End -->
                                            <ul>
                                                <li>
                                                    <a href="<?php echo $customer; ?>"><?php echo $text_list_customer; ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo $customer_group; ?>"><?php echo $text_customer_group; ?>&nbsp;<b>(<?php echo $total_customer_group; ?>)</b></a>
                                                </li>
                                            </ul>
                                        </li>
                                              
                                        <li id="coupon">
                                            <a class="top"><?php echo $text_coupon; ?>&nbsp;<b>(<?php echo $total_coupon; ?>)</b></a>
                                            <ul>
                                                <li><a href="<?php echo $add_coupon; ?>"><?php echo $text_add_coupon; ?></a></li>
                                                <li><a href="<?php echo $coupon; ?>"><?php echo $text_list_coupon; ?></a></li>
                                            </ul>        
                                        </li>
                                        
                                        <li id="tax">
                                            <a class="top"><?php echo $text_tax; ?></a>
                                                <ul>
                                                    <li><a href="<?php echo $tax_class; ?>"><?php echo $text_tax_class; ?>&nbsp;<b>(<?php echo $total_tax_class; ?>)</b></a></li>
                                                    <li><a href="<?php echo $tax_rate; ?>"><?php echo $text_tax_rate; ?>&nbsp;<b>(<?php echo $total_tax_rate; ?>)</b></a></li>
                                                </ul>
                                        </li>
                                        
                                        <li id="localization">
                                            <a class="top"><?php echo $text_localisation; ?></a>
                                            <ul>
                                                <li><a href="<?php echo $language; ?>"><?php echo $text_language; ?>&nbsp;<b>(<?php echo $total_language; ?>)</b></a></li>
                                                <li><a href="<?php echo $currency; ?>"><?php echo $text_currency; ?>&nbsp;<b>(<?php echo $total_currency; ?>)</b></a></li>
                                                <li><a href="<?php echo $stock_status; ?>"><?php echo $text_stock_status; ?>&nbsp;<b>(<?php echo $total_stock_status; ?>)</b></a></li>
                                                <li><a href="<?php echo $order_status_list; ?>"><?php echo $text_order_status; ?>&nbsp;<b>(<?php echo $total_order_status; ?>)</b></a></li>
                                                <li><a class="parent"><?php echo $text_return; ?></a>
                                                        <ul>
                                                                <li><a href="<?php echo $return_status; ?>"><?php echo $text_return_status; ?>&nbsp;<b>(<?php echo $total_return_status; ?>)</b></a></li>
                                                                <li><a href="<?php echo $return_action; ?>"><?php echo $text_return_action; ?>&nbsp;<b>(<?php echo $total_return_action; ?>)</b></a></li>
                                                                <li><a href="<?php echo $return_reason; ?>"><?php echo $text_return_reason; ?>&nbsp;<b>(<?php echo $total_return_reason; ?>)</b></a></li>
                                                        </ul>
                                                </li>
                                                <li><a href="<?php echo $country; ?>"><?php echo $text_country; ?>&nbsp;<b>(<?php echo $total_country; ?>)</b></a></li>
                                                <li><a href="<?php echo $zone; ?>"><?php echo $text_zone; ?>&nbsp;<b>(<?php echo $total_zone; ?>)</b></a></li>
                                                <li><a href="<?php echo $geo_zone; ?>"><?php echo $text_geo_zone; ?>&nbsp;<b>(<?php echo $total_geo_zone; ?>)</b></a></li>									
                                                
                                                <li><a href="<?php echo $city; ?>">Cities&nbsp;<b>(<?php echo $total_city; ?>)</b></a></li>
                                                
                                                <li><a href="<?php echo $length_class; ?>"><?php echo $text_length_class; ?>&nbsp;<b>(<?php echo $total_length_class; ?>)</b></a></li>
                                                <li><a href="<?php echo $weight_class; ?>"><?php echo $text_weight_class; ?>&nbsp;<b>(<?php echo $total_weight_class; ?>)</b></a></li>
                                            </ul>
                                        </li>
                                         
                                        <li id="pages">
                                            <a class="top"><?php echo $text_information; ?>&nbsp;<b>(<?php echo $total_information; ?>)</b></a>
                                            <ul>
                                                <li>
                                                    <a href="<?php echo $add_information; ?>"><?php echo $text_add_information; ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo $information; ?>"><?php echo $text_list_information; ?></a>
                                                </li>
                                            </ul>
                                        </li>
                                        
					<li id="extension"><a class="top"><?php echo $text_extension; ?></a>
						<ul>
							<li><a href="<?php echo $module; ?>"><?php echo $text_module; ?>&nbsp;<b>(<?php echo $total_module; ?>)</b></a></li>
							<li><a href="<?php echo $shipping; ?>"><?php echo $text_shipping; ?>&nbsp;<b>(<?php echo $total_shipping; ?>)</b></a></li>
							<li><a href="<?php echo $payment; ?>"><?php echo $text_payment; ?>&nbsp;<b>(<?php echo $total_payment; ?>)</b></a></li>
							<li><a href="<?php echo $total; ?>"><?php echo $text_total; ?>&nbsp;<b>(<?php echo $total_total; ?>)</b></a></li>
							<li><a href="<?php echo $feed; ?>"><?php echo $text_feed; ?>&nbsp;<b>(<?php echo $total_feed; ?>)</b></a></li>
						</ul>
					</li>
					<li id="sale"><a class="top"><?php echo $text_sale; ?></a>
						<ul>
							<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?>&nbsp;<b>(<?php echo $total_affiliate; ?>)</b></a></li>
							
							<li><a class="parent"><?php echo $text_voucher; ?></a>
								<ul>
									<li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?>&nbsp;<b>(<?php echo $total_voucher; ?>)</b></a></li>
									<li><a href="<?php echo $voucher_theme; ?>"><?php echo $text_voucher_theme; ?>&nbsp;<b>(<?php echo $total_voucher_theme; ?>)</b></a></li>
								</ul>
							</li>
							<li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
						</ul>
					</li>
                                                
                                        <li id="user">
                                            <a class="top"><?php echo $text_users; ?></a>
                                            <ul>
                                                <li><a href="<?php echo $user; ?>"><?php echo $text_user; ?>&nbsp;<b>(<?php echo $total_user; ?>)</b></a></li>
                                                <li><a href="<?php echo $user_group; ?>"><?php echo $text_user_group; ?>&nbsp;<b>(<?php echo $total_user_group; ?>)</b></a></li>
                                            </ul>
                                        </li>

					<li id="system"><a class="top"><?php echo $text_system; ?></a>
						<ul>
							<li><a href="<?php echo $setting; ?>"><?php echo $text_setting; ?>&nbsp;<b>(<?php echo $total_store; ?>)</b></a></li>
							<li><a class="parent"><?php echo $text_design; ?></a>
								<ul>
									<li><a href="<?php echo $layout; ?>"><?php echo $text_layout; ?>&nbsp;<b>(<?php echo $total_layout; ?>)</b></a></li>
									<li><a href="<?php echo $banner; ?>"><?php echo $text_banner; ?>&nbsp;<b>(<?php echo $total_banner; ?>)</b></a></li>
								</ul>
							</li>							
							<li><a href="<?php echo $error_log; ?>"><?php echo $text_error_log; ?></a></li>
							<li><a href="<?php echo $backup; ?>"><?php echo $text_backup; ?></a></li>
						</ul>
					</li>
					<li id="reports">
                                            <a class="top"><?php echo $text_reports; ?></a>
						<ul>
							<li><a class="parent"><?php echo $text_sale; ?></a>
								<ul>
									<li><a href="<?php echo $report_sale_order; ?>"><?php echo $text_report_sale_order; ?></a></li>
									<li><a href="<?php echo $report_sale_tax; ?>"><?php echo $text_report_sale_tax; ?></a></li>
									<li><a href="<?php echo $report_sale_shipping; ?>"><?php echo $text_report_sale_shipping; ?></a></li>
									<li><a href="<?php echo $report_sale_return; ?>"><?php echo $text_report_sale_return; ?></a></li>
									<li><a href="<?php echo $report_sale_coupon; ?>"><?php echo $text_report_sale_coupon; ?></a></li>
								</ul>
							</li>
							<li><a class="parent"><?php echo $text_product; ?></a>
								<ul>
									<li><a href="<?php echo $report_product_viewed; ?>"><?php echo $text_report_product_viewed; ?></a></li>
									<li><a href="<?php echo $report_product_purchased; ?>"><?php echo $text_report_product_purchased; ?></a></li>
								</ul>
							</li>
							<li><a class="parent"><?php echo $text_customer; ?></a>
								<ul>
									<li><a href="<?php echo $report_customer_order; ?>"><?php echo $text_report_customer_order; ?></a></li>
									<li><a href="<?php echo $report_customer_reward; ?>"><?php echo $text_report_customer_reward; ?></a></li>
									<li><a href="<?php echo $report_customer_credit; ?>"><?php echo $text_report_customer_credit; ?></a></li>
								</ul>
							</li>
							<li><a class="parent"><?php echo $text_affiliate; ?></a>
								<ul>
									<li><a href="<?php echo $report_affiliate_commission; ?>"><?php echo $text_report_affiliate_commission; ?></a></li>
								</ul>
							</li>
						</ul>
					</li>
				</ul>
			</div>


			<div class="sidebar copyright">
				<div class="sidebar-base5builder">
					<!--<p>ImpulsePro Admin Template By <a href="http://base5builder.com/" target="_blank">Base5Builder.com</a>. Built with <a href="http://getbootstrap.com/" target="_blank">Bootstap</a> v2.3.2. <br />Icons by: <a href="http://iconsweets2.com/" target="_blank">iconSweets2</a></p>-->
				</div>
				<div class="sidebar-opencart"><?php echo $text_footer; ?></div>
			</div>
		</div>
		<div class="right-header-content clearfix">
                <div class="secondary-menu">
                    <ul>

                    <!--===================== Notification 
                                             =====================================================-->
                    <li id="notification">
                        <a class="top">
                            Notification &nbsp;
                            <strong><span class="label label-warning"><?php echo $alerts; ?></span></strong>
                        </a>
                        <ul>
                            <li class="dropdown-header">
                                <a><?php echo $text_order; ?></a>
                            </li>
                            <li>
                                <a href="<?php echo $order_status; ?>" style="display: block; overflow: auto;">
                                    Pending
                                    <span class="label label-warning pull-right">
                                        <?php echo $order_status_total; ?>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $complete_status; ?>">
                                    Completed
                                    <span class="label label-success pull-right">
                                        <?php echo $complete_status_total; ?>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $return; ?>">
                                    Return
                                    <span class="label label-danger pull-right">
                                        <?php echo $return_total; ?>
                                    </span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-header">
                                <a><?php echo $text_customer; ?></a>
                            </li>
                            <li>
                                <a href="<?php echo $customer_approval; ?>">
                                    Pending approval
                                    <span class="label label-danger pull-right">
                                        <?php echo $customer_total; ?>
                                    </span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-header">
                                <a><?php echo $text_product; ?></a>
                            </li>
                            <li>
                                <a href="<?php echo $product.'&filter_quantity=0'; ?>">
                                    Out of stock
                                    <span class="label label-danger pull-right">
                                        <?php echo $product_total; ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $review; ?>">
                                    <?php echo $text_review; ?> 
                                    <span class="label label-danger pull-right">
                                        <?php echo $review_total; ?>
                                    </span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-header">
                                <a><?php echo $text_affiliate; ?></a>
                            </li>
                            <li>
                                <a href="<?php echo $affiliate_approval; ?>">
                                    Pending Approval
                                    <span class="label label-danger pull-right">
                                        <?php echo $affiliate_total; ?>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li><!-- END notofocation -->
                    
                    <li id="cart">
                        <a class="top" >Orders <strong><span class="label label-warning"><?php echo $total_order;?></span></strong></a>
                        <ul id="ulid" style="display: block;"> 
                            <li><!--<a href="<?php echo $order; ?>">My Orders</a>-->
                                <table cellpadding="0" cellspacing="0" border="0">                                	
                                <?php $sql = "SELECT * FROM `order` o, `order_total` t where o.order_id = t.order_id and t.code='total' ORDER BY o.order_id desc limit 10";
                                      $query = $this->db->query($sql); $orderdata=''; $tocken=$this->session->data['token'];
                                      foreach ($query->rows as $result) { $oid=$result['order_id'];
                                          $orderdata .= "<tr><td>
                                          <a href='index.php?route=sale/order/update&token=".$tocken."&order_id=".$oid."'>Order No. ".$oid."&nbsp;&nbsp;with total &nbsp;&nbsp;".$result['text']."</a></td></tr>";
                                      }
                                      echo $orderdata;
                               ?>
                               </table>
                           </li>
                        </ul>
                    </li>
                    
                    <li id="store"><a onClick="window.open('<?php echo $store; ?>');" class="top"><?php echo $text_front; ?></a>
                            <ul style="display:none">
                                    <?php foreach ($stores as $stores) { ?>
                                    <li><a onClick="window.open('<?php echo $stores['href']; ?>');"><?php echo $stores['name']; ?></a></li>
                                    <?php } ?>
                            </ul>
                    </li>
                    <li id="logout"><a class="top" href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                </ul>
            </div>
            <div class="admin-info"><?php echo $logged; ?></div>
        </div>

<?php } ?>
       