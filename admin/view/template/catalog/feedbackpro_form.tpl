<?php echo $header; 
$heading_title = $info['name']; 
?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/admin_theme/base5builder_impulsepro/icon-information-large.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $text_back; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <?php $inputs = unserialize($info['input']); ?> 
        <table class="form">
        <?php foreach ($inputs as $key=>$input) { ?>
                  <tr>
                    <td><?php echo $key; ?>:</td>
                    <td><?php if (is_array($input)) { ?>
                      <?php echo html_entity_decode(implode($input,'<br>')); ?>
                      <?php } else { ?>
                      <?php echo $input; ?>
                      <?php } ?>
                    </td>
                  </tr>
        <?php } ?>
        </table>
      </form>
    </div>
  </div>
</div>

<?php echo $footer; ?>