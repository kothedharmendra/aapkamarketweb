<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <?php
            if(isset($error)){
                echo('<div style="color:red;text-align:center;font-weight:bold;">'.$error.'</div>');
            }
          ?>
        <table class="form">
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="rest_api_status">
                <?php if ($rest_api_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
            <tr>
             <td>
          	    <label for="entry_client_id"><?php echo $entry_client_id; ?></label>
              </td>
              <td>
                <input type="text" name="rest_api_client_id" value="<?php echo $rest_api_client_id; ?>" />
              </td>
            </tr>
            <tr>
                <td>
                    <label for="entry_client_secret"><?php echo $entry_client_secret; ?></label>
                </td>
                <td>
                    <input type="text" name="rest_api_client_secret" value="<?php echo $rest_api_client_secret; ?>" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="entry_token_ttl"><?php echo $entry_token_ttl; ?></label>
                </td>
                <td>
                    <input type="text" name="rest_api_token_ttl" value="<?php echo $rest_api_token_ttl; ?>" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="entry_order_id"><?php echo $entry_order_id; ?></label>
                </td>
                <td>
                    <input type="text" name="rest_api_order_id" value="<?php echo $rest_api_order_id; ?>" required="required"/>
                </td>
            </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>