<?php echo $header; ?>

<?php 

// Default values

if(empty($dailyshopin_background_color)) $dailyshopin_background_color ="eeeeee";

if(empty($dailyshopin_title_font)) $dailyshopin_title_font ="Archivo+Narrow";
if(empty($dailyshopin_title_font_family)) $dailyshopin_title_font_family ="Archivo Narrow";

if(empty($dailyshopin_menu_font)) $dailyshopin_menu_font ="Archivo+Narrow";
if(empty($dailyshopin_menu_font_family)) $dailyshopin_menu_font_family ="Archivo Narrow";
if(empty($dailyshopin_menu_font_size)) $dailyshopin_menu_font_size ="19";

if(empty($dailyshopin_boxheading_font)) $dailyshopin_boxheading_font ="Archivo+Narrow";
if(empty($dailyshopin_boxheading_font_family)) $dailyshopin_boxheading_font_family ="Archivo Narrow";
if(empty($dailyshopin_boxheading_font_size)) $dailyshopin_boxheading_font_size ="25";

if(empty($dailyshopin_time_text)) $dailyshopin_time_text ="";

if(empty($dailyshopin_firstblock_title)) $dailyshopin_firstblock_title ="";
if(empty($dailyshopin_firstblock_desc)) $dailyshopin_firstblock_desc ="";

if(empty($dailyshopin_secondblock_title)) $dailyshopin_secondblock_title ="";
if(empty($dailyshopin_secondblock_desc)) $dailyshopin_secondblock_desc ="";

if(empty($dailyshopin_thirdblock_title)) $dailyshopin_thirdblock_title ="";
if(empty($dailyshopin_thirdblock_desc)) $dailyshopin_thirdblock_desc ="";

if(empty($dailyshopin_fourthblock_title)) $dailyshopin_fourthblock_title ="";
if(empty($dailyshopin_fourthblock_desc)) $dailyshopin_fourthblock_desc ="";

if(empty($dailyshopin_address)) $dailyshopin_address ="";
if(empty($dailyshopin_phone)) $dailyshopin_phone ="";
if(empty($dailyshopin_phone_second)) $dailyshopin_phone_second ="";
if(empty($dailyshopin_email)) $dailyshopin_email ="";
if(empty($dailyshopin_email_second)) $dailyshopin_email_second ="";

if(empty($dailyshopin_custom_widget_title)) $dailyshopin_custom_widget_title ="";
if(empty($dailyshopin_footer_info_text)) $dailyshopin_footer_info_text ="";
if(empty($dailyshopin_shipping_text)) $dailyshopin_shipping_text ="";
if(empty($dailyshopin_shipping_last_text)) $dailyshopin_shipping_last_text ="";

if(empty($dailyshopin_facebook_id)) $dailyshopin_facebook_id ="";
if(empty($dailyshopin_twitter_username)) $dailyshopin_twitter_username ="";

/*if(empty($dailyshopin_facebook_link)) $dailyshopin_facebook_link ="https://www.facebook.com/envato";
if(empty($dailyshopin_twitter_link)) $dailyshopin_twitter_link ="https://www.twitter.com/envato";*/
if(empty($dailyshopin_facebook_link)) $dailyshopin_facebook_link ="https://www.facebook.com/";
if(empty($dailyshopin_twitter_link)) $dailyshopin_twitter_link ="https://www.twitter.com/";
if(empty($dailyshopin_google_link)) $dailyshopin_google_link ="";
if(empty($dailyshopin_youtube_link)) $dailyshopin_youtube_link ="";
if(empty($dailyshopin_flickr_link)) $dailyshopin_flickr_link ="";

if(empty($option_slideshow)) $option_slideshow ="grid_6";
?>

<style type="text/css">
	.customhelp { color: #666; font-size:0.9em; }
	.color { border:1px solid #AAA; }
	.pttrn {width:32px; display: inline-block; text-align: center;}
</style>



<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>

<div class="box">

	<div class="heading">
		<h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
		<div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
	</div>

	<div class="content wrapper">
	
    
    <div class="back_ptrn">

	<div class="option_wrap">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        
    	<div class="heading">
        	
            <select name="dailyshopin_status" class="status_select">
				<?php if ($dailyshopin_status) { ?>
				<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
				<option value="0"><?php echo $text_disabled; ?></option>

				<?php } else { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
			</select>
        </div><!--/heading-->
    
		
		
		<div id="settings_tabs" class="htabs-custom clearfix depts">
			<a href="#layout_settings" class="bod"><img src="../admin/view/image/bod.png" alt="" > Body</a>
            <a href="#container_settings" class="cont"><img src="../admin/view/image/cont.png" alt="" > Container</a>
            <a href="#colors_settings" class="colo"><img src="../admin/view/image/colors.png" alt="" > Colors</a>
            <a href="#fonts_settings" class="font"><img src="../admin/view/image/font.png" alt="" > Fonts</a>
            <a href="#buttons_settings" class="butt"><img src="../admin/view/image/buttons.png" alt="" > Buttons</a>
            <a href="#footer2_settings" class="foot"><img src="../admin/view/image/sett.png" alt="" > Custom Options</a>  
		</div>

		
		
   		<div class="data_right">
        
        <!--**************************************************************************************** Body ***************************-->
		<div id="layout_settings" class="divtab">

			<table class="form">

				<tr><td colspan="2"><h3><?php echo $entry_pattern_sub; ?></h3></td></tr>


 <!--****************************************************************
 ********************************************************************
 
 Body:
 
 *******************************************************************
  *******************************************************************
 -->


				<!--** Choose Pattern **--> 
				<tr>
					<td><?php echo $entry_pattern_overlay; ?></td>
					<td>
						<div>
							<?php for ($i = 1; $i <= 36; $i++) { ?>
								<div class="pttrn"><span class="customhelp"><?php echo $i; ?></span><img src="view/image/patterns/<?php echo $i; ?>.png" alt="pattern <?php echo $i; ?>"></div>
								<?php if(!($i%12)): ?>
									<br />
								<?php endif ?>
							<?php } ?>
						</div><br>
						<select name="dailyshopin_pattern_overlay">
							<option value="none"selected="selected">none</option>
							<?php for ($i = 1; $i <= 36; $i++) { 
									($this->config->get('dailyshopin_pattern_overlay')== $i) ? $currentpat = 'selected' : $currentpat = '';
								?>
								<option value="<?php echo $i; ?>" <?php echo $currentpat; ?>><?php echo $i; ?></option>';
								<?php } ?>
						</select>
						<span class="customhelp"><?php echo $entry_pattern_overlay_help; ?></span>
					</td>
				</tr>
				<!--** Upload Pattern **-->
				<tr>
					<td><?php echo $entry_custom_pattern; ?> </td>
					<td>
						<input type="hidden" name="dailyshopin_custom_pattern" value="<?php echo $dailyshopin_custom_pattern; ?>" id="dailyshopin_custom_pattern" />
						<img src="<?php echo $dailyshopin_pattern_preview; ?>" id="dailyshopin_pattern_preview" />
						<br /><a onclick="image_upload('dailyshopin_custom_pattern', 'dailyshopin_pattern_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_pattern_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_custom_pattern').attr('value', '');"><?php echo $text_clear; ?></a>
					</td>
				</tr>                             
                <!--** Upload Background Image **-->
				<tr>   
					<td>
						<?php echo $entry_custom_image; ?><br>
						<span class="customhelp"><?php echo $entry_custom_image_help; ?></span>
					</td>
					<td>
						<input type="hidden" name="dailyshopin_custom_image" value="<?php echo $dailyshopin_custom_image; ?>" id="dailyshopin_custom_image" />
						<img src="<?php echo $dailyshopin_image_preview; ?>" alt="" id="dailyshopin_image_preview" />
						<br /><a onclick="image_upload('dailyshopin_custom_image', 'dailyshopin_image_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_image_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_custom_image').attr('value', '');"><?php echo $text_clear; ?></a>
					</td>
				</tr>
            	<!--** Main background color **-->
                    <tr>
                        <td><?php echo $entry_background_color; ?></td>
                        <td><input type="text" name="dailyshopin_background_color" value="<?php echo $dailyshopin_background_color; ?>" size="6" class="color {required:false,hash:true}"  /><span class="customhelp"><?php echo $entry_body_help; ?></span></td>
                    </tr> 
				</table>
				
			</div><!--/layout_settings-->

			
            
            <!--***************************************************************************************** container *******************-->
            <div id="container_settings" class="divtab">
    
                <table class="form">
                
                	  <tr><td colspan="2"><h3><?php echo $entry_pattern_sub_cont; ?></h3></td></tr>
                      <!--** Choose Pattern **-->                  
                      <tr>
                        <td><?php echo $entry_pattern_overlay; ?></td>
                        <td>
                            <div>
                                <?php for ($i = 1; $i <= 36; $i++) { ?>
                                    <div class="pttrn"><span class="customhelp"><?php echo $i; ?></span><img src="view/image/patterns/<?php echo $i; ?>.png" alt="pattern <?php echo $i; ?>"></div>
                                    <?php if(!($i%12)): ?>
                                        <br />
                                    <?php endif ?>
                                <?php } ?>
                            </div><br>
                            <select name="dailyshopin_pattern_cont">
                                <option value="none"selected="selected">none</option>
                                <?php for ($i = 1; $i <= 36; $i++) { 
                                        ($this->config->get('dailyshopin_pattern_cont')== $i) ? $currentpat = 'selected' : $currentpat = '';
                                    ?>
                                    <option value="<?php echo $i; ?>" <?php echo $currentpat; ?>><?php echo $i; ?></option>';
                                    <?php } ?>
                            </select>
                            <span class="customhelp"><?php echo $entry_pattern_overlay_help; ?></span>
                        </td>
                    </tr>             
                    <!--** Upload Pattern **-->
                    <tr>
                        <td><?php echo $entry_custom_pattern; ?> </td>
                        <td>
                            <input type="hidden" name="dailyshopin_custom_pattern_cont" value="<?php echo $dailyshopin_custom_pattern_cont; ?>" id="dailyshopin_custom_pattern_cont" />
                            <img src="<?php echo $dailyshopin_pattern_preview_cont; ?>" id="dailyshopin_pattern_preview_cont" />
                            <br /><a onclick="image_upload('dailyshopin_custom_pattern_cont', 'dailyshopin_pattern_preview_cont');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_pattern_preview_cont').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_custom_pattern_cont').attr('value', '');"><?php echo $text_clear; ?></a>
                        </td>
                    </tr>                 
                    <!--** Upload Background Image **-->
                    <tr>   
                        <td>
                            <?php echo $entry_custom_image; ?><br>
                            <span class="customhelp"><?php echo $entry_custom_image_help; ?></span>
                        </td>
                        <td>
                            <input type="hidden" name="dailyshopin_custom_image_cont" value="<?php echo $dailyshopin_custom_image_cont; ?>" id="dailyshopin_custom_image_cont" />
                            <img src="<?php echo $dailyshopin_image_preview_cont; ?>" alt="" id="dailyshopin_image_preview_cont" />
                            <br /><a onclick="image_upload('dailyshopin_custom_image_cont', 'dailyshopin_image_preview_cont');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_image_preview_cont').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_custom_image_cont').attr('value', '');"><?php echo $text_clear; ?></a>
                        </td>
                    </tr>                     
                    <!--** Container background color **-->
                    <tr>
                        <td><?php echo $entry_dailyshopin_background_color_cont; ?></td>
                        <td><input type="text" name="dailyshopin_background_color_cont" value="<?php echo $dailyshopin_background_color_cont; ?>" size="6" class="color {required:false,hash:true}"  /><span class="customhelp"><?php echo $entry_cont_help; ?></span></td>
                    </tr>         
                
                </table>
				
			</div>
        

            
<!--*******************************************************************************************
***********************************************************************************************


 Colors
 
 
 **********************************************************************************************
 *******************************************************************************************-->
 
<div id="colors_settings" class="divtab">
	
    <ul class="tabs-head tabs">
        <li><a href="#top"><?php echo $entry_header_tit; ?></a></li>      
        <li><a href="#blocks"><?php echo $entry_blocks; ?></a></li>
        <li><a href="#menuu"><?php echo $entry_menu_tit; ?></a></li>
        <li><a href="#cart"><?php echo $entry_cart_tit; ?></a></li>
        <li><a href="#search"><?php echo $entry_search_tit; ?></a></li>
        <li><a href="#category"><?php echo $entry_category_tit; ?></a></li>
        <li><a href="#box"><?php echo $entry_box_tit; ?></a></li>
        <li><a href="#product"><?php echo $entry_product_tit; ?></a></li>
        <li><a href="#general"><?php echo $entry_general_tit; ?></a></li>
        <li><a href="#footerr"><?php echo $entry_dailyshopin_entry_footer; ?></a></li>
    </ul>
    
    <div class="tab_container">
    <!--///////////////////////////////////////////////////////////////////////////////// top-->
        <div id="top" class="tab_content">
        	<table class="form">
                <!--** First Theme color **-->
                <tr>
                    <td><?php echo $entry_thickbar_color; ?></td>
                    <td><input type="text" name="dailyshopin_thickbar_color" value="<?php echo $dailyshopin_thickbar_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** Welcome text color **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_welcome_text_color; ?></td>
                    <td><input type="text" name="dailyshopin_welcome_text_color" value="<?php echo $dailyshopin_welcome_text_color; ?>" size="6" class="color {required:false,hash:true}"  /><span class="customhelp"><?php echo $entry_welcome_help; ?></span>
                    </td>
                </tr>
            </table>
        </div><!--/content-->
    <!--///////////////////////////////////////////////////////////////////////////////// blocks-->
        <div id="blocks" class=" tab_content">
            <table class="form">
            	<!--** block title color **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_block_title_color; ?></td>
                    <td><input type="text" name="dailyshopin_block_title_color" value="<?php echo $dailyshopin_block_title_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** block desc color **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_block_desc_color; ?></td>
                    <td><input type="text" name="dailyshopin_block_desc_color" value="<?php echo $dailyshopin_block_desc_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
            </table>
        </div><!--/content-->
    <!--///////////////////////////////////////////////////////////////////////////////// menu-->
        <div id="menuu" class=" tab_content">
            <table class="form">
            	<!--** Menu background color **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_menu_background; ?></td>
                    <td><input type="text" name="dailyshopin_menu_background" value="<?php echo $dailyshopin_menu_background; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>  
                <!--** Menu background border **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_menu_border; ?></td>
                    <td><input type="text" name="dailyshopin_menu_border" value="<?php echo $dailyshopin_menu_border; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>      
                <!--** Menu links background color on hover **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_menu_bg_links_hover; ?></td>
                    <td><input type="text" name="dailyshopin_menu_bg_links_hover" value="<?php echo $dailyshopin_menu_bg_links_hover; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>                  
                <!--** Top menu links color **-->
                <tr>
                    <td><?php echo $entry_menu_color; ?></td>
                    <td><input type="text" name="dailyshopin_menu_color" value="<?php echo $dailyshopin_menu_color; ?>" size="6" class="color {required:false,hash:true}"  /><span class="customhelp"><?php echo $entry_menulinks_help; ?></span>
                    </td>
                </tr>
                <!--** Top menu links color :hover **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_menu_color_hover; ?></td>
                    <td><input type="text" name="dailyshopin_menu_color_hover" value="<?php echo $dailyshopin_menu_color_hover; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>             
                <!--** sub menu headings **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_sub_menu_heading; ?></td>
                    <td><input type="text" name="dailyshopin_sub_menu_heading" value="<?php echo $dailyshopin_sub_menu_heading; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** sub menu links **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_smenu_color; ?></td>
                    <td><input type="text" name="dailyshopin_smenu_color" value="<?php echo $dailyshopin_smenu_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** sub menu links on hover **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_smenuh_color; ?></td>
                    <td><input type="text" name="dailyshopin_smenuh_color" value="<?php echo $dailyshopin_smenuh_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
            </table>
        </div><!--/content-->
    <!--///////////////////////////////////////////////////////////////////////////////// cart-->
        <div id="cart" class=" tab_content">
            <table class="form">
            	<!--** cart title **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_cart_title_color; ?></td>
                    <td><input type="text" name="dailyshopin_cart_title_color" value="<?php echo $dailyshopin_cart_title_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** cart desc **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_cart_desc_color; ?></td>
                    <td><input type="text" name="dailyshopin_cart_desc_color" value="<?php echo $dailyshopin_cart_desc_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
            </table>
        </div><!--/content-->
    <!--///////////////////////////////////////////////////////////////////////////////// search-->
        <div id="search" class=" tab_content">
            <table class="form">
            	<!--** search bg **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_search_bg; ?></td>
                    <td><input type="text" name="dailyshopin_search_bg" value="<?php echo $dailyshopin_search_bg; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>                                 
                <!--** search border **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_search_border; ?></td>
                    <td><input type="text" name="dailyshopin_search_border" value="<?php echo $dailyshopin_search_border; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
            </table>
        </div><!--/content-->
    <!--///////////////////////////////////////////////////////////////////////////////// category-->
        <div id="category" class=" tab_content">
            <table class="form">
            	<!--** category heading bg **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_category_heading_bg; ?></td>
                    <td><input type="text" name="dailyshopin_category_heading_bg" value="<?php echo $dailyshopin_category_heading_bg; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>        
                <!--** category heading border **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_category_heading_border; ?></td>
                    <td><input type="text" name="dailyshopin_category_heading_border" value="<?php echo $dailyshopin_category_heading_border; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** category heading color **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_category_heading_color; ?></td>
                    <td><input type="text" name="dailyshopin_category_heading_color" value="<?php echo $dailyshopin_category_heading_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>                
                <!--** category links color **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_category_link_color; ?></td>
                    <td><input type="text" name="dailyshopin_category_link_color" value="<?php echo $dailyshopin_category_link_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>                
                <!--** category links color on hover **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_category_link_color_hover; ?></td>
                    <td><input type="text" name="dailyshopin_category_link_color_hover" value="<?php echo $dailyshopin_category_link_color_hover; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>                
                <!--** category internal title **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_category_title; ?></td>
                    <td><input type="text" name="dailyshopin_category_title" value="<?php echo $dailyshopin_category_title; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
            </table>
        </div><!--/content-->
    <!--///////////////////////////////////////////////////////////////////////////////// box-->
        <div id="box" class=" tab_content">
            <table class="form">
            	<!--** box heading bg **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_heading_bg; ?></td>
                    <td><input type="text" name="dailyshopin_heading_bg" value="<?php echo $dailyshopin_heading_bg; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>               
                <!--** box heading border **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_heading_border; ?></td>
                    <td><input type="text" name="dailyshopin_heading_border" value="<?php echo $dailyshopin_heading_border; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>               
                <!--** box heading color **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_heading_color; ?></td>
                    <td><input type="text" name="dailyshopin_heading_color" value="<?php echo $dailyshopin_heading_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>                
                <!--** tabs heading bg **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_heading_tabs_bg; ?></td>
                    <td><input type="text" name="dailyshopin_heading_tabs_bg" value="<?php echo $dailyshopin_heading_tabs_bg; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr> 
                <!--** tabs heading border **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_heading_tabs_border; ?></td>
                    <td><input type="text" name="dailyshopin_heading_tabs_border" value="<?php echo $dailyshopin_heading_tabs_border; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>  
                <!--** prodbcat title **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_prodbcat_title; ?></td>
                    <td><input type="text" name="dailyshopin_prodbcat_title" value="<?php echo $dailyshopin_prodbcat_title; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** prodbcat viewall **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_prodbcat_viewall; ?></td>
                    <td><input type="text" name="dailyshopin_prodbcat_viewall" value="<?php echo $dailyshopin_prodbcat_viewall; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** prodbcat tab bg **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_prodbcat_tab_bg; ?></td>
                    <td><input type="text" name="dailyshopin_prodbcat_tab_bg" value="<?php echo $dailyshopin_prodbcat_tab_bg; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** prodbcat tab bg select **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_prodbcat_tab_bg_select; ?></td>
                    <td><input type="text" name="dailyshopin_prodbcat_tab_bg_select" value="<?php echo $dailyshopin_prodbcat_tab_bg_select; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** prodbcat tab link **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_prodbcat_tab_link; ?></td>
                    <td><input type="text" name="dailyshopin_prodbcat_tab_link" value="<?php echo $dailyshopin_prodbcat_tab_link; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>    
            </table>
        </div><!--/content-->
    <!--///////////////////////////////////////////////////////////////////////////////// product-->
        <div id="product" class=" tab_content">
            <table class="form">
            	<!--** product border **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_product_border; ?></td>
                    <td><input type="text" name="dailyshopin_product_border" value="<?php echo $dailyshopin_product_border; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>  
            	<!--** sale bg **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_sale_bg; ?></td>
                    <td><input type="text" name="dailyshopin_sale_bg" value="<?php echo $dailyshopin_sale_bg; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>               
                <!--** price color **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_price_color; ?></td>
                    <td><input type="text" name="dailyshopin_price_color" value="<?php echo $dailyshopin_price_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>                
                <!--** old price color **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_other_oldprice_color; ?></td>
                    <td><input type="text" name="dailyshopin_other_oldprice_color" value="<?php echo $dailyshopin_other_oldprice_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>                
                <!--** product name color **--> 
                <tr>
                    <td><?php echo $entry_dailyshopin_product_name_color; ?></td>
                    <td><input type="text" name="dailyshopin_product_name_color" value="<?php echo $dailyshopin_product_name_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
            </table>
        </div><!--/content-->
    <!--///////////////////////////////////////////////////////////////////////////////// general-->
        <div id="general" class=" tab_content">
            <table class="form">
            	<!--** Titles color **--> 
                <tr>
                    <td><?php echo $entry_title_color; ?></td>
                    <td><input type="text" name="dailyshopin_title_color" value="<?php echo $dailyshopin_title_color; ?>" size="6" class="color {required:false,hash:true}"  />
                        <span class="customhelp"><?php echo $entry_title_color_help; ?></span></td>
                </tr>               
                <!--** Body text color **-->
                <tr>
                    <td><?php echo $entry_body_color; ?></td>
                    <td><input type="text" name="dailyshopin_bodytext_color" value="<?php echo $dailyshopin_bodytext_color; ?>" size="6" class="color {required:false,hash:true}"  />
                        <span class="customhelp"><?php echo $entry_body_color_help; ?></span></td>
                </tr>
                <!--** Other links color **-->
                <tr>
                    <td><?php echo $entry_links_color; ?></td>
                    <td><input type="text" name="dailyshopin_links_color" value="<?php echo $dailyshopin_links_color; ?>" size="6" class="color {required:false,hash:true}"  /></td>
                </tr>
            </table>
        </div><!--/content-->
    <!--///////////////////////////////////////////////////////////////////////////////// footer-->
        <div id="footerr" class=" tab_content">
            <table class="form">
            	<!--** Footer bg **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_footer_bg; ?></td>
                    <td><input type="text" name="dailyshopin_footer_bg" value="<?php echo $dailyshopin_footer_bg; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>  
                <!--** Footer bottom bg **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_footer_bottom_bg; ?></td>
                    <td><input type="text" name="dailyshopin_footer_bottom_bg" value="<?php echo $dailyshopin_footer_bottom_bg; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>  
                <!--** Footer Headers color **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_footerheaders_color; ?></td>
                    <td><input type="text" name="dailyshopin_footerheaders_color" value="<?php echo $dailyshopin_footerheaders_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** Footer Headers widgets color **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_footerheaderswid_color; ?></td>
                    <td><input type="text" name="dailyshopin_footerheaderswid_color" value="<?php echo $dailyshopin_footerheaderswid_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** Footer Headers widgets bg **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_footerheaderswid_bg; ?></td>
                    <td><input type="text" name="dailyshopin_footerheaderswid_bg" value="<?php echo $dailyshopin_footerheaderswid_bg; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** Footer Headers widgets border **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_footerheaderswid_border; ?></td>
                    <td><input type="text" name="dailyshopin_footerheaderswid_border" value="<?php echo $dailyshopin_footerheaderswid_border; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** Footer first border color **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_first_border_color; ?></td>
                    <td><input type="text" name="dailyshopin_first_border_color" value="<?php echo $dailyshopin_first_border_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** Footer second border color **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_second_border_color; ?></td>
                    <td><input type="text" name="dailyshopin_second_border_color" value="<?php echo $dailyshopin_second_border_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>   
                <!--** Footer info text color **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_footer_info_color; ?></td>
                    <td><input type="text" name="dailyshopin_footer_info_color" value="<?php echo $dailyshopin_footer_info_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** Footer links color **-->
                <tr>
                    <td><?php echo $entry_dailyshopin_footerlinks_color; ?></td>
                    <td><input type="text" name="dailyshopin_footerlinks_color" value="<?php echo $dailyshopin_footerlinks_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>
                <!--** Facebook Background color **-->
                <tr>
                    <td><?php echo $entry_facebook_bg_color; ?></td>
                    <td><input type="text" name="facebook_bg_color" value="<?php echo $facebook_bg_color; ?>" size="6" class="color {required:false,hash:true}"  />
                    </td>
                </tr>  
            </table>
        </div><!--/content-->
        
 	</div><!--tab_container-->

</div><!--/colors_settings-->
    
   
<!--*******************************************************************************************
***********************************************************************************************


 Fonts
 
 
 **********************************************************************************************
 *******************************************************************************************-->
 
            <div id="fonts_settings" class="divtab">

            <ul class="tabs-head tabs">
                <li><a href="#titlefont"><?php echo $entry_titles_sub; ?></a></li>
                <li><a href="#bodyfont"><?php echo $entry_body_sub; ?></a></li>
                <li><a href="#buttonfont"><?php echo $entry_buttons_sub; ?></a></li>
                <li><a href="#categoryfont"><?php echo $entry_category_sub; ?></a></li>
                <li><a href="#menufont"><?php echo $entry_menu_sub; ?></a></li>
                <li><a href="#productfont"><?php echo $entry_product_sub; ?></a></li>
                <li><a href="#pricefont"><?php echo $entry_price_sub; ?></a></li>
                <li><a href="#boxheadingfont"><?php echo $entry_boxheading_sub; ?></a></li>
            </ul>

			<div class="tab_container">
                <!--///////////////////////////////////////////////////////////////////////////////// title-->
                    <div id="titlefont" class=" tab_content">
						<table class="form">
                            <!--** Title font on link **-->
                            <tr>
                                <td><?php echo $entry_title_font; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_title_font" value="<?php echo $dailyshopin_title_font; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font on css **-->
                            <tr>
                                <td><?php echo $entry_title_font_family; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_title_font_family" value="<?php echo $dailyshopin_title_font_family; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_family_help; ?></span>
                                </td>
                            </tr>
                        </table>
					</div>
                <!--///////////////////////////////////////////////////////////////////////////////// body-->
                    <div id="bodyfont" class=" tab_content">
						<table class="form">
                            <!--** Title font on link **-->
                            <tr>
                                <td><?php echo $entry_title_font; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_body_font" value="<?php echo $dailyshopin_body_font; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font on css **-->
                            <tr>
                                <td><?php echo $entry_title_font_family; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_body_font_family" value="<?php echo $dailyshopin_body_font_family; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_family_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font size **-->
                            <tr>
                                <td><?php echo $entry_title_font_size; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_body_font_size" value="<?php echo $dailyshopin_body_font_size; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_size_help; ?></span>
                                </td>
                            </tr>
                        </table>
					</div>
                <!--///////////////////////////////////////////////////////////////////////////////// buttons-->
                    <div id="buttonfont" class=" tab_content">
						<table class="form">
                            <!--** Title font on link **-->
                            <tr>
                                <td><?php echo $entry_title_font; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_button_font" value="<?php echo $dailyshopin_button_font; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font on css **-->
                            <tr>
                                <td><?php echo $entry_title_font_family; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_button_font_family" value="<?php echo $dailyshopin_button_font_family; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_family_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font size **-->
                            <tr>
                                <td><?php echo $entry_title_font_size; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_button_font_size" value="<?php echo $dailyshopin_button_font_size; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_size_help; ?></span>
                                </td>
                            </tr>
                        </table>
					</div>
               <!--///////////////////////////////////////////////////////////////////////////////// category-->
                    <div id="categoryfont" class=" tab_content">
						<table class="form">
                            <!--** Title font on link **-->
                            <tr>
                                <td><?php echo $entry_title_font; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_category_font" value="<?php echo $dailyshopin_category_font; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font on css **-->
                            <tr>
                                <td><?php echo $entry_title_font_family; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_category_font_family" value="<?php echo $dailyshopin_category_font_family; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_family_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font size **-->
                            <tr>
                                <td><?php echo $entry_title_font_size; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_category_font_size" value="<?php echo $dailyshopin_category_font_size; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_size_help; ?></span>
                                </td>
                            </tr>
                        </table>
					</div>
               <!--///////////////////////////////////////////////////////////////////////////////// menu-->
                    <div id="menufont" class=" tab_content">
						<table class="form">
                            <!--** Title font on link **-->
                            <tr>
                                <td><?php echo $entry_title_font; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_menu_font" value="<?php echo $dailyshopin_menu_font; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font on css **-->
                            <tr>
                                <td><?php echo $entry_title_font_family; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_menu_font_family" value="<?php echo $dailyshopin_menu_font_family; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_family_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font size **-->
                            <tr>
                                <td><?php echo $entry_title_font_size; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_menu_font_size" value="<?php echo $dailyshopin_menu_font_size; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_size_help; ?></span>
                                </td>
                            </tr>
                        </table>
					</div>
                <!--///////////////////////////////////////////////////////////////////////////////// product-->
                    <div id="productfont" class=" tab_content">
						<table class="form">
                            <!--** Title font on link **-->
                            <tr>
                                <td><?php echo $entry_title_font; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_product_font" value="<?php echo $dailyshopin_product_font; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font on css **-->
                            <tr>
                                <td><?php echo $entry_title_font_family; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_product_font_family" value="<?php echo $dailyshopin_product_font_family; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_family_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font size **-->
                            <tr>
                                <td><?php echo $entry_title_font_size; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_product_font_size" value="<?php echo $dailyshopin_product_font_size; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_size_help; ?></span>
                                </td>
                            </tr>
                        </table>
					</div>
                <!--///////////////////////////////////////////////////////////////////////////////// price-->
                    <div id="pricefont" class=" tab_content">
						<table class="form">
                            <!--** Title font on link **-->
                            <tr>
                                <td><?php echo $entry_title_font; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_price_font" value="<?php echo $dailyshopin_price_font; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font on css **-->
                            <tr>
                                <td><?php echo $entry_title_font_family; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_price_font_family" value="<?php echo $dailyshopin_price_font_family; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_family_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font size **-->
                            <tr>
                                <td><?php echo $entry_title_font_size; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_price_font_size" value="<?php echo $dailyshopin_price_font_size; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_size_help; ?></span>
                                </td>
                            </tr>
                        </table>
					</div>
               <!--///////////////////////////////////////////////////////////////////////////////// boxheading-->
                    <div id="boxheadingfont" class=" tab_content">
						<table class="form">
                            <!--** Title font on link **-->
                            <tr>
                                <td><?php echo $entry_title_font; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_boxheading_font" value="<?php echo $dailyshopin_boxheading_font; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font on css **-->
                            <tr>
                                <td><?php echo $entry_title_font_family; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_boxheading_font_family" value="<?php echo $dailyshopin_boxheading_font_family; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_family_help; ?></span>
                                </td>
                            </tr>
                            <!--** Title font size **-->
                            <tr>
                                <td><?php echo $entry_title_font_size; ?></td>
                                <td>
                                    <input type="text" name="dailyshopin_boxheading_font_size" value="<?php echo $dailyshopin_boxheading_font_size; ?>" size="6"  />
                                    <span class="customhelp"><?php echo $entry_title_font_size_help; ?></span>
                                </td>
                            </tr>
                        </table>
					</div>
            </div>

            </div><!--/fonts_settings-->
            
            
            
            
            
<!--*******************************************************************************************
***********************************************************************************************


 Buttons 
 
 
 **********************************************************************************************
 *******************************************************************************************-->
 
            <div id="buttons_settings" class="divtab">

                <table class="form">
                	<!--** Add to cart **-->
                    <tr>
                        <td><?php echo $entry_dailyshopin_details_addtocart; ?></td>
                        <td><input type="text" name="dailyshopin_details_addtocart" value="<?php echo $dailyshopin_details_addtocart; ?>" size="6" class="color {required:false,hash:true}"  /></td>
                    </tr>
                    <!--** Product Details Add to cart : hover **-->
                    <tr>
                        <td><?php echo $entry_dailyshopin_details_addtocart_hover; ?></td>
                        <td><input type="text" name="dailyshopin_details_addtocart_hover" value="<?php echo $dailyshopin_details_addtocart_hover; ?>" size="6" class="color {required:false,hash:true}"  /></td>
                    </tr>
                    <!--** Add to cart - color **-->
                    <tr>
                        <td><?php echo $entry_dailyshopin_details_button_color; ?></td>
                        <td><input type="text" name="dailyshopin_details_button_color" value="<?php echo $dailyshopin_details_button_color; ?>" size="6" class="color {required:false,hash:true}"  />
                        </td>
                    </tr>
                    <!--** All other Add to cart **-->
                    <tr>
                        <td><?php echo $entry_dailyshopin_all_addtocart; ?></td>
                        <td><input type="text" name="dailyshopin_all_addtocart" value="<?php echo $dailyshopin_all_addtocart; ?>" size="6" class="color {required:false,hash:true}"  /><span class="customhelp"><?php echo $entry_all_addtocart_button_help; ?></span></td>
                    </tr>          
                    <!--** All other Add to cart : hover **-->
                    <tr>
                        <td><?php echo $entry_dailyshopin_all_addtocart_hover; ?></td>
                        <td><input type="text" name="dailyshopin_all_addtocart_hover" value="<?php echo $dailyshopin_all_addtocart_hover; ?>" size="6" class="color {required:false,hash:true}"  /><span class="customhelp"><?php echo $entry_all_addtocart_button_hover_help; ?></span></td>
                    </tr>                  
                    <!--** All other buttons color **-->
                    <tr>
                        <td><?php echo $entry_dailyshopin_all_buttons_color; ?></td>
                        <td><input type="text" name="dailyshopin_all_buttons_color" value="<?php echo $dailyshopin_all_buttons_color; ?>" size="6" class="color {required:false,hash:true}"  /><span class="customhelp"><?php echo $entry_dailyshopin_all_buttons_color_help; ?></span></td>
                    </tr>
                </table>
            
            </div><!--/buttons_settings-->
            
            
            
			
            
<!--*******************************************************************************************
***********************************************************************************************


  Custom Settings 
 
 
 **********************************************************************************************
 *******************************************************************************************-->
 
			<div id="footer2_settings" class="divtab">
			
            
            	<ul class="tabs-head tabs">
                	<li><a href="#timer"><?php echo $entry_dailyshopin_timer; ?></a></li>
                    <li><a href="#firstblock"><?php echo $entry_dailyshopin_firstblock; ?></a></li>
                    <li><a href="#secondblock"><?php echo $entry_dailyshopin_secondblock; ?></a></li>
                    <li><a href="#thirdblock"><?php echo $entry_dailyshopin_thirdblock; ?></a></li>
                    <li><a href="#fourthblock"><?php echo $entry_dailyshopin_fourthblock; ?></a></li>
                    <li><a href="#contactWidget"><?php echo $entry_dailyshopin_contact; ?></a></li>
                    <li><a href="#customWidget"><?php echo $entry_dailyshopin_custom; ?></a></li>
                    <li><a href="#socialWidgets"><?php echo $entry_dailyshopin_entry_widgets; ?></a></li>
                    <li><a href="#socialIcons"><?php echo $entry_dailyshopin_entry_socialIcons; ?></a></li>
                    <li><a href="#payment"><?php echo $entry_dailyshopin_payment_text; ?></a></li>
                    <li><a href="#others"><?php echo $entry_dailyshopin_others; ?></a></li>
                    <li><a href="#customcss"><?php echo $entry_custom_css; ?></a></li>
                </ul>
            
            	<div class="tab_container">
                	<!--///////////////////////////////////////////////////////////////////////////////// -->
                    <div id="timer" class="tab_content">
                    	<table class="form">
                        	<tr>   
                                <td><input type="text" name="dailyshopin_time_text" value="<?php echo $dailyshopin_time_text; ?>" /></td>
                            </tr>
                        </table>
                    </div>
                    
                    <!--///////////////////////////////////////////////////////////////////////////////// -->
                    <div id="firstblock" class="tab_content">
                        <table class="form">
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_firstblock_img" value="<?php echo $dailyshopin_firstblock_img; ?>" id="dailyshopin_firstblock_img" />
                                    <img src="<?php echo $dailyshopin_firstblock_preview; ?>" alt="" id="dailyshopin_firstblock_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_firstblock_img', 'dailyshopin_firstblock_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_firstblock_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_firstblock_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_firstblock_title; ?></td>
                                <td><input type="text" name="dailyshopin_firstblock_title" value="<?php echo $dailyshopin_firstblock_title; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_firstblock_desc; ?></td>
                                <td><input type="text" name="dailyshopin_firstblock_desc" value="<?php echo $dailyshopin_firstblock_desc; ?>" /></td>
                            </tr>
                        </table>
                    </div>
                    <!--///////////////////////////////////////////////////////////////////////////////// -->
                    <div id="secondblock" class="tab_content">
                        <table class="form">
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_secondblock_img" value="<?php echo $dailyshopin_secondblock_img; ?>" id="dailyshopin_secondblock_img" />
                                    <img src="<?php echo $dailyshopin_secondblock_preview; ?>" alt="" id="dailyshopin_secondblock_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_secondblock_img', 'dailyshopin_secondblock_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_secondblock_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_secondblock_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_secondblock_title; ?></td>
                                <td><input type="text" name="dailyshopin_secondblock_title" value="<?php echo $dailyshopin_secondblock_title; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_secondblock_desc; ?></td>
                                <td><input type="text" name="dailyshopin_secondblock_desc" value="<?php echo $dailyshopin_secondblock_desc; ?>" /></td>
                            </tr>
                        </table>
                    </div>
                    <!--///////////////////////////////////////////////////////////////////////////////// -->
                    <div id="thirdblock" class="tab_content">
                        <table class="form">
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_thirdblock_img" value="<?php echo $dailyshopin_thirdblock_img; ?>" id="dailyshopin_thirdblock_img" />
                                    <img src="<?php echo $dailyshopin_thirdblock_preview; ?>" alt="" id="dailyshopin_thirdblock_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_thirdblock_img', 'dailyshopin_thirdblock_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_thirdblock_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_thirdblock_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_thirdblock_title; ?></td>
                                <td><input type="text" name="dailyshopin_thirdblock_title" value="<?php echo $dailyshopin_thirdblock_title; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_thirdblock_desc; ?></td>
                                <td><input type="text" name="dailyshopin_thirdblock_desc" value="<?php echo $dailyshopin_thirdblock_desc; ?>" /></td>
                            </tr>
                        </table>
                    </div>
                    <!--///////////////////////////////////////////////////////////////////////////////// -->
                    <div id="fourthblock" class="tab_content">
                        <table class="form">
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_fourthblock_img" value="<?php echo $dailyshopin_fourthblock_img; ?>" id="dailyshopin_fourthblock_img" />
                                    <img src="<?php echo $dailyshopin_fourthblock_preview; ?>" alt="" id="dailyshopin_fourthblock_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_fourthblock_img', 'dailyshopin_fourthblock_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_fourthblock_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_fourthblock_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_fourthblock_title; ?></td>
                                <td><input type="text" name="dailyshopin_fourthblock_title" value="<?php echo $dailyshopin_fourthblock_title; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_fourthblock_desc; ?></td>
                                <td><input type="text" name="dailyshopin_fourthblock_desc" value="<?php echo $dailyshopin_fourthblock_desc; ?>" /></td>
                            </tr>
                        </table>
                    </div>
                    <!--///////////////////////////////////////////////////////////////////////////////// -->
                    <div id="contactWidget" class="tab_content">
                        <table class="form">
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_contact_img" value="<?php echo $dailyshopin_contact_img; ?>" id="dailyshopin_contact_img" />
                                    <img src="<?php echo $dailyshopin_contact_preview; ?>" alt="" id="dailyshopin_contact_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_contact_img', 'dailyshopin_contact_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_contact_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_contact_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_address; ?></td>
                                <td><input type="text" name="dailyshopin_address" value="<?php echo $dailyshopin_address; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_phone; ?></td>
                                <td><input type="text" name="dailyshopin_phone" value="<?php echo $dailyshopin_phone; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_phone_second; ?></td>
                                <td><input type="text" name="dailyshopin_phone_second" value="<?php echo $dailyshopin_phone_second; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_email; ?></td>
                                <td><input type="text" name="dailyshopin_email" value="<?php echo $dailyshopin_email; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_dailyshopin_email_second; ?></td>
                                <td><input type="text" name="dailyshopin_email_second" value="<?php echo $dailyshopin_email_second; ?>" /></td>
                            </tr>
                        </table>
                    </div>
                    <!--///////////////////////////////////////////////////////////////////////////////// -->
                    <div id="customWidget" class="tab_content">
                        <table class="form">
                        	<tr>   
                                <td><?php echo $entry_widget_title; ?></td>
                                <td><input type="text" name="dailyshopin_custom_widget_title" value="<?php echo $dailyshopin_custom_widget_title; ?>" /></td>
                            </tr>
                            <tr>   
                                <td><?php echo $entry_dailyshopin_footer_info_text; ?></td>
                                <td><textarea cols="50" rows="5" name="dailyshopin_footer_info_text"><?php echo $dailyshopin_footer_info_text; ?></textarea></td>
                            </tr>
                            <tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_cus_img" value="<?php echo $dailyshopin_cus_img; ?>" id="dailyshopin_cus_img" />
                                    <img src="<?php echo $dailyshopin_cus_preview; ?>" alt="" id="dailyshopin_cus_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_cus_img', 'dailyshopin_cus_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_cus_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_cus_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                            </tr>
                            <tr>   
                                <td><?php echo $entry_dailyshopin_shipping_text; ?></td>
                                <td><input type="text" name="dailyshopin_shipping_text" value="<?php echo $dailyshopin_shipping_text; ?>" /></td>
                            </tr>
                            
                            <tr>   
                                <td><?php echo $entry_dailyshopin_shipping_last_text; ?></td>
                                <td><input type="text" name="dailyshopin_shipping_last_text" value="<?php echo $dailyshopin_shipping_last_text; ?>" /></td>
                            </tr>
                        </table>
                    </div>
                    <!--///////////////////////////////////////////////////////////////////////////////// -->
                    <div id="socialWidgets" class="tab_content">
                        <table class="form">
                        	<tr>
                                <td><?php echo $entry_facebook_id; ?></td>
                                <td><input type="text" name="dailyshopin_facebook_id" value="<?php echo $dailyshopin_facebook_id; ?>" /></td>
                            </tr>
                        </table>
                        <table class="form">
                        	<tr>
                            	<p style="font-size: 14px; font-weight: bold; color: #333; line-height: 19px;"><?php echo $entry_twitter_info; ?></p>
                            </tr>
                            <tr>
                                <td><?php echo $entry_twitter_username; ?></td>
                                <td><input type="text" name="dailyshopin_twitter_username" value="<?php echo $dailyshopin_twitter_username; ?>" /></td>
                            </tr> 
                            
                            <tr>
                                <td><?php echo $entry_twitter_consumer_key; ?></td>
                                <td><input type="text" name="consumer_keyy" value="<?php echo $consumer_keyy; ?>" /></td>
                            </tr> 
                            
                            <tr>
                                <td><?php echo $entry_twitter_consumer_secret; ?></td>
                                <td><input type="text" name="consumer_secrett" value="<?php echo $consumer_secrett; ?>" /></td>
                            </tr> 
                            
                            <tr>
                                <td><?php echo $entry_twitter_access_token; ?></td>
                                <td><input type="text" name="access_token" value="<?php echo $access_token; ?>" /></td>
                            </tr> 
                            
                            <tr>
                                <td><?php echo $entry_twitter_token_secret; ?></td>
                                <td><input type="text" name="token_secret" value="<?php echo $token_secret; ?>" /></td>
                            </tr> 
                        </table>
                    </div>
                    <!--///////////////////////////////////////////////////////////////////////////////// -->
                    <div id="socialIcons" class="tab_content">
                        <table class="form">
                        	<tr>
                                <td><?php echo $entry_facebook_link; ?></td>
                                <td><input type="text" name="dailyshopin_facebook_link" value="<?php echo $dailyshopin_facebook_link; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_twitter_link; ?></td>
                                <td><input type="text" name="dailyshopin_twitter_link" value="<?php echo $dailyshopin_twitter_link; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_google_link; ?></td>
                                <td><input type="text" name="dailyshopin_google_link" value="<?php echo $dailyshopin_google_link; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_youtube_link; ?></td>
                                <td><input type="text" name="dailyshopin_youtube_link" value="<?php echo $dailyshopin_youtube_link; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_flickr_link; ?></td>
                                <td><input type="text" name="dailyshopin_flickr_link" value="<?php echo $dailyshopin_flickr_link; ?>" /></td>
                            </tr>
                            
                             <tr>
                                <td><?php echo $entry_vimeo_link; ?></td>
                                <td><input type="text" name="dailyshopin_vimeo_link" value="<?php echo $dailyshopin_vimeo_link; ?>" /></td>
                            </tr>
                            
                             <tr>
                                <td><?php echo $entry_linkedin_link; ?></td>
                                <td><input type="text" name="dailyshopin_linkedin_link" value="<?php echo $dailyshopin_linkedin_link; ?>" /></td>
                            </tr>
                            
                             <tr>
                                <td><?php echo $entry_digg_link; ?></td>
                                <td><input type="text" name="dailyshopin_digg_link" value="<?php echo $dailyshopin_digg_link; ?>" /></td>
                            </tr>
                            
                             <tr>
                                <td><?php echo $entry_dribbble_link; ?></td>
                                <td><input type="text" name="dailyshopin_dribbble_link" value="<?php echo $dailyshopin_dribbble_link; ?>" /></td>
                            </tr>
                            
                             <tr>
                                <td><?php echo $entry_pinterest_link; ?></td>
                                <td><input type="text" name="dailyshopin_pinterest_link" value="<?php echo $dailyshopin_pinterest_link; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_skype_link; ?></td>
                                <td><input type="text" name="dailyshopin_skype_link" value="<?php echo $dailyshopin_skype_link; ?>" /></td>
                            </tr>
                            
                            <tr>
                                <td><?php echo $entry_rss_link; ?></td>
                                <td><input type="text" name="dailyshopin_rss_link" value="<?php echo $dailyshopin_rss_link; ?>" /></td>
                            </tr>
                        </table>
                    </div>
                    <!--/////////////////////////////////////////////////////////////////////////////////-->
                    <div id="payment" class="tab_content">
                    	<table class="form">
                        
                        	<!--============================================== pay 1 -->
                        	
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_pay1_img" value="<?php echo $dailyshopin_pay1_img; ?>" id="dailyshopin_pay1_img" />
                                    <img src="<?php echo $dailyshopin_pay1_preview; ?>" alt="" id="dailyshopin_pay1_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_pay1_img', 'dailyshopin_pay1_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_pay1_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_pay1_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                                <td><?php echo $entry_dailyshopin_pay_link; ?></td>
                                <td><input type="text" name="dailyshopin_pay1_link" value="<?php echo $dailyshopin_pay1_link; ?>" /></td>
                            </tr>
                            
                            <!--============================================== pay 2 -->
                        	
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_pay2_img" value="<?php echo $dailyshopin_pay2_img; ?>" id="dailyshopin_pay2_img" />
                                    <img src="<?php echo $dailyshopin_pay2_preview; ?>" alt="" id="dailyshopin_pay2_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_pay2_img', 'dailyshopin_pay2_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_pay2_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_pay2_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                                <td><?php echo $entry_dailyshopin_pay_link; ?></td>
                                <td><input type="text" name="dailyshopin_pay2_link" value="<?php echo $dailyshopin_pay2_link; ?>" /></td>
                            </tr>
                            
                            <!--============================================== pay 3 -->
                        	
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_pay3_img" value="<?php echo $dailyshopin_pay3_img; ?>" id="dailyshopin_pay3_img" />
                                    <img src="<?php echo $dailyshopin_pay3_preview; ?>" alt="" id="dailyshopin_pay3_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_pay3_img', 'dailyshopin_pay3_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_pay3_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_pay3_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                                <td><?php echo $entry_dailyshopin_pay_link; ?></td>
                                <td><input type="text" name="dailyshopin_pay3_link" value="<?php echo $dailyshopin_pay3_link; ?>" /></td>
                            </tr>
                            
                            <!--============================================== pay 4 -->
                        	
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_pay4_img" value="<?php echo $dailyshopin_pay4_img; ?>" id="dailyshopin_pay4_img" />
                                    <img src="<?php echo $dailyshopin_pay4_preview; ?>" alt="" id="dailyshopin_pay4_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_pay4_img', 'dailyshopin_pay4_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_pay4_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_pay4_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                                <td><?php echo $entry_dailyshopin_pay_link; ?></td>
                                <td><input type="text" name="dailyshopin_pay4_link" value="<?php echo $dailyshopin_pay4_link; ?>" /></td>
                            </tr>
                            
                            <!--============================================== pay 5 -->
                        	
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_pay5_img" value="<?php echo $dailyshopin_pay5_img; ?>" id="dailyshopin_pay5_img" />
                                    <img src="<?php echo $dailyshopin_pay5_preview; ?>" alt="" id="dailyshopin_pay5_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_pay5_img', 'dailyshopin_pay5_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_pay5_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_pay5_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                                <td><?php echo $entry_dailyshopin_pay_link; ?></td>
                                <td><input type="text" name="dailyshopin_pay5_link" value="<?php echo $dailyshopin_pay5_link; ?>" /></td>
                            </tr>
                            
                            <!--============================================== pay 6 -->
                        	
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_pay6_img" value="<?php echo $dailyshopin_pay6_img; ?>" id="dailyshopin_pay6_img" />
                                    <img src="<?php echo $dailyshopin_pay6_preview; ?>" alt="" id="dailyshopin_pay6_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_pay6_img', 'dailyshopin_pay6_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_pay6_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_pay6_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                                <td><?php echo $entry_dailyshopin_pay_link; ?></td>
                                <td><input type="text" name="dailyshopin_pay6_link" value="<?php echo $dailyshopin_pay6_link; ?>" /></td>
                            </tr>
                            
                            <!--============================================== pay 7 -->
                        	
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_pay7_img" value="<?php echo $dailyshopin_pay7_img; ?>" id="dailyshopin_pay7_img" />
                                    <img src="<?php echo $dailyshopin_pay7_preview; ?>" alt="" id="dailyshopin_pay7_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_pay7_img', 'dailyshopin_pay7_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_pay7_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_pay7_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                                <td><?php echo $entry_dailyshopin_pay_link; ?></td>
                                <td><input type="text" name="dailyshopin_pay7_link" value="<?php echo $dailyshopin_pay7_link; ?>" /></td>
                            </tr>
                            
                            <!--============================================== pay 8 -->
                        	
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_pay8_img" value="<?php echo $dailyshopin_pay8_img; ?>" id="dailyshopin_pay8_img" />
                                    <img src="<?php echo $dailyshopin_pay8_preview; ?>" alt="" id="dailyshopin_pay8_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_pay8_img', 'dailyshopin_pay8_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_pay8_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_pay8_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                                <td><?php echo $entry_dailyshopin_pay_link; ?></td>
                                <td><input type="text" name="dailyshopin_pay8_link" value="<?php echo $dailyshopin_pay8_link; ?>" /></td>
                            </tr>
                            
                            <!--============================================== pay 9 -->
                        	
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_pay9_img" value="<?php echo $dailyshopin_pay9_img; ?>" id="dailyshopin_pay9_img" />
                                    <img src="<?php echo $dailyshopin_pay9_preview; ?>" alt="" id="dailyshopin_pay9_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_pay9_img', 'dailyshopin_pay9_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_pay9_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_pay9_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                                <td><?php echo $entry_dailyshopin_pay_link; ?></td>
                                <td><input type="text" name="dailyshopin_pay9_link" value="<?php echo $dailyshopin_pay9_link; ?>" /></td>
                            </tr>
                            
                            <!--============================================== pay 10 -->
                        	
                        	<tr>   
                                <td>
                                    <?php echo $entry_custom_image; ?><br>
                                </td>
                                <td>
                                    <input type="hidden" name="dailyshopin_pay10_img" value="<?php echo $dailyshopin_pay10_img; ?>" id="dailyshopin_pay10_img" />
                                    <img src="<?php echo $dailyshopin_pay10_preview; ?>" alt="" id="dailyshopin_pay10_preview" />
                                    <br /><a onclick="image_upload('dailyshopin_pay10_img', 'dailyshopin_pay10_preview');"><?php echo $text_select; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#dailyshopin_pay10_preview').attr('src', '<?php echo $no_image; ?>'); $('#dailyshopin_pay10_img').attr('value', '');"><?php echo $text_clear; ?></a>
                                </td>
                                <td><?php echo $entry_dailyshopin_pay_link; ?></td>
                                <td><input type="text" name="dailyshopin_pay10_link" value="<?php echo $dailyshopin_pay10_link; ?>" /></td>
                            </tr>
                        </table> 
                        
                        
                    </div>
                    <!--///////////////////////////////////////////////////////////////////////////////// -->
                    <div id="others" class="tab_content">
                        <table class="form">
                        	<tr>
                                <td><?php echo $entry_option_slideshow; ?></td>
                                <td><input type="text" name="option_slideshow" value="<?php echo $option_slideshow; ?>" /></td>
                                <td><span class="customhelp"><?php echo $entry_slideshow_help; ?></span></td>
                                <td><span class="customhelp"><?php echo $entry_slideshow_values_help; ?></span></td>
                            </tr>
                        </table>
                    </div>
                    <!--///////////////////////////////////////////////////////////////////////////////// -->
                    <div id="customcss" class="tab_content">
                        <table class="form">
                        	<tr>
                            <td>
                            <textarea name="custom_css" cols="150" rows="50" style="border-radius: 5px 5px 5px 5px; border-color: #fff; padding: 20px; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;"><?php echo $custom_css; ?>
                            </textarea>
                            </td>
                            </tr>
                        </table>
                    </div>
                </div><!--/tab_container-->

			</div><!--/footer_settings-->
	   </div><!--/data_right-->
	</form>
		
	</div>
    </div>
</div><!--back_ptrn-->
</div>

<?php echo $footer; ?>

<script>
// Tabs
//---------------------------------------------
/* <![CDATA[ */
$(document).ready(function(){
	$(".tab_content").hide();
$("ul.tabs").each(function() {
    $(this).find('li:first').addClass("active");
    $(this).next('.tab_container').find('.tab_content:first').show();
});

$("ul.tabs li a").click(function() {
    var cTab = $(this).closest('li');
    cTab.siblings('li').removeClass("active");
    cTab.addClass("active");
    cTab.closest('ul.tabs').nextAll('.tab_container:first').find('.tab_content').hide(); 

    var activeTab = $(this).attr("href"); //Find the href attribute value to identify the active tab + content
    $(activeTab).fadeIn(); //Fade in the active ID content
    return false;
});
});
/* ]]> */
</script>

<script type="text/javascript">

	$('#settings_tabs a').tabs(); 
	$('#footer_settings_tabs a').tabs();

</script>

<script type="text/javascript" src="view/javascript/jscolor/jscolor.js"></script> 

<script type="text/javascript"><!--

$(document).ready(function() {

	$('	#dailyshopin_background_color').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});
	 });

//--></script>

<script type="text/javascript"><!--
function image_upload(field, preview) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
					dataType: 'text',
					success: function(data) {
						$('#' + preview).replaceWith('<img src="' + data + '" alt="" id="' + preview + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 700,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script> 

<script>

</script>