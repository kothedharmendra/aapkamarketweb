<?php echo $header; ?>
<script>
  function get_activ(){
    var sid = 0;
    $('.htabs a').each(function(i, element) { 
      if ($(element).hasClass("selected")) {
        sid = i;
        } 
    });
    return sid+1;
  }
  function get_count(){
    var scount = $('.view_'+get_activ()).length;
    return scount+1;
  }
</script>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons">
      <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
      <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
    </div>
  </div>
 <div class="content">
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
  <div class="content">
        <div id="tabs" class="htabs">
          <?php $module_row = 1; ?>
          <?php if (isset($modules['feedback'])) { foreach ($modules['feedback'] as $module) { ?>
          <a href="#tab-module-<?php echo $module_row; ?>" id="module-<?php echo $module_row; ?>"><?php echo $tab_module . ' ' . $module_row; ?>&nbsp;<img src="view/image/delete.png" alt="" onclick="$('.htabs a:first').trigger('click'); $('#module-<?php echo $module_row; ?>').remove(); $('#tab-module-<?php echo $module_row; ?>').remove(); return false;" style="margin-bottom:-3px;margin-left:6px;" /></a>
          <?php $module_row++; ?>
          <?php } } ?>
          <span id="module-add"><?php echo $button_add_module; ?>&nbsp;<img src="view/image/add.png" alt="" onclick="addModule();" style="margin-bottom:-3px;margin-left:6px;" /></span> 
        </div>
        <?php $module_row = 1; ?>
        <?php if (!empty($modules['feedback'])) { foreach ($modules['feedback'] as $module) { ?>
        <div id="tab-module-<?php echo $module_row; ?>" class="htabs-content">
        <table class="form">
            <tr>
              <td><?php echo $text_namemodule; ?> <a class="faq" faq="<?php echo $text_namemodulefaq; ?>">[?]</a></td>
              <td><input type="text" size="45" name="feedback[<?php echo $module_row; ?>][heading]" value="<?=(!empty($module['heading']))?$module['heading']:'';?>" /></td>
            </tr>
            <tr>
              <td><?php echo $text_nameemail; ?> <a class="faq" faq="<?php echo $text_nameemailfaq; ?>">[?]</a></td>
              <td><input type="text" size="45" name="feedback[<?php echo $module_row; ?>][adminemail]" value="<?=(!empty($module['heading']))?$module['adminemail']:'';?>" /></td>
            </tr>

            <table class="list" id="tablefield">
              <thead>
                <tr>
                  <td class="left"><?php echo $text_namefield; ?> <a class="faq" faq="<?php echo $text_namefieldfaq; ?>">[?]</a></td>
                  <td style="text-align:center;"><?php echo $text_nameonlytext; ?> <a class="faq" faq="<?php echo $text_nameonlytextfaq; ?>">[?]</a></td>
                  <td style="text-align:center;"><?php echo $text_namedatatime; ?> <a class="faq" faq="<?php echo $text_namedatatimefaq; ?>">[?]</a></td>
                  <td style="text-align:center;"><?php echo $text_namerequired; ?> <a class="faq" faq="<?php echo $text_namerequiredfaq; ?>">[?]</a></td>
                  <td style="text-align:center;"><?php echo $text_namecomment; ?> <a class="faq" faq="<?php echo $text_namecommentfaq; ?>">[?]</a></td>
                  <td style="text-align:center;"><?php echo $text_namemainfield; ?> <a class="faq" faq="<?php echo $text_namemainfieldfaq; ?>">[?]</a></td>
                  <td style="text-align:center;"><?php echo $text_nameaction; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php $module_row2 = $module_row; $fieldscount = 1; if(!empty($module['filds'])) { foreach($module['filds'] as $filed) { ?>
                 <tr id="tr-<?php echo $module_row2; ?>" class="view_<?=$module_row?>">
                  <td class="left"><input type="text" size="30" name="feedback[<?php echo $module_row2; ?>][filds][<?=$fieldscount?>][fieldname]" value="<?=(!empty($filed['fieldname']))?$filed['fieldname']:'';?>" />
                    <a id="linktext_<?php echo $fieldscount; ?><?php echo $module_row2; ?>" onclick="showhide(<?php echo $fieldscount; ?><?php echo $module_row2; ?>);" ><span id="img_1_<?php echo $fieldscount; ?><?php echo $module_row2; ?>" class="button" ><?php echo $text_addmask; ?></span></a>
                    <div id="show_hide" class="sh_<?php echo $fieldscount; ?><?php echo $module_row2; ?>">
                      <input type="text" size="30" name="feedback[<?php echo $module_row2; ?>][filds][<?=$fieldscount?>][mask]" value="<?=(!empty($filed['mask']))?$filed['mask']:'';?>"/>
                      <a class="faq" faq="<?php echo $text_maskfaq; ?>">[?]</a>
                    </div>
               
                  </td>
                  <td style="text-align:center;"><input type="checkbox" name="feedback[<?php echo $module_row2; ?>][filds][<?=$fieldscount?>][onlytext]" value="1" <?=(!empty($filed['onlytext']))?'checked':'';?>/></td>
                  <td style="text-align:center;"><input type="checkbox" name="feedback[<?php echo $module_row2; ?>][filds][<?=$fieldscount?>][time]" value="1" <?=(!empty($filed['time']))?'checked':'';?>/></td>
                  <td style="text-align:center;"><input type="checkbox" name="feedback[<?php echo $module_row2; ?>][filds][<?=$fieldscount?>][requaried]" value="1" <?=(!empty($filed['requaried']))?'checked':'';?> /></td>
                  <td style="text-align:center;"><input type="checkbox" name="feedback[<?php echo $module_row2; ?>][filds][<?=$fieldscount?>][comment]" value="1" <?=(!empty($filed['comment']))?'checked':'';?> /></td>
                  <td style="text-align:center;"><input type="radio" name="feedback[<?php echo $module_row2; ?>][main]" value="<?=$fieldscount?>" <?=( (!empty($module['main'])) AND ($module['main']==$fieldscount) )?'checked':'';?> /></td>
                  <td style="text-align:center;"><a onclick="$(this).parent().parent().remove(); return false;" class="button"><?php echo $text_dellfield; ?></a></td>
                </tr>
                <?php $fieldscount++; } }?>
                <?php $module_row2++; ?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="6"></td><td class="right" style="text-align:center;"><a onclick="addField(this);" class="button"><?php echo $text_addfield; ?></a></td>
                </tr>
              </tfoot>
            </table>
        </table>
        </div>
        <?php $module_row++; ?>
        <?php } } ?>
  </div>
  </form>
  </div>
</div>
</div>
<script type="text/javascript">
var addmask = '<?php echo $text_addmask; ?>';
var dellmask = '<?php echo $text_dellmask; ?>';
function showhide(fieldscount) {
  $('#linktext_' + fieldscount).click(function () {
    $('#img_1_' + fieldscount).toggle(function() {
      $('.sh_' + fieldscount).css('display','block');
      $('#img_1_' + fieldscount).html(dellmask);
    }, function() {
      $('.sh_' + fieldscount).css('display','none');
      $('#img_1_' + fieldscount).html(addmask);
    });
  });
}          
</script>
<script type="text/javascript"><!--
var module_row2 = get_activ();
var module_row =  <?=$module_row; ?>;
function addField(key) {  
var module_row2 = get_activ();
  html = '<tbody>';
  html = '<tr id="tr-'+module_row+'" class="view_'+module_row2+'">';
  html += '<td class="left"><input type="text" size="30" name="feedback['+module_row2+'][filds]['+get_count()+'][fieldname]" value="" />';
  html += ' <a id="linktext_'+get_count()+module_row2+'" onclick="showhide('+get_count()+module_row2+');" ><span id="img_1_'+get_count()+module_row2+'" class="button"><?php echo $text_addmask; ?></span></a>';            
    html += '<div id="show_hide" class="sh_'+get_count()+module_row2+'">';
    html += '<input type="text" size="30" name="feedback['+module_row2+'][filds]['+get_count()+'][mask]" value="<?=(!empty($filed['mask']))?$filed['mask']:'';?>"/>';
    html += ' <a class="faq" faq="<?php echo $text_maskfaq; ?>">[?]</a>';
    html += '</div>';
  html += '</td>';
  html += '<td style="text-align:center;"><input type="checkbox" name="feedback['+module_row2+'][filds]['+get_count()+'][onlytext]" value="1" /></td>';
  html += '<td style="text-align:center;"><input type="checkbox" name="feedback['+module_row2+'][filds]['+get_count()+'][time]" value="1" /></td>';
  html += '<td style="text-align:center;"><input type="checkbox" name="feedback['+module_row2+'][filds]['+get_count()+'][requaried]" value="1" /></td>';
    html += '<td style="text-align:center;"><input type="checkbox" name="feedback['+module_row2+'][filds]['+get_count()+'][comment]" value="1" /></td>';
  html += '<td style="text-align:center;"><input type="radio" name="feedback['+module_row2+'][main]" value="'+get_count()+'" /></td>';
  html += '<td style="text-align:center;"><a onclick="$(this).parent().parent().remove(); return false;" class="button"><?php echo $text_dellfield; ?></a></td>';
  html += '</tr>';
  html += '</tbody>';
  $(key).parent().parent().parent().before(html);
}
//--></script>
<script type="text/javascript"><!--          
function addModule() {  
  html  = '<div id="tab-module-' + module_row + '" class="htabs-content">';
  html  += '<table class="form">'
  html  += '<tr>';
  html += '<td><?php echo $text_namemodule; ?> <a class="faq" faq="<?php echo $text_namemodulefaq; ?>">[?]</a></td>';
  html += '<td><input type="text" size="45" name="feedback[' + module_row + '][heading]" value="<?php echo isset($module['heading']) ? $module['heading'] : ''; ?>" /></td>';
  html  += '</tr>'
  html  += '<tr>';
  html += '<td><?php echo $text_nameemail; ?> <a class="faq" faq="<?php echo $text_nameemailfaq; ?>">[?]</a></td>';
  html += '<td><input type="text" size="45" name="feedback[' + module_row + '][adminemail]" value="<?php echo isset($module['adminemail']) ? $module['adminemail'] : ''; ?>" /></td>';
  html  += '</tr>';

  html += '<table class="list" id="tablefield">';
  html += '<thead>';
  html  += '<tr>';
  html += '<td class="left"><?php echo $text_namefield; ?> <a class="faq" faq="<?php echo $text_namefieldfaq; ?>">[?]</a></td>';
  html += '<td style="text-align:center;"><?php echo $text_nameonlytext; ?> <a class="faq" faq="<?php echo $text_nameonlytextfaq; ?>">[?]</a></td>';
  html += '<td style="text-align:center;"><?php echo $text_namedatatime; ?> <a class="faq" faq="<?php echo $text_namedatatimefaq; ?>">[?]</a></td>';
  html += '<td style="text-align:center;"><?php echo $text_namerequired; ?> <a class="faq" faq="<?php echo $text_namerequiredfaq; ?>">[?]</a></td>';
   html += '<td style="text-align:center;"><?php echo $text_namecomment; ?> <a class="faq" faq="<?php echo $text_namecommentfaq; ?>">[?]</a></td>';
  html += '<td style="text-align:center;"><?php echo $text_namemainfield; ?> <a class="faq" faq="<?php echo $text_namemainfieldfaq; ?>">[?]</a></td>';
  html += '<td style="text-align:center;"><?php echo $text_nameaction; ?></td>';
  html  += '</tr>';
  html += '</thead>';
  html += '<tbody>';
  html  += '<tr id="tr-' + module_row2 + '" class="view_'+module_row+'" >';
  html += '<td class="left"><input type="text" size="30" name="feedback['+module_row+'][filds][1][fieldname]" value="" />';
  html += ' <a id="linktext_'+get_count()+module_row2+'" onclick="showhide('+get_count()+module_row2+');" ><span id="img_1_'+get_count()+module_row2+'" class="button"><?php echo $text_addmask; ?></span></a>';
    html += '<div id="show_hide" class="sh_'+get_count()+module_row2+'">';
    html += '<input type="text" size="30" name="feedback['+module_row+'][filds]['+get_count()+'][mask]" value="<?=(!empty($filed['mask']))?$filed['mask']:'';?>"/>';
    html += ' <a class="faq" faq="<?php echo $text_maskfaq; ?>">[?]</a>';
    html += '</div>';
  html += '</td>';
  html += '<td style="text-align:center;"><input type="checkbox" name="feedback['+module_row+'][filds][1][onlytext]" value="1" /></td>';
  html += '<td style="text-align:center;"><input type="checkbox" name="feedback['+module_row+'][filds][1][time]" value="1" /></td>';
  html += '<td style="text-align:center;"><input type="checkbox" name="feedback['+module_row+'][filds][1][requaried]" value="1" /></td>';
   html += '<td style="text-align:center;"><input type="checkbox" name="feedback['+module_row+'][filds][1][comment]" value="1" /></td>';
  html += '<td style="text-align:center;"><input type="radio" name="feedback['+module_row+'][main]" value="1" /></td>';
  html += '<td style="text-align:center;"><a onclick="$(\'#tr-'+module_row2+ ' :input \').attr(\'value\', \'\'); $(\'#tr-'+module_row2+'\').remove(); return false;" class="button"><?php echo $text_dellfield; ?></a></td>';
  html  += '</tr>'
  html += '</tbody>';
  html += '<tfoot>';
  html  += '<tr>';
  html += '<td colspan="6"></td><td class="right" style="text-align:center;"><a onclick="addField(this);" class="button"><?php echo $text_addfield; ?></a></td>';
  html  += '</tr>'
  html += '</tfoot>';         
  html += '</table>';
  html += '</table>';
  html += '</div>';
  $('#form').append(html);
  $('#module-add').before('<a href="#tab-module-' + module_row + '" id="module-' + module_row + '"><?php echo $tab_module; ?> ' + module_row + '&nbsp;<img src="view/image/delete.png" style="margin-bottom:-3px;margin-left:6px;" alt="" onclick="$(\'.htabs a:first\').trigger(\'click\'); $(\'#module-' + module_row + '\').remove(); $(\'#tab-module-' + module_row + '\').remove(); return false;" /></a>');
  $('.htabs a').tabs();
  $('#module-' + module_row).trigger('click');
  module_row++;
}
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
//--></script>
<style type="text/css">
.faq {color: #0F71C8 !important;position: relative;font-weight: bold;text-decoration: none;}
.faq:hover {color: #f00 !important;position: relative;}
.faq[faq]:hover:after {content: attr(faq);padding: 4px 8px;color: #fff;position: absolute;text-align: left;left: 2px;bottom: 15px;z-index: 20px;width: 200px;max-width: 250px;display: block;font-weight: bold;background: #000;}
#show_hide {display: none;}
#ocjoy-copyright {padding:15px 15px;border:1px solid #ccc;margin-top:15px;box-shadow:0 0px 5px rgba(0,0,0,0.1);}
.htabs span {border-top: 1px solid #DDDDDD;border-left: 1px solid #DDDDDD;border-right: 1px solid #DDDDDD;background:  url('../image/tab.png') repeat-x;padding: 7px 15px 6px 15px;float: left;font-family: Arial, Helvetica, sans-serif;
  font-size: 13px;font-weight: bold;text-align: center;text-decoration: none;color: #000000;margin-right: 2px;}
span.button {background: none repeat scroll 0 0 #003A88;border-radius: 10px 10px 10px 10px;color: #FFFFFF;display: inline-block;padding: 5px 15px;text-decoration: none;}
</style>
<?php echo $footer; ?>