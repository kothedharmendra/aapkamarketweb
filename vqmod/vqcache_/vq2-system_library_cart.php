<?php
class Cart {
	private $config;
	private $db;
	private $data = array();
	
  	public function __construct($registry) {
            $this->config = $registry->get('config');
            $this->customer = $registry->get('customer');
            $this->session = $registry->get('session');
            $this->db = $registry->get('db');
            $this->tax = $registry->get('tax');
            $this->weight = $registry->get('weight');

            if (!isset($this->session->data['cart']) || !is_array($this->session->data['cart'])) {
                $this->session->data['cart'] = array();
            }
	}
	      
  	public function getProducts() {
		if (!$this->data) {
			foreach ($this->session->data['cart'] as $key => $quantity) {
				$product = explode(':', $key);
				$product_id = $product[0];
				$stock = true;
	
                                // Options
				if (!empty($product[1])) {
                                	$options =  unserialize(base64_decode($product[1]));
				} else {
					$options = array();
				} 
				
				$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.date_available <= NOW() AND p.status = '1'");
				
				if ($product_query->num_rows) {
					$option_price = 0;

      				$option_cost = 0;
            
					$option_points = 0;
					$option_weight = 0;
	
					$option_data = array();
	
					foreach ($options as $product_option_id => $option_value) {
						$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");
						
						if ($option_query->num_rows) {
							if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
								$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price
, pov.price_prefix, pov.cost, pov.cost_prefix
            , pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$option_value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
								
								if ($option_value_query->num_rows) {
									if ($option_value_query->row['price_prefix'] == '+') {
										$option_price += $option_value_query->row['price'];
									} elseif ($option_value_query->row['price_prefix'] == '-') {
										$option_price -= $option_value_query->row['price'];
									}
	

									if ($option_value_query->row['cost_prefix'] == '+') {
										$option_cost += $option_value_query->row['cost'];
									} elseif ($option_value_query->row['cost_prefix'] == '-') {
										$option_cost -= $option_value_query->row['cost'];
									}
            
									if ($option_value_query->row['points_prefix'] == '+') {
										$option_points += $option_value_query->row['points'];
									} elseif ($option_value_query->row['points_prefix'] == '-') {
										$option_points -= $option_value_query->row['points'];
									}
																
									if ($option_value_query->row['weight_prefix'] == '+') {
										$option_weight += $option_value_query->row['weight'];
									} elseif ($option_value_query->row['weight_prefix'] == '-') {
										$option_weight -= $option_value_query->row['weight'];
									}
									
									if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
										$stock = false;
									}
									
									$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => $option_value,
										'option_id'               => $option_query->row['option_id'],
										'option_value_id'         => $option_value_query->row['option_value_id'],
										'name'                    => $option_query->row['name'],
										'option_value'            => $option_value_query->row['name'],
										'type'                    => $option_query->row['type'],
										'quantity'                => $option_value_query->row['quantity'],
										'subtract'                => $option_value_query->row['subtract'],
										'price'                   => $option_value_query->row['price'],
										'price_prefix'            => $option_value_query->row['price_prefix'],

										'cost'                    => $option_value_query->row['cost'],
										'cost_prefix'             => $option_value_query->row['cost_prefix'],
            
										'points'                  => $option_value_query->row['points'],
										'points_prefix'           => $option_value_query->row['points_prefix'],									
										'weight'                  => $option_value_query->row['weight'],
										'weight_prefix'           => $option_value_query->row['weight_prefix']
									);								
								}
							} elseif ($option_query->row['type'] == 'checkbox' && is_array($option_value)) {
								foreach ($option_value as $product_option_value_id) {
									$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price
, pov.price_prefix, pov.cost, pov.cost_prefix
            , pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
									
									if ($option_value_query->num_rows) {
										if ($option_value_query->row['price_prefix'] == '+') {
											$option_price += $option_value_query->row['price'];
										} elseif ($option_value_query->row['price_prefix'] == '-') {
											$option_price -= $option_value_query->row['price'];
										}
	

									if ($option_value_query->row['cost_prefix'] == '+') {
										$option_cost += $option_value_query->row['cost'];
									} elseif ($option_value_query->row['cost_prefix'] == '-') {
										$option_cost -= $option_value_query->row['cost'];
									}
            
										if ($option_value_query->row['points_prefix'] == '+') {
											$option_points += $option_value_query->row['points'];
										} elseif ($option_value_query->row['points_prefix'] == '-') {
											$option_points -= $option_value_query->row['points'];
										}
																	
										if ($option_value_query->row['weight_prefix'] == '+') {
											$option_weight += $option_value_query->row['weight'];
										} elseif ($option_value_query->row['weight_prefix'] == '-') {
											$option_weight -= $option_value_query->row['weight'];
										}
										
										if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
											$stock = false;
										}
										
										$option_data[] = array(
											'product_option_id'       => $product_option_id,
											'product_option_value_id' => $product_option_value_id,
											'option_id'               => $option_query->row['option_id'],
											'option_value_id'         => $option_value_query->row['option_value_id'],
											'name'                    => $option_query->row['name'],
											'option_value'            => $option_value_query->row['name'],
											'type'                    => $option_query->row['type'],
											'quantity'                => $option_value_query->row['quantity'],
											'subtract'                => $option_value_query->row['subtract'],
											'price'                   => $option_value_query->row['price'],
											'price_prefix'            => $option_value_query->row['price_prefix'],

										'cost'                    => $option_value_query->row['cost'],
										'cost_prefix'             => $option_value_query->row['cost_prefix'],
            
											'points'                  => $option_value_query->row['points'],
											'points_prefix'           => $option_value_query->row['points_prefix'],
											'weight'                  => $option_value_query->row['weight'],
											'weight_prefix'           => $option_value_query->row['weight_prefix']
										);								
									}
								}						
							} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
								$option_data[] = array(
									'product_option_id'       => $product_option_id,
									'product_option_value_id' => '',
									'option_id'               => $option_query->row['option_id'],
									'option_value_id'         => '',
									'name'                    => $option_query->row['name'],
									'option_value'            => $option_value,
									'type'                    => $option_query->row['type'],
									'quantity'                => '',
									'subtract'                => '',
									'price'                   => '',
									'price_prefix'            => '',

									'cost'                    => '',
									'cost_prefix'             => '',
            
									'points'                  => '',
									'points_prefix'           => '',								
									'weight'                  => '',
									'weight_prefix'           => ''
								);						
							}
						}
					} 
				
					if ($this->customer->isLogged()) {
						$customer_group_id = $this->customer->getCustomerGroupId();
					} else {
						$customer_group_id = $this->config->get('config_customer_group_id');
					}
					
					$price = $product_query->row['price'];

					$cost = $product_query->row['cost'];
            
					
					// Code Changes  - Starts Here
                                        $price_real = $product_query->row['price'];
                                        $price_real_tax = $this->tax->calculate($product_query->row['price'], $product_query->row['tax_class_id'], $this->config->get('config_tax'));       
                                       // Code Changes Ends Here
					
					// Product Discounts
					$discount_quantity = 0;
					
					foreach ($this->session->data['cart'] as $key_2 => $quantity_2) {
						$product_2 = explode(':', $key_2);
						
						if ($product_2[0] == $product_id) {
							$discount_quantity += $quantity_2;
						}
					}
					
					$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");
					
					if ($product_discount_query->num_rows) {
						$price = $product_discount_query->row['price'];
					}
					
					// Product Specials
					$product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");
				
					if ($product_special_query->num_rows) {
						$price = $product_special_query->row['price'];
					}						
			
					// Reward Points
					$product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "'");
					
					if ($product_reward_query->num_rows) {	
						$reward = $product_reward_query->row['points'];
					} else {
						$reward = 0;
					}
					
					// Downloads		
					$download_data = array();     		
					
					$download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download p2d LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE p2d.product_id = '" . (int)$product_id . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
				
					foreach ($download_query->rows as $download) {
						$download_data[] = array(
							'download_id' => $download['download_id'],
							'name'        => $download['name'],
							'filename'    => $download['filename'],
							'mask'        => $download['mask'],
							'remaining'   => $download['remaining']
						);
					}
					
					// Stock
                                        if(array_key_exists('customer_id', $this->session->data) && $this->session->data['customer_id']){
                                            $customer_id = $this->session->data['customer_id'];
                                        }else{
                                            $customer_id = 'null';
                                        }
                                        
                                        $a = $this->db->query("select cg.name from ".DB_PREFIX."customer c inner join ".DB_PREFIX."customer_group_description cg on cg.customer_group_id = c.customer_group_id where c.customer_id='".$customer_id."'")->row;
                                        
                                        if(array_key_exists('name', $a)){
                                            $user_group = $a['name'];
                                        }else{
                                            $user_group = '';
                                        }
                                        
                                        //check stock available 
					if ($user_group != 'Wholesaler' && (!$product_query->row['quantity'] || $product_query->row['quantity'] < $quantity)) {
                                            $stock = false;
					}
                                        
                                        //check max quantity 
                                        if ($user_group != 'Wholesaler' &&  !empty($product_query->row['maximum']) && $product_query->row['maximum'] < $quantity){
                                            $stock = false;
                                        }
                                        
					// Code Changes Starts Here
                                        $savings=$this->tax->calculate( ($price_real - $price + $option_price) * $quantity , $product_query->row['tax_class_id'], $this->config->get('config_tax')); 
                                        // Code Changes Ends Here
					
					$this->data[$key] = array(
						'key'             => $key,
						'product_id'      => $product_query->row['product_id'],
						'name'            => $product_query->row['name'],
						'model'           => $product_query->row['model'],
						'shipping'        => $product_query->row['shipping'],
						'image'           => $product_query->row['image'],
						'option'          => $option_data,
						'download'        => $download_data,
						'quantity'        => $quantity,
						'minimum'         => $product_query->row['minimum'],

        			'maximum'         => $product_query->row['maximum'],
            
						'subtract'        => $product_query->row['subtract'],
						'stock'           => $stock,
						'price'           => ($price + $option_price),

						'cost'            => ($cost + $option_cost),
            
						'total'           => ($price + $option_price) * $quantity,
						
						// Code Changes Starts Here
                                                'price_real'      => ($price_real + $option_price),
                                                                                                    'total_real'      => ($price_real + $option_price) * $quantity,
                                                'price_real_tax'  => ($price_real_tax + $option_price),	
                                                'total_real_tax'  => $savings ,					
                                                // Code Changes Ends Here
						
						'reward'          => $reward * $quantity,
						'points'          => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $quantity : 0),
						'tax_class_id'    => $product_query->row['tax_class_id'],
						'weight'          => ($product_query->row['weight'] + $option_weight) * $quantity,
						'weight_class_id' => $product_query->row['weight_class_id'],
						'length'          => $product_query->row['length'],
						'width'           => $product_query->row['width'],
						'height'          => $product_query->row['height'],
						'length_class_id' => $product_query->row['length_class_id']					
					);
				} else {
					$this->remove($key);
				}

			//Q: Coupon Free Product
			$this->data[$key]['cfp_total'] = $this->data[$key]['total'];
			
			}
		}
		

		// loop through the finished package and update cfp total if the correct coupon products exist or no coupon products are needed
		//Q: Coupon Free Product
		if (isset($this->session->data['coupon']) && isset($this->session->data['coupon_free'])) {

			$bFound = false;
			$coupon_product_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon_product cp LEFT JOIN " . DB_PREFIX . "coupon c ON (c.coupon_id = cp.coupon_id) WHERE c.code = '" . $this->session->data['coupon'] . "'");
			if ($coupon_product_info->num_rows) { // Check if certain coupon products required first

				// Get array of all coupon product product ids
				foreach ($coupon_product_info->rows as $coupon_products) {
					$xcoupon_products[] = $coupon_products['product_id'];
				}

				// If coupon products found, first check that the required item is in the cart
				foreach ($this->data as $key => $product) {
					$this->data[$key]['cfp_total'] = $product['total']; // reset the cpf_total
					if (in_array($product['product_id'], $xcoupon_products)) {
						$bFound = true;
					}
				}

				// if we found the necessary coupon product in the cart, update the free items with the new cfp_total
				if ($bFound) {
					foreach ($this->data as $key => $product) {
						$this->data[$key]['cfp_total'] = $product['total'];
						$free_product_code = $this->db->query("SELECT code FROM " . DB_PREFIX . "coupon c LEFT JOIN " . DB_PREFIX . "coupon_free_product cfp ON (c.coupon_id = cfp.coupon_id) WHERE cfp.product_id = '" . (int)$product['product_id'] . "'");
						if ($free_product_code->num_rows) {
							$free_product_code = $free_product_code->row['code'];
							if ($this->session->data['coupon'] == $free_product_code) {
								$this->data[$key]['cfp_total'] -= $product['price'] * 1;
							}
						}
					}
				}

			} else {  // If no coupon products then just apply it.
				foreach ($this->data as $key => $product) {
					$this->data[$key]['cfp_total'] = $product['total'];
					$free_product_code = $this->db->query("SELECT code FROM " . DB_PREFIX . "coupon c LEFT JOIN " . DB_PREFIX . "coupon_free_product cfp ON (c.coupon_id = cfp.coupon_id) WHERE cfp.product_id = '" . (int)$product['product_id'] . "'");
					if ($free_product_code->num_rows) {
						$free_product_code = $free_product_code->row['code'];
						if ($this->session->data['coupon'] == $free_product_code) {
							$this->data[$key]['cfp_total'] -= $product['price'] * 1;
						}
					}
				}
			}
		}//
			
		return $this->data;
  	}
		  
  	public function add($product_id, $qty = 1, $option = array()) {
                if (!$option) {
                    $key = (int)$product_id;
                } else {
                    $key = (int)$product_id . ':' . base64_encode(serialize($option));
                }

		if ((int)$qty && ((int)$qty > 0)) {
                    if (!isset($this->session->data['cart'][$key])) {
                        $this->session->data['cart'][$key] = (int)$qty;
                    } else {
                        $this->session->data['cart'][$key] += (int)$qty;
                    }
		}
		
		$this->data = array();
  	}

  	public function update($key, $qty) {
		
                if ((int)$qty && ((int)$qty > 0)) {
                        $this->session->data['cart'][$key] = (int)$qty;
                } else {
	  		$this->remove($key);
		}
		
		$this->data = array();
  	}
	
	public function update1($key, $qty, $option = array()) {
		if (!$option) {
      		$key = (int)$product_id;
    	} else {
      		$key = (int)$product_id . ':' . base64_encode(serialize($option));
    	}
		
    	if ((int)$qty && ((int)$qty > 0)) {
      		$this->session->data['cart'][$key] = (int)$qty;
    	} else {
	  		$this->remove($key);
		}
		
		$this->data = array();
  	}

  	public function remove($key) {
		if (isset($this->session->data['cart'][$key])) {
     		unset($this->session->data['cart'][$key]);
  		}
		
		$this->data = array();
	}
	
  	public function clear() {
		$this->session->data['cart'] = array();
		$this->data = array();
  	}
	
  	public function getWeight() {
		$weight = 0;
	
    	foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
      			$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
			}
		}
	
		return $weight;
	}
	
  	public function getSubTotal() {
		$total = 0;
		
		foreach ($this->getProducts() as $product) {
			$total += $product['total'];
		}

		return $total;
  	}
	
	public function getTaxes() {
		$tax_data = array();
		
		foreach ($this->getProducts() as $product) {
			if ($product['tax_class_id']) {
				$tax_rates = $this->tax->getRates($product['price'], $product['tax_class_id']);
				
				foreach ($tax_rates as $tax_rate) {
					if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
						$tax_data[$tax_rate['tax_rate_id']] = ($tax_rate['amount'] * $product['quantity']);
					} else {
						$tax_data[$tax_rate['tax_rate_id']] += ($tax_rate['amount'] * $product['quantity']);
					}
				}
			}
		}
		
		return $tax_data;
  	}

  	public function getTotal() {
		$total = 0;
		
		foreach ($this->getProducts() as $product) {
			$total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
		}

		return $total;
  	}
	  	
  	public function countProducts() {
		$product_total = 0;
			
		$products = $this->getProducts();
			
		foreach ($products as $product) {
			$product_total += $product['quantity'];
		}		
					
		return $product_total;
	}
	  
  	public function hasProducts() {
    	return count($this->session->data['cart']);
  	}
  
  	public function hasStock() {
		$stock = true;
		
		foreach ($this->getProducts() as $product) {
                    if (!$product['stock']) {
                        $stock = false;
                    }
		}
		
            return $stock;
  	}
  
  	public function hasShipping() {
		$shipping = false;
		
		foreach ($this->getProducts() as $product) {
	  		if ($product['shipping']) {
	    		$shipping = true;
				
				break;
	  		}		
		}
		
		return $shipping;
	}
	

  	public function getProductStock($product_id, $options) {

		$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN `" . DB_PREFIX . "product_description` pd ON (p.product_id = pd.product_id) WHERE p.product_id = " . $product_id . " AND p.date_available <= NOW() AND p.status = '1'");
		
		$stock = array();
				
		if ($product_query->num_rows) {
	
			$option_data = array();
			foreach ($options as $product_option_id => $option_value) {
				$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");
				
				if ($option_query->num_rows) {
					if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
								$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$option_value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

                                            if ($option_value_query->num_rows) {
                                                $stock[] = array(
                                                'product_option_id'       => $product_option_id,
                                                'product_option_value_id' => $option_value,
                                                'option_id'               => $option_query->row['option_id'],
                                                'option_value_id'         => $option_value_query->row['option_value_id'],
                                                'name'                    => $option_query->row['name'],
                                                'option_value'            => $option_value_query->row['name'],
                                                'type'                    => $option_query->row['type'],
                                                'quantity'                => $option_value_query->row['quantity']);
                                            }
								
					} elseif ($option_query->row['type'] == 'checkbox' && is_array($option_value)) {
						foreach ($option_value as $product_option_value_id) {
									$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

                                                if ($option_value_query->num_rows) {
                                                    $stock[] = array(
                                                    'product_option_id'       => $product_option_id,
                                                    'product_option_value_id' => $option_value,
                                                    'option_id'               => $option_query->row['option_id'],
                                                    'option_value_id'         => $option_value_query->row['option_value_id'],
                                                    'name'                    => $option_query->row['name'],
                                                    'option_value'            => $option_value_query->row['name'],
                                                    'type'                    => $option_query->row['type'],
                                                    'quantity'                => $option_value_query->row['quantity']);									
                                                }
                                            }						
					} 
				}
			} 

			if (!$stock) {
				$stock[] = array(
					'quantity'	=> $product_query->row['quantity'],
					'name'		=> $product_query->row['name']);
			}
		}
	return $stock;
  	}
            
  	public function hasDownload() {
		$download = false;
		
		foreach ($this->getProducts() as $product) {
	  		if ($product['download']) {
	    		$download = true;
				
				break;
	  		}		
		}
		
		return $download;
	}	
}
?>