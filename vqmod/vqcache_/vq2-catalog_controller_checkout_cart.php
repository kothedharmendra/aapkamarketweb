<?php 
class ControllerCheckoutCart extends Controller {
	private $error = array();
	
	public function index() {
		$this->language->load('checkout/cart');

		if (!isset($this->session->data['vouchers'])) {
			$this->session->data['vouchers'] = array();
		}
                
                // Stock
                if(array_key_exists('customer_id', $this->session->data) && $this->session->data['customer_id']){
                    $customer_id = $this->session->data['customer_id'];
                }else{
                    $customer_id = 'null';
                }

                $a = $this->db->query("select cg.name from ".DB_PREFIX."customer c inner join ".DB_PREFIX."customer_group_description cg on cg.customer_group_id = c.customer_group_id where c.customer_id='".$customer_id."'")->row;

                if(array_key_exists('name', $a)){
                    $user_group = $a['name'];
                }else{
                    $user_group = '';
                }

		// Update
		if (!empty($this->request->post['quantity'])) {
			foreach ($this->request->post['quantity'] as $key => $value) {
				$this->cart->update($key, $value);
			}
			
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']); 
			unset($this->session->data['deliverytime']);  //Use for Delivery Time
			unset($this->session->data['reward']);
			
			$this->redirect($this->url->link('checkout/cart'));  			
		}
       	
		// Remove
		if (isset($this->request->get['remove'])) {
			$this->cart->remove($this->request->get['remove']);
			
			unset($this->session->data['vouchers'][$this->request->get['remove']]);
			
			$this->session->data['success'] = $this->language->get('text_remove');
		
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']); 
			unset($this->session->data['deliverytime']);  //Use for Delivery Time
			unset($this->session->data['reward']);  
								
			$this->redirect($this->url->link('checkout/cart'));
		}
			
		// Coupon    
		if (isset($this->request->post['coupon']) && $this->validateCoupon()) { 
		
			// Delivery Time    
			if (isset($this->request->post['deliverytime'])) {
				$this->session->data['deliverytime'] = $this->request->post['deliverytime'];
							
				$this->session->data['success'] = $this->language->get('text_deliverytime');
				$this->redirect($this->url->link('checkout/cart'));
			}
		
			$this->session->data['coupon'] = $this->request->post['coupon'];
				
			$this->session->data['success'] = $this->language->get('text_coupon');
			
			$this->redirect($this->url->link('checkout/cart'));
		}
		
		// Voucher
		if (isset($this->request->post['voucher']) && $this->validateVoucher()) { 
			$this->session->data['voucher'] = $this->request->post['voucher'];
				
			$this->session->data['success'] = $this->language->get('text_voucher');
				
			$this->redirect($this->url->link('checkout/cart'));
		}

		// Reward
		if (isset($this->request->post['reward']) && $this->validateReward()) { 
			$this->session->data['reward'] = abs($this->request->post['reward']);
				
			$this->session->data['success'] = $this->language->get('text_reward');
				
			$this->redirect($this->url->link('checkout/cart'));
		}
		
		// Shipping
		if (isset($this->request->post['shipping_method']) && $this->validateShipping()) {
			$shipping = explode('.', $this->request->post['shipping_method']);
			
			$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];
			
			$this->session->data['success'] = $this->language->get('text_shipping');
			
			$this->redirect($this->url->link('checkout/cart'));
		}
		
		$this->document->setTitle($this->language->get('heading_title'));

                $this->data['breadcrumbs'] = array();

                $this->data['breadcrumbs'][] = array(
                        'href'      => $this->url->link('common/home'),
                        'text'      => $this->language->get('text_home'),
                        'separator' => false
                ); 

                $this->data['breadcrumbs'][] = array(
                        'href'      => $this->url->link('checkout/cart'),
                        'text'      => $this->language->get('heading_title'),
                        'separator' => $this->language->get('text_separator')
                );

                if ($this->cart->hasProducts() || !empty($this->session->data['vouchers'])) {
                    
			$points = $this->customer->getRewardPoints();
			
			$points_total = 0;
			
			foreach ($this->cart->getProducts() as $product) {
				if ($product['points']) {
					$points_total += $product['points'];
				}
			}		
			
			$points_total = $points;	
				
                        $this->data['heading_title'] = $this->language->get('heading_title');
			
			$this->data['text_next'] = $this->language->get('text_next');
			$this->data['text_next_choice'] = $this->language->get('text_next_choice');
                        $this->data['text_use_coupon'] = $this->language->get('text_use_coupon');
			$this->data['text_use_voucher'] = $this->language->get('text_use_voucher');
			
			$this->data['text_use_deliverytime'] = $this->language->get('text_use_deliverytime');
			
			$this->data['text_use_reward'] = sprintf($this->language->get('text_use_reward'), $points);
			$this->data['text_shipping_estimate'] = $this->language->get('text_shipping_estimate');
			$this->data['text_shipping_detail'] = $this->language->get('text_shipping_detail');
			$this->data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$this->data['text_select'] = $this->language->get('text_select');
			$this->data['text_none'] = $this->language->get('text_none');
						
			$this->data['column_image'] = $this->language->get('column_image');
                        $this->data['column_name'] = $this->language->get('column_name');
                        $this->data['column_model'] = $this->language->get('column_model');
                        $this->data['column_quantity'] = $this->language->get('column_quantity');
                        $this->data['column_price'] = $this->language->get('column_price');
                        $this->data['column_total'] = $this->language->get('column_total');
			
			$this->data['entry_coupon'] = $this->language->get('entry_coupon');
			$this->data['entry_voucher'] = $this->language->get('entry_voucher');
			
			$this->data['entry_deliverytime'] = $this->language->get('entry_deliverytime');
			
			$this->data['entry_reward'] = sprintf($this->language->get('entry_reward'), $points_total);
			$this->data['entry_country'] = $this->language->get('entry_country');
			$this->data['entry_zone'] = $this->language->get('entry_zone');
			$this->data['entry_postcode'] = $this->language->get('entry_postcode');
						
			$this->data['button_update'] = $this->language->get('button_update');
			$this->data['button_remove'] = $this->language->get('button_remove');
			$this->data['button_coupon'] = $this->language->get('button_coupon');
			
			$this->data['button_deliverytime'] = $this->language->get('button_deliverytime');
			
			$this->data['button_voucher'] = $this->language->get('button_voucher');
			$this->data['button_reward'] = $this->language->get('button_reward');
			$this->data['button_quote'] = $this->language->get('button_quote');
			$this->data['button_shipping'] = $this->language->get('button_shipping');			
                        $this->data['button_shopping'] = $this->language->get('button_shopping');
                        $this->data['button_checkout'] = $this->language->get('button_checkout');

			// Code Changes  - Starts Here
                        $this->data['column_savings'] = $this->language->get('column_savings'); 
                        // Code Changes Ends Here
			
			if (isset($this->error['warning'])) {
                            $this->data['error_warning'] = $this->error['warning'];
                        } elseif (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
                            $this->data['error_warning'] = $this->language->get('error_stock');		
                        } else {
                            $this->data['error_warning'] = '';
			}
                        
			if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
				$this->data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
			} else {
				$this->data['attention'] = '';
			}
						
			if (isset($this->session->data['success'])) {
				$this->data['success'] = $this->session->data['success'];
			
				unset($this->session->data['success']);
			} else {
				$this->data['success'] = '';
			}
			
			$this->data['action'] = $this->url->link('checkout/cart');   
						
			if ($this->config->get('config_cart_weight')) {
				$this->data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
			} else {
				$this->data['weight'] = '';
			}
						 
			$this->load->model('tool/image');
			


	//Q: Coupon Free Product
	if (isset($this->session->data['coupon'])) {
		$coupon_product_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon_product cp LEFT JOIN " . DB_PREFIX . "coupon c ON (c.coupon_id = cp.coupon_id) WHERE c.code = '" . $this->session->data['coupon'] . "'");
		if ($coupon_product_info->num_rows) { // Check that if there are coupon products with this coupon that they exist first
			foreach ($coupon_product_info->rows as $coupon_product) {
				foreach ($this->cart->getProducts() as $cart_product) {
					if ($coupon_product['product_id'] == $cart_product['product_id']) {
						$free_product_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon_free_product cfp LEFT JOIN " . DB_PREFIX . "coupon c ON (c.coupon_id = cfp.coupon_id) WHERE c.code = '" . $this->session->data['coupon'] . "'");
						if ($free_product_info->num_rows) {
							foreach ($free_product_info->rows as $free_product) {
								$found = false;
								foreach ($this->cart->getProducts() as $cart_product2) {
									if ($free_product['product_id'] == $cart_product2['product_id']) {
										$found = true;
									}
								}
								if (!$found) {
									$this->cart->add($free_product['product_id'], 1, array(), 0);
								}
							}
						}
					}
				}
			}
		} else { // Else if there are no coupon_products, then just apply it
			$free_product_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon_free_product cfp LEFT JOIN " . DB_PREFIX . "coupon c ON (c.coupon_id = cfp.coupon_id) WHERE c.code = '" . $this->session->data['coupon'] . "'");
			if ($free_product_info->num_rows) {
				foreach ($free_product_info->rows as $free_product) {
					$found = false;
					foreach ($this->cart->getProducts() as $cart_product2) {
						if ($free_product['product_id'] == $cart_product2['product_id']) {
							$found = true;
						}
					}
					if (!$found) {
						$this->cart->add($free_product['product_id'], 1, array(), 0);
					}
				}
			}
		}
	}//

			
      		$this->data['products'] = array();
			
		$products = $this->cart->getProducts();

      		foreach ($products as $product) {
				$product_total = 0;
					
				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}			
				
				if ($product['minimum'] > $product_total) {
                                    $this->data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);

				}	
					
                                

                if($user_group != 'Wholesaler' && ($product['maximum']) > 0 && ($product['maximum']) < ($product_total)) {
                        $this->data['error_warning'] = sprintf($this->language->get('error_max_qty'), $product['name'], $product['maximum']);
                }
            
                                if ($product['image']) {
                                    $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				} else {
                                    $image = '';
				}
                                
				$option_data = array();
                                  
                                foreach ($product['option'] as $option) {
                                    if ($option['type'] != 'file') {
                                            $value = $option['option_value'];	
                                    } else {
                                            $filename = $this->encryption->decrypt($option['option_value']);

                                            $value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
                                    }

                                    $option_data[] = array(
                                            'name'  => $option['name'],
                                            'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                                    );
                                }
				
				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                                    $price = $this->currency->format($this->tax->calculate($product['price_real'], $product['tax_class_id'], $this->config->get('config_tax')));
                                    $savings =  $this->currency->format($this->tax->calculate($product['price_real'], $product['tax_class_id'], $this->config->get('config_tax')) - $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))) ;
                                } else {
                                    $price = false;
				}
				
				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
				} else {
					$total = false;
				}
				

				//Q: Coupon Free Product
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $cfp_total = $this->currency->format($this->tax->calculate($product['cfp_total'], $product['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $cfp_total = false;
                }//
			
        		$this->data['products'][] = array(
'cfp_total'               => isset($cfp_total) ? $cfp_total : false, //Q: coupon free product
                            'key'      => $product['key'],
                            'thumb'    => $image,
                            'name'     => $product['name'],
                            'model'    => $product['model'],
                            'option'   => $option_data,
                            'quantity' => $product['quantity'],
                            'stock'    => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
                            'reward'   => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
                            'price'    => $price,

          			'cost'     => $product['cost'],
            
                            'savings'  => $savings,
                            'total'    => $total,
                            'href'     => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                            'remove'   => $this->url->link('checkout/cart', 'remove=' . $product['key'])
                        );
      		}//end foreach 
			

			// Gift Voucher
			$this->data['vouchers'] = array();
			
			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
					$this->data['vouchers'][] = array(
						'key'         => $key,
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount']),
						'remove'      => $this->url->link('checkout/cart', 'remove=' . $key)   
					);
				}
			}

			if (isset($this->request->post['next'])) {
				$this->data['next'] = $this->request->post['next'];
			} else {
				$this->data['next'] = '';
			}
						 
			$this->data['coupon_status'] = $this->config->get('coupon_status');
			
			if (isset($this->request->post['coupon'])) {
				$this->data['coupon'] = $this->request->post['coupon'];			
			} elseif (isset($this->session->data['coupon'])) {
				$this->data['coupon'] = $this->session->data['coupon'];
			} else {
				$this->data['coupon'] = '';
			}
			
			$this->data['voucher_status'] = $this->config->get('voucher_status');
			
			$this->data['deliverytime_status'] = $this->config->get('deliverytime_status');
			
			if (isset($this->request->post['deliverytime'])) {
				$this->data['deliverytime'] = $this->request->post['deliverytime'];				
			} elseif (isset($this->session->data['deliverytime'])) {
				$this->data['deliverytime'] = $this->session->data['deliverytime'];
			} else {
				$this->data['deliverytime'] = '';
			}
						
			$this->data['text_morning'] = $this->language->get('text_morning');
			$this->data['text_midnight'] = $this->language->get('text_midnight');
			$this->data['text_200'] = $this->currency->format($this->language->get('text_200'));
			$this->data['text_250'] = $this->currency->format($this->language->get('text_250')); 
			
			if (isset($this->request->post['voucher'])) {
				$this->data['voucher'] = $this->request->post['voucher'];				
			} elseif (isset($this->session->data['voucher'])) {
				$this->data['voucher'] = $this->session->data['voucher'];
			} else {
				$this->data['voucher'] = '';
			}
			
			//$this->data['reward_status'] = ($points && $points_total && $this->config->get('reward_status'));
			$this->data['reward_status'] = ($points &&  $this->config->get('reward_status'));
			
			if (isset($this->request->post['reward'])) {
				$this->data['reward'] = $this->request->post['reward'];				
			} elseif (isset($this->session->data['reward'])) {
				$this->data['reward'] = $this->session->data['reward'];
			} else {
				$this->data['reward'] = '';
			}

			$this->data['shipping_status'] = $this->config->get('shipping_status') && $this->config->get('shipping_estimator') && $this->cart->hasShipping();	
								
			if (isset($this->request->post['country_id'])) {
				$this->data['country_id'] = $this->request->post['country_id'];				
			} elseif (isset($this->session->data['shipping_country_id'])) {
				$this->data['country_id'] = $this->session->data['shipping_country_id'];			  	
			} else {
				$this->data['country_id'] = $this->config->get('config_country_id');
			}
				
			$this->load->model('localisation/country');
			
			$this->data['countries'] = $this->model_localisation_country->getCountries();
						
			if (isset($this->request->post['zone_id'])) {
				$this->data['zone_id'] = $this->request->post['zone_id'];				
			} elseif (isset($this->session->data['shipping_zone_id'])) {
				$this->data['zone_id'] = $this->session->data['shipping_zone_id'];			
			} else {
				$this->data['zone_id'] = '';
			}
			
			if (isset($this->request->post['postcode'])) {
				$this->data['postcode'] = $this->request->post['postcode'];				
			} elseif (isset($this->session->data['shipping_postcode'])) {
				$this->data['postcode'] = $this->session->data['shipping_postcode'];					
			} else {
				$this->data['postcode'] = '';
			}
			
			if (isset($this->request->post['shipping_method'])) {
				$this->data['shipping_method'] = $this->request->post['shipping_method'];				
			} elseif (isset($this->session->data['shipping_method'])) {
				$this->data['shipping_method'] = $this->session->data['shipping_method']['code']; 
			} else {
				$this->data['shipping_method'] = '';
			}
						
			// Totals
			$this->load->model('setting/extension');
			
			$total_data = array();					
			$total = 0;
			$taxes = $this->cart->getTaxes();
			
			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$sort_order = array(); 
				
				$results = $this->model_setting_extension->getExtensions('total');
				
				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}
				
				array_multisort($sort_order, SORT_ASC, $results);
				
				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('total/' . $result['code']);
			
						$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
					}
					
					$sort_order = array(); 
				  
					foreach ($total_data as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}
		
					array_multisort($sort_order, SORT_ASC, $total_data);			
				}
			}
			
			$this->data['totals'] = $total_data;
						
			$this->data['continue'] = $this->url->link('common/home');
						
			$this->data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/checkout/cart.tpl';
			} else {
				$this->template = 'default/template/checkout/cart.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_bottom',
				'common/content_top',
				'common/footer',
				'common/header'	
			);
						
			$this->response->setOutput($this->render());					
    	} else {
      		$this->data['heading_title'] = $this->language->get('heading_title');

      		$this->data['text_error'] = $this->language->get('text_empty');

      		$this->data['button_continue'] = $this->language->get('button_continue');
			
      		$this->data['continue'] = $this->url->link('common/home');

			unset($this->session->data['success']);

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'	
			);
					
			$this->response->setOutput($this->render());			
    	}
  	}
	
	private function validateCoupon($coupon = false) {
		
		$this->load->model('checkout/coupon');
				
                if(!$coupon){
                    $coupon_info = $this->model_checkout_coupon->getCoupon($this->request->post['coupon']);			
                }else{
                    $coupon_info = $this->model_checkout_coupon->getCoupon($coupon);			    
                }
                
		if (!$coupon_info) {			
			$this->error['warning'] = $this->language->get('error_coupon');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
	
	private function validateDeliveryTime() {
            $deliverytime_info = $this->request->post['deliverytime'];
            if (!$deliverytime_info) {			
                    $this->error['warning'] = $this->language->get('error_deliverytime');
            }

            if (!$this->error) {
                    return true;
            } else {
                    return false;
            }		
	}
	
	private function validateVoucher() {
		$this->load->model('checkout/voucher');
				
		$voucher_info = $this->model_checkout_voucher->getVoucher($this->request->post['voucher']);			
		
		if (!$voucher_info) {			
			$this->error['warning'] = $this->language->get('error_voucher');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
	
	private function validateReward() {
		$points = $this->customer->getRewardPoints();
		
		$points_total = 0;
		
		foreach ($this->cart->getProducts() as $product) {
			if ($product['points']) {
				$points_total += $product['points'];
			}
		}	
		
		$points_total = $points;
				
		if (empty($this->request->post['reward'])) {
			$this->error['warning'] = $this->language->get('error_reward');
		}
	
		if ($this->request->post['reward'] > $points) {
			$this->error['warning'] = sprintf($this->language->get('error_points'), $this->request->post['reward']);
		}
		
		if ($this->request->post['reward'] > $points_total) {
			$this->error['warning'] = sprintf($this->language->get('error_maximum'), $points_total);
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
	
	private function validateShipping() {
		if (!empty($this->request->post['shipping_method'])) {
			$shipping = explode('.', $this->request->post['shipping_method']);
					
			if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {			
				$this->error['warning'] = $this->language->get('error_shipping');
			}
		} else {
			$this->error['warning'] = $this->language->get('error_shipping');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
								
	public function add() {
		$this->language->load('checkout/cart');
		// Stock
                if(array_key_exists('customer_id', $this->session->data) && $this->session->data['customer_id']){
                    $customer_id = $this->session->data['customer_id'];
                }else{
                    $customer_id = 'null';
                }

                $a = $this->db->query("select cg.name from ".DB_PREFIX."customer c inner join ".DB_PREFIX."customer_group_description cg on cg.customer_group_id = c.customer_group_id where c.customer_id='".$customer_id."'")->row;

                if(array_key_exists('name', $a)){
                    $user_group = $a['name'];
                }else{
                    $user_group = '';
                }

		$json = array();
		
		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->load->model('catalog/product');
						
		$product_info = $this->model_catalog_product->getProduct($product_id);
		
		if ($product_info) {			
			if (isset($this->request->post['quantity'])) {
				$quantity = $this->request->post['quantity'];
			} else {
				$quantity = 1;
			}
														

            $product_total = 0;

            $products = $this->cart->getProducts();

            foreach ($products as $product_2) {
                    if ($product_2['product_id'] == $this->request->post['product_id']) {
                            $product_total += $product_2['quantity'];
                    }
            }

            //check stock available 
            if($user_group != 'Wholesaler' && $product_info['maximum'] > 0 && $product_info['maximum'] < ($product_total + $quantity)) {
                    $json['error']['maximum'] = sprintf($this->language->get('error_max_qty'), $product_info['name'], $product_info['maximum']);
            }
                
            if($user_group != 'Wholesaler' && $product_info['minimum'] > 0 && $product_info['minimum'] > ($product_total + $quantity)) {
                    $json['error']['warning'] = sprintf($this->language->get('error_minimum'), $product_info['name'], $product_info['minimum']);
            }

            
			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();	
			}
			
			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
			
			foreach ($product_options as $product_option) {

                if($user_group != 'Wholesaler' && ($product_info['maximum']) < ($product_total + $quantity)) {
                        $json['error']['warning'] = sprintf($this->language->get('error_max_qty'), $product_info['name'], $product_info['maximum']);
                }
            
                            if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                                $json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
                            }
			}
			
                        //if no error or warnings 

			$the_stock = array();
			$option_test = false;
			$original_cart = $this->session->data['cart'];

			$this->cart->add($this->request->post['product_id'], $quantity, $option);
			
			$qty = $this->cart->getProductStock($this->request->post['product_id'], $option);

			foreach ($qty as $qtys) {
				if (isset($qtys['type'])) {
					if ($qtys['type'] == 'select' || $qtys['type'] == 'radio' || $qtys['type'] == 'image' || $qtys['type'] == 'checkbox') {
						$option_test = true;
					}
				}
			}

			if ($this->cart->hasStock()== false) {
				if ($option_test == true) {
					foreach ($qty as $quantity) {
			   
						$the_stock[] = array (
							'stock_name' => $quantity['option_value'],
							'stock_quantity' => $quantity['quantity']
						);
					}
					
					foreach ($the_stock as $key => $row) {
						$stock_name[$key] = $row['stock_name'];
						$stock_quantity[$key] = $row['stock_quantity'];
					}
					array_multisort($stock_quantity, SORT_ASC, $the_stock);
					
					$fewest = reset($the_stock);
					
					$json['error']['warning'] = sprintf($this->language->get('stock_error'), $fewest['stock_quantity'], $fewest['stock_name']);
					$this->session->data['stock_error']  = $json['error']['warning'];
				} else {
					$json['error']['warning'] = sprintf($this->language->get('stock_error'), $qty[0]['quantity'], $qty[0]['name']);
					$this->session->data['stock_error']  = $json['error']['warning'];
				}	
			}

			$this->cart->clear();
			unset($this->session->data['stock_error']);
			
			$this->session->data['cart'] = $original_cart;
			
			if (!$json) {
                                $product_id = $this->request->post['product_id'];
                                
				$this->cart->add($product_id, $quantity, $option);
                                
                                //validate 
                                $err_validation = $this->validateCart();

                                if(!empty($err_validation)){
                                    $json = $err_validation;
                                    $this->cart->remove($product_id);
			
                                    unset($this->session->data['vouchers'][$product_id]);

                                    $this->session->data['success'] = $this->language->get('text_remove');

                                    unset($this->session->data['shipping_method']);
                                    unset($this->session->data['shipping_methods']);
                                    unset($this->session->data['payment_method']);
                                    unset($this->session->data['payment_methods']); 
                                    unset($this->session->data['deliverytime']);  //Use for Delivery Time
                                    unset($this->session->data['reward']);  
                                    
                                } else {
                                    $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));
                                }
                                
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
				unset($this->session->data['deliverytime']);  //Use for Delivery Time
				
				// Totals
				$this->load->model('setting/extension');
				
				$total_data = array();					
				$total = 0;
				$taxes = $this->cart->getTaxes();
				
				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$sort_order = array(); 
					
					$results = $this->model_setting_extension->getExtensions('total');
					
					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}
					
					array_multisort($sort_order, SORT_ASC, $results);
					
					foreach ($results as $result) {
						if ($this->config->get($result['code'] . '_status')) {
							$this->load->model('total/' . $result['code']);
				
							$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
						}
						
						$sort_order = array(); 
					  
						foreach ($total_data as $key => $value) {
							$sort_order[$key] = $value['sort_order'];
						}
			
						array_multisort($sort_order, SORT_ASC, $total_data);			
					}
				}
				
				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}
		
		$this->response->setOutput(json_encode($json));		
	}
	
	function validateCart(){
            
            $data = array();
            $data['error']['warning'] = '';
            
            $products = $this->cart->getProducts();

            foreach ($products as $product) {
                $product_total = 0;

                foreach ($products as $product_2) {
                    if ($product_2['product_id'] == $product['product_id']) {
                            $product_total += $product_2['quantity'];
                    }
                }			

                if ($product['minimum'] > $product_total) {
                    $data['error']['warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']).'<br />';
                }	
                
                if (!$product['stock']) {
                    $data['error']['warning'] = 'Products named ('.$product['name'].') is not available in the desired quantity or not in stock!';
                }
            }   
            /*
            if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
                $data['error']['warning'] = 'Some products in cart are not available in the desired quantity or not in stock!';		
            }
            */
            if($data['error']['warning'])
                return $data;
            else 
                return false;
        }
	
	public function update() {
		$this->language->load('checkout/cart');
                
		// Stock
                if(array_key_exists('customer_id', $this->session->data) && $this->session->data['customer_id']){
                    $customer_id = $this->session->data['customer_id'];
                }else{
                    $customer_id = 'null';
                }

                $a = $this->db->query("select cg.name from ".DB_PREFIX."customer c inner join ".DB_PREFIX."customer_group_description cg on cg.customer_group_id = c.customer_group_id where c.customer_id='".$customer_id."'")->row;

                if(array_key_exists('name', $a)){
                    $user_group = $a['name'];
                }else{
                    $user_group = '';
                }

		$json = array();
		
		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->load->model('catalog/product');
						
		$product_info = $this->model_catalog_product->getProduct($product_id);
		
		if ($product_info) {			
			if (isset($this->request->post['quantity'])) {
				$quantity = $this->request->post['quantity'];
			} else {
				$quantity = 1;
			}
														

            $product_total = 0;

            $products = $this->cart->getProducts();

            foreach ($products as $product_2) {
                    if ($product_2['product_id'] == $this->request->post['product_id']) {
                            $product_total += $product_2['quantity'];
                    }
            }

            //check stock available 
            if($user_group != 'Wholesaler' && $product_info['maximum'] > 0 && $product_info['maximum'] < ($product_total + $quantity)) {
                    $json['error']['maximum'] = sprintf($this->language->get('error_max_qty'), $product_info['name'], $product_info['maximum']);
            }
                
            if($user_group != 'Wholesaler' && $product_info['minimum'] > 0 && $product_info['minimum'] > ($product_total + $quantity)) {
                    $json['error']['warning'] = sprintf($this->language->get('error_minimum'), $product_info['name'], $product_info['minimum']);
            }

            
			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();	
			}
			
			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
			
			foreach ($product_options as $product_option) {

                if($user_group != 'Wholesaler' && ($product_info['maximum']) < ($product_total + $quantity)) {
                        $json['error']['warning'] = sprintf($this->language->get('error_max_qty'), $product_info['name'], $product_info['maximum']);
                }
            
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}
			
			if (!$json) {
                                $product_id = $this->request->post['product_id'];
                            
                                if(array_key_exists($product_id,$this->session->data['cart']))
                                    $product_qty_bkp = $this->session->data['cart'][$product_id];
                                else
                                    $product_qty_bkp = 0;
                                
				$this->cart->update($product_id, $quantity, $option);
                                
                                //validate 
                                $err_validation = $this->validateCart();

                                if(!empty($err_validation)){
                                    $json = $err_validation; 
                                    
                                    //restore quantity if not valid
                                    $this->cart->update($product_id, $product_qty_bkp, $option);

                                    unset($this->session->data['shipping_method']);
                                    unset($this->session->data['shipping_methods']);
                                    unset($this->session->data['payment_method']);
                                    unset($this->session->data['payment_methods']); 
                                    unset($this->session->data['deliverytime']);  //Use for Delivery Time
                                    unset($this->session->data['reward']);

                                } else {
                                    $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));
                                }
			//	$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));
				
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
				unset($this->session->data['deliverytime']);  //Use for Delivery Time
				
				// Totals
				$this->load->model('setting/extension');
				
				$total_data = array();					
				$total = 0;
				$taxes = $this->cart->getTaxes();
				
				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$sort_order = array(); 
					
					$results = $this->model_setting_extension->getExtensions('total');
					
					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}
					
					array_multisort($sort_order, SORT_ASC, $results);
					
					foreach ($results as $result) {
						if ($this->config->get($result['code'] . '_status')) {
							$this->load->model('total/' . $result['code']);
				
							$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
						}
						
						$sort_order = array(); 
					  
						foreach ($total_data as $key => $value) {
							$sort_order[$key] = $value['sort_order'];
						}
			
						array_multisort($sort_order, SORT_ASC, $total_data);			
					}
				}
				
				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
			} else {
                        	$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}
		
		$this->response->setOutput(json_encode($json));		
	}
	
	public function quote() {
		$this->language->load('checkout/cart');
		
		$json = array();	
		
		if (!$this->cart->hasProducts()) {
			$json['error']['warning'] = $this->language->get('error_product');				
		}				

		if (!$this->cart->hasShipping()) {
			$json['error']['warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));				
		}				
		
		if ($this->request->post['country_id'] == '') {
			$json['error']['country'] = $this->language->get('error_country');
		}
		
		if ($this->request->post['zone_id'] == '') {
			$json['error']['zone'] = $this->language->get('error_zone');
		}
			
		$this->load->model('localisation/country');
		
		$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);
		
		if ($country_info && $country_info['postcode_required'] && (utf8_strlen($this->request->post['postcode']) < 2) || (utf8_strlen($this->request->post['postcode']) > 10)) {
			$json['error']['postcode'] = $this->language->get('error_postcode');
		}
						
		if (!$json) {		
			$this->tax->setShippingAddress($this->request->post['country_id'], $this->request->post['zone_id']);
		
			// Default Shipping Address
			$this->session->data['shipping_country_id'] = $this->request->post['country_id'];
			$this->session->data['shipping_zone_id'] = $this->request->post['zone_id'];
			$this->session->data['shipping_postcode'] = $this->request->post['postcode'];
		
			if ($country_info) {
				$country = $country_info['name'];
				$iso_code_2 = $country_info['iso_code_2'];
				$iso_code_3 = $country_info['iso_code_3'];
				$address_format = $country_info['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';	
				$address_format = '';
			}
			
			$this->load->model('localisation/zone');
		
			$zone_info = $this->model_localisation_zone->getZone($this->request->post['zone_id']);
			
			if ($zone_info) {
				$zone = $zone_info['name'];
				$zone_code = $zone_info['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}	
		 
			$address_data = array(
				'firstname'      => '',
				'lastname'       => '',
				'company'        => '',
				'address_1'      => '',
				'address_2'      => '',
				'postcode'       => $this->request->post['postcode'],
				'city'           => '',
				'zone_id'        => $this->request->post['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $this->request->post['country_id'],
				'country'        => $country,	
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format
			);
		
			$quote_data = array();
			
			$this->load->model('setting/extension');
			
			$results = $this->model_setting_extension->getExtensions('shipping');
			
			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('shipping/' . $result['code']);
					
					$quote = $this->{'model_shipping_' . $result['code']}->getQuote($address_data); 
		
					if ($quote) {
						$quote_data[$result['code']] = array( 
							'title'      => $quote['title'],
							'quote'      => $quote['quote'], 
							'sort_order' => $quote['sort_order'],
							'error'      => $quote['error']
						);
					}
				}
			}
	
			$sort_order = array();
		  
			foreach ($quote_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}
	
			array_multisort($sort_order, SORT_ASC, $quote_data);
			
			$this->session->data['shipping_methods'] = $quote_data;
			
			if ($this->session->data['shipping_methods']) {
				$json['shipping_method'] = $this->session->data['shipping_methods']; 
			} else {
				$json['error']['warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
			}				
		}	
		
		$this->response->setOutput(json_encode($json));						
	}
	
	public function country() {
		$json = array();
		
		$this->load->model('localisation/country');

                $country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);
		
		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']		
			);
		}
		
		$this->response->setOutput(json_encode($json));
	}
        
        // mini cart coupon  form submit   
        public function couponAjax(){
		if (isset($_POST['coupon']) && $this->validateCoupon($_POST['coupon'])) { 
                    
                    $this->session->data['coupon'] = $_POST['coupon'];
                    			
                    // Totals
                    $this->load->model('setting/extension');

                    $total_data = array();					
                    $total = 0;
                    $taxes = $this->cart->getTaxes();

                    // Display prices
                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                            $sort_order = array(); 

                            $results = $this->model_setting_extension->getExtensions('total');

                            foreach ($results as $key => $value) {
                                    $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                            }

                            array_multisort($sort_order, SORT_ASC, $results);

                            foreach ($results as $result) {
                                    if ($this->config->get($result['code'] . '_status')) {
                                            $this->load->model('total/' . $result['code']);

                                            $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                                    }

                                    $sort_order = array(); 

                                    foreach ($total_data as $key => $value) {
                                            $sort_order[$key] = $value['sort_order'];
                                    }

                                    array_multisort($sort_order, SORT_ASC, $total_data);			
                            }
                    }

                    $json['status'] = 1;
                    $json['msg'] = "Success: Your coupon discount has been applied!";
                    $json['total_data'] = $total_data;                    
                    
		}else{                  
                    
                    $json['status'] = 0;
                    $json['msg'] = "Warning: Coupon is either invalid, expired or reached it's usage limit!";
                    
                }
                
                echo json_encode($json);
        }
}
?>