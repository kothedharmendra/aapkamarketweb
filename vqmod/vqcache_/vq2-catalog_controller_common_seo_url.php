<?php
class ControllerCommonSeoUrl extends Controller {

	private $productCache = array();
	private $categoryCache = array();
	
	public function productLink($data) {
		$tracking = isset($data['tracking']) ? $data['tracking'] : '';
		if(!empty($data['route']) && $data['route'] == 'product/product') {
			$product_id = empty($data['product_id']) ? 0 : (int) $data['product_id'];
			$data = array('route' => 'product/product');
			
			if($product_id) {
				if(isset($this->productCache[$product_id])) {
					$path = $this->productCache[$product_id];
					if(!empty($path)) {
						$data['path'] = $path;
					}
					
					$data['product_id'] = $product_id;
				} else {
					$store_id = (int) $this->config->get('config_store_id');
					
					$query = sprintf('
						SELECT		`c`.*
						FROM		`%1$sproduct_to_category` `p2c`
						INNER JOIN	`%1$scategory_to_store` `c2s`
						ON			`p2c`.`category_id` = `c2s`.`category_id`
						INNER JOIN	`%1$scategory` `c`
						ON			`c`.`category_id` = `p2c`.`category_id`
						WHERE		`p2c`.`product_id` = %2$s
						AND			`c2s`.`store_id` = %3$s
						AND			`c`.`status` = 1
						ORDER BY	`c`.`sort_order`
						ASC
						LIMIT 1
						',
						DB_PREFIX,
						$product_id,
						$store_id
					);
					
					$result = $this->db->query($query);
					
					if($result->num_rows) {
						$path = array();
						$path[] = (int) $result->row['category_id'];
						
						while((int) $result->row['parent_id'] > 0) {
							$parent_id = (int) $result->row['parent_id'];
							$path[] = $parent_id;
							
							if(isset($this->categoryCache[$parent_id])) {
								$result->row['parent_id'] = $this->categoryCache[$parent_id];
							} else {
								$query = sprintf(
									'SELECT `parent_id`
									FROM `%1$scategory`
									WHERE `category_id` = %2$s
									AND `status` = 1',
									DB_PREFIX,
									(int) $result->row['parent_id']
								);
								
								$result = $this->db->query($query);
								if($result->num_rows) {
									$this->categoryCache[$parent_id] = $result->row['parent_id'];
								}
								
							}
							
							if(!$result->rows || in_array((int)$result->row['parent_id'], $path)) {
								break;
							}
						}
						
						if(!empty($path)) {
							$path = implode('_', array_reverse($path));
							$this->productCache[$product_id] = $path;
							$data['path'] = $path;
						} else {
							$this->productCache[$product_id] = '';
						}
						
						$data['product_id'] = $product_id;
					}
				}
			}
		}
		if($tracking) {
			$data['tracking'] = $tracking;
		}
		return $data;
	}
			
	public function index() {
		// Add rewrite to url class
		if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);
		}
		
		// Decode URL
		if (isset($this->request->get['_route_'])) {
			$parts = explode('/', $this->request->get['_route_']);
			$route = "";
			foreach ($parts as $part) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "'");
				
				if ($query->num_rows) {
					$url = explode('=', $query->row['query']);
					
					if ($url[0] == 'product_id') {
						$this->request->get['product_id'] = $url[1];
					}
					//articles url
					if ($url[0] == 'news_id') {
						$this->request->get['news_id'] = $url[1];
					}
					if ($url[0] == 'ncategory_id') {
						if (!isset($this->request->get['ncat'])) {
							$this->request->get['ncat'] = $url[1];
						} else {
							$this->request->get['ncat'] .= '_' . $url[1];
						}
					}
					//articles url
					if ($url[0] == 'category_id') {
						if (!isset($this->request->get['path'])) {
							$this->request->get['path'] = $url[1];
						} else {
							$this->request->get['path'] .= '_' . $url[1];
						}
					}	
					
					if ($url[0] == 'manufacturer_id') {
						$this->request->get['manufacturer_id'] = $url[1];
					}
					
					if ($url[0] == 'information_id') {
						$this->request->get['information_id'] = $url[1];
					}	else{
				        $route = $url[0];
			        }
				} else {
					$this->request->get['route'] = 'error/not_found';	
				}
			}
			
			if (isset($this->request->get['product_id'])) {
				$this->request->get['route'] = 'product/product';
			} elseif (isset($this->request->get['path'])) {
				$this->request->get['route'] = 'product/category';
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$this->request->get['route'] = 'product/manufacturer/product';
			} elseif (isset($this->request->get['information_id'])) {
				$this->request->get['route'] = 'information/information';
			} elseif (isset($this->request->get['news_id'])) {
				$this->request->get['route'] = 'news/article';
			} elseif (isset($this->request->get['ncat'])) {
				$this->request->get['route'] = 'news/ncategory';
			}else {
			    $this->request->get['route'] = $route;
		    }
			
			if (isset($this->request->get['route'])) {
				return $this->forward($this->request->get['route']);
			}
		}
	}
	
	public function rewrite($link) {
		if ($this->config->get('config_seo_url')) {
			$url_data = parse_url(str_replace('&amp;', '&', $link));
		
			$url = ''; 
			
			$data = array();
			
			parse_str($url_data['query'], $data);
$data = $this->productLink($data);
			
			foreach ($data as $key => $value) {
				if (isset($data['route'])) {
					if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id') || ($data['route'] == 'news/article' && $key == 'news_id')) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");
					
						if ($query->num_rows) {
							$url .= '/' . $query->row['keyword'];
							
							unset($data[$key]);
						}					
					} elseif ($key == 'path') {
						$categories = explode('_', $value);
						
						foreach ($categories as $category) {
							$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int)$category . "'");
					
							if ($query->num_rows) {
								$url .= '/' . $query->row['keyword'];
							}							
						}
						
						unset($data[$key]);
					} elseif ($key == 'ncat') {
						$ncategories = explode('_', $value);
						
						foreach ($ncategories as $ncategory) {
							$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'ncategory_id=" . (int)$ncategory . "'");
					
							if ($query->num_rows) {
								$url .= '/' . $query->row['keyword'];
							}							
						}
						
						unset($data[$key]);
					} elseif ($key == 'route') {
			   $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($value) . "'");
			
			   if ($query->num_rows) {
				  $url .= '/' . $query->row['keyword'];
				  
				  unset($data[$key]);
			   }               
			}
				}
			}
		
			if ($url) {
				unset($data['route']);
			
				$query = '';
			
				if ($data) {
					foreach ($data as $key => $value) {
                                                if(is_array($value)) return $link;
						$query .= '&' . $key . '=' . $value;
					}
					
					if ($query) {
						$query = '?' . trim($query, '&');
					}
				}

				return $url_data['scheme'] . '://' . $url_data['host'] . (isset($url_data['port']) ? ':' . $url_data['port'] : '') . str_replace('/index.php', '', $url_data['path']) . $url . $query;
			} else {
				return $link;
			}
		} else {
			return $link;
		}		
	}	
}
?>