<?php 
class ControllerCommonHeader extends Controller {
	protected function index() {
		
		/*$this->load->model('sale/order');

		$this->data['total_sale'] = $this->currency->format($this->model_sale_order->getTotalSales(), $this->config->get('config_currency'));
		$this->data['total_sale_year'] = $this->currency->format($this->model_sale_order->getTotalSalesByYear(date('Y')), $this->config->get('config_currency'));
		$this->data['total_order'] = $this->model_sale_order->getTotalOrders();
		
		$this->load->model('sale/customer');
		
		$this->data['total_customer'] = $this->model_sale_customer->getTotalCustomers();
		$this->data['total_customer_approval'] = $this->model_sale_customer->getTotalCustomersAwaitingApproval();*/
		
		$this->load->model('catalog/category');
		$this->data['total_cat'] = count($this->model_catalog_category->getCategories());
		
		$this->load->model('catalog/product');
		$this->data['total_product'] = $this->model_catalog_product->getTotalProducts();
		
		$this->load->model('catalog/attribute');
		$this->data['total_attribute'] = $this->model_catalog_attribute->getTotalAttributes();
		
		$this->load->model('catalog/attribute_group');
		$this->data['total_attribute_group'] = $this->model_catalog_attribute_group->getTotalAttributeGroups();
		
		$this->load->model('catalog/option');
		$this->data['total_option'] = $this->model_catalog_option->getTotalOptions();
		
		$this->load->model('catalog/manufacturer');
		$this->data['total_brand'] = $this->model_catalog_manufacturer->getTotalManufacturers();
		
		$this->load->model('catalog/download');
		$this->data['total_download'] = $this->model_catalog_download->getTotalDownloads();
		
		$this->load->model('catalog/review');
		$this->data['total_review'] = $this->model_catalog_review->getTotalReviews();
		
		$this->load->model('catalog/information');
		$this->data['total_information'] = $this->model_catalog_information->getTotalInformations();
		
		$this->load->model('catalog/information');
		$this->data['total_information'] = $this->model_catalog_information->getTotalInformations();
		
		$exten = array('module','shipping','payment','total','feed');
		for($x=0;$x<count($exten);$x++) {			 
				$this->data['total_'.$exten[$x]] = count(glob(DIR_APPLICATION . 'controller/'.$exten[$x].'/*.php'));
		}
		
		$sale = array('order','return','customer','customer_group','affiliate','coupon','voucher','voucher_theme');
		for($x=0;$x<count($sale);$x++) {
			$this->load->model('sale/'.$sale[$x]);
			$val = 'model_sale_'.$sale[$x];
			if($x==0) $this->data['total_'.$sale[$x]] = $this->$val->getTotalOrders();
			if($x==1) $this->data['total_'.$sale[$x]] = $this->$val->getTotalReturns();
			if($x==2) $this->data['total_'.$sale[$x]] = $this->$val->getTotalCustomers();
			if($x==3) $this->data['total_'.$sale[$x]] = $this->$val->getTotalCustomerGroups();
			if($x==4) $this->data['total_'.$sale[$x]] = $this->$val->getTotalAffiliates();
			if($x==5) $this->data['total_'.$sale[$x]] = $this->$val->getTotalCoupons();
			if($x==6) $this->data['total_'.$sale[$x]] = $this->$val->getTotalVouchers();
			if($x==7) $this->data['total_'.$sale[$x]] = $this->$val->getTotalVoucherThemes();
		}
		
                $this->data['total_city'] = $this->db->query('select count(*) as total from city')->row['total'];
                
		$this->load->model('setting/store');
		$this->data['total_store'] = 1+$this->model_setting_store->getTotalStores();
		
		$this->load->model('design/layout');
		$this->data['total_layout'] = $this->model_design_layout->getTotalLayouts();
		
		$this->load->model('design/banner');
		$this->data['total_banner'] = $this->model_design_banner->getTotalBanners();
		
		$this->load->model('localisation/language');
		$this->data['total_language'] = $this->model_localisation_language->getTotalLanguages();
		
		$this->load->model('localisation/currency');
		$this->data['total_currency'] = $this->model_localisation_currency->getTotalCurrencies();
				
		$this->load->model('localisation/stock_status');
		$this->data['total_stock_status'] = $this->model_localisation_stock_status->getTotalStockStatuses();
		
		$this->load->model('localisation/order_status');
		$this->data['total_order_status'] = $this->model_localisation_order_status->getTotalOrderStatuses();
		
		$this->load->model('localisation/return_status');
		$this->data['total_return_status'] = $this->model_localisation_return_status->getTotalReturnStatuses();
		
		$this->load->model('localisation/return_action');
		$this->data['total_return_action'] = $this->model_localisation_return_action->getTotalReturnActions();
		
		$this->load->model('localisation/return_reason');
		$this->data['total_return_reason'] = $this->model_localisation_return_reason->getTotalReturnReasons();
		
		$this->load->model('localisation/country');
		$this->data['total_country'] = $this->model_localisation_country->getTotalCountries();
		
		$this->load->model('localisation/zone');
		$this->data['total_zone'] = $this->model_localisation_zone->getTotalZones();
		
		$this->load->model('localisation/geo_zone');
		$this->data['total_geo_zone'] = $this->model_localisation_geo_zone->getTotalGeoZones();
		
		$this->load->model('localisation/tax_class');
		$this->data['total_tax_class'] = $this->model_localisation_tax_class->getTotalTaxClasses();
		
		$this->load->model('localisation/tax_rate');
		$this->data['total_tax_rate'] = $this->model_localisation_tax_rate->getTotalTaxRates();
		
		$this->load->model('localisation/length_class');
		$this->data['total_length_class'] = $this->model_localisation_length_class->getTotalLengthClasses();
		
		$this->load->model('localisation/weight_class');
		$this->data['total_weight_class'] = $this->model_localisation_weight_class->getTotalWeightClasses();
		
		/*$this->load->model('user/user_permission');
		$this->data['total_user_group'] = $this->model_user_user_group->getTotalUserGroups();*/
		$query = $this->db->query("SELECT COUNT( * ) AS total FROM user_group"); 
		foreach($query->rows as $row) { 
			$this->data['total_user_group'] = $row['total'];
		}
		
		$query = $this->db->query("SELECT COUNT( * ) AS total FROM user"); 
		foreach($query->rows as $row) { 
			$this->data['total_user'] = $row['total'];
		}
		
		/*$this->load->model('report/sale_order');
		$this->data['total_sale_order'] = $this->model_report_sale->getTotalOrders($data);*/
		
				
		/*---------------------- JIPL ----------------------*/	
		
		$this->data['title'] = $this->document->getTitle(); 
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = HTTPS_SERVER;
		} else {
			$this->data['base'] = HTTP_SERVER;
		}
		
		$this->data['description'] = $this->document->getDescription();
		$this->data['keywords'] = $this->document->getKeywords();
		$this->data['links'] = $this->document->getLinks();	
		$this->data['styles'] = $this->document->getStyles();
		$this->data['scripts'] = $this->document->getScripts();
		$this->data['lang'] = $this->language->get('code');
		$this->data['direction'] = $this->language->get('direction');
		
		$this->load->language('common/header');


			$this->load->language('common/footer');
			$this->data['text_footer'] = sprintf($this->language->get('text_footer'), VERSION);
			
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_affiliate'] = $this->language->get('text_affiliate');
		$this->data['text_attribute'] = $this->language->get('text_attribute');
		$this->data['text_attribute_group'] = $this->language->get('text_attribute_group');
		$this->data['text_backup'] = $this->language->get('text_backup');
$this->data['text_export'] = $this->language->get('text_export');
		$this->data['text_banner'] = $this->language->get('text_banner');
		$this->data['text_catalog'] = $this->language->get('text_catalog');
		$this->data['text_category'] = $this->language->get('text_category');
                $this->data['text_add_category'] = $this->language->get('text_add_category');
                $this->data['text_list_category'] = $this->language->get('text_list_category');
		$this->data['text_confirm'] = $this->language->get('text_confirm');
		$this->data['text_country'] = $this->language->get('text_country');
		$this->data['text_coupon'] = $this->language->get('text_coupon');
                $this->data['text_add_coupon'] = $this->language->get('text_add_coupon');
                $this->data['text_list_coupon'] = $this->language->get('text_list_coupon');
		$this->data['text_currency'] = $this->language->get('text_currency');			
		$this->data['text_customer'] = $this->language->get('text_customer');
                $this->data['text_list_customer'] = $this->language->get('text_list_customer');
		$this->data['text_customer_group'] = $this->language->get('text_customer_group');
		$this->data['text_customer_blacklist'] = $this->language->get('text_customer_blacklist');
		$this->data['text_sale'] = $this->language->get('text_sale');
		$this->data['text_design'] = $this->language->get('text_design');
		$this->data['text_documentation'] = $this->language->get('text_documentation');
		$this->data['text_download'] = $this->language->get('text_download');
		$this->data['text_error_log'] = $this->language->get('text_error_log');
		$this->data['text_extension'] = $this->language->get('text_extension');
		$this->data['text_feed'] = $this->language->get('text_feed');
		$this->data['text_front'] = $this->language->get('text_front');
		$this->data['text_geo_zone'] = $this->language->get('text_geo_zone');
		$this->data['text_dashboard'] = $this->language->get('text_dashboard');
		$this->data['text_help'] = $this->language->get('text_help');
		$this->data['text_information'] = $this->language->get('text_information');
                $this->data['text_list_information'] = $this->language->get('text_list_information');
                $this->data['text_add_information'] = $this->language->get('text_add_information');
		$this->data['text_language'] = $this->language->get('text_language');
		$this->data['text_layout'] = $this->language->get('text_layout');
		$this->data['text_localisation'] = $this->language->get('text_localisation');
		$this->data['text_logout'] = $this->language->get('text_logout');
		$this->data['text_contact'] = $this->language->get('text_contact');
		$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$this->data['text_module'] = $this->language->get('text_module');
		$this->data['text_option'] = $this->language->get('text_option');
		$this->data['text_order'] = $this->language->get('text_order');
                $this->data['text_pending_order'] = $this->language->get('text_pending_order');
                $this->data['text_complete_order'] = $this->language->get('text_complete_order');
                $this->data['text_list_order'] = $this->language->get('text_list_order');
		$this->data['text_order_status'] = $this->language->get('text_order_status');
		$this->data['text_opencart'] = $this->language->get('text_opencart');
		$this->data['text_payment'] = $this->language->get('text_payment');
		$this->data['text_product'] = $this->language->get('text_product'); 
                $this->data['text_low_stock_product'] = $this->language->get('text_low_stock_product'); 
                $this->data['text_list_product'] = $this->language->get('text_list_product'); 
                $this->data['text_add_product'] = $this->language->get('text_add_product'); 
		$this->data['text_reports'] = $this->language->get('text_reports');
		$this->data['text_report_sale_order'] = $this->language->get('text_report_sale_order');

		$this->data['text_report_adv_sale_profit'] = $this->language->get('text_report_adv_sale_profit');
            
		$this->data['text_report_sale_tax'] = $this->language->get('text_report_sale_tax');
		$this->data['text_report_sale_shipping'] = $this->language->get('text_report_sale_shipping');
		$this->data['text_report_sale_return'] = $this->language->get('text_report_sale_return');
		$this->data['text_report_sale_coupon'] = $this->language->get('text_report_sale_coupon');
		$this->data['text_report_product_viewed'] = $this->language->get('text_report_product_viewed');
		$this->data['text_report_product_purchased'] = $this->language->get('text_report_product_purchased');
$this->data['text_report_adv_product_profit'] = $this->language->get('text_report_adv_product_profit');
		$this->data['text_report_customer_online'] = $this->language->get('text_report_customer_online');
		$this->data['text_report_customer_order'] = $this->language->get('text_report_customer_order');
$this->data['text_report_adv_customer_profit'] = $this->language->get('text_report_adv_customer_profit');
		$this->data['text_report_customer_reward'] = $this->language->get('text_report_customer_reward');
		$this->data['text_report_customer_credit'] = $this->language->get('text_report_customer_credit');
		$this->data['text_report_affiliate_commission'] = $this->language->get('text_report_affiliate_commission');
		$this->data['text_report_sale_return'] = $this->language->get('text_report_sale_return');
		$this->data['text_report_product_purchased'] = $this->language->get('text_report_product_purchased');
$this->data['text_report_adv_product_profit'] = $this->language->get('text_report_adv_product_profit');
		$this->data['text_report_product_viewed'] = $this->language->get('text_report_product_viewed');
		$this->data['text_report_customer_order'] = $this->language->get('text_report_customer_order');
$this->data['text_report_adv_customer_profit'] = $this->language->get('text_report_adv_customer_profit');
		$this->data['text_review'] = $this->language->get('text_review');
		$this->data['text_return'] = $this->language->get('text_return');
		$this->data['text_return_action'] = $this->language->get('text_return_action');
		$this->data['text_return_reason'] = $this->language->get('text_return_reason');
		$this->data['text_return_status'] = $this->language->get('text_return_status');
		$this->data['text_support'] = $this->language->get('text_support'); 
		$this->data['text_shipping'] = $this->language->get('text_shipping');		
		$this->data['text_setting'] = $this->language->get('text_setting');
		$this->data['text_stock_status'] = $this->language->get('text_stock_status');
		$this->data['text_system'] = $this->language->get('text_system');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_tax_class'] = $this->language->get('text_tax_class');
		$this->data['text_tax_rate'] = $this->language->get('text_tax_rate');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_user'] = $this->language->get('text_user');
		$this->data['text_user_group'] = $this->language->get('text_user_group');
		$this->data['text_users'] = $this->language->get('text_users');
		$this->data['text_voucher'] = $this->language->get('text_voucher');
		$this->data['text_voucher_theme'] = $this->language->get('text_voucher_theme');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_length_class'] = $this->language->get('text_length_class');
		$this->data['text_zone'] = $this->language->get('text_zone');
                $this->data['text_feedback'] = $this->language->get('text_feedback');
                $this->data['text_suggestion'] = $this->language->get('text_suggestion');
                
		if (!$this->user->isLogged() || !isset($this->request->get['token']) || !isset($this->session->data['token']) || ($this->request->get['token'] != $this->session->data['token'])) {
			$this->data['logged'] = '';
			
			$this->data['home'] = $this->url->link('common/login', '', 'SSL');
		} else {
			
			$this->data['logged'] = sprintf($this->language->get('text_logged'), ": <img src='view/image/admin_theme/base5builder_impulsepro/icon-admin-user.png' /> " . $this->user->getUserName());
			
	
                        $this->data['city'] = $this->url->link('localisation/city', 'token=' . $this->session->data['token'], 'SSL');
                        
			$this->data['home'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['affiliate'] = $this->url->link('sale/affiliate', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['attribute'] = $this->url->link('catalog/attribute', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['attribute_group'] = $this->url->link('catalog/attribute_group', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['backup'] = $this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL');
$this->data['export'] = $this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['banner'] = $this->url->link('design/banner', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['category'] = $this->url->link('catalog/category', 'token=' . $this->session->data['token'], 'SSL');
                        $this->data['add_category'] = $this->url->link('catalog/category/insert', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['country'] = $this->url->link('localisation/country', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['coupon'] = $this->url->link('sale/coupon', 'token=' . $this->session->data['token'], 'SSL');
                        $this->data['add_coupon'] = $this->url->link('sale/coupon/insert', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['currency'] = $this->url->link('localisation/currency', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['customer'] = $this->url->link('sale/customer', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['customer_group'] = $this->url->link('sale/customer_group', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['customer_blacklist'] = $this->url->link('sale/customer_blacklist', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['download'] = $this->url->link('catalog/download', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['error_log'] = $this->url->link('tool/error_log', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['feed'] = $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL');			
			$this->data['geo_zone'] = $this->url->link('localisation/geo_zone', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['information'] = $this->url->link('catalog/information', 'token=' . $this->session->data['token'], 'SSL');
                        $this->data['add_information'] = $this->url->link('catalog/information/insert', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['language'] = $this->url->link('localisation/language', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['layout'] = $this->url->link('design/layout', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['logout'] = $this->url->link('common/logout', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['contact'] = $this->url->link('sale/contact', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['manufacturer'] = $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['module'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['option'] = $this->url->link('catalog/option', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['order'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL');
                        $this->data['pending_order'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] .'&filter_order_status_id=1', 'SSL');
                        $this->data['complete_order'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] .'&filter_order_status_id=5', 'SSL');                            
			$this->data['order_status_list'] = $this->url->link('localisation/order_status', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['payment'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['product'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'], 'SSL');
                        $this->data['low_stock_product'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'].'&filter_quantity=<10', 'SSL');
                        $this->data['add_product'] = $this->url->link('catalog/product/insert', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['report_sale_order'] = $this->url->link('report/sale_order', 'token=' . $this->session->data['token'], 'SSL');

			$this->data['report_adv_sale_profit'] = $this->url->link('report/adv_sale_profit', 'token=' . $this->session->data['token'], 'SSL');
            
			$this->data['report_sale_tax'] = $this->url->link('report/sale_tax', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['report_sale_shipping'] = $this->url->link('report/sale_shipping', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['report_sale_return'] = $this->url->link('report/sale_return', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['report_sale_coupon'] = $this->url->link('report/sale_coupon', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['report_product_viewed'] = $this->url->link('report/product_viewed', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['report_product_purchased'] = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'], 'SSL');
$this->data['report_adv_product_profit'] = $this->url->link('report/adv_product_profit', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['report_customer_online'] = $this->url->link('report/customer_online', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['report_customer_order'] = $this->url->link('report/customer_order', 'token=' . $this->session->data['token'], 'SSL');
$this->data['report_adv_customer_profit'] = $this->url->link('report/adv_customer_profit', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['report_customer_reward'] = $this->url->link('report/customer_reward', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['report_customer_credit'] = $this->url->link('report/customer_credit', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['report_affiliate_commission'] = $this->url->link('report/affiliate_commission', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['review'] = $this->url->link('catalog/review', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['return'] = $this->url->link('sale/return', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['return_action'] = $this->url->link('localisation/return_action', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['return_reason'] = $this->url->link('localisation/return_reason', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['return_status'] = $this->url->link('localisation/return_status', 'token=' . $this->session->data['token'], 'SSL');			
			$this->data['shipping'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['setting'] = $this->url->link('setting/store', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['store'] = HTTP_CATALOG;
			$this->data['stock_status'] = $this->url->link('localisation/stock_status', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['tax_class'] = $this->url->link('localisation/tax_class', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['tax_rate'] = $this->url->link('localisation/tax_rate', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['total'] = $this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['user'] = $this->url->link('user/user', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['user_group'] = $this->url->link('user/user_permission', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['voucher'] = $this->url->link('sale/voucher', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['voucher_theme'] = $this->url->link('sale/voucher_theme', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['weight_class'] = $this->url->link('localisation/weight_class', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['length_class'] = $this->url->link('localisation/length_class', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['zone'] = $this->url->link('localisation/zone', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['insert'] = $this->url->link('catalog/category/insert', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['insertpro'] = $this->url->link('catalog/product/insert', 'token=' . $this->session->data['token'] , 'SSL');
			$this->data['insertbrand'] = $this->url->link('catalog/manufacturer/insert', 'token=' . $this->session->data['token'] , 'SSL');
			$this->data['ordercom'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status_id=5' , 'SSL');
			$this->data['orderpen'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status_id=1' , 'SSL');
			$this->data['ordercan'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status_id=7' , 'SSL');
			$this->data['ordercreate'] = $this->url->link('sale/order/insert', 'token=' . $this->session->data['token'] , 'SSL');
			$this->data['featuredpro'] = $this->url->link('module/featured', 'token=' . $this->session->data['token'], 'SSL');
                        $this->data['insertinfo'] = $this->url->link('catalog/information/insert', 'token=' . $this->session->data['token'] , 'SSL');
			$this->data['insertcoupon'] = $this->url->link('sale/coupon/insert', 'token=' . $this->session->data['token'] , 'SSL');
			$this->data['insertcust'] = $this->url->link('sale/customer/insert', 'token=' . $this->session->data['token'] , 'SSL');
			$this->data['insertunit'] = $this->url->link('localisation/weight_class/insert', 'token=' . $this->session->data['token'] , 'SSL');
			$this->data['theme'] = $this->url->link('module/dailyshopin', 'token=' . $this->session->data['token'], 'SSL');
                        $this->data['feedback'] = $this->url->link('module/feedback', 'token=' . $this->session->data['token'], 'SSL');
                        $this->data['suggestion'] = $this->url->link('module/suggestion', 'token=' . $this->session->data['token'], 'SSL');
			
			$this->data['stores'] = array();
			
			$this->load->model('setting/store');
			
			$results = $this->model_setting_store->getStores();
			
			foreach ($results as $result) {
				$this->data['stores'][] = array(
					'name' => $result['name'],
					'href' => $result['url']
				);
			}	
                        
			// Orders
			$this->load->model('sale/order');

			// Processing Orders
			$this->data['order_status_total'] = $this->model_sale_order->getTotalOrdersByOrderStatusId(1);
			$this->data['order_status'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status_id=1', 'SSL');

			// Complete Orders
			$this->data['complete_status_total'] = $this->model_sale_order->getTotalOrdersByOrderStatusId(5);
			$this->data['complete_status'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status_id=5', 'SSL');

			// Returns
			$this->load->model('sale/return');

			$return_total = $this->model_sale_return->getTotalReturnsByReturnStatusId(1);

			$this->data['return_total'] = $return_total;

			$this->data['return'] = $this->url->link('sale/return', 'token=' . $this->session->data['token'], 'SSL');

			// Customers
			$this->load->model('sale/customer');

			$customer_total = $this->model_sale_customer->getTotalCustomers(array('filter_approved' => false));

			$this->data['customer_total'] = $customer_total;
			$this->data['customer_approval'] = $this->url->link('sale/customer', 'token=' . $this->session->data['token'] . '&filter_approved=0', 'SSL');

			// Products
			$this->load->model('catalog/product');

			$product_total = $this->model_catalog_product->getTotalProducts(array('filter_quantity' => 0));

			$this->data['product_total'] = $product_total;

			// Reviews
			$this->load->model('catalog/review');

			$review_total = $this->model_catalog_review->getTotalReviews(array('filter_status' => false));

			$this->data['review_total'] = $review_total;

			$this->data['review'] = $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . '&filter_status=0', 'SSL');

			// Affliate
			$this->load->model('sale/affiliate');

			$affiliate_total = $this->model_sale_affiliate->getTotalAffiliates(array('filter_approved' => false));

			$this->data['affiliate_total'] = $affiliate_total;
			$this->data['affiliate_approval'] = $this->url->link('sale/affiliate', 'token=' . $this->session->data['token'] . '&filter_approved=1', 'SSL');

			$this->data['alerts'] = $customer_total + $product_total + $review_total + $return_total + $affiliate_total;
                         
		}
                
		
			$this->template = 'admin_theme/base5builder_impulsepro/common/header.tpl';
			
		
		$this->render();
	}
}
?>