<?php if ($this->customer->isLogged()) {
        $customer_group_id = $this->customer->getCustomerGroupId();
} else {
        $customer_group_id = $this->config->get('config_customer_group_id');
}

$a = $this->db->query("select name from ".DB_PREFIX."customer_group_description where customer_group_id='".$customer_group_id."'")->row;

if(array_key_exists('name', $a)){
    $user_group = $a['name'];
}else{
    $user_group = '';
}

?>
<?php echo $header; ?>
<?php echo $column_left; ?>
<?php echo $column_right; ?>
<div id="content">
<?php echo $content_top; ?>

<?php
$query = $this->db->query("select * from product_similar where product_id='".$product_id."'");
$rsallproducts = $query->rows;
$num_rows = sizeof($rsallproducts);
$product_url = $this->url->link('product/product', 'product_id=' . $product_id); 
?>

  <!-- breadcrumb 
  ===========================================================-->
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  
  <div id="productwholedetail<?=$product_id?>">
  
  <!-- Title 
  ===========================================================-->
  <h1 class="product-title"><?php echo $heading_title; ?></h1>
  
  <!-- product-info 
  ===========================================================-->
  <div class="product-info">
  
  	<!-- **** Left **** -->
    <?php if ($thumb || $images) { ?>
        <div class="left">
        
          <?php if ($thumb) { ?>
          <div class="image">
            <span class="zoom"></span>
            <a href="<?php echo $popup; ?>" class = 'cloud-zoom' id='zoom1' rel="">
                <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" />
            </a>
          </div>
          <?php } ?>
          
          <?php if ($images) { ?>
          
          <div class="image-additional">
            <?php foreach ($images as $image) { ?>
            
            <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="cloud-zoom-gallery" rel="useZoom: 'zoom1', smallImage: '<?php echo $image['thumb']; ?>' ">
            <img src="<?php echo $image['thumb']; ?>"  title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
            </a>
            
            <?php } ?>
          </div>
          
          <?php } ?>
          
        </div>
    <?php } ?>
    
    <!-- **** right **** -->
    <div class="right">
    
      <!-- description -->
      <div class="description">
        <?php if ($product_info['mpn']) { ?>
        <div class="offer"><?php echo $product_info['mpn']; ?></div><br />
        <?php } ?>
        <?php if ($manufacturer) { ?>
        <span><?php echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a><br />
        <?php } ?>
        <span><?php echo $text_model; ?></span> <?php echo $model; ?><br />
        <?php if ($reward) { ?>
        <span><?php echo $text_reward; ?></span> <?php echo $reward; ?><br />
        <?php } ?>
        <?php if ($product_info['isbn']==1) { ?>
        <span>Type : </span> <img src="image/data/assets/veg.png" /> This item is Veg<br />
        <?php } elseif ($product_info['isbn']==2) { ?>
        <span>Type : </span> <img src="image/data/assets/nonveg.png" /> This item is Non-Veg<br />
        <?php } ?>
        <?php if ($product_info['jan']) { ?>
        <span>Food Coupon : </span> <img src="image/data/assets/sodexo.png" /><br />
        <?php } ?>
        <span><?php echo $text_stock; ?></span> <?php echo $stock; ?></div>
        
      <!-- price -->
      <?php if ($price) { ?>
      <div class="price">
        <div class="price-text"><?php echo $text_price; ?></div>
        
        <div class="price-data">
        <?php if (!$special) { ?>
        <?php echo $price; ?>
        <?php } else { ?>
        <span class="price-old"><?php echo $price; ?></span> <span class="price-new"><?php echo $special; ?></span>
        <?php } ?>
        <br />
        <?php if ($tax) { ?>
        <!-- <span class="price-tax"><?php echo $text_tax; ?> <?php echo $tax; ?></span><br /> -->
        <?php } ?>
        <?php if ($points) { ?>
        <span class="reward"><small><?php echo $text_points; ?> <?php echo $points; ?></small></span><br />
        <?php } ?>
        <?php if ($discounts) { ?>
        <div class="discount">
          <?php foreach ($discounts as $discount) { ?>
          <?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?><br />
          <?php } ?>
        </div>
        <?php } ?>
      </div>
      </div>
      <?php } ?>
      

      <div class="cart">
        <div><?php echo $text_qty; ?>
          <input type="text" class="quantity" name="quantity" size="2" value="<?php echo $minimum; ?>" />
          <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
          &nbsp;
          <input type="button" value="<?php echo $button_cart; ?>" id="button-cart" class="button cart-product" />
        </div>
        <div class="wish-compare">
          <a class="wish tooltip" onclick="addToWishList('<?php echo $product_id; ?>');"><span><?php echo $button_wishlist; ?></span></a>
          <a class="compare tooltip" onclick="addToCompare('<?php echo $product_id; ?>');"><span><?php echo $button_compare; ?></span></a>
        </div>

        <?php if ($maximum ) { ?>
        <div class="minimum"><?php echo $text_max_qty; ?></div>
        <?php } ?>
            
        <?php if ($minimum > 1) { ?>
        <div class="minimum"><?php echo $text_minimum; ?></div>
        <?php } ?>
      </div>
      
      
      
      <?php if ($review_status) { ?>
      <div class="review">
      	
        <div class="review-text">
        <img class="review-img" src="catalog/view/theme/grocery/image/stars-<?php echo $rating; ?>.png" alt="<?php echo $reviews; ?>" />
        <a onclick="$('a[href=\'#tab-review\']').trigger('click');"><?php echo $reviews; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;
        <a onclick="$('a[href=\'#tab-review\']').trigger('click');"><?php echo $text_write; ?></a>
        </div>
        
        
        <div class="share">
            <div class="addthis_toolbox addthis_default_style ">
            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
            <a class="addthis_button_tweet"></a>
            <a class="addthis_button_pinterest_pinit"></a>
            <a class="addthis_counter addthis_pill_style"></a>
            </div>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50f539c042f85d82"></script>
        </div>
      </div>
      
      
      
      <?php if ($options) { ?>
      <div class="options">
        <h2 class="option-title"><?php echo $text_option; ?></h2>
        <br />
        <?php foreach ($options as $option) { ?>
        <?php if ($option['type'] == 'select') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <select name="option[<?php echo $option['product_option_id']; ?>]">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($option['option_value'] as $option_value) { ?>
            <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
            </option>
            <?php } ?>
          </select>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'radio') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <?php foreach ($option['option_value'] as $option_value) { ?>
          <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
          <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
          </label>
          <br />
          <?php } ?>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'checkbox') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <?php foreach ($option['option_value'] as $option_value) { ?>
          <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
          <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
          </label>
          <br />
          <?php } ?>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'image') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <table class="option-image">
            <?php foreach ($option['option_value'] as $option_value) { ?>
            <tr>
              <td style="width: 1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /></td>
              <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
              <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                  <?php if ($option_value['price']) { ?>
                  (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                  <?php } ?>
                </label></td>
            </tr>
            <?php } ?>
          </table>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'text') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'textarea') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'file') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="button" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>" class="button">
          <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'date') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'datetime') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'time') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
        </div>
        <br />
        <?php } ?>
        <?php } ?>
      </div>
      <?php } ?>
      
      <?php } ?>
		</div>            
        </div>      
</div>
<?php  
	$y=0; 	
            
        $query = $this->db->query("select * from product_similar where product_id='".$product_id."'");
        $rsallproducts1 = $query->rows;
        $num_rows = sizeof($rsallproducts1);
    	if($num_rows>0) { 
        foreach($rsallproducts1 as $row) 
        {        	
        	$z=0;
        	foreach ($products as $product1)
        	{
                        
            if($row['similar_id']==$product1['product_id'])
            {
            
           $query = $this->db->query("select * from product where product_id='".$product1['product_id']."'");
           $rsproductwholedet = $query->row; 
            
    ?>
      
      
      
      <div id="productwholedetail<?=$product_id.'_'.$product1['product_id']?>" style="display:none">

  <!-- Title 
  ===========================================================-->
  <h1 class="product-title"><?php echo $product1['name']; ?></h1>
  
  <!-- product-info 
  ===========================================================-->
  <div class="product-info">
  
  	<!-- **** Left **** -->
    <?php if ($product1['thumb']) { ?>
        <div class="left">
        
          <?php if ($product1['thumb']) { ?>
          <div class="image">
          	<span class="zoom"></span>
            <a href="<?php echo $product1['popup']; ?>" class = 'cloud-zoom-gallery' rel="useZoom: 'zoom<?=$product1['product_id']?>', smallImage: '<?php echo $product1['thumb']; ?>'" >
            <img src="<?php echo $product1['thumb']; ?>" title="<?php echo $product1['name']; ?>" alt="<?php echo $product1['name']; ?>" />
            </a>
          </div>
          <?php } ?>
          
          <?php if ($images) { ?>
          
          <div class="image-additional">
            <?php foreach ($images as $image) { ?>
            
            <a href="<?php echo $product1['popup']; ?>" title="<?php echo $product1['name']; ?>" class="cloud-zoom-gallery" rel="useZoom: 'zoom<?=$product1['product_id']?>', smallImage: '<?php echo $product1['thumb']; ?>' ">
            <img src="<?php echo $product1['thumb']; ?>"  title="<?php echo $product1['name']; ?>" alt="<?php echo $product1['name']; ?>" />
            </a>
            
            <?php } ?>
          </div>
          
          <?php } ?>
          
        </div>
    <?php } ?>
    
    
    <div class="right">
    
      <!-- description -->
      <div class="description">
        <?php if ($manufacturer) { ?>
        <span><?php echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a><br />
        <?php } ?>
        <span><?php echo $text_model; ?></span> <?php echo $rsproductwholedet['model']; ?><br />
        <?php if ($reward) { ?>
        <span><?php echo $text_reward; ?></span> <?php echo $reward; ?><br />
        <?php } ?>
        <?php if ($product_info['isbn']==1) { ?>
        <span>Type : </span> <img src="image/data/assets/veg.png" /> This item is Veg<br />
        <?php } elseif ($product_info['isbn']==2) { ?>
        <span>Type : </span> <img src="image/data/assets/nonveg.png" /> This item is Non-Veg<br />
        <?php } ?>
        <?php if ($product_info['jan']) { ?>
        <span>Food Coupon : </span> <img src="image/data/assets/sodexo.png" /><br />
        <?php } ?>
        <span><?php echo $text_stock; ?></span> 
        <?php
        echo $product1['stock_status']
        ?>
        </div>
        
        
        
      <?php if ($product1['price']) { ?>
      <div class="price">
        <div class="price-text"><?php echo $text_price; ?></div>
        
        <div class="price-data">
        <?php if (!$product1['special']) { ?>
        <?php echo $product1['price']; ?>
        <?php } else { ?>
        <span class="price-old"><?php echo $product1['price']; ?></span> <span class="price-new"><?php echo $product1['special']; ?></span>
        <?php } ?>
        <br />
        <?php if ($tax) { ?>
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product1['tax']; ?></span><br />
        <?php } ?>
        <?php if ($points) { ?>
        <span class="reward"><small><?php echo $text_points; ?> <?php echo $points; ?></small></span><br />
        <?php } ?>
        <?php if ($discounts) { ?>
        <div class="discount">
          <?php foreach ($discounts as $discount) { ?>
          <?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?><br />
          <?php } ?>
        </div>
        <?php } ?>
      </div>
      </div>
      <?php } ?>
      
      
      
      <?php if ($product1['review_status']) { ?>
      <div class="review">
      	
        <div class="review-text">
        <img class="review-img" src="catalog/view/theme/grocery/image/stars-<?php echo $product1['rating']; ?>.png" alt="<?php echo $product1['rating']; ?>" />
        <a onclick="$('a[href=\'#tab-review\']').trigger('click');"><?php echo $product1['reviews']; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;
        <a onclick="$('a[href=\'#tab-review\']').trigger('click');"><?php echo $text_write; ?></a>
        </div>
        
        
        <div class="share">
            <div class="addthis_toolbox addthis_default_style ">
            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
            <a class="addthis_button_tweet"></a>
            <a class="addthis_button_pinterest_pinit"></a>
            <a class="addthis_counter addthis_pill_style"></a>
            </div>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50f539c042f85d82"></script>
        </div>
      </div>
      
      
      
      <!-- options -->
      <?php if ($options) { ?>
      <div class="options">
        <h2 class="option-title"><?php echo $text_option; ?></h2>
        <br />
        <?php foreach ($options as $option) { ?>
        <?php if ($option['type'] == 'select') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <select name="option[<?php echo $option['product_option_id']; ?>]">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($option['option_value'] as $option_value) { ?>
            <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
            </option>
            <?php } ?>
          </select>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'radio') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <?php foreach ($option['option_value'] as $option_value) { ?>
          <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
          <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
          </label>
          <br />
          <?php } ?>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'checkbox') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <?php foreach ($option['option_value'] as $option_value) { ?>
          <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
          <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
          </label>
          <br />
          <?php } ?>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'image') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <table class="option-image">
            <?php foreach ($option['option_value'] as $option_value) { ?>
            <tr>
              <td style="width: 1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /></td>
              <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
              <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                  <?php if ($option_value['price']) { ?>
                  (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                  <?php } ?>
                </label></td>
            </tr>
            <?php } ?>
          </table>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'text') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'textarea') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'file') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="button" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>" class="button">
          <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'date') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'datetime') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'time') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
        </div>
        <br />
        <?php } ?>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
	</div>      
    </div><!--/product-info-->      
</div>
 
      <?php }
    	} 
        $y++;
    		}                               
    	}
    ?>
      
      <div class="product-grid setwidthin960">
      <div>
      <div id="divproduct<?=$product_id?>">
            <?php if ($thumb) { ?>
              <div class="image">
                 <a href="<?php echo isset($product['href'])?$product['href']:$product_url; ?>">
                     <img oversrc="<?php echo $thumb; ?>" src="<?php echo $thumb; ?>" 
                     title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" style="border:none" width="156" height="156"/>
                  </a>
              </div>

              <?php } else { ?>

              <div class="image">
                  <a href="<?php echo isset($product['href'])?$product['href']:$product_url; ?>">
                      <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" 
                      alt="<?php echo $heading_title; ?>" style="border:none" width="156" height="156"/>
                  </a>
              </div>

              <?php } ?>
      
      <?php
      if($num_rows>0) {  
		
      foreach($rsallproducts as $row) {            	                
      
            	$query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
                $rsprod = $query->row;
                
            	$rs=strspn($heading_title ^ $rsprod['name'], "\0");
                break;
            }
            
            $query = $this->db->query("select * from product_similar where product_id='".$product_id."'");
            $rsallproducts = $query->rows;
            $num_rows = sizeof($rsallproducts);
       }
      ?>
      <?php if($num_rows>0) { ?>
      <div class="name"><a href="<?php echo isset($product['href'])?$product['href']:$product_url; ?>"><?=substr($heading_title,0,$rs);?></a></div>
      <?php } else { ?>
      <div class="name"><a href="<?php echo isset($product['href'])?$product['href']:$product_url; ?>"><?php echo $heading_title; ?></a></div>
      <?php } ?>
      
      
      <div class="description"><?php echo $product['description']; ?></div>
      <?php foreach ($discounts as $discount) { 
      	$price = $discount['price'];
        }
      ?>
      
      <?php if ($price) { ?>
      <div class="price">
		
        
        <?php
        if($num_rows>0) {   
        	
        ?>
        <ul class="ultabonproductdisplay">
        	<li><a onclick="displayblocknone('divproduct<?=$product_id?>','divproduct<?=$product_id?>','productwholedetail<?=$product_id?>','productwholedetail<?=$product_id?>')"><?=substr($heading_title,$rs);?></a></li>
        	<?php
            foreach($rsallproducts as $row) {
            
            	$query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
                $rsprod = $query->row;
                
                $rs=strspn($heading_title ^ $rsprod['name'], "\0");
                
                $pname=substr($rsprod['name'],($rs-1),1);                
                
                if(is_numeric($pname) && $pname!=' ')
                {
                	$rs=$rs-2;
                	$pname=substr($rsprod['name'],$rs,1);                	
                }
                
            ?>
            <li style="border:none"><a onclick="displayblocknone('divproduct<?=$product_id.'_'.$rsprod['product_id']?>','divproduct<?=$product_id?>','productwholedetail<?=$product_id.'_'.$rsprod['product_id']?>','productwholedetail<?=$product_id?>')"><?=substr($rsprod['name'],$rs);?></a></li>
            <?php } ?>
        </ul>
        <div class="clear"></div>
        <div style=" border-top:1px #BDBDBD solid;"></div>        
        <div style="padding-top:10px;"></div>        
        <?php } else {?>
       <div style="padding-top:29px;"></div>
		<?php } ?>
        
		
        <?php if (!$special) { ?>
        <?php echo $price; ?>
        <?php } else { ?>
        <span class="price-new"><?php echo $special; ?></span>
        <span class="price-old"><?php echo $price; ?></span>
        <span class="sale"><b><?php echo $saving; ?>/-<br />Off</b></span>
        <?php if ($product_info['mpn']) { ?>
            <div class="offer"><?php echo $product_info['mpn']; ?></div>
            <?php } ?>
        <?php } ?>
        <?php if ($tax) { ?>
        <br />
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $tax; ?></span>
        <?php } ?>

      </div>
      <?php } ?>
      
      
      <div class="cart">
        
        <?php
        $cartproducts = $this->cart->getProducts();
        ?>
        
        <?php 
        $cflag=0;$cquantity=0;
        foreach($cartproducts as $cproduct) {
            if($cproduct['product_id']==$product_id)
            { 
            	$cquantity=$cproduct['quantity'];
                $cflag=1;
            }
        }
		?>
        
        
        <?php  if($cquantity!=0) { ?>
                
        <div class="floatleft" id="shqtydiv<?php echo $product_id; ?>">
        <div class="productplus">
            <img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg" onclick="minusquantity('<?php echo $product_id; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg'"/>
        </div> 

        <div class="quantityinput"><input class="order_qua" type="text" name="quantity<?php echo $product_id; ?>" id="quantity<?php echo $product_id; ?>" value="<?=$cquantity?>" onchange="callupdate('<?php echo $product_id; ?>')" onkeyup="chkzerominus('<?php echo $product_id; ?>',event)"/></div>
        </div>
        
        <div class="productminus" id="changemplus<?php echo $product_id; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity('<?php echo $product_id; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
        
        <div class="clear"></div>
        
        <?php } else {
         
        if($quantity > 0 || $user_group == 'Wholesaler'){  ?>
        
        <div class="floatleft" id="shqtydiv<?php echo $product_id; ?>">
        <a class="addquantity" href="javascript:plusquantity1('<?php echo $product_id; ?>')">Add&nbsp;&nbsp;</a>
        </div>
        
        <div class="productminus" id="changemplus<?php echo $product_id; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity1('<?php echo $product_id; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
        
        <?php }else{ ?>

        <img class="out_of_stock" src="<?= HTTP_SERVER ?>image/data/assets/no_stock.png" />

        <?php } ?>
                
        <div class="clear"></div>
        
        <?php } ?>
        
        
 
      </div>
      
      <!--<div class="wishlist">
      <a class="wish tooltip" onclick="addToWishList('<?php echo $product_id; ?>');"><span><?php echo $button_wishlist; ?></span></a></div> 
      <div class="comparee"><a class="compare tooltip" onclick="addToCompare('<?php echo $product_id; ?>');"><span><?php echo $button_compare; ?></span></a></div>-->
         
      <div class="clear"></div>      
    </div>
      
      <?php  
      	$y=0; 	
            
        $query = $this->db->query("select * from product_similar where product_id='".$product_id."'");
        $rsallproducts1 = $query->rows;
        $num_rows = sizeof($rsallproducts1);
        
    	if($num_rows>0) { 
        
        foreach($rsallproducts as $row) 
        {   
            $z=0;    
            if(array_key_exists($row['similar_id'],$products))
                $product1 = $products[$row['similar_id']];
            else
                continue;        	

        ?>
      
      <div id="divproduct<?=$row['product_id'].'_'.$product1['product_id']?>" style="display:none">
                <?php if ($product1['thumb']) { ?>
                  <div class="image">
                      <a href="<?php echo $product1['href']; ?>">
                         <img oversrc="<?php echo $product1['thumb']; ?>" src="<?php echo $product1['thumb']; ?>" 
                         title="<?php echo $product1['name']; ?>" alt="<?php echo $product1['name']; ?>" style="border:none" width="156" height="156"/>
                      </a>
                  </div>
    
                  <?php } else {?>
    
                  <div class="image">
                      <a href="<?php echo $product1['href']; ?>">
                          <img src="<?php echo $product1['thumb']; ?>" title="<?php echo $product1['name']; ?>" 
                          alt="<?php echo $product1['name']; ?>" style="border:none" width="156" height="156"/>
                      </a>
                  </div>
    
                  <?php } ?>
                
          <div class="name"><a href="<?php echo $product1['href']; ?>"><?=substr($heading_title,0,$rs);?></a></div>
          
          <div class="description"><?php echo $product1['description']; ?></div>
          <?php if ($product1['price']) { ?>
          <div class="price">
    
    		
            <?php
             $query = $this->db->query("select * from product_similar where product_id='".$product_id."'");
             $rsallproducts = $query->rows;
             $num_rows = sizeof($rsallproducts);
            
            if($num_rows>0) {   
            ?>
            <ul class="ultabonproductdisplay">
                <li style="border:none"><a onclick="displayblocknone('divproduct<?=$product_id?>','divproduct<?=$product_id;?>','productwholedetail<?=$product_id?>','productwholedetail<?=$product_id?>')"><?=substr($heading_title,$rs);?></a></li>
                <?php
                foreach($rsallproducts as $row) 
                {                	
                    $query = $this->db->query("select * from product_description where product_id='".$row['similar_id']."'");
                    $rsprod = $query->row;
                    
                    $rs=strspn($heading_title ^ $rsprod['name'], "\0");
                
                    $pname=substr($rsprod['name'],($rs-1),1);                
                    
                    if(is_numeric($pname) && $pname!=' ')
                    {
                        $rs=$rs-2;
                        $pname=substr($rsprod['name'],$rs,1);                	
                    }
                ?>
                <?php if($y==$z) {?>
                <li><a onclick="displayblocknone('divproduct<?=$product_id.'_'.$rsprod['product_id']?>','divproduct<?=$product_id;?>','productwholedetail<?=$product_id.'_'.$rsprod['product_id']?>','productwholedetail<?=$product_id?>')"><?=substr($rsprod['name'],$rs);?></a></li>
                <?php } else { ?>
                <li style="border:none"><a onclick="displayblocknone('divproduct<?=$product_id.'_'.$rsprod['product_id']?>','divproduct<?=$product_id?>','productwholedetail<?=$product_id.'_'.$rsprod['product_id']?>','productwholedetail<?=$product_id?>')"><?=substr($rsprod['name'],$rs);?></a></li>
                <?php } ?>
                <?php $z++; 
                } ?>
            </ul>
            <div class="clear"></div>
            <div style="border-top:1px #BDBDBD solid;"></div>        
            <div style="padding-top:10px;"></div>        
            <?php } else {?>
           <div style="padding-top:29px;"></div>
            <?php } ?>
    
            <?php if (!$product1['special']) { ?>
            <?php echo $product1['price']; ?>
            <?php } else { ?>
            <span class="price-new"><?php echo $product1['special']; ?></span>
            <span class="price-old"><?php echo $product1['price']; ?></span>
            <span class="sale"><b><?php echo $product1['saving']; ?>/-<br />Off</b></span>
            <div class="offer">Buy 1 Get 1 Free</div>
            <?php } ?>
            <?php //if ($product1['tax']) { ?>
            <br />
            <span class="price-tax"><?php echo $text_tax; ?> <?php //echo $product1['tax']; ?></span>
            <?php //} ?>
          </div>
          <?php } ?>
          
          <div class="cart">
        
            <?php
            $cartproducts = $this->cart->getProducts();
            ?>
            
            <?php 
            $cflag=0;$cquantity=0;
            foreach($cartproducts as $cproduct) {
                if($cproduct['product_id']==$product1['product_id'])
                { 
                    $cquantity=$cproduct['quantity'];
                    $cflag=1;
                }
            }
            ?>
            
            
            <?php  if($cquantity!=0) { ?>
            
            <div class="floatleft" id="shqtydiv<?php echo $product1['product_id']; ?>">
            <div class="productplus"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg" onclick="minusquantity('<?php echo $product1['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg'"/></div> 
    
            <div class="quantityinput"><input class="order_qua" type="text" name="quantity<?php echo $product1['product_id']; ?>" id="quantity<?php echo $product1['product_id']; ?>" value="<?=$cquantity?>" onchange="callupdate('<?php echo $product1['product_id']; ?>')" onkeyup="chkzerominus('<?php echo $product1['product_id']; ?>',event)"/></div>
            </div>
            
            <div class="productminus" id="changemplus<?php echo $product1['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity('<?php echo $product1['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
            
            <div class="clear"></div>
            
            <?php } else { 
            
            if($product1['quantity'] > 0 || $user_group == 'Wholesaler'){  ?>
            
            <div class="floatleft" id="shqtydiv<?php echo $product1['product_id']; ?>">
                <a class="addquantity" href="javascript:plusquantity1('<?php echo $product1['product_id']; ?>')">Add&nbsp;&nbsp;</a>
            </div>
            
            <div class="productminus" id="changemplus<?php echo $product1['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity1('<?php echo $product1['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
            
            <?php }else{ ?>

                <img class="out_of_stock" src="<?= HTTP_SERVER ?>image/data/assets/no_stock.png" />

            <?php } ?>
                
            <div class="clear"></div>
            
            <?php } ?>
     
          </div>
          
          <!--<div class="wishlist"><a class="wish tooltip" onclick="addToWishList('<?php echo $product1['product_id']; ?>');"><span><?php echo $button_wishlist; ?></span></a></div> 
          <div class="comparee"><a class="compare tooltip" onclick="addToCompare('<?php echo $product1['product_id']; ?>');"><span><?php echo $button_compare; ?></span></a></div>-->
             
            
          <div class="clear"></div>
          
          
          
          
        </div>
      
      
      
      <?php
        $y++;
    		}                               
    	}
    ?>
      
      </div>
      
      </div>
      
      
      
      
      
      <div style="clear:both"></div>

      
    

  
  
  <!-- Tabs 
  =========================================================-->
  <div id="tabs" class="htabs"><a href="#tab-description"><?php echo $tab_description; ?></a>
    <?php if ($attribute_groups) { ?>
    <a href="#tab-attribute"><?php echo $tab_attribute; ?></a>
    <?php } ?>
    <?php if ($review_status) { ?>
    <a href="#tab-review"><?php echo $tab_review; ?></a>
    <?php } ?>
    <?php if ($tags) { ?>
    <a href="#tab-tags"><?php echo $text_tags; ?></a>
    <?php } ?>
  </div>
  
  
  <div id="tab-description" class="tab-content"><?php echo $description; ?></div>
  <?php if ($attribute_groups) { ?>
  <div id="tab-attribute" class="tab-content">
    <table class="attribute">
      <?php foreach ($attribute_groups as $attribute_group) { ?>
      <thead>
        <tr>
          <td colspan="2"><?php echo $attribute_group['name']; ?></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
        <tr>
          <td><?php echo $attribute['name']; ?></td>
          <td><?php echo $attribute['text']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
      <?php } ?>
    </table>
  </div>
  <?php } ?>
  <?php if ($review_status) { ?>
  <div id="tab-review" class="tab-content">
    <div id="review"></div>
    <h2 id="review-title"><?php echo $text_write; ?></h2>
    <b><?php echo $entry_name; ?></b><br /><br />
    <input type="text" name="name" value="" />
    <br />
    <br />
    <b><?php echo $entry_review; ?></b><br /><br />
    <textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea>
    <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />
    <br />
    <b><?php echo $entry_rating; ?></b> <span><?php echo $entry_bad; ?></span>&nbsp;
    <input type="radio" name="rating" value="1" />
    &nbsp;
    <input type="radio" name="rating" value="2" />
    &nbsp;
    <input type="radio" name="rating" value="3" />
    &nbsp;
    <input type="radio" name="rating" value="4" />
    &nbsp;
    <input type="radio" name="rating" value="5" />
    &nbsp;<span><?php echo $entry_good; ?></span><br />
    <br />
    <b><?php echo $entry_captcha; ?></b><br /><br />
    <input type="text" name="captcha" value="" />
    <br /><br />
    <img src="index.php?route=product/product/captcha" alt="" id="captcha" /><br />
    <br />
    <div class="buttons">
      <div class="right"><a id="button-review" class="button"><?php echo $button_continue; ?></a></div>
    </div>
  </div>
  <?php } ?>
  
  
  
  
  <?php if ($tags) { ?>
  <div id="tab-tags" class="tab-content">
  <div class="tags">
    <?php for ($i = 0; $i < count($tags); $i++) { ?>
    <?php if ($i < (count($tags) - 1)) { ?>
    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
    <?php } else { ?>
    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
    <?php } ?>
    <?php } ?>
  </div>
  </div>
  <?php } ?>
  
  
  
  <?php if ($related_products) { ?>
  <div class="related-product">
  	<h2 class="realted-title"><?php echo $tab_related; ?>(<?php echo count($related_products); ?>)</h2>
    <div class="box-product">
      <?php foreach ($related_products as $product) { ?>
      <div class="related grid_2">
        <?php if ($product['thumb']) { ?>
        <div class="image">
        	<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
        </div>
        <?php } ?>
        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
        <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
          <?php } ?>
        </div>
        <?php } ?>
        <div class="rating">
        	<img src="catalog/view/theme/grocery/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" />
        </div>
        <a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><?php echo $button_cart; ?></a>
        
        <div class="cart">
        
        <?php
        $cartproducts = $this->cart->getProducts();
        ?>
        
        <?php 
        $cflag=0;$cquantity=0;
        foreach($cartproducts as $cproduct) {
            if($cproduct['product_id']==$product['product_id'])
            { 
            	$cquantity=$cproduct['quantity'];
                $cflag=1;
            }
        }
		?>
        
        
        <?php  if($cquantity!=0) { ?>
        
        <div class="floatleft" id="shqtydivo<?php echo $product['product_id']; ?>">
        <div class="productplus"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg" onclick="minusquantityo('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg'"/></div> 

        <div class="quantityinput"><input class="order_qua" type="text" name="quantityo<?php echo $product['product_id']; ?>" id="quantityo<?php echo $product['product_id']; ?>" value="<?=$cquantity?>" onchange="callupdateo('<?php echo $product['product_id']; ?>')" onkeyup="chkzerominuso('<?php echo $product['product_id']; ?>',event)"/></div>
        </div>
        
        <div class="productminus" id="changempluso<?php echo $product['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantityo('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
        
        <div class="clear"></div>
        
        <?php } else {?>
        
        <div class="floatleft" id="shqtydivo<?php echo $product['product_id']; ?>">
        <a class="addquantity" href="javascript:plusquantity1o('<?php echo $product['product_id']; ?>')">Add&nbsp;&nbsp;</a>
        </div>
        
        <div class="productminus" id="changempluso<?php echo $product['product_id']; ?>"><img src="<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg" onclick="plusquantity1o('<?php echo $product['product_id']; ?>')" onmousemove="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg'" onmouseout="this.src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg'"/></div>
        
        <div class="clear"></div>
        
        <?php } ?>
        
        </div>
   
        <!--<div class="wishlist"><a class="wish tooltip" onclick="addToWishList('<?php echo $product1['product_id']; ?>');"><span><?php echo $button_wishlist; ?></span></a></div> 
          <div class="comparee"><a class="compare tooltip" onclick="addToCompare('<?php echo $product1['product_id']; ?>');"><span><?php echo $button_compare; ?></span></a></div>-->
             
            
          <div class="clear"></div>
        
        </div>
      <?php } ?>
    </div>
  </div>
  <?php } ?>
  
  
  <?php echo $content_bottom; ?>

</div>
<script type="text/javascript"><!--
$('.colorbox').colorbox({
	overlayClose: true,
	opacity: 0.5
});
//--></script> 
<script type="text/javascript"><!--
$('#button-cart').bind('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
			

			if (json['error']) {
				if (json['error']['warning']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				
					$('.warning').fadeIn('slow');
					
					$('html, body').animate({ scrollTop: 0 }, 'slow'); 
				}
				
				if (json['error']['maximum']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error']['maximum'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				
					$('.warning').fadeIn('slow');
					
					$('html, body').animate({ scrollTop: 0 }, 'slow'); 
				}
				
				for (i in json['error']) {
					$('#option-' + i).after('<span class="error">' + json['error'][i] + '</span>');
				}
			}	 
            
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
					}
				}
			} 
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/grocery/image/close.png" alt="" class="close" /></div>');
					
				$('.success').fadeIn('slow');
					
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
});
//--></script>
<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/grocery/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
		
		$('.error').remove();
		
		if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
		}
		

			if (json['error']) {
				if (json['error']['warning']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				
					$('.warning').fadeIn('slow');
					
					$('html, body').animate({ scrollTop: 0 }, 'slow'); 
				}
				
				if (json['error']['maximum']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error']['maximum'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				
					$('.warning').fadeIn('slow');
					
					$('html, body').animate({ scrollTop: 0 }, 'slow'); 
				}
				
				for (i in json['error']) {
					$('#option-' + i).after('<span class="error">' + json['error'][i] + '</span>');
				}
			}	 
            
		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}
		
		$('.loading').remove();	
	}
});
//--></script>
<?php } ?>
<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');
		
	$('#review').load(this.href);
	
	$('#review').fadeIn('slow');
	
	return false;
});			

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/grocery/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
if ($.browser.msie && $.browser.version == 6) {
	$('.date, .datetime, .time').bgIframe();
}

$('.date').datepicker({dateFormat: 'yy-mm-dd'});
$('.datetime').datetimepicker({
	dateFormat: 'yy-mm-dd',
	timeFormat: 'h:m'
});
$('.time').timepicker({timeFormat: 'h:m'});


function displayblocknone(divtoshow,common,divwholeproduct,divwholeproductcom)
{
	document.getElementById(common).style.display="none";
	var $eles = $("div[id^="+common+"]").css("display","none");
	document.getElementById(divtoshow).style.display="block";	
	var $eles = $("div[id^="+divwholeproductcom+"]").css("display","none");
	document.getElementById(divwholeproduct).style.display="block";	
}

function callupdate(productid)
{
	//alert(document.getElementById('quantity'+productid).value);
	updateToCart(productid,document.getElementById('quantity'+productid).value);
	if(document.getElementById('quantity'+productid).value==0)
	{
		document.getElementById('shqtydiv'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
	}
}



function plusquantity(productid)
{
	document.getElementById('quantity'+productid).value = parseInt(document.getElementById('quantity'+productid).value)+parseInt(1);	
	updateToCart(productid,document.getElementById('quantity'+productid).value);
}
function minusquantity(productid)
{
	document.getElementById('quantity'+productid).value = parseInt(document.getElementById('quantity'+productid).value)-parseInt(1);
	updateToCart(productid,document.getElementById('quantity'+productid).value);
	if(document.getElementById('quantity'+productid).value==0)
	{
		document.getElementById('shqtydiv'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
	}
}

function chkzerominus(productid,e)
{
	var keynum;

            if(window.event){ // IE					
            	keynum = e.keyCode;
            }else
                if(e.which){ // Netscape/Firefox/Opera					
            		keynum = e.which;
                 }
           // alert(String.fromCharCode(keynum));
		   if(String.fromCharCode(keynum)=='m')
		   {
			   updateToCart(productid,"0");
			   
			   document.getElementById('shqtydiv'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1("+productid+")'>Add&nbsp;&nbsp;</a>";
			   document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
			   
		   }
}

function plusquantity1(productid)
{
	document.getElementById('shqtydiv'+productid).innerHTML="<div class='productplus'><img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg' onclick='minusquantity("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg\"'/></div><div class='quantityinput'><input class='order_qua' type='text' name='quantity"+productid+"' id='quantity"+productid+"' value='0' onchange='callupdate("+productid+")' onkeyup='chkzerominus("+productid+",event)'/></div>";	
	
	document.getElementById('changemplus'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"'/>";
	
	document.getElementById('quantity'+productid).value = parseInt(document.getElementById('quantity'+productid).value)+parseInt(1);
	
	updateToCart(productid,document.getElementById('quantity'+productid).value);
}



function callupdateo(productid)
{
	//alert(document.getElementById('quantity'+productid).value);
	updateToCart(productid,document.getElementById('quantityo'+productid).value);
	if(document.getElementById('quantityo'+productid).value==0)
	{
		document.getElementById('shqtydivo'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1o("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changempluso'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1o("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
	}
}

function plusquantityo(productid)
{
	document.getElementById('quantityo'+productid).value = parseInt(document.getElementById('quantityo'+productid).value)+parseInt(1);	
	updateToCart(productid,document.getElementById('quantityo'+productid).value);
}
function minusquantityo(productid)
{
	document.getElementById('quantityo'+productid).value = parseInt(document.getElementById('quantityo'+productid).value)-parseInt(1);
	updateToCart(productid,document.getElementById('quantityo'+productid).value);
	if(document.getElementById('quantityo'+productid).value==0)
	{
		document.getElementById('shqtydivo'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1o("+productid+")'>Add&nbsp;&nbsp;</a>";
		document.getElementById('changempluso'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1o("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
	}
}

function chkzerominuso(productid,e)
{
	var keynum;

            if(window.event){ // IE					
            	keynum = e.keyCode;
            }else
                if(e.which){ // Netscape/Firefox/Opera					
            		keynum = e.which;
                 }
           // alert(String.fromCharCode(keynum));
		   if(String.fromCharCode(keynum)=='m')
		   {
			   updateToCart(productid,"0");
			   
			   document.getElementById('shqtydivo'+productid).innerHTML="<a class='addquantity' href='javascript:plusquantity1o("+productid+")'>Add&nbsp;&nbsp;</a>";
			   document.getElementById('changempluso'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantity1("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"' />";
			   
		   }
}

function plusquantity1o(productid)
{
	document.getElementById('shqtydivo'+productid).innerHTML="<div class='productplus'><img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg' onclick='minusquantityo("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/minus.jpg\"'/></div><div class='quantityinput'><input class='order_qua' type='text' name='quantityo"+productid+"' id='quantityo"+productid+"' value='0' onchange='callupdateo("+productid+")' onkeyup='chkzerominuso("+productid+",event)'/></div>";	
	
	document.getElementById('changempluso'+productid).innerHTML="<img src='<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg' onclick='plusquantityo("+productid+")' onmousemove='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plushover.jpg\"' onmouseout='this.src=\"<?= HTTP_SERVER ?>catalog/view/theme/grocery/image/plus.jpg\"'/>";
	
	document.getElementById('quantityo'+productid).value = parseInt(document.getElementById('quantityo'+productid).value)+parseInt(1);
	
	updateToCart(productid,document.getElementById('quantityo'+productid).value);
}




//--></script> 

<?php echo $footer; ?>

<style>
.product-grid .image {
    display: block !important;
    margin-bottom: 0 !important;
    margin-left: auto !important;
    margin-right: auto !important;
    text-align: center !important;
	
	  border:none !important;
    float: none !important;
    padding: 10px !important;
    width: auto;
}



.product-grid .price {
    color: #000000 !important;
    font-size: 13px !important;
    font-weight: bold !important;
    margin-bottom: 5px !important;
	padding:0px !important;
	background:none !important;
	border:none;
}

.product-grid .cart {
    float: left;
    margin-bottom:0px !important;
    padding:0px !important;
    width:auto !important;
}

.product-grid .price-new {
    float: none !important;
    font-size:100% !important;
    margin-right:0px !important;
}

.product-grid .price-old {
    color: #FF0000;
    font-size:100% !important;
    text-decoration: line-through;
}


.product-grid {
    float: right;
    margin-top: -100px;
	margin-right:20px;
}

@media screen and (max-device-width : 960px)
{
	.product-grid
	{
		width:auto !important;
	}
}
</style>